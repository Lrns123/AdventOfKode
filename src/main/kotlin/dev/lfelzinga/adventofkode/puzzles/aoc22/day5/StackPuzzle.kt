package dev.lfelzinga.adventofkode.puzzles.aoc22.day5

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import java.util.*

class StackPuzzle : Puzzle<String>(2022, 5, StringTransformer()) {
    companion object {
        private val crateRegex = Regex("\\[(\\w)]")
        private val moveRegex = Regex("move (\\d+) from (\\d+) to (\\d+)")
    }

    data class Move(val quantity: Int, val from: Int, val to: Int)

    private fun buildInitialState(initialState: List<String>): List<Stack<Char>> {
        val numStacks = Regex("\\d+").findAll(initialState.first()).count()
        val stacks = List(numStacks) { Stack<Char>() }

        initialState.drop(1).forEach { line ->
            for (crate in crateRegex.findAll(line)) {
                val position = crate.range.first / 4
                val crateName = crate.destructured.component1().first()

                stacks[position].push(crateName)
            }
        }

        return stacks
    }

    private fun parseInput(input: String): Pair<List<Stack<Char>>, List<Move>> {
        val (initialState, moveLines) = input.split("\n\n").map { it.lineSequence() }

        val state = buildInitialState(initialState.toList().reversed())
        val moves = moveLines
            .map { moveRegex.matchEntire(it)?.destructured ?: error("Could not parse line $it") }
            .map { (qty, from, to) -> Move(qty.toInt(), from.toInt() - 1, to.toInt() - 1) }
            .toList()

        return state to moves
    }

    override fun solveFirstPart(input: String): String {
        val (stacks, moves) = parseInput(input)

        for (move in moves) {
            repeat(move.quantity) {
                stacks[move.to].push(stacks[move.from].pop())
            }
        }

        return stacks.joinToString("") { it.peek().toString() }
    }

    override fun solveSecondPart(input: String): String {
        val (stacks, moves) = parseInput(input)

        for (move in moves) {
            List(move.quantity) { stacks[move.from].pop() }.asReversed().forEach {
                stacks[move.to].push(it)
            }
        }

        return stacks.joinToString("") { it.peek().toString() }
    }
}
