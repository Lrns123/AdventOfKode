package dev.lfelzinga.adventofkode.puzzles.aoc22.day20

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class DecryptionPuzzle : Puzzle<Sequence<String>>(2022, 20, LinesTransformer()) {

    private class Number(val num: Long) {
        override fun toString(): String {
            return num.toString()
        }
    }

    private fun mixNumbers(numbers: List<Number>, iterations: Int): List<Number> {
        val mixed = numbers.toMutableList()

        repeat(iterations) {
            for (number in numbers) {
                val pos = mixed.indexOf(number)
                mixed.removeAt(pos)

                val newPos = ((pos + number.num) % mixed.size).let { if (it < 0) it + mixed.size else it }
                mixed.add(newPos.toInt(), number)
            }
        }

        return mixed
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val numbers = input.map { Number(it.toLong()) }.toList()
        val mixed = mixNumbers(numbers, 1)
        val startPos = mixed.indexOfLast { it.num == 0L }

        return listOf(1000, 2000, 3000).sumOf { mixed[(startPos + it) % mixed.size].num }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val numbers = input.map { Number(it.toLong() * 811589153L) }.toList()
        val mixed = mixNumbers(numbers, 10)
        val startPos = mixed.indexOfLast { it.num == 0L }

        return listOf(1000, 2000, 3000).sumOf { mixed[(startPos + it) % mixed.size].num }.toString()
    }
}
