package dev.lfelzinga.adventofkode.puzzles.aoc22.day25

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class SnafuConversionPuzzle : Puzzle<Sequence<String>>(2022, 25, LinesTransformer()) {
    private fun snafuToDecimal(snafu: String): Long {
        var factor = 1L

        return snafu.foldRight(0L) { ch, acc ->
            val value = when (ch) {
                '0', '1', '2' -> ch - '0'
                '-' -> -1
                '=' -> -2
                else -> error("Unknown character '$ch'")
            }

            acc + value * factor.also { factor *= 5 }
        }
    }

    private fun decimalToSnafu(number: Long): String {
        var factor = 1L
        while (factor < number) {
            factor *= 5
        }

        val digits = mutableListOf<Int>()
        var remaining = number

        fun applyCarry(position: Int) {
            if (digits[position] == 2) {
                applyCarry(position - 1)
                digits[position] = -2
            } else {
                digits[position]++
            }
        }

        while (factor > 0) {
            val digitValue = (remaining / factor).toInt()
            when (digitValue) {
                0, 1, 2 -> digits.add(digitValue)
                3 -> digits.add(-2)
                4 -> digits.add(-1)
            }

            if (digitValue > 2) applyCarry(digits.lastIndex - 1)

            remaining %= factor
            factor /= 5
        }

        return digits.map {
            when (it) {
                -2 -> '='
                -1 -> '-'
                0, 1, 2 -> it.digitToChar()
                else -> error("Invalid digit value $it")
            }
        }.joinToString("").trimStart('0')
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return decimalToSnafu(input.map { snafuToDecimal(it) }.sum())
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return "Merry Christmas!"
    }
}
