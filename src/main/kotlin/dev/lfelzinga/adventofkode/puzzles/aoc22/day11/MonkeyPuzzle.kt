package dev.lfelzinga.adventofkode.puzzles.aoc22.day11

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.lcm

class MonkeyPuzzle : Puzzle<String>(2022, 11, StringTransformer()) {
    companion object {
        private val monkeyRegex = Regex(
            "Monkey \\d+:\n" +
            " {2}Starting items: (.*)\n" +
            " {2}Operation: new = old (.) (.*)\n" +
            " {2}Test: divisible by (\\d+)\n" +
            " {4}If true: throw to monkey (\\d+)\n" +
            " {4}If false: throw to monkey (\\d+)"
        )
    }

    private class Monkey(
        items: List<Long>,
        val operation: (Long) -> Long,
        val testDivisor: Long,
        val trueTarget: Int,
        val falseTarget: Int
    ) {
        val items = ArrayDeque(items)
        var itemsInspected = 0L
    }

    private fun createOperation(operator: String, operand: String): (Long) -> Long {
        val op = operand.toLongOrNull()

        return when (operator) {
            "*" -> op?.let { { x -> x * op } } ?: { x -> x * x }
            "+" -> op?.let { { x -> x + op } } ?: { x -> x + x }
            else -> error("Unknown operator $operator")
        }
    }

    private fun parseInput(input: String): List<Monkey> {
        return input
            .splitToSequence("\n\n")
            .map { monkeyRegex.matchEntire(it)?.destructured ?: error("Could not parse monkey") }
            .map { (items, operator, operand, test, ifTrue, ifFalse) ->
                Monkey(
                    items.split(", ").map { it.toLong() },
                    createOperation(operator, operand),
                    test.toLong(),
                    ifTrue.toInt(),
                    ifFalse.toInt()
                )
            }
            .toList()
    }

    private fun simulateRound(monkeys: List<Monkey>, reduceWorry: Boolean) {
        val lcm = monkeys.map { it.testDivisor }.reduce(::lcm)

        for (monkey in monkeys) {
            while (monkey.items.isNotEmpty()) {
                var item = monkey.items.removeFirst()

                item = monkey.operation(item)
                if (reduceWorry)
                    item /= 3

                item %= lcm

                if (item % monkey.testDivisor == 0L) {
                    monkeys[monkey.trueTarget].items.addLast(item)
                } else {
                    monkeys[monkey.falseTarget].items.addLast(item)
                }

                monkey.itemsInspected++
            }
        }
    }

    private fun getMonkeyBusiness(monkeys: List<Monkey>): Long = monkeys
        .map { it.itemsInspected }
        .sortedDescending()
        .take(2)
        .reduce { acc, x -> acc * x }

    override fun solveFirstPart(input: String): String {
        val monkeys = parseInput(input)

        repeat(20) { simulateRound(monkeys, true) }

        return getMonkeyBusiness(monkeys).toString()
    }

    override fun solveSecondPart(input: String): String {
        val monkeys = parseInput(input)

        repeat(10_000) { simulateRound(monkeys, false) }

        return getMonkeyBusiness(monkeys).toString()
    }
}
