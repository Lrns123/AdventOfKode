package dev.lfelzinga.adventofkode.puzzles.aoc22.day19

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import kotlin.math.max

class RobotPuzzle : Puzzle<Sequence<String>>(2022, 19, LinesTransformer()) {
    companion object {
        private val blueprintRegex = Regex(
            "Blueprint (\\d+): " +
                "Each ore robot costs (\\d+) ore. " +
                "Each clay robot costs (\\d+) ore. " +
                "Each obsidian robot costs (\\d+) ore and (\\d+) clay. " +
                "Each geode robot costs (\\d+) ore and (\\d+) obsidian."
        )
    }

    data class Blueprint(
        val id: Int,
        val oreRobotOreCost: Int,
        val clayRobotOreCost: Int,
        val obsidianRobotOreCost: Int,
        val obsidianRobotClayCost: Int,
        val geodeRobotOreCost: Int,
        val geodeRobotObsidianCost: Int,
    ) {
        val maxOreCost by lazy {
            listOf(oreRobotOreCost, clayRobotOreCost, obsidianRobotOreCost, geodeRobotOreCost).max()
        }
    }

    private fun maxGeodesForBlueprint(
        blueprint: Blueprint,
        time: Int,
        ore: Int = 0,
        oreRobots: Int = 1,
        clay: Int = 0,
        clayRobots: Int = 0,
        obsidian: Int = 0,
        obsidianRobots: Int = 0,
        geodes: Int = 0,
        geodeRobots: Int = 0
    ): Int {
        if (time == 0) {
            return geodes
        }

        var maxGeodes = geodes
        val newOre = ore + oreRobots
        val newClay = clay + clayRobots
        val newObsidian = obsidian + obsidianRobots
        val newGeodes = geodes + geodeRobots

        if (ore >= blueprint.geodeRobotOreCost && obsidian >= blueprint.geodeRobotObsidianCost) {
            maxGeodes = max(maxGeodes, maxGeodesForBlueprint(
                blueprint,
                time - 1,
                newOre - blueprint.geodeRobotOreCost,
                oreRobots,
                newClay,
                clayRobots,
                newObsidian - blueprint.geodeRobotObsidianCost,
                obsidianRobots,
                newGeodes,
                geodeRobots + 1
            ))
            return maxGeodes
        }

        if (ore <= blueprint.maxOreCost) {
            maxGeodes = max(
                maxGeodes, maxGeodesForBlueprint(
                    blueprint,
                    time - 1,
                    newOre,
                    oreRobots,
                    newClay,
                    clayRobots,
                    newObsidian,
                    obsidianRobots,
                    newGeodes,
                    geodeRobots
                )
            )
        }

        if (ore >= blueprint.oreRobotOreCost && oreRobots < blueprint.maxOreCost) {
            maxGeodes = max(maxGeodes, maxGeodesForBlueprint(
                blueprint,
                time - 1,
                newOre - blueprint.oreRobotOreCost,
                oreRobots + 1,
                newClay,
                clayRobots,
                newObsidian,
                obsidianRobots,
                newGeodes,
                geodeRobots
            ))
        }

        if (ore >= blueprint.clayRobotOreCost && clayRobots < blueprint.obsidianRobotClayCost) {
            maxGeodes = max(maxGeodes, maxGeodesForBlueprint(
                blueprint,
                time - 1,
                newOre - blueprint.clayRobotOreCost,
                oreRobots,
                newClay,
                clayRobots + 1,
                newObsidian,
                obsidianRobots,
                newGeodes,
                geodeRobots
            ))
        }

        if (ore >= blueprint.obsidianRobotOreCost && clay >= blueprint.obsidianRobotClayCost && obsidianRobots < blueprint.geodeRobotObsidianCost) {
            maxGeodes = max(maxGeodes, maxGeodesForBlueprint(
                blueprint,
                time - 1,
                newOre - blueprint.obsidianRobotOreCost,
                oreRobots,
                newClay - blueprint.obsidianRobotClayCost,
                clayRobots,
                newObsidian,
                obsidianRobots + 1,
                newGeodes,
                geodeRobots
            ))
        }

        return maxGeodes
    }

    private fun parseInput(input: Sequence<String>): List<Blueprint> {
        return input
            .map { blueprintRegex.matchEntire(it)?.destructured ?: error("Could not parse line '$it'") }
            .map { (id, c1, c2, c3a, c3b, c4a, c4b) ->
                Blueprint(id.toInt(), c1.toInt(), c2.toInt(), c3a.toInt(), c3b.toInt(), c4a.toInt(), c4b.toInt())
            }
            .toList()
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return parseInput(input)
            .sumOf { it.id * maxGeodesForBlueprint(it, 24) }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return parseInput(input)
            .take(3)
            .map { maxGeodesForBlueprint(it, 32) }
            .reduce { acc, x -> acc * x }
            .toString()
    }
}
