package dev.lfelzinga.adventofkode.puzzles.aoc22.day22

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.gcd
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.plus

class GrovePathPuzzle : Puzzle<String>(2022, 22, StringTransformer()) {
    companion object {
        private val directionsRegex = Regex("\\d+|L|R")
    }

    private fun parseInput(input: String): Pair<CharGrid, Sequence<String>> {
        val (rawMap, directions) = input.split("\n\n")
        val map = rawMap.lines()

        val grid = CharGrid(map.maxOf { it.length }, map.size).filledWith(' ')

        for ((y, line) in map.withIndex()) {
            for ((x, ch) in line.withIndex()) {
                grid[x, y] = ch
            }
        }

        return grid to directionsRegex.findAll(directions).map { it.value }
    }

    private fun findStartingPosition(map: CharGrid): Vector2 {
        val pos = map.array.indexOfFirst { it != ' ' }
        return Vector2(pos % map.width, pos / map.width)
    }

    private fun findOppositeSide(map: CharGrid, position: Vector2, facing: Direction): Vector2 {
        val bounds = map.bounds()
        var pos = position
        val oppFacing = facing.opposite()

        while (true) {
            val newPos = pos + oppFacing
            if (newPos !in bounds || map[newPos] == ' ')
                return pos

            pos = newPos
        }
    }

    // Hardcoded for input (using paper cube as reference). TODO: Make this programmatic and general
    private val adjacencyTable = mapOf(
        (Vector2(1, 0) to Direction.WEST) to (Vector2(0, 2) to Direction.WEST),
        (Vector2(1, 0) to Direction.NORTH) to (Vector2(0, 3) to Direction.WEST),

        (Vector2(2, 0) to Direction.NORTH) to (Vector2(0, 3) to Direction.SOUTH),
        (Vector2(2, 0) to Direction.EAST) to (Vector2(1, 2) to Direction.EAST),
        (Vector2(2, 0) to Direction.SOUTH) to (Vector2(1, 1) to Direction.EAST),

        (Vector2(1, 1) to Direction.WEST) to (Vector2(0, 2) to Direction.NORTH),
        (Vector2(1, 1) to Direction.EAST) to (Vector2(2, 0) to Direction.SOUTH),

        (Vector2(0, 2) to Direction.WEST) to (Vector2(1, 0) to Direction.WEST),
        (Vector2(0, 2) to Direction.NORTH) to (Vector2(1, 1) to Direction.WEST),

        (Vector2(1, 2) to Direction.EAST) to (Vector2(2, 0) to Direction.EAST),
        (Vector2(1, 2) to Direction.SOUTH) to (Vector2(0, 3) to Direction.EAST),

        (Vector2(0, 3) to Direction.WEST) to (Vector2(1, 0) to Direction.NORTH),
        (Vector2(0, 3) to Direction.SOUTH) to (Vector2(2, 0) to Direction.NORTH),
        (Vector2(0, 3) to Direction.EAST) to (Vector2(1, 2) to Direction.SOUTH),
    )

    private fun transformPositionForEdgeTransition(
        localPosition: Vector2,
        quadrantSize: Int,
        fromEdge: Direction,
        toEdge: Direction
    ): Vector2 {
        return when (fromEdge) {
            Direction.NORTH -> when (toEdge) {
                Direction.NORTH -> Vector2(quadrantSize - localPosition.x - 1, 0)
                Direction.SOUTH -> Vector2(localPosition.x, quadrantSize - 1)
                Direction.EAST -> Vector2(quadrantSize - 1, quadrantSize - localPosition.x - 1)
                Direction.WEST -> Vector2(0, localPosition.x)
            }
            Direction.SOUTH -> when (toEdge) {
                Direction.NORTH -> Vector2(localPosition.x, 0)
                Direction.SOUTH -> Vector2(quadrantSize - localPosition.x - 1, quadrantSize - 1)
                Direction.EAST -> Vector2(quadrantSize - 1, localPosition.x)
                Direction.WEST -> Vector2(0, quadrantSize - localPosition.x - 1)
            }
            Direction.EAST -> when (toEdge) {
                Direction.NORTH -> Vector2(quadrantSize - localPosition.y - 1, 0)
                Direction.SOUTH -> Vector2(localPosition.y, quadrantSize - 1)
                Direction.EAST -> Vector2(quadrantSize - 1, quadrantSize - localPosition.y -  1)
                Direction.WEST -> Vector2(0, localPosition.y)
            }
            Direction.WEST -> when (toEdge) {
                Direction.NORTH -> Vector2(localPosition.y, 0)
                Direction.SOUTH -> Vector2(quadrantSize - localPosition.y - 1, quadrantSize - 1)
                Direction.EAST -> Vector2(quadrantSize - 1, localPosition.y)
                Direction.WEST -> Vector2(0, quadrantSize - localPosition.y - 1)
            }
        }
    }

    private fun findAdjacentSide(map: CharGrid, position: Vector2, facing: Direction): Pair<Vector2, Direction> {
        val quadrantSize = gcd(map.width, map.height)
        val quadrant = position / quadrantSize
        val localPosition = position % quadrantSize

        val (connectedQuadrant, connectedEdge) = adjacencyTable[quadrant to facing] ?: error("No mapping for edge ($quadrant $facing)")

        val newLocalPosition = transformPositionForEdgeTransition(localPosition, quadrantSize, facing, connectedEdge)

        return connectedQuadrant * quadrantSize + newLocalPosition to connectedEdge.opposite()
    }

    private fun tracePath(map: CharGrid, directions: Sequence<String>, cubeMode: Boolean): Pair<Vector2, Direction> {
        val bounds = map.bounds()
        var pos = findStartingPosition(map)
        var facing = Direction.EAST

        for (direction in directions) {
            when (direction) {
                "L" -> facing--
                "R" -> facing++
                else -> {
                    for (i in 0 until direction.toInt()) {
                        var newPos = pos + facing
                        var newFacing = facing

                        if (newPos !in bounds || map[newPos] == ' ') {
                            if (cubeMode) {
                                val (correctedPos, correctedFacing) = findAdjacentSide(map, pos, facing)
                                newPos = correctedPos
                                newFacing = correctedFacing
                            } else {
                                newPos = findOppositeSide(map, pos, facing)
                            }
                        }

                        when (map[newPos]) {
                            '#' -> break
                            '.' -> {
                                pos = newPos
                                facing = newFacing
                            }
                            else -> error("Unexpected map point at $newPos")
                        }
                    }
                }
            }
        }

        return pos to facing
    }

    private val Direction.points: Int
        get() = when (this) {
            Direction.EAST -> 0
            Direction.SOUTH -> 1
            Direction.WEST -> 2
            Direction.NORTH -> 3
        }

    override fun solveFirstPart(input: String): String {
        val (map, directions) = parseInput(input)
        val (destination, facing) = tracePath(map, directions, false)

        return ((destination.y + 1) * 1000 + (destination.x + 1) * 4 + facing.points).toString()
    }

    override fun solveSecondPart(input: String): String {
        val (map, directions) = parseInput(input)
        val (destination, facing) = tracePath(map, directions, true)

        return ((destination.y + 1) * 1000 + (destination.x + 1) * 4 + facing.points).toString()
    }
}
