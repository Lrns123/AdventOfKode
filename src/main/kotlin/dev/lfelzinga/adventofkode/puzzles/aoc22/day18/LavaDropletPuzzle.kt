package dev.lfelzinga.adventofkode.puzzles.aoc22.day18

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Vector3
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.expand

class LavaDropletPuzzle : Puzzle<Sequence<String>>(2022, 18, LinesTransformer()) {
    companion object {
        private val lineRegex = Regex("(\\d+),(\\d+),(\\d+)")
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val voxels = input
            .map { lineRegex.matchEntire(it)?.destructured ?: error("Could not parse line '$it'") }
            .map { (x, y, z) -> Vector3(x.toInt(), y.toInt(), z.toInt()) }
            .toSet()

        return voxels.sumOf { it.neighbours().count { neighbour -> neighbour !in voxels } }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val voxels = input
            .map { lineRegex.matchEntire(it)?.destructured ?: error("Could not parse line '$it'") }
            .map { (x, y, z) -> Vector3(x.toInt(), y.toInt(), z.toInt()) }
            .toSet()

        val bounds = voxels.bounds().expand(1, 1, 1)
        var surfaceArea = 0
        val visited = mutableSetOf<Vector3>()
        val queue = ArrayDeque<Vector3>()
        queue += bounds.min

        while (queue.isNotEmpty()) {
            val pos = queue.removeFirst()
            if (pos in visited) continue
            visited += pos

            for (neighbour in pos.neighbours()) {
                if (neighbour in voxels) {
                    surfaceArea++
                } else if (pos in bounds) {
                    queue += neighbour
                }
            }
        }

        return surfaceArea.toString()
    }
}
