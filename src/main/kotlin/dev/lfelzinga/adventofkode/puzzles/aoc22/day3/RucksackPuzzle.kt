package dev.lfelzinga.adventofkode.puzzles.aoc22.day3

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class RucksackPuzzle : Puzzle<Sequence<String>>(2022, 3, LinesTransformer()) {
    private val Char.priority
        get() = when (this) {
            in 'a'..'z' -> (this - 'a') + 1
            in 'A'..'Z' -> (this - 'A') + 27
            else -> error("Invalid item in rucksack: $this")
        }

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map {it.substring(0, it.length / 2).toSet() intersect it.substring(it.length / 2).toSet() }
            .sumOf { it.first().priority }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .chunked(3) { group -> group.map { it.toSet() }.reduce { acc, set -> acc intersect set } }
            .sumOf { it.first().priority }
            .toString()
    }
}
