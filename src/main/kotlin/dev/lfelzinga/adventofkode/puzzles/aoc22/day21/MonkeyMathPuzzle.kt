package dev.lfelzinga.adventofkode.puzzles.aoc22.day21

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.binarySearch

class MonkeyMathPuzzle : Puzzle<Sequence<String>>(2022, 21, LinesTransformer()) {
    private sealed interface Variable {
        fun compute(variables: Map<String, Variable>): Long

        data class Constant(val value: Long) : Variable {
            override fun compute(variables: Map<String, Variable>): Long {
                return value
            }
        }

        data class Equation(val lhs: String, val rhs: String, val op: Char) : Variable {
            override fun compute(variables: Map<String, Variable>): Long {
                val lhsValue = variables[lhs]!!.compute(variables)
                val rhsValue = variables[rhs]!!.compute(variables)

                return when (op) {
                    '+' -> lhsValue + rhsValue
                    '-' -> lhsValue - rhsValue
                    '*' -> lhsValue * rhsValue
                    '/' -> lhsValue / rhsValue
                    else -> error("Unknown operator '$op'")
                }
            }
        }
    }

    private fun parseInput(input: Sequence<String>): Map<String, Variable> {
        return input
            .map { it.split(": ") }
            .map { (name, value) ->
                if (' ' in value) {
                    val (lhs, op, rhs) = value.split(' ')
                    name to Variable.Equation(lhs, rhs, op.first())
                } else {
                    name to Variable.Constant(value.toLong())
                }
            }
            .toMap()
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val monkeys = parseInput(input)
        return monkeys["root"]!!.compute(monkeys).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val monkeys = parseInput(input).toMutableMap()
        val root = monkeys["root"] as Variable.Equation
        val lhs = monkeys[root.lhs]!!
        val rhs = monkeys[root.rhs]!!

        return binarySearch(0L, 100_000_000_000_000L) {
            monkeys["humn"] = Variable.Constant(it)
            lhs.compute(monkeys) <= rhs.compute(monkeys)
        }.toString()
    }
}
