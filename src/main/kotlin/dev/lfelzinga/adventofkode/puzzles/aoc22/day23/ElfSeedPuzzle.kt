package dev.lfelzinga.adventofkode.puzzles.aoc22.day23

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds

class ElfSeedPuzzle : Puzzle<Sequence<String>>(2022, 23, LinesTransformer()) {
    companion object {
        private val neighboursNorth = listOf(Vector2(-1, -1), Vector2(0, -1), Vector2(1, -1))
        private val neighboursSouth = listOf(Vector2(-1, 1), Vector2(0, 1), Vector2(1, 1))
        private val neighboursWest = listOf(Vector2(-1, -1), Vector2(-1, 0), Vector2(-1, 1))
        private val neighboursEast = listOf(Vector2(1, -1), Vector2(1, 0), Vector2(1, 1))
        private val neighboursAll = listOf(
            Vector2(-1, -1),
            Vector2(0, -1),
            Vector2(1, -1),
            Vector2(1, 0),
            Vector2(1, 1),
            Vector2(0, 1),
            Vector2(-1, 1),
            Vector2(-1, 0),
        )
    }

    private fun parseInput(input: Sequence<String>): Set<Vector2> = buildSet {
        input.forEachIndexed { y, line -> line.forEachIndexed { x, ch -> if (ch == '#') add(Vector2(x, y))  } }
    }

    private fun runStep(positions: Set<Vector2>, offset: Int): Set<Vector2> {
        val intendedPositions = mutableMapOf<Vector2, MutableList<Vector2>>()

        fun checkMove(elf: Vector2, adjacencyList: List<Vector2>, direction: Vector2): Boolean {
            if (adjacencyList.none { elf + it in positions }) {
                intendedPositions.computeIfAbsent(elf + direction) { mutableListOf() } += elf
                return true
            }

            return false
        }

        outer@for (elf in positions) {
            if (checkMove(elf, neighboursAll, Vector2.ORIGIN)) continue

            for (i in 0 until 4) when ((i + offset) % 4) {
                0 -> if (checkMove(elf, neighboursNorth, Direction.NORTH.delta)) continue@outer
                1 -> if (checkMove(elf, neighboursSouth, Direction.SOUTH.delta)) continue@outer
                2 -> if (checkMove(elf, neighboursWest, Direction.WEST.delta)) continue@outer
                3 -> if (checkMove(elf, neighboursEast, Direction.EAST.delta)) continue@outer
            }

            intendedPositions.computeIfAbsent(elf) { mutableListOf() } += elf
        }

        return buildSet {
            for ((target, elves) in intendedPositions) {
                if (elves.size < 2) add(target) else addAll(elves)
            }
        }
    }
    override fun solveFirstPart(input: Sequence<String>): String {
        var positions = parseInput(input)
        repeat(10) {
            positions = runStep(positions, it)
        }

        return (positions.bounds().area - positions.size).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var positions = parseInput(input)
        var steps = 0
        while (true) {
            val newPositions = runStep(positions, steps++)
            if (newPositions == positions) break
            positions = newPositions
        }

        return steps.toString()
    }
}
