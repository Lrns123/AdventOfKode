package dev.lfelzinga.adventofkode.puzzles.aoc22.day10

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.ocr.ocr

class CRTPuzzle : Puzzle<Sequence<String>>(2022, 10, LinesTransformer()) {
    private fun executeProgram(program: List<String>): IntArray {
        val totalCycles = program.count { it == "noop" } + program.count { it.startsWith("addx") } * 2
        val output = IntArray(totalCycles)

        var cycle = 0
        var x = 1

        for (instruction in program) {
            val tokens = instruction.split(' ')
            when (tokens[0]) {
                "noop" -> output[cycle++] = x
                "addx" -> {
                    output[cycle++] = x
                    output[cycle++] = x
                    x += tokens[1].toInt()
                }
            }
        }

        return output
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val output = executeProgram(input.toList())
        val cyclesOfInterest = listOf(20, 60, 100, 140, 180, 220)

        return cyclesOfInterest.sumOf { it * output[it - 1] }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val output = executeProgram(input.toList())
        val grid = CharGrid(40, 6).filledWith('.')

        for (y in 0 until 6) {
            for (x in 0 until 40) {
                if (output[y * 40 + x] in (x - 1)..(x + 1)) {
                    grid[x, y] = '#'
                }
            }
        }

        return grid.ocr('#')
    }
}
