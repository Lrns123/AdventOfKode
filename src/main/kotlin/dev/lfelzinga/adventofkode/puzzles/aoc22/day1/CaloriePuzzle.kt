package dev.lfelzinga.adventofkode.puzzles.aoc22.day1

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class CaloriePuzzle : Puzzle<String>(2022, 1, StringTransformer()) {
    private fun parseInput(input: String) = input
        .split("\n\n")
        .map { line -> line.split('\n').sumOf { it.toInt() } }

    override fun solveFirstPart(input: String): String {
        return parseInput(input).max().toString()
    }

    override fun solveSecondPart(input: String): String {
        return parseInput(input).sortedDescending().take(3).sum().toString()
    }
}
