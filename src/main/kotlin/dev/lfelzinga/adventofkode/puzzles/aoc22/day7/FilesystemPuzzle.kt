package dev.lfelzinga.adventofkode.puzzles.aoc22.day7

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class FilesystemPuzzle : Puzzle<Sequence<String>>(2022, 7, LinesTransformer()) {
    private sealed class Node {
        abstract val name: String
        abstract val size: Long
        abstract val nestedSize: Long

        class File(override val name: String, override val size: Long) : Node() {
            override val nestedSize: Long = size
        }

        class Dir(override val name: String, val parent: Dir?) : Node() {
            private var cachedNestedSize = -1L
            override val size: Long = 0
            override val nestedSize: Long
                get() = if (cachedNestedSize != -1L)
                    cachedNestedSize
                else
                    contents.sumOf { it.nestedSize }.also { cachedNestedSize = it }

            val contents = mutableListOf<Node>()
        }
    }

    private fun parseInput(input: Sequence<String>): Node.Dir {
        val root = Node.Dir("/", null)
        var dir: Node.Dir = root

        for (line in input) {
            if (line.startsWith("$ ")) {
                val args = line.split(' ').drop(1)
                if (args[0] == "cd") {
                    dir = when (args[1]) {
                        "/" -> root
                        ".." -> dir.parent ?: root
                        else -> dir.contents.find { it.name == args[1] } as? Node.Dir ?: error("Cannot find dir ${args[1]}")
                    }
                }
            } else {
                val (size, name) = line.split(' ')
                if (size == "dir") {
                    dir.contents += Node.Dir(name, dir)
                } else {
                    dir.contents += Node.File(name, size.toLong())
                }
            }
        }

        return root
    }

    private fun Node.Dir.walkDirs(): Sequence<Node.Dir> = sequence {
        yield(this@walkDirs)
        contents.filterIsInstance<Node.Dir>().forEach { yieldAll(it.walkDirs()) }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return parseInput(input).walkDirs().filter { it.nestedSize <= 100000L }.sumOf { it.nestedSize }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val root = parseInput(input)
        val freeSpace = 70000000L - root.nestedSize
        val spaceNeeded = 30000000L - freeSpace

        return root.walkDirs().map { it.nestedSize }.sorted().first { it >= spaceNeeded }.toString()
    }
}
