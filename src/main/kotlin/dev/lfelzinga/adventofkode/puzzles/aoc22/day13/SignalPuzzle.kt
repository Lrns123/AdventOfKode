package dev.lfelzinga.adventofkode.puzzles.aoc22.day13

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import kotlin.math.min

class SignalPuzzle : Puzzle<Sequence<String>>(2022, 13, LinesTransformer()) {
    companion object {
        private val signalTokenRegex = Regex("\\[|]|,|(\\d+)")
    }

    private sealed interface Signal {
        data class Number(val value: Int) : Signal
        data class Nested(val children: List<Signal>) : Signal
    }

    private fun parseSignal(input: String): Signal.Nested {
        val tokens = signalTokenRegex.findAll(input).map { it.value }.toList().listIterator()
        return parseSignal(tokens) as? Signal.Nested ?: error("Unexpected top-level number")
    }

    private fun parseSignal(tokenStream: ListIterator<String>): Signal {
        val token = tokenStream.next()

        if (token == "[") {
            if (tokenStream.next() == "]")
                return Signal.Nested(emptyList())

            tokenStream.previous()

            val children = mutableListOf<Signal>()
            while (true) {
                children += parseSignal(tokenStream)
                when (val token2 = tokenStream.next()) {
                    "]" -> return Signal.Nested(children)
                    "," -> continue
                    else -> error("Unexpected token '$token2'")
                }
            }
        } else if (token[0] in '0'..'9') {
            return Signal.Number(token.toInt())
        } else {
            error("Unexpected token '$token'")
        }
    }

    private fun nestedOf(signal: Signal) = Signal.Nested(listOf(signal))
    private fun createDivider(value: Int): Signal.Nested = nestedOf(nestedOf(Signal.Number(value)))

    private fun isCorrectOrder(first: Signal.Nested, second: Signal.Nested): Boolean? {
        val len = min(first.children.size, second.children.size)
        for (i in 0 until len) {
            val left = first.children[i]
            val right = second.children[i]

            return when (left) {
                is Signal.Number -> when (right) {
                    is Signal.Number -> if (left.value == right.value) continue else left.value < right.value
                    is Signal.Nested -> isCorrectOrder(nestedOf(left), right) ?: continue
                }
                is Signal.Nested -> when (right) {
                    is Signal.Number -> isCorrectOrder(left, nestedOf(right)) ?: continue
                    is Signal.Nested -> isCorrectOrder(left, right) ?: continue
                }
            }
        }

        if (first.children.size == second.children.size) return null
        return first.children.size < second.children.size
    }

    private fun parseInput(input: Sequence<String>): Sequence<Signal.Nested> {
        return input
            .filter { it.isNotEmpty() }
            .map { parseSignal(it) }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return parseInput(input)
            .chunked(2)
            .foldIndexed(0) { index, acc, (first, second) ->
                if (isCorrectOrder(first, second) != false) acc + index + 1 else acc
            }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val divider1 = createDivider(2)
        val divider2 = createDivider(6)

        val signals = (parseInput(input).toList() + listOf(divider1, divider2))
            .sortedWith { left, right -> if (isCorrectOrder(left, right) ?: return@sortedWith 0) -1 else 1 }

        return ((signals.indexOf(divider1) + 1) * (signals.indexOf(divider2) + 1)).toString()
    }
}
