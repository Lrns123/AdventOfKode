package dev.lfelzinga.adventofkode.puzzles.aoc22.day12

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.breadthFirstSearch
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.breadthFirstSearchTo
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get

class HillClimbingPuzzle : Puzzle<Sequence<String>>(2022, 12, LinesTransformer()) {
    private fun findStartAndEndPoint(map: CharGrid): Pair<Vector2, Vector2> {
        val start = map.array.indexOf('S')
        val end = map.array.indexOf('E')

        return Vector2(start % map.width, start / map.width) to Vector2(end % map.width, end / map.width)
    }

    private val Char.height: Int
        get() = when (this) {
            in 'a'..'z' -> this - 'a'
            'S' -> 0
            'E' -> 25
            else -> error("Invalid height ($this)")
        }

    override fun solveFirstPart(input: Sequence<String>): String {
        val map = input.toList().toCharGrid()
        val (start, end) = findStartAndEndPoint(map)

        val graph = object : Graph<Vector2> {
            private val bounds = map.bounds()

            override fun neighbours(node: Vector2): List<Vector2> {
                val height = map[node].height
                return node.neighbours()
                    .filter { it in bounds }
                    .filter { map[it].height in 0..(height + 1) }
            }
        }

        val path = graph.breadthFirstSearchTo(start, end) ?: error("No path found")
        return path.cost.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val map = input.toList().toCharGrid()
        val (_, end) = findStartAndEndPoint(map)

        val graph = object : Graph<Vector2> {
            private val bounds = map.bounds()

            override fun neighbours(node: Vector2): List<Vector2> {
                val height = map[node].height
                return node.neighbours()
                    .filter { it in bounds }
                    .filter { map[it].height in (height - 1)..25 }
            }
        }

        val path = graph.breadthFirstSearch(end) { map[it.node].height == 0 } ?: error("No path found")
        return path.cost.toString()
    }
}
