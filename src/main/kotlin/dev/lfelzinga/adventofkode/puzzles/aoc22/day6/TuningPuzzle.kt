package dev.lfelzinga.adventofkode.puzzles.aoc22.day6

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class TuningPuzzle : Puzzle<String>(2022, 6, StringTransformer()) {
    private fun findMarker(input: String, len: Int): Int {
        val occurrences = IntArray(26)

        for (i in 0 until len) {
            occurrences[input[i] - 'a']++
        }

        if (occurrences.none { it > 1 }) return len

        for (i in len until input.length) {
            occurrences[input[i - len] - 'a']--
            occurrences[input[i] - 'a']++

            if (occurrences.none { it > 1 }) return i + 1
        }

        return -1
    }

    override fun solveFirstPart(input: String): String {
        return findMarker(input, 4).toString()
    }

    override fun solveSecondPart(input: String): String {
        return findMarker(input, 14).toString()
    }
}
