package dev.lfelzinga.adventofkode.puzzles.aoc22.day17

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.lcm
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import kotlin.math.max

class TetrisPuzzle : Puzzle<String>(2022, 17, StringTransformer()) {
    companion object {
        private val shapes = listOf(
            listOf(Vector2(0, 0), Vector2(1, 0), Vector2(2, 0), Vector2(3, 0)),
            listOf(Vector2(1, 0), Vector2(0, 1), Vector2(1, 1), Vector2(2, 1), Vector2(1, 2)),
            listOf(Vector2(2, 0), Vector2(2, 1), Vector2(2, 2), Vector2(1, 0), Vector2(0, 0)),
            listOf(Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3)),
            listOf(Vector2(0, 0), Vector2(1, 0), Vector2(0, 1), Vector2(1, 1))
        )
    }

    private fun <T> Iterable<T>.infiniteIterator() = sequence { while(true) yieldAll(this@infiniteIterator) }.iterator()
    private fun getShapesIterator() = shapes.withIndex().infiniteIterator()
    private fun getWindIterator(windDirections: String) = windDirections.withIndex().infiniteIterator()

    private fun simulateRocks(windDirections: String, rockCount: Long): Long {
        val shapes = getShapesIterator()
        val wind = getWindIterator(windDirections)
        val positions = mutableSetOf<Vector2>()
        var height = 0
        var iteration = 0L
        val heights = IntArray(7)
        val heightAtIteration = mutableListOf<Long>()
        val cache = mutableMapOf<String, Long>()

        while(iteration < rockCount) {
            var offset = Vector2(2, height + 3)
            val (shapeIdx, shape) = shapes.next()

            while (true) {
                val (windIdx, windDir) = wind.next()

                val windOffset = offset + if (windDir == '<') Vector2(-1, 0) else Vector2(1, 0)
                if (shape.none { it.x + windOffset.x !in 0..6 || it + windOffset in positions }) {
                    offset = windOffset
                }

                val fallOffset = offset + Vector2(0, -1)
                if (shape.none { it + fallOffset in positions || it.y + fallOffset.y < 0}) {
                    offset = fallOffset
                } else {
                    shape.forEach { positions += it + offset }
                    height = max(height, shape.maxOf { offset.y + it.y + 1 })
                    heightAtIteration += height.toLong()

                    for (point in positions) {
                        heights[point.x] = max(heights[point.x], point.y + 1)
                    }

                    val pruneHeight = heights.min() - 5

                    positions.removeIf { it.y < pruneHeight }

                    val cacheKey = "$shapeIdx|$windIdx|${heights.map { it - height }}"
                    if (cacheKey in cache) {
                        val loopIteration = cache[cacheKey]!!
                        val cycleSize = iteration - loopIteration
                        val heightPerCycle = height - heightAtIteration[loopIteration.toInt()]
                        val cyclesRemaining = (rockCount - iteration - 1) / cycleSize
                        val rocksRemaining = (rockCount - iteration - 1) % cycleSize

                        return height + (heightPerCycle * cyclesRemaining) + (heightAtIteration[loopIteration.toInt() + rocksRemaining.toInt()] - heightAtIteration[loopIteration.toInt()])
                    } else {
                        cache[cacheKey] = iteration
                    }

                    break
                }
            }

            iteration++
        }

        return height.toLong()
    }

    override fun solveFirstPart(input: String): String {
        return simulateRocks(input, 2022).toString()
    }

    override fun solveSecondPart(input: String): String {
        return simulateRocks(input, 1_000_000_000_000L).toString()
    }
}
