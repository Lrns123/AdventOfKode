package dev.lfelzinga.adventofkode.puzzles.aoc22.day9

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import kotlin.math.absoluteValue
import kotlin.math.sign

class RopePuzzle : Puzzle<Sequence<String>>(2022, 9, LinesTransformer()) {
    private fun parseLine(line: String): Pair<Direction, Int> {
        return line.split(' ').let { (dir, distance) -> Direction.fromChar(dir.first()) to distance.toInt() }
    }

    private fun computeMovement(knot: Vector2, parent: Vector2): Vector2 {
        val delta = parent - knot
        if (delta.x.absoluteValue > 1 || delta.y.absoluteValue > 1) {
            return Vector2(delta.x.sign, delta.y.sign)
        }

        return Vector2.ORIGIN
    }

    private fun visitedTailPositions(motions: Sequence<Pair<Direction, Int>>, ropeLength: Int): Int {
        val knots = Array(ropeLength) { Vector2(0, 0) }
        val visited = mutableSetOf(knots.last())

        for ((dir, distance) in motions) {
            repeat(distance) {
                knots[0] += dir.delta
                for (i in 1 until ropeLength) {
                    knots[i] += computeMovement(knots[i], knots[i - 1])
                }
                visited += knots.last()
            }
        }

        return visited.size
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return visitedTailPositions(input.map(::parseLine), 2).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return visitedTailPositions(input.map(::parseLine), 10).toString()
    }
}
