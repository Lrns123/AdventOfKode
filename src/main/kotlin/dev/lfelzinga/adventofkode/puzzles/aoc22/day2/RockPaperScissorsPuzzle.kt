package dev.lfelzinga.adventofkode.puzzles.aoc22.day2

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class RockPaperScissorsPuzzle : Puzzle<Sequence<String>>(2022, 2, LinesTransformer()) {
    private enum class Move {
        ROCK,
        PAPER,
        SCISSORS;

        val winningCounter: Move
            get() = when(this) {
                ROCK -> PAPER
                PAPER -> SCISSORS
                SCISSORS -> ROCK
            }

        val losingCounter: Move
            get() = when(this) {
                ROCK -> SCISSORS
                PAPER -> ROCK
                SCISSORS -> PAPER
            }
    }

    private fun moveForOrdinal(ordinal: Int): Move = Move.values()[ordinal]

    private fun scoreForGame(opponentMove: Move, yourMove: Move): Int {
        val moveScore = yourMove.ordinal + 1
        val gameScore = when {
            yourMove === opponentMove -> 3
            yourMove === opponentMove.winningCounter -> 6
            else -> 0
        }

        return moveScore + gameScore
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { it.split(' ').map(String::first) }
            .sumOf { (opponentMove, yourMove) ->
                scoreForGame(moveForOrdinal(opponentMove - 'A'), moveForOrdinal(yourMove - 'X'))
            }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { it.split(' ').map(String::first) }
            .sumOf { (opponentMove, yourMove) ->
                val move = moveForOrdinal(opponentMove - 'A')
                scoreForGame(
                    move,
                    when (yourMove) {
                        'X' -> move.losingCounter
                        'Y' -> move
                        else -> move.winningCounter
                    }
                )
            }
            .toString()
    }
}
