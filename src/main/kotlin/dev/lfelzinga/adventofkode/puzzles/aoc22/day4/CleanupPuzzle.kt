package dev.lfelzinga.adventofkode.puzzles.aoc22.day4

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class CleanupPuzzle : Puzzle<Sequence<String>>(2022, 4, LinesTransformer()) {
    companion object {
        private val inputRegex = Regex("(\\d+)-(\\d+),(\\d+)-(\\d+)")
    }

    private infix fun IntRange.isWithin(other: IntRange) = first >= other.first && last <= other.last
    private infix fun IntRange.overlapsWith(other: IntRange) = !(first > other.last || last < other.first)

    private fun parseInput(input: Sequence<String>) = input
        .map { inputRegex.matchEntire(it)?.destructured ?: error("Cannot parse line $it") }
        .map { (min1, max1, min2, max2) -> min1.toInt()..max1.toInt() to min2.toInt()..max2.toInt() }

    override fun solveFirstPart(input: Sequence<String>): String {
        return parseInput(input)
            .count { (first, second) -> first isWithin second || second isWithin first}
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return parseInput(input)
            .count { (first, second) -> first overlapsWith second }
            .toString()
    }
}
