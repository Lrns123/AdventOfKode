package dev.lfelzinga.adventofkode.puzzles.aoc22.day16

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.explore
import kotlin.math.max

class PressurePuzzle : Puzzle<Sequence<String>>(2022, 16, LinesTransformer()) {
    companion object {
        private val valveRegex = Regex("Valve (\\w+) has flow rate=(\\d+); tunnels? leads? to valves? (.*)")
    }

    data class Location(val name: String, val flowRate: Int, val neighbours: List<String>)

    private fun parseInput(input: Sequence<String>): Map<String, Location> {
        return input
            .map { valveRegex.matchEntire(it)?.destructured ?: error("Could not parse line '$it'") }
            .map { (name, flowRate, neighbours) ->
                Location(name, flowRate.toInt(), neighbours.split(", "))
            }
            .associateBy { it.name }
    }

    private fun precomputePathsBetweenValves(locations: Map<String, Location>): Map<String, List<Pair<String, Int>>> {
        val paths = mutableMapOf<String, List<Pair<String, Int>>>()
        val graph = object : Graph<String> {
            override fun neighbours(node: String): List<String> {
                return locations[node]?.neighbours ?: error("Unknown location")
            }
        }

        for (location in locations.keys) {
            val destinations = mutableListOf<Pair<String, Int>>()
            graph.explore(location) {
                if (locations[it.node]!!.flowRate != 0) {
                    destinations += it.node to it.cost
                }
            }
            paths[location] = destinations
        }

        return paths
    }

    private fun maxPressure(
        location: String,
        pressure: Int,
        remaining: Int,
        visited: Set<String>,
        locations: Map<String, Location>,
        paths: Map<String, List<Pair<String, Int>>>
    ): Int {
        return paths[location]!!
            .filter { (neighbour, distance) -> neighbour !in visited && distance <= remaining - 1 }
            .maxOfOrNull { (neighbour, distance) ->
                maxPressure(
                    location = neighbour,
                    pressure = pressure + (remaining - distance - 1) * locations[neighbour]!!.flowRate,
                    remaining = remaining - distance - 1,
                    visited = visited + neighbour,
                    locations = locations,
                    paths = paths
                )
            } ?: pressure
    }

    private fun maxPressureForValves(
        startLocation: String,
        remaining: Int,
        locations: Map<String, Location>,
        paths: Map<String, List<Pair<String, Int>>>
    ): Map<Set<String>, Int> {
        val valvePressure = mutableMapOf<Set<String>, Int>()
        fun computeMaxValvePressure(
            location: String,
            pressure: Int,
            remaining: Int,
            visited: Set<String>,
            locations: Map<String, Location>,
            paths: Map<String, List<Pair<String, Int>>>,
            valvePressure: MutableMap<Set<String>, Int>
        ) {
            valvePressure[visited] = max(valvePressure[visited] ?: 0, pressure)

            paths[location]!!
                .filter { (neighbour, distance) -> neighbour !in visited && distance <= remaining - 1 }
                .forEach { (neighbour, distance) ->
                    computeMaxValvePressure(
                        location = neighbour,
                        pressure = pressure + (remaining - distance - 1) * locations[neighbour]!!.flowRate,
                        remaining = remaining - distance - 1,
                        visited = visited + neighbour,
                        locations = locations,
                        paths = paths,
                        valvePressure = valvePressure
                    )
                }
        }

        computeMaxValvePressure(startLocation, 0, remaining, emptySet(), locations, paths, valvePressure)

        return valvePressure
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val locations = parseInput(input)
        val paths = precomputePathsBetweenValves(locations)

        return maxPressure("AA", 0, 30, emptySet(), locations, paths).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val locations = parseInput(input)
        val paths = precomputePathsBetweenValves(locations)
        val valvePressure = maxPressureForValves("AA", 26, locations, paths)

        var maxPressure = 0
        for (set1 in valvePressure.keys) {
            for (set2 in valvePressure.keys) {
                if (set1.none { it in set2 }) {
                    maxPressure = max(maxPressure, valvePressure[set1]!! + valvePressure[set2]!!)
                }
            }
        }

        return maxPressure.toString()
    }
}
