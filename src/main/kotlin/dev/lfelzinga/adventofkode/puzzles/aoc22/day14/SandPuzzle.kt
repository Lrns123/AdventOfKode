package dev.lfelzinga.adventofkode.puzzles.aoc22.day14

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.set
import kotlin.math.max
import kotlin.math.min

class SandPuzzle : Puzzle<Sequence<String>>(2022, 14, LinesTransformer()) {
    companion object {
        private val sandMoves = listOf(
            Vector2(0, 1),
            Vector2(-1, 1),
            Vector2(1, 1)
        )
    }

    private fun parseInput(input: Sequence<String>, addFloor: Boolean): Pair<BooleanGrid, Vector2> {
        val paths = input.map { line ->
            line.split(" -> ")
                .map { it.split(',') }
                .map { (x, y) -> Vector2(x.toInt(), y.toInt()) }
        }.toList()

        val pathBounds = (paths.flatten().toSet() + Vector2(500, 0)).bounds()
        val margin = if (addFloor) Vector2(200, 3) else Vector2(1, 1)
        val bounds = pathBounds.min - margin to pathBounds.max + margin
        val size = bounds.second - bounds.first
        val start = Vector2(500, 0) - bounds.first

        val grid = BooleanGrid(size.x, size.y)

        for (path in paths) {
            for ((p1, p2) in path.windowed(2)) {
                if (p1.x == p2.x) {
                    for (y in min(p1.y, p2.y)..max(p1.y, p2.y)) {
                        grid[p1.x - bounds.first.x, y - bounds.first.y] = true
                    }
                } else {
                    for (x in min(p1.x, p2.x)..max(p1.x, p2.x)) {
                        grid[x - bounds.first.x, p1.y - bounds.first.y] = true
                    }
                }
            }
        }

        if (addFloor) {
            val floorY = grid.height - 1
            for (x in 0 until grid.width) {
                grid[x, floorY] = true
            }
        }

        return grid to start
    }

    private fun simulateSand(grid: BooleanGrid, start: Vector2): Boolean {
        if (grid[start]) return false

        var pos = start
        while (pos.y != grid.height - 1) {
            when (val move = sandMoves.find { !grid[pos + it] }) {
                null -> {
                    grid[pos] = true
                    return true
                }
                else -> pos += move
            }
        }

        return false
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val (grid, start) = parseInput(input, false)
        var grains = 0

        while (simulateSand(grid, start)) grains++

        return grains.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val (grid, start) = parseInput(input, true)
        var grains = 0

        while (simulateSand(grid, start)) grains++

        require(grid[start]) { "Sanity check failed" }

        return grains.toString()
    }
}
