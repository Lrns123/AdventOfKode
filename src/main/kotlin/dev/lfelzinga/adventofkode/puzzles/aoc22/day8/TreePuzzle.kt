package dev.lfelzinga.adventofkode.puzzles.aoc22.day8

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import kotlin.math.max

class TreePuzzle : Puzzle<String>(2022, 8, StringTransformer()) {
    private fun findVisibleTrees(lines: List<String>): Set<Vector2> {
        val visibleTrees = mutableSetOf<Vector2>()

        for (y in lines.indices) {
            var highest = -1
            for (x in lines[y].indices) {
                if (lines[y][x] - '0' > highest) {
                    visibleTrees += Vector2(x, y)
                    highest = lines[y][x] - '0'
                }
            }

            highest = -1
            for (x in lines[y].indices.reversed()) {
                if (lines[y][x] - '0' > highest) {
                    visibleTrees += Vector2(x, y)
                    highest = lines[y][x] - '0'
                }
            }
        }

        for (x in lines.first().indices) {
            var highest = -1
            for (y in lines.indices) {
                if (lines[y][x] - '0' > highest) {
                    visibleTrees += Vector2(x, y)
                    highest = lines[y][x] - '0'
                }
            }

            highest = -1
            for (y in lines.indices.reversed()) {
                if (lines[y][x] - '0' > highest) {
                    visibleTrees += Vector2(x, y)
                    highest = lines[y][x] - '0'
                }
            }
        }

        return visibleTrees
    }

    private fun scenicScore(lines: List<String>, x: Int, y: Int): Int {
        val treeHouseHeight = lines[y][x] - '0'

        val leftVis = x - ((x - 1 downTo 0).firstOrNull { lines[y][it] - '0' >= treeHouseHeight } ?: 0)
        val rightVis = ((x + 1 until lines[y].length).firstOrNull { lines[y][it] - '0' >= treeHouseHeight } ?: (lines[y].length - 1)) - x
        val upVis = y - ((y - 1 downTo 0).firstOrNull { lines[it][x] - '0' >= treeHouseHeight } ?: 0)
        val downVis = ((y + 1 until lines.size).firstOrNull { lines[it][x] - '0' >= treeHouseHeight } ?: (lines.size - 1)) - y

        return leftVis * rightVis * upVis * downVis
    }

    private fun maxScenicScore(lines: List<String>): Int {
        var maxScore = -1
        for (y in 1 until lines.size - 1) {
            for (x in 1 until lines[y].length - 1) {
                maxScore = max(maxScore, scenicScore(lines, x, y))
            }
        }

        return maxScore
    }

    override fun solveFirstPart(input: String): String {
        return findVisibleTrees(input.split('\n')).size.toString()
    }

    override fun solveSecondPart(input: String): String {
        return maxScenicScore(input.split('\n')).toString()
    }
}
