package dev.lfelzinga.adventofkode.puzzles.aoc22.day15

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Rect
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import kotlin.math.absoluteValue

class BeaconPuzzle : Puzzle<Sequence<String>>(2022, 15, LinesTransformer()) {
    companion object {
        private val beaconRegex = Regex("Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)")
    }

    private data class Sensor(val pos: Vector2, val nearestBeacon: Vector2) {
        val range = (nearestBeacon - pos).magnitude
    }

    private fun Iterable<IntRange>.flattenRanges(): Sequence<IntRange> = sequence {
        yield(reduce { acc, range ->
            if (acc.last < range.first) {
                yield(acc)
                range
            } else if (acc.last < range.last) {
                acc.first..range.last
            } else {
                acc
            }
        })
    }

    private fun coverageAtY(sensors: List<Sensor>, y: Int): List<IntRange> {
        return sensors
            .mapNotNull {
                val yDistance = (it.pos.y - y).absoluteValue
                val xRange = it.range - yDistance

                if (xRange < 0) null else (it.pos.x - xRange) .. (it.pos.x + xRange)
            }.sortedBy { it.first }
    }

    private fun sensorBottomRightPerimeter(sensor: Sensor, area: Rect) = sequence {
        val perimeterDistance = sensor.range + 1

        for (dx in 0..perimeterDistance) {
            if (sensor.pos.x + dx !in area.min.x..area.max.x) continue

            val dy = perimeterDistance - (dx.absoluteValue)
            if (dy == 0) {
                if (sensor.pos.y in area.min.y..area.max.y)
                    yield(Vector2(sensor.pos.x + dx, sensor.pos.y))
            } else {
                if (sensor.pos.y + dy in area.min.y..area.max.y)
                    yield(Vector2(sensor.pos.x + dx, sensor.pos.y + dy))
            }
        }
    }

    private fun parseInput(input: Sequence<String>): List<Sensor> {
        return input
            .map { beaconRegex.matchEntire(it)?.destructured ?: error("Cannot parse line '$it'") }
            .map { (sx, sy, bx, by) -> Sensor(Vector2(sx.toInt(), sy.toInt()), Vector2(bx.toInt(), by.toInt())) }
            .toList()
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val sensors = parseInput(input)
        return coverageAtY(sensors, 2_000_000).flattenRanges().sumOf { it.last.toLong() - it.first.toLong() }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val rect = Rect(Vector2(0, 0), Vector2(4_000_000, 4_000_000))
        val sensors = parseInput(input)

        return sensors
            .asSequence()
            .flatMap { sensorBottomRightPerimeter(it, rect) }
            .firstOrNull { p -> sensors.all { (it.pos - p).magnitude - it.range >= 1 } }
            ?.let { it.x.toLong() * 4_000_000L + it.y.toLong() }
            ?.toString() ?: "???"
    }
}
