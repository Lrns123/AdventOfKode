package dev.lfelzinga.adventofkode.puzzles.aoc22.day24

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.lcm
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.breadthFirstSearch
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Rect
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.expand
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get

class BlizzardPuzzle : Puzzle<Sequence<String>>(2022, 24, LinesTransformer()) {

    private fun getBlizzards(grid: CharGrid): List<Pair<Vector2, Direction>> = buildList {
        for (y in 0 until grid.height) {
            for (x in 0 until grid.width) {
                when (grid[x, y]) {
                    '>', '<', '^', 'v' -> add(Vector2(x, y) to Direction.fromChar(grid[x, y]))
                }
            }
        }
    }

    private infix fun Vector2.wrappedIn(rect: Rect) = Vector2(x.mod(rect.width), y.mod(rect.height))

    private fun precomputeBlizzardLocations(grid: CharGrid): List<Set<Vector2>> {
        val blizzards = getBlizzards(grid)
        val offset = Vector2(1, 1)
        val bounds = grid.bounds().expand(-1, -1)

        return buildList {
            for (i in 0 until lcm(bounds.width, bounds.height)) {
                add(blizzards.map { (start, dir) -> offset + (((start - offset) + dir.delta * i) wrappedIn bounds) }.toSet())
            }
        }
    }

    data class State(val pos: Vector2, val blizzardOffset: Int)

    private fun createGraph(grid: CharGrid): Graph<State> {
        val bounds = grid.bounds()
        val blizzardPositions = precomputeBlizzardLocations(grid)

        return object : Graph<State> {
            override fun neighbours(node: State): List<State> = buildList {
                val nextBlizzardIndex = (node.blizzardOffset + 1) % blizzardPositions.size
                val nextBlizzardPositions = blizzardPositions[nextBlizzardIndex]

                if (node.pos !in nextBlizzardPositions) {
                    add(node.copy(blizzardOffset = nextBlizzardIndex))
                }

                for (neighbour in node.pos.neighbours()) {
                    if (neighbour in bounds && grid[neighbour] != '#' && neighbour !in nextBlizzardPositions) {
                        add(State(neighbour, nextBlizzardIndex))
                    }
                }
            }
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()
        val graph = createGraph(grid)

        val path = graph.breadthFirstSearch(State(Vector2(1, 0), 0)) {
            it.node.pos.y == grid.height - 1
        }

        return path?.cost.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()
        val graph = createGraph(grid)

        val firstLeg = graph.breadthFirstSearch(State(Vector2(1, 0), 0)) {
            it.node.pos.y == grid.height - 1
        } ?: error("Could not find path for first leg")

        val secondLeg = graph.breadthFirstSearch(firstLeg.node) {
            it.node.pos.y == 0
        } ?: error("Could not find path for second leg")

        val finalLeg = graph.breadthFirstSearch(secondLeg.node) {
            it.node.pos.y == grid.height - 1
        } ?: error("Could not find path for second leg")

        return (firstLeg.cost + secondLeg.cost + finalLeg.cost).toString()
    }
}
