package dev.lfelzinga.adventofkode.puzzles.aoc16.day19

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.NumberTransformer
import kotlin.math.floor
import kotlin.math.ln
import kotlin.math.pow

class WhiteElephantPuzzle : Puzzle<Long>(2016, 19, NumberTransformer()) {
    private fun findWinningElf(numElves: Int): Int {
        // Pattern: If the number of a power of two, then the first elf wins
        //          Otherwise, the winner is 2 * l + 1, where l is the number of elves minus the highest power of two below this number.
        val a = numElves.takeHighestOneBit()
        val l = numElves - a
        return 2 * l + 1
    }

    private fun findWinningElfAcross(numElves: Int): Int {
        // Pattern: If the number of elves is a power of three, the winning elf is the last elf (said power of three)
        //          Otherwise, the winning elf goes up by 1 (starting at 1), for the next n elves, where n is said power of three,
        //          after which it goes up by two.
        val powersOfThree = floor(ln(numElves.toDouble()) / ln(3.0)).toInt()
        val highestPower = 3.0.pow(powersOfThree.toDouble()).toInt()

        if (highestPower == numElves) return numElves

        return if (numElves - highestPower <= highestPower) numElves - highestPower else 2 * numElves - 3 * highestPower
    }

    override fun solveFirstPart(input: Long): String {
        return findWinningElf(input.toInt()).toString()
    }

    override fun solveSecondPart(input: Long): String {
        return findWinningElfAcross(input.toInt()).toString()
    }
}
