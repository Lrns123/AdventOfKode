package dev.lfelzinga.adventofkode.puzzles.aoc16.day11

data class RTGObject(val emitter: Boolean, val element: String)
