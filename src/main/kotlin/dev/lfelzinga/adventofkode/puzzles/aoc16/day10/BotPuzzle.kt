package dev.lfelzinga.adventofkode.puzzles.aoc16.day10

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.puzzles.aoc16.day10.OperationTarget.BotTarget
import dev.lfelzinga.adventofkode.puzzles.aoc16.day10.OperationTarget.OutputTarget
import dev.lfelzinga.adventofkode.toolbox.matching.MultiRegexMatcher

class BotPuzzle : Puzzle<Sequence<String>>(2016, 10, LinesTransformer()) {
    companion object {
        private val valueRegex = Regex("value (\\d+) goes to bot (\\d+)")
        private val giveRegex = Regex("bot (\\d+) gives low to (\\w+) (\\d+) and high to (\\w+) (\\d+)")
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val controller = readInstructions(input)

        controller.runUntilStable()

        return controller.findComparisonBotId(17, 61).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val controller = readInstructions(input)

        controller.runUntilStable()

        return (0..2)
            .map { controller.getOutputs(it).first() }
            .fold(1L) { acc, value -> acc * value }
            .toString()
    }

    private fun readInstructions(input: Sequence<String>): BotController {
        val controller = BotController()

        MultiRegexMatcher()
            .add(valueRegex) { (value, botId) -> controller.giveChip(BotTarget(botId.toInt()), value.toInt()) }
            .add(giveRegex) { (botId, lowType, lowId, highType, highId) ->
                with(controller.getBot(botId.toInt())) {
                    lowTarget = createOperationTarget(lowType, lowId.toInt())
                    highTarget = createOperationTarget(highType, highId.toInt())
                }
            }
            .failOnUnknown()
            .match(input)

        return controller
    }

    private fun createOperationTarget(type: String, id: Int): OperationTarget {
        return when (type) {
            "bot" -> BotTarget(id)
            "output" -> OutputTarget(id)
            else -> error("Unknown operation target $type")
        }
    }
}

sealed class OperationTarget {
    class BotTarget(val botId: Int) : OperationTarget()
    class OutputTarget(val outputId: Int)  : OperationTarget()
}

class Bot(val id: Int) {
    private val heldMicrochips = mutableListOf<Int>()

    lateinit var lowTarget: OperationTarget
    lateinit var highTarget: OperationTarget

    val readyForOperation: Boolean
        get() = heldMicrochips.size >= 2

    fun giveMicrochip(value: Int) {
        heldMicrochips += value
    }

    fun run(controller: BotController) {
        if (!readyForOperation) return

        val (lowChip, highChip) = heldMicrochips.take(2).sorted()

        controller.logComparison(lowChip, highChip, id)
        controller.giveChip(lowTarget, lowChip)
        controller.giveChip(highTarget, highChip)

        heldMicrochips.removeAt(0)
        heldMicrochips.removeAt(0)
    }
}

class BotController {
    private val bots = mutableMapOf<Int, Bot>()
    private val outputs = mutableMapOf<Int, MutableList<Int>>()
    private val comparisons = mutableMapOf<Pair<Int, Int>, Int>()
    private val botQueue = ArrayDeque<Int>()

    fun step(): Boolean {
        if (botQueue.isEmpty())
            return false

        bots.getValue(botQueue.removeFirst()).run(this)
        return true
    }

    fun runUntilStable() {
        while (step()) {
            // Keep looping
        }
    }

    fun getBot(botId: Int) = bots.computeIfAbsent(botId) { Bot(it) }

    fun getOutputs(outputId: Int): List<Int> {
        return outputs[outputId] ?: emptyList()
    }

    fun findComparisonBotId(lowChip: Int, highChip: Int): Int? {
        return comparisons[lowChip to highChip]
    }

    fun logComparison(lowChip: Int, highChip: Int, botId: Int) {
        comparisons[lowChip to highChip] = botId
    }

    fun giveChip(target: OperationTarget, value: Int) = when (target) {
        is BotTarget -> with(bots.computeIfAbsent(target.botId) { Bot(it) }) {
            giveMicrochip(value)
            if (readyForOperation) {
                botQueue += id
            }
        }
        is OutputTarget -> outputs.computeIfAbsent(target.outputId) { mutableListOf() } += value
    }
}
