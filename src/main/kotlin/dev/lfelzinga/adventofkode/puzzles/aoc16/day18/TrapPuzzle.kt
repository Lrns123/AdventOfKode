package dev.lfelzinga.adventofkode.puzzles.aoc16.day18

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class TrapPuzzle : Puzzle<String>(2016, 18, StringTransformer()) {

    private fun nextRow(currentRow: String): String {
        val newRow = CharArray(currentRow.length)

        for (i in newRow.indices) {
            val leftSafe = i == 0 || currentRow[i - 1] == '.'
            val centerSafe = currentRow[i] == '.'
            val rightSafe = i == newRow.lastIndex || currentRow[i + 1] == '.'

            newRow[i] = when {
                !leftSafe && !centerSafe && rightSafe -> '^'
                leftSafe && !centerSafe && !rightSafe -> '^'
                !leftSafe && centerSafe && rightSafe -> '^'
                leftSafe && centerSafe && !rightSafe -> '^'
                else -> '.'
            }
        }

        return String(newRow)
    }

    override fun solveFirstPart(input: String): String {
        return generateSequence(input, this::nextRow).take(40).sumOf { it.count { ch -> ch == '.' } }.toString()
    }

    override fun solveSecondPart(input: String): String {
        return generateSequence(input, this::nextRow).take(400_000).sumOf { it.count { ch -> ch == '.' } }.toString()
    }
}
