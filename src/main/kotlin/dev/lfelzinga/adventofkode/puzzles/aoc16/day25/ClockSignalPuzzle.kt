package dev.lfelzinga.adventofkode.puzzles.aoc16.day25

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.assembunny.AssembunnyVM

class ClockSignalPuzzle : Puzzle<Sequence<String>>(2016, 25, LinesTransformer()) {

    private fun isClockSignal(signal: List<Int>): Boolean {
        for ((index, i) in signal.withIndex()) {
            if (i != index % 2)
                return false
        }

        return true
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val vm = AssembunnyVM()
        input.forEach(vm::addAssembly)

        var initValue = -1
        while (true) {
            vm.reset()
            vm.setRegister('a', ++initValue)
            vm.outputLimit = 4
            vm.run()

            if (!isClockSignal(vm.output))
                continue

            vm.outputLimit = 32
            vm.run()

            if (isClockSignal(vm.output)) {
                return initValue.toString()
            }
        }
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return "Merry Christmas!"
    }
}
