package dev.lfelzinga.adventofkode.puzzles.aoc16.day17

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.breadthFirstSearch
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.maxDepth
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.plus
import java.security.MessageDigest

class VaultMazePuzzle : Puzzle<String>(2016, 17, StringTransformer()) {
    companion object {
        private val md5 = MessageDigest.getInstance("MD5")
        private val target = Vector2(3, 3)
    }

    private class Node(val position: Vector2, val path: ByteArray)

    private inner class VaultMaze(val passcode: ByteArray) : Graph<Node> {
        override fun neighbours(node: Node): List<Node> = buildList {
            if (node.position != target) {
                for (dir in getAvailableDirections(passcode, node.path)) {
                    val newPos = node.position + dir
                    if (newPos.x !in 0..3 || newPos.y !in 0..3) continue

                    add(Node(newPos, node.path.appendDirection(dir)))
                }
            }
        }
    }

    private fun ByteArray.appendDirection(dir: Direction): ByteArray {
        val result = ByteArray(size + 1)
        copyInto(result)
        result[size] = when (dir) {
            Direction.NORTH -> 'U'.code.toByte()
            Direction.SOUTH -> 'D'.code.toByte()
            Direction.WEST -> 'L'.code.toByte()
            Direction.EAST -> 'R'.code.toByte()
        }
        return result
    }

    private fun getAvailableDirections(passcode: ByteArray, path: ByteArray): List<Direction> {
        md5.reset()
        md5.update(passcode)
        val hash = md5.digest(path)

        return buildList {
            if ((hash[0].toInt() and 0xF0) ushr 4 > 10) add(Direction.UP)
            if (hash[0].toInt() and 0x0F > 10) add(Direction.DOWN)
            if ((hash[1].toInt() and 0xF0) ushr 4 > 10) add(Direction.LEFT)
            if (hash[1].toInt() and 0x0F > 10) add(Direction.RIGHT)
        }
    }

    override fun solveFirstPart(input: String): String {
        return VaultMaze(input.encodeToByteArray()).breadthFirstSearch(Node(Vector2(0, 0), ByteArray(0))) {
            it.node.position == target
        }?.node?.path?.decodeToString() ?: "???"
    }

    override fun solveSecondPart(input: String): String {
       return VaultMaze(input.encodeToByteArray()).maxDepth(Node(Vector2(0, 0), ByteArray(0))).cost.toString()
    }
}
