package dev.lfelzinga.adventofkode.puzzles.aoc16.day11

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.combinations
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.breadthFirstSearch

class RTGPuzzle : Puzzle<Sequence<String>>(2016, 11, LinesTransformer()) {
    companion object {
        private val generatorRegex = Regex("(\\w+) generator")
        private val microchipRegex = Regex("(\\w+)-compatible microchip")
    }

    object RTGGraph : Graph<State> {
        override fun neighbours(node: State): List<State> = buildList {
            val canMoveUp = node.elevatorFloor != node.floors.size - 1
            val canMoveDown = node.elevatorFloor != 0

            val objectsOnFloor = node.floors[node.elevatorFloor]

            for (obj in objectsOnFloor) {
                if (canMoveDown) node.move(node.elevatorFloor - 1, obj).takeIf { it.isSafe }?.let(this::add)
                if (canMoveUp) node.move(node.elevatorFloor + 1, obj).takeIf { it.isSafe }?.let(this::add)
            }

            if (objectsOnFloor.size > 1) {
                for ((obj1, obj2) in objectsOnFloor.toList().combinations(2)) {
                    if (canMoveDown) node.move(node.elevatorFloor - 1, obj1, obj2).takeIf { it.isSafe }?.let(this::add)
                    if (canMoveUp) node.move(node.elevatorFloor + 1, obj1, obj2).takeIf { it.isSafe }?.let(this::add)
                }
            }
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return RTGGraph.breadthFirstSearch(readInitialState(input)) {
            it.node.isGoal
        }?.cost?.toString() ?: "???"
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val initialState = readInitialState(input).addObjects(0, listOf(
            RTGObject(true, "elerium"),
            RTGObject(false, "elerium"),
            RTGObject(true, "dilithium"),
            RTGObject(false, "dilithium")
        ))

        return RTGGraph.breadthFirstSearch(initialState) {
            it.node.isGoal
        }?.cost?.toString() ?: "???"
    }

    private fun readInitialState(input: Sequence<String>): State {
        return State(input.map { line ->
            buildSet {
                for (match in generatorRegex.findAll(line)) {
                    add(RTGObject(true, match.groupValues[1]))
                }

                for (match in microchipRegex.findAll(line)) {
                    add(RTGObject(false, match.groupValues[1]))
                }
            }
        }.toList())
    }
}
