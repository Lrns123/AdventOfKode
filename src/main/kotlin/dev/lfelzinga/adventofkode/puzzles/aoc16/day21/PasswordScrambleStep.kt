package dev.lfelzinga.adventofkode.puzzles.aoc16.day21

import dev.lfelzinga.adventofkode.toolbox.extensions.rotate
import dev.lfelzinga.adventofkode.toolbox.matching.MultiRegexMatcher
import kotlin.math.max
import kotlin.math.min

sealed interface PasswordScrambleStep {
    companion object {
        private val swapPositionRegex = Regex("swap position (\\d+) with position (\\d+)")
        private val swapLetterRegex = Regex("swap letter ([a-z]) with letter ([a-z])")
        private val rotateRegex = Regex("rotate (left|right) (\\d+) steps?")
        private val rotatePosBasedRegex = Regex("rotate based on position of letter ([a-z])")
        private val reverseRegex = Regex("reverse positions (\\d+) through (\\d+)")
        private val moveRegex = Regex("move position (\\d+) to position (\\d+)")

        fun parseSteps(input: Sequence<String>): List<PasswordScrambleStep> {
            val steps = mutableListOf<PasswordScrambleStep>()

            MultiRegexMatcher()
                .add(swapPositionRegex) { (x, y) -> steps += SwapPositionStep(x.toInt(), y.toInt()) }
                .add(swapLetterRegex) { (x, y) -> steps += SwapLetterStep(x.first(), y.first()) }
                .add(rotateRegex) { (dir, num) -> steps += RotateStep(num.toInt() * (if (dir == "left") -1 else 1)) }
                .add(rotatePosBasedRegex) { (letter) -> steps += RotateBasedOnPosStep(letter.first()) }
                .add(reverseRegex) { (x, y) -> steps += ReverseStep(x.toInt(), y.toInt()) }
                .add(moveRegex) { (x, y) -> steps += MoveStep(x.toInt(), y.toInt()) }
                .failOnUnknown()
                .match(input)

            return steps
        }
    }

    fun apply(input: String): String
    fun undo(input: String): String
}

private class SwapPositionStep(private val x: Int, private val y: Int) : PasswordScrambleStep {
    override fun apply(input: String): String {
        val chars = input.toCharArray()

        val tmp = chars[x]
        chars[x] = chars[y]
        chars[y] = tmp

        return String(chars)
    }

    override fun undo(input: String): String = apply(input)
}

private class SwapLetterStep(private val x: Char, private val y: Char) : PasswordScrambleStep {
    override fun apply(input: String): String {
        return input.replace(y, '\u0000').replace(x, y).replace('\u0000', x)
    }

    override fun undo(input: String): String = apply(input)
}

private class RotateStep(private val steps: Int) : PasswordScrambleStep {
    override fun apply(input: String): String {
        return input.rotate(steps)
    }

    override fun undo(input: String): String {
        return input.rotate(-steps)
    }
}

private class RotateBasedOnPosStep(private val letter: Char) : PasswordScrambleStep {
    override fun apply(input: String): String {
        val shift = input.indexOf(letter).takeIf { it >= 0 } ?: error("Character $letter not found in $input")

        return input.rotate(1 + shift + if (shift >= 4) 1 else 0)
    }

    override fun undo(input: String): String {
        val shift = findReverseShift(input).takeIf { it >= 0 } ?: error("Could not find reverse shift for $letter in $input")

        return input.rotate(-shift)
    }

    private fun findReverseShift(input: String): Int {
        val newIndex: Int = input.indexOf(letter)
        val length: Int = input.length

        if (newIndex == -1) return -1

        for (shift in 1..length) {
            val oldIndex: Int = (newIndex - shift).mod(length)
            val oldShift = 1 + oldIndex + if (oldIndex >= 4) 1 else 0
            if ((oldIndex + oldShift) % length == newIndex) return shift % length
        }

        return -1
    }
}

private class ReverseStep(private val start: Int, private val end: Int) : PasswordScrambleStep {
    override fun apply(input: String): String {
        return input.substring(0 until start) + input.substring(start .. end).reversed() + input.substring(end + 1)
    }

    override fun undo(input: String): String = apply(input)
}

private class MoveStep(private val from: Int, private val to: Int) : PasswordScrambleStep {
    override fun apply(input: String): String {
        return move(input, from, to)
    }

    override fun undo(input: String): String {
        return move(input, to, from)
    }

    private fun move(input: String, from: Int, to: Int): String {
        if (from == to) return input

        val first = min(from, to)
        val second = max(from, to)

        return buildString(input.length) {
            append(input.substring(0, first))
            if (to < from) {
                append(input[from])
                append(input.substring(first, second))
            } else {
                append(input.substring(first + 1, second + 1))
                append(input[from])
            }
            append(input.substring(second + 1))
        }
    }
}
