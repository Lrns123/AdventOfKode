package dev.lfelzinga.adventofkode.puzzles.aoc16.day11

class State(val floors: List<Set<RTGObject>>, val elevatorFloor: Int = 0) {

    val stateCode: String = buildString {
        for (floor in floors) {
            val gens = floor.count { it.emitter }
            val chips = floor.size - gens

            append(gens)
            append(chips)
        }

        append(elevatorFloor)
    }

    val isSafe: Boolean
        get() = floors.all { floor ->
            val generators = floor.mapNotNullTo(LinkedHashSet()) { it.element.takeIf { _ -> it.emitter } }

            generators.isEmpty() || !floor.any { !it.emitter && it.element !in generators }
        }

    val isGoal: Boolean
        get() {
            for (floor in 0 until floors.size - 1) {
                if (floors[floor].isNotEmpty())
                    return false
            }
            return true
        }

    override fun equals(other: Any?): Boolean {
        return other is State && other.stateCode == stateCode
    }

    override fun hashCode(): Int {
        return stateCode.hashCode()
    }
}

inline fun State.transform(newFloor: Int = elevatorFloor, updateState: (List<MutableSet<RTGObject>>) -> Unit): State {
    return State(floors.map { it.toMutableSet() }.apply(updateState), newFloor)
}

fun State.addObjects(floor: Int, objects: List<RTGObject>) = transform { state ->
    state[floor] += objects
}

fun State.move(toFloor: Int, withObject: RTGObject): State {
    require(withObject in floors[elevatorFloor]) { "$withObject is not on floor $elevatorFloor" }

    return transform(toFloor) { floors ->
        floors[elevatorFloor] -= withObject
        floors[toFloor] += withObject
    }
}

fun State.move(toFloor: Int, withObject1: RTGObject, withObject2: RTGObject): State {
    require(withObject1 in floors[elevatorFloor]) { "$withObject1 is not on floor $elevatorFloor" }
    require(withObject2 in floors[elevatorFloor]) { "$withObject2 is not on floor $elevatorFloor" }

    return transform(toFloor) { floors ->
        with(floors[elevatorFloor]) {
            remove(withObject1)
            remove(withObject2)
        }
        with(floors[toFloor]) {
            add(withObject1)
            add(withObject2)
        }
    }
}
