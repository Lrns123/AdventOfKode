package dev.lfelzinga.adventofkode.puzzles.aoc16.day16

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class DragonChecksumPuzzle : Puzzle<String>(2016, 16, StringTransformer()) {

    private tailrec fun expandDragonCurve(input: CharArray, length: Int): CharArray {
        return if (input.size == length) {
            input
        } else if (input.size > length) {
            val finalCurve = CharArray(length)
            input.copyInto(finalCurve, 0, 0, length)
            finalCurve
        } else {
            val expansion = CharArray(2 * input.size + 1)
            input.copyInto(expansion)
            expansion[input.size] = '0'

            for (i in input.indices) {
                expansion[expansion.size - i - 1] = if (input[i] == '0') '1' else '0'
            }

            expandDragonCurve(expansion, length)
        }
    }

    private tailrec fun dragonChecksum(input: CharArray): CharArray {
        val checksum = CharArray(input.size / 2)

        for (i in 0 until input.size / 2) {
            checksum[i] = if (input[i * 2] == input[i * 2 + 1]) '1' else '0'
        }

        return if (checksum.size % 2 != 0) {
            checksum
        } else {
            dragonChecksum(checksum)
        }
    }

    override fun solveFirstPart(input: String): String {
        val curve = expandDragonCurve(input.toCharArray(), 272)

        return String(dragonChecksum(curve))
    }

    override fun solveSecondPart(input: String): String {
        val curve = expandDragonCurve(input.toCharArray(), 35651584)

        return String(dragonChecksum(curve))
    }
}
