package dev.lfelzinga.adventofkode.puzzles.aoc16.day21

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class PasswordScramblingPuzzle : Puzzle<Sequence<String>>(2016, 21, LinesTransformer()) {

    private fun createScrambler(input: Sequence<String>): PasswordScrambler {
        return PasswordScrambler(PasswordScrambleStep.parseSteps(input))
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return createScrambler(input).scramble("abcdefgh")
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return createScrambler(input).unscramble("fbgdceah")
    }
}
