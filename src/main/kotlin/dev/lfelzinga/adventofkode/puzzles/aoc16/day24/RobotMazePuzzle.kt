package dev.lfelzinga.adventofkode.puzzles.aoc16.day24

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.permutations
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.explore
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2

class RobotMazePuzzle : Puzzle<Sequence<String>>(2016, 24, LinesTransformer()) {

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()
        val pointsOfInterest = findPointsOfInterest(grid)
        val paths = findShortestPathsBetweenPOIs(grid, pointsOfInterest)

        return (pointsOfInterest.keys - '0').toList()
            .permutations()
            .map { listOf('0') + it }
            .minOf { it.windowed(2).sumOf { points -> paths.getValue(points[0] to points[1]) } }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()
        val pointsOfInterest = findPointsOfInterest(grid)
        val paths = findShortestPathsBetweenPOIs(grid, pointsOfInterest)

        return (pointsOfInterest.keys - '0').toList()
            .permutations()
            .map { listOf('0') + it + '0'}
            .minOf { it.windowed(2).sumOf { points -> paths.getValue(points[0] to points[1]) } }
            .toString()
    }

    private fun findShortestPathsBetweenPOIs(grid: CharGrid, pointsOfInterest: Map<Char, Vector2>): Map<Pair<Char, Char>, Int> {
        val paths = mutableMapOf<Pair<Char, Char>, Int>()
        val maze = object : Graph<Vector2> {
            override fun neighbours(node: Vector2): List<Vector2> {
                return node.neighbours().filter { grid[it.x, it.y] != '#' }
            }
        }

        for ((poi, position) in pointsOfInterest) {
            maze.explore(position) {
                val ch = grid[it.node.x, it.node.y]
                if (ch != poi && ch in '0'..'9') {
                    paths[poi to ch] = it.cost
                }
            }
        }

        return paths
    }

    private fun findPointsOfInterest(grid: CharGrid): Map<Char, Vector2> {
        val pointsOfInterest = mutableMapOf<Char, Vector2>()

        for (x in 0 until grid.width)
            for (y in 0 until grid.height)
                if (grid[x, y] in '0'..'9')
                    pointsOfInterest[grid[x, y]] = Vector2(x, y)

        return pointsOfInterest
    }
}
