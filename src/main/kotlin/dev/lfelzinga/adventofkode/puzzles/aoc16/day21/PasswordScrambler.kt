package dev.lfelzinga.adventofkode.puzzles.aoc16.day21

class PasswordScrambler(private val steps: List<PasswordScrambleStep>) {
    fun scramble(password: String): String = steps.fold(password) { pw, step ->
        val result = step.apply(pw)
        require(result.length == pw.length) { "Step $step changed the pw size" }
        result
    }
    fun unscramble(scrambled: String): String = steps.asReversed().fold(scrambled) { pw, step -> step.undo(pw) }
}
