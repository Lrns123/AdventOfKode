package dev.lfelzinga.adventofkode.puzzles.aoc16.day1

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.TokenizingTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.times
import kotlin.math.absoluteValue

class CityGridPuzzle : Puzzle<List<String>>(2016, 1, TokenizingTransformer(", ")) {

    override fun solveFirstPart(input: List<String>): String {
        var pos = Vector2.ORIGIN
        var dir = Direction.NORTH

        input.forEach { instruction ->
            when(instruction[0]) {
                'L' -> dir = dir.left()
                'R' -> dir = dir.right()
            }

            val distance = instruction.substring(1).toInt()

            pos += dir * distance
        }

        return (pos.x.absoluteValue + pos.y.absoluteValue).toString()
    }

    override fun solveSecondPart(input: List<String>): String {
        var pos = Vector2.ORIGIN
        var dir = Direction.NORTH
        val visited = mutableSetOf<Vector2>()

        input.forEach { instruction ->
            when (instruction[0]) {
                'L' -> dir = dir.left()
                'R' -> dir = dir.right()
            }

            val distance = instruction.substring(1).toInt()

            repeat(distance) {
                pos += dir.delta

                if (!visited.add(pos)) {
                    return (pos.x.absoluteValue + pos.y.absoluteValue).toString()
                }
            }
        }

        return (pos.x.absoluteValue + pos.y.absoluteValue).toString()
    }
}
