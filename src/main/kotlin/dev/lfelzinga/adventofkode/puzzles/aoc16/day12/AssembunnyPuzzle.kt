package dev.lfelzinga.adventofkode.puzzles.aoc16.day12

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.assembunny.AssembunnyVM

class AssembunnyPuzzle : Puzzle<Sequence<String>>(2016, 12, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val vm = AssembunnyVM()

        input.forEach(vm::addAssembly)

        vm.run()
        return vm.getRegister('a').toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val vm = AssembunnyVM()

        input.forEach(vm::addAssembly)

        vm.setRegister('c', 1)
        vm.run()
        return vm.getRegister('a').toString()
    }
}
