package dev.lfelzinga.adventofkode.puzzles.aoc16.day6

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class ErrorCorrectionPuzzle : Puzzle<Sequence<String>>(2016, 6, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val charFrequency = getCharFrequency(input.toList())

        return charFrequency.joinToString("") { ('a' + it.indexOf(it.maxOrNull()!!)).toString() }
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val charFrequency = getCharFrequency(input.toList())

        return charFrequency.joinToString("") { ('a' + it.indexOf(it.minOrNull()!!)).toString() }
    }

    private fun getCharFrequency(input: List<String>): List<IntArray> {
        val charFrequency = List(input.first().length) { IntArray(26) }

        for (word in input) {
            for ((pos, ch) in word.withIndex()) {
                charFrequency[pos][ch - 'a']++
            }
        }

        return charFrequency
    }
}
