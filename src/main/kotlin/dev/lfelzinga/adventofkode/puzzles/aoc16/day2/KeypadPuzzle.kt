package dev.lfelzinga.adventofkode.puzzles.aoc16.day2

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Direction

private typealias KeypadDefinition = Array<Array<Char>>
private const val BAD = '\u0000'

class KeypadPuzzle : Puzzle<Sequence<String>>(2016, 2, LinesTransformer()) {

    private val normalKeypad: KeypadDefinition = arrayOf(
        arrayOf('1', '2', '3'),
        arrayOf('4', '5', '6'),
        arrayOf('7', '8', '9')
    )

    private val realKeypad: KeypadDefinition = arrayOf(
        arrayOf(BAD, BAD, '1', BAD, BAD),
        arrayOf(BAD, '2', '3', '4', BAD),
        arrayOf('5', '6', '7', '8', '9'),
        arrayOf(BAD, 'A', 'B', 'C', BAD),
        arrayOf(BAD, BAD, 'D', BAD, BAD)
    )

    private fun KeypadDefinition.isValidPosition(x: Int, y: Int) = y in indices && x in this[0].indices && this[y][x] != BAD

    private fun parseInput(input: String): List<Direction> = input
        .map { Direction.fromChar(it) }

    private fun getKeyCode(keypad: KeypadDefinition, directions: Sequence<String>, startX: Int, startY: Int): String {
        var x = startX
        var y = startY

        val builder = StringBuilder()

        directions.forEach { line ->
            parseInput(line).forEach {
                if (keypad.isValidPosition(x + it.dx, y + it.dy)) {
                    x += it.dx
                    y += it.dy
                }
            }

            builder.append(keypad[y][x])
        }

        return builder.toString()
    }

    override fun solveFirstPart(input: Sequence<String>) = getKeyCode(normalKeypad, input, 1, 1)

    override fun solveSecondPart(input: Sequence<String>) = getKeyCode(realKeypad, input, 0, 2)
}
