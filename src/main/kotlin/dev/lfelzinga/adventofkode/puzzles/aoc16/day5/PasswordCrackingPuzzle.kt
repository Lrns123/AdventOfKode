package dev.lfelzinga.adventofkode.puzzles.aoc16.day5

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import java.security.MessageDigest
import java.util.*
import java.util.concurrent.ConcurrentSkipListSet
import java.util.stream.Collectors
import java.util.stream.IntStream

class PasswordCrackingPuzzle : Puzzle<String>(2016, 5, StringTransformer()) {
    companion object {
        private val hexAlphabet = listOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f")
        private val md5 = ThreadLocal.withInitial { MessageDigest.getInstance("MD5") }
    }

    override fun solveFirstPart(input: String): String {
        return IntStream.iterate(0) { it + 1 }
            .parallel()
            .mapToObj { Hash(input, it) }
            .filter { it.hasFiveLeadingZeroes }
            .map { it.hexChar(5) }
            .limit(8)
            .collect(Collectors.joining())
    }

    override fun solveSecondPart(input: String): String {
        val mapped = ConcurrentSkipListSet<Int>()

        return IntStream.iterate(0) { it + 1 }
            .parallel()
            .mapToObj { Hash(input, it) }
            .filter { it.hasFiveLeadingZeroes }
            .filter { it.hexChar(5)[0].let { char -> char in '0'..'7' && mapped.add(char - '0') } }
            .limit(8)
            .sorted(Comparator.comparing { it.hexChar(5) })
            .map { it.hexChar(6) }
            .collect(Collectors.joining())
    }

    class Hash(prefix: String, number: Int) {

        private val hash: ByteArray = md5.get().digest((prefix + number).toByteArray())

        fun hexChar(pos: Int): String {
            require(pos in 0 until hash.size * 2) { "Invalid position" }

            val byte = hash[pos ushr 1].toInt()
            return if (pos and 1 == 0) hexAlphabet[byte ushr 4 and 0xF] else hexAlphabet[byte and 0xF]
        }

        val hasFiveLeadingZeroes: Boolean
            get() = hash[0].toInt() == 0 && hash[1].toInt() == 0 && (hash[2].toInt() and 0xF0) == 0
    }
}
