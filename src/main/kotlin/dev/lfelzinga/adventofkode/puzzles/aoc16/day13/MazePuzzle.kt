package dev.lfelzinga.adventofkode.puzzles.aoc16.day13

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.NumberTransformer
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.breadthFirstSearchTo
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.explore
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2

class MazePuzzle : Puzzle<Long>(2016, 13, NumberTransformer()) {
    class MazeGraph(private val designerNumber: Long) : Graph<Vector2> {
        override fun neighbours(node: Vector2): List<Vector2> {
            return node.neighbours().filter { it.isPassable() }
        }

        private fun Vector2.isPassable(): Boolean {
            return x >= 0 && y >= 0 && isOpen()
        }

        private fun Vector2.isOpen(): Boolean {
            return (x*x + 3*x + 2*x*y + y + y*y + designerNumber).countOneBits() % 2 == 0
        }
    }

    override fun solveFirstPart(input: Long): String {
        return MazeGraph(input)
            .breadthFirstSearchTo(Vector2(1, 1), Vector2(31, 39))
            ?.cost.toString()
    }

    override fun solveSecondPart(input: Long): String {
        val visited = mutableSetOf<Vector2>()

        MazeGraph(input)
            .explore(Vector2(1, 1), 50) { visited += it.node }

        return visited.size.toString()
    }
}
