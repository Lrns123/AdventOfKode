package dev.lfelzinga.adventofkode.puzzles.aoc16.day22

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.HeuristicAware
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.aStarTo
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds

class StorageGridPuzzle : Puzzle<Sequence<String>>(2016, 22, LinesTransformer()) {
    companion object {
        private val lineRegex = Regex("/dev/grid/node-x(\\d+)-y(\\d+)\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)T\\s+\\d+%")
    }

    data class StorageNode(val location: Vector2, val size: Int, val used: Int, val avail: Int)

    private fun parseInput(input: Sequence<String>): Sequence<StorageNode> = input
        .drop(2)
        .map { lineRegex.matchEntire(it)?.destructured ?: error("Could not parse line") }
        .map { (x, y, size, used, avail) -> StorageNode(Vector2(x.toInt(), y.toInt()), size.toInt(), used.toInt(), avail.toInt()) }

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = parseInput(input).toList()

        var viablePairs = 0
        for (nodeA in grid) {
            for (nodeB in grid) {
                if (nodeA.used != 0 && nodeA !== nodeB && nodeA.used < nodeB.avail) {
                    viablePairs++
                }
            }
        }

        return viablePairs.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = parseInput(input).associateBy { it.location }
        val (min, max) = grid.bounds()
        val emptyNode = grid.values.find { it.used == 0 } ?: error("Cannot find empty node")
        val initialTarget = Vector2(max.x - 1, 0) // Target is to the left of the data node

        // Observations:
        // * There is one empty node, and a line of very large nodes blocking its path to the data node (as visualized by drawGrid).
        // * There are no blockers between the data node and the target node.
        //
        // Strategy:
        // * Use pathfinding to find the shortest path from the empty node to the data node.
        //   To ensure it comes in from the right direction, set the target one node to the left.
        // * Moving the data one step closer to the target requires the empty drive to do 5 moves. (Down, 2x Right, Up, Left)
        // * Once the path from the empty drive to the data node is found, the rest can be calculated directly.

        val maze = object : Graph<Vector2>, HeuristicAware<Vector2> {
            override fun neighbours(node: Vector2): List<Vector2> {
                return node.neighbours().filter { it.x in min.x..max.x && it.y in min.y..max.y && grid.getValue(it).used <= 100 }
            }

            override fun heuristic(node: Vector2, target: Vector2): Int {
                return node.distanceTo(target)
            }
        }

        val path = maze.aStarTo(emptyNode.location, initialTarget) ?: error("Could not find path")

        return ((path.cost + 1) + 5 * (max.x - 1)).toString()
    }

    private fun drawGrid(grid: Map<Vector2, StorageNode>) {
        val (min, max) = grid.bounds()

        println()
        for (y in min.y..max.y) {
            for (x in min.x..max.x) {
                val node = grid[Vector2(x, y)] ?: error("Missing node at $x,$y")
                print(when {
                    x == 0 && y == 0 -> "T "
                    x == max.x && y == 0 -> "D "
                    node.used == 0 -> "_ "
                    node.size > 100 -> "# "
                    else -> ". "
                })
            }
            println()
        }
    }
}
