package dev.lfelzinga.adventofkode.puzzles.aoc16.day7

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class IPPuzzle : Puzzle<Sequence<String>>(2016, 7, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input.count { it.supportsTLS() }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input.count { it.supportsSSL() }.toString()
    }

    private fun String.supportsTLS(): Boolean {
        var inHyperNet = false
        var abbaFound = false

        for (i in 0 until length - 3) {
            when (val ch = this[i]) {
                '[' -> inHyperNet = true
                ']' -> inHyperNet = false
                this[i + 3] -> if (this[i + 1] == this[i + 2] && this[i + 1] != ch) {
                    if (inHyperNet) return false

                    abbaFound = true
                }
            }
        }

        return abbaFound
    }

    private fun String.supportsSSL(): Boolean {
        data class SSLBlock(val char1: Char, val char2: Char)

        var inHyperNet = false
        val superNetBlocks = mutableSetOf<SSLBlock>()
        val hyperNetBlocks = mutableSetOf<SSLBlock>()

        for (i in 0 until length - 2) {
            when (val ch = this[i]) {
                '[' -> inHyperNet = true
                ']' -> inHyperNet = false
                this[i + 2] -> if (this[i + 1] != ch) {
                    if (inHyperNet) {
                        hyperNetBlocks += SSLBlock(this[i + 1], ch)
                    } else {
                        superNetBlocks += SSLBlock(ch, this[i + 1])
                    }
                }
            }
        }

        return (superNetBlocks intersect hyperNetBlocks).isNotEmpty()
    }
}
