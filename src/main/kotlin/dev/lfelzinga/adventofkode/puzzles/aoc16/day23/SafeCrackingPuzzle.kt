package dev.lfelzinga.adventofkode.puzzles.aoc16.day23

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.assembunny.AssembunnyVM

class SafeCrackingPuzzle : Puzzle<Sequence<String>>(2016, 23, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val vm = AssembunnyVM()

        input.forEach(vm::addAssembly)

        vm.setRegister('a', 7)
        vm.run()
        return vm.getRegister('a').toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val vm = AssembunnyVM()

        input.forEach(vm::addAssembly)

        vm.setRegister('a', 12)
        vm.run()
        return vm.getRegister('a').toString()
    }
}
