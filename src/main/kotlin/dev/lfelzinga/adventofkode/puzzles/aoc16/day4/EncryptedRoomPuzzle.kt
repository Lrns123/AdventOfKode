package dev.lfelzinga.adventofkode.puzzles.aoc16.day4

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class EncryptedRoomPuzzle : Puzzle<Sequence<String>>(2016, 4, LinesTransformer()) {
    private val roomRegex = Regex("""([a-z-]+)-(\d+)\[([a-z]+)]""")

    private class EncryptedRoom(val encryptedName: String, val sectorId: Int, val checksum: String) {
        val computedChecksum: String
            get() = encryptedName
                .replace("-", "")
                .groupBy { it }
                .entries
                .map { it.key to it.value.size }
                .sortedWith(compareBy({ -it.second }, { it.first }))
                .take(5)
                .joinToString("") { it.first.toString() }

        val isDecoy: Boolean
            get() = computedChecksum != checksum

        val name: String
            get() = encryptedName
                .map { if (it == '-') ' ' else 'a' + ((it - 'a') + sectorId) % 26 }
                .joinToString("")
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { roomRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Could not parse room '$it'") }
            .map { (name, sectorId, checksum) -> EncryptedRoom(name, sectorId.toInt(), checksum) }
            .filter { !it.isDecoy }
            .sumOf { it.sectorId }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { roomRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Could not parse room '$it'") }
            .map { (name, sectorId, checksum) -> EncryptedRoom(name, sectorId.toInt(), checksum) }
            .filter { !it.isDecoy }
            .first { it.name.contains("northpole") }
            .sectorId
            .toString()
    }
}
