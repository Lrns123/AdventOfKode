package dev.lfelzinga.adventofkode.puzzles.aoc16.day14

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.skip
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import java.security.MessageDigest

class OneTimePadPuzzle : Puzzle<String>(2016, 14, StringTransformer()) {
    companion object {
        private val md5 = MessageDigest.getInstance("MD5")
        private val HEX_ARRAY = "0123456789abcdef".encodeToByteArray()

        private fun getHash(salt: ByteArray, index: Int, iterations: Int): ByteArray {
            val hexArray = ByteArray(32)

            md5.reset()
            md5.update(salt)
            md5.digest(index.toString().encodeToByteArray()).toHexArray(hexArray)

            repeat(iterations) {
                md5.reset()
                md5.digest(hexArray).toHexArray(hexArray)
            }

            return hexArray
        }

        private fun ByteArray.toHexArray(hexArray: ByteArray) {
            for (i in indices) {
                val v = this[i].toInt() and 0xFF
                hexArray[i * 2] = HEX_ARRAY[v ushr 4]
                hexArray[i * 2 + 1] = HEX_ARRAY[v and 0x0F]
            }
        }
    }

    private fun getNStreaks(hex: ByteArray, streakSize: Int) = sequence {
        var last: Byte = 0
        var streak = 1

        for (ch in hex) {
            if (last == ch) {
                if (++streak == streakSize) {
                    yield(ch)
                }
            } else {
                streak = 1
            }

            last = ch
        }
    }

    private fun generateOneTimePad(salt: ByteArray, iterations: Int) = sequence {
        var index = 0
        val candidateKeys = mutableMapOf<Byte, MutableList<Int>>()

        while (true) {
            val hash = getHash(salt, index, iterations)

            getNStreaks(hash, 5).forEach { ch ->
                val positions = candidateKeys.computeIfAbsent(ch) { mutableListOf() }
                yieldAll(positions.filter { it >= index - 1000 })
                positions.clear()
            }

            getNStreaks(hash, 3).firstOrNull()?.let {
                candidateKeys.computeIfAbsent(it) { mutableListOf() } += index
            }

            index++
        }
    }

    override fun solveFirstPart(input: String): String {
        return generateOneTimePad(input.encodeToByteArray(), 0).take(64).sorted().last().toString()
    }

    override fun solveSecondPart(input: String): String {
        return skip("22423") {
            return generateOneTimePad(input.encodeToByteArray(), 2016).take(64).sorted().last().toString()
        }
    }
}
