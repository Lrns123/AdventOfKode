package dev.lfelzinga.adventofkode.puzzles.aoc16.day20

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class FirewallPuzzle : Puzzle<Sequence<String>>(2016, 20, LinesTransformer()) {

    data class FirewallRule(val begin: Long, val end: Long) {
        operator fun contains(value: Long) = value in begin..end
    }

    private fun parseInput(input: Sequence<String>): List<FirewallRule> {
        return input
            .map { it.split('-') }
            .map { (begin, end) -> FirewallRule(begin.toLong(), end.toLong() ) }
            .sortedBy { it.begin }
            .toList()
    }

    private fun findFirstWhitelistedIP(rules: List<FirewallRule>): Long {
        var ip = 0L

        for (rule in rules) {
            if (ip in rule) {
                ip = rule.end + 1
            }
        }
        return ip
    }

    private fun countWhitelistedIPs(rules: List<FirewallRule>, maxIp: Long): Long {
        var ip = 0L
        var whitelisted = 0L

        for (rule in rules) {
            if (ip !in rule && ip < rule.begin) {
                whitelisted += rule.begin - ip
            }
            if (rule.end > ip) {
                ip = rule.end + 1
            }
        }

        return whitelisted + (maxIp - ip + 1)
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return findFirstWhitelistedIP(parseInput(input)).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return countWhitelistedIPs(parseInput(input), 4294967295L).toString()
    }
}
