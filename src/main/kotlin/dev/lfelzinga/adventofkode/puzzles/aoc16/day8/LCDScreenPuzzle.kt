package dev.lfelzinga.adventofkode.puzzles.aoc16.day8

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.matching.MultiRegexMatcher
import dev.lfelzinga.adventofkode.toolbox.ocr.ocr

class LCDScreenPuzzle : Puzzle<Sequence<String>>(2016, 8, LinesTransformer()) {
    companion object {
        val rectRegex = Regex("rect (\\d+)x(\\d+)")
        val rotateRowRegex = Regex("rotate row y=(\\d+) by (\\d+)")
        val rotateColumnRegex = Regex("rotate column x=(\\d+) by (\\d+)")
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return readImage(input).array.count { it == '#' }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return readImage(input).ocr('#')
    }

    private fun readImage(input: Sequence<String>): CharGrid {
        val grid = CharGrid(50, 6)

        MultiRegexMatcher()
            .add(rectRegex) { (width, height) -> grid.rect(width.toInt(), height.toInt()) }
            .add(rotateRowRegex) { (row, amount) -> grid.rotateRow(row.toInt(), amount.toInt()) }
            .add(rotateColumnRegex) { (col, amount) -> grid.rotateCol(col.toInt(), amount.toInt()) }
            .failOnUnknown()
            .match(input)

        return grid
    }

    private fun CharGrid.rotateRow(row: Int, amount: Int) {
        val newRow = CharArray(width) { this[(width + it - amount) % width, row] }

        newRow.forEachIndexed { col, ch -> this[col, row] = ch }
    }

    private fun CharGrid.rotateCol(col: Int, amount: Int) {
        val newCol = CharArray(height) { this[col, (height + it - amount) % height] }

        newCol.forEachIndexed { row, ch -> this[col, row] = ch }
    }

    private fun CharGrid.rect(width: Int, height: Int) {
        for (x in 0 until width) {
            for (y in 0 until height) {
                this[x, y] = '#'
            }
        }
    }
}
