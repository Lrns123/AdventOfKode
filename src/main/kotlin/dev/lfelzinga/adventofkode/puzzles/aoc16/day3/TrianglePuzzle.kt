package dev.lfelzinga.adventofkode.puzzles.aoc16.day3

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class TrianglePuzzle : Puzzle<Sequence<String>>(2016, 3, LinesTransformer()) {
    private val whitespaceRegex = Regex("\\s+")

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { it.trim().split(whitespaceRegex).map(String::toInt) }
            .filter { it.size == 3 }
            .count { it[0] + it[1] > it[2] && it[1] + it[2] > it[0] && it[0] + it[2] > it[1] }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { it.trim().split(whitespaceRegex).map(String::toInt) }
            .filter { it.size == 3 }
            .chunked(3) { chunk ->
                List(3) { row ->
                    List(3) {
                        chunk[it][row]
                    }
                }
            }
            .flatten()
            .count { it[0] + it[1] > it[2] && it[1] + it[2] > it[0] && it[0] + it[2] > it[1] }
            .toString()
    }
}
