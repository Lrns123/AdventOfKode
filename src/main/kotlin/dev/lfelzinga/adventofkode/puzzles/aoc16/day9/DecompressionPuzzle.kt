package dev.lfelzinga.adventofkode.puzzles.aoc16.day9

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class DecompressionPuzzle : Puzzle<String>(2016, 9, StringTransformer()) {
    companion object {
        private val markerRegex = Regex("\\((\\d+)x(\\d+)\\)")
    }

    override fun solveFirstPart(input: String): String {
        return input.decompressedLength(false).toString()
    }

    override fun solveSecondPart(input: String): String {
        return input.decompressedLength(true).toString()
    }

    private fun String.decompressedLength(recursive: Boolean): Long {
        var length = 0L
        var pos = 0

        while (pos < this.length) {
            if (this[pos] == '(') {
                val match = markerRegex.find(this, pos) ?: error("Could not parse marker block")
                require(match.range.first == pos) { "Could not parse marker block" }

                val (numChars, repetitions) = match.destructured

                val repeatedSegment = substring(match.range.last + 1, match.range.last + 1 + numChars.toInt())

                if (recursive) {
                    length += repetitions.toLong() * repeatedSegment.decompressedLength(true)
                } else {
                    length += repetitions.toInt() * repeatedSegment.length
                }

                pos = match.range.last + 1 + numChars.toInt()
            } else {
                ++length
                ++pos
            }
        }

        return length
    }
}
