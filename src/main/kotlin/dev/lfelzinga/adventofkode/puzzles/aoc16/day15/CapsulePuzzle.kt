package dev.lfelzinga.adventofkode.puzzles.aoc16.day15

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class CapsulePuzzle : Puzzle<Sequence<String>>(2016, 15, LinesTransformer()) {
    companion object {
        val lineRegex = Regex("^Disc #\\d+ has (\\d+) positions; at time=0, it is at position (\\d+).$")
    }

    class Disc(val positions: Int, private val startPosition: Int) {
        fun positionAt(time: Int): Int {
            return (startPosition + time) % positions
        }

        fun firstSlotTime(): Int {
            return (-startPosition + positions) % positions
        }
    }

    private fun findSyncPoint(discs: List<Disc>): Int {
        val biggestDisc = discs.maxByOrNull { it.positions } ?: return 0
        var time = biggestDisc.firstSlotTime() - discs.indexOf(biggestDisc)
        val period = biggestDisc.positions

        while (true) {
            if (areDiscsInSyncAt(discs, time)) {
                return time - 1
            }
            time += period
        }
    }

    private fun areDiscsInSyncAt(discs: List<Disc>, time: Int): Boolean {
        for (i in discs.indices) {
            if (discs[i].positionAt(time + i) != 0)
                return false
        }

        return true
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val discs = parseInput(input)

        return findSyncPoint(discs).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val discs = parseInput(input) + Disc(11, 0)

        return findSyncPoint(discs).toString()
    }

    private fun parseInput(input: Sequence<String>): List<Disc> {
        return input
            .map { lineRegex.matchEntire(it)?.destructured ?: error("Cannot parse line $it") }
            .map { (positions, start) -> Disc(positions.toInt(), start.toInt()) }
            .toList()
    }
}
