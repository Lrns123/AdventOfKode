package dev.lfelzinga.adventofkode.puzzles.aoc20.day3

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class TreePuzzle : Puzzle<Sequence<String>>(2020, 3, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return checkCollisions(input.toList(), 3, 1).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val map = input.toList()
        val slopes = listOf(
            1 to 1,
            3 to 1,
            5 to 1,
            7 to 1,
            1 to 2
        )

        return slopes
            .map { checkCollisions(map, it.first, it.second) }
            .fold(1L) { a, b -> a * b }
            .toString()
    }

    private fun checkCollisions(map: List<String>, dx: Int, dy: Int): Int {
        val width = map.first().length

        var posX = 0
        var posY = 0
        var trees = 0

        do {
            if (map[posY][posX % width] == '#') {
                trees++
            }

            posX += dx
            posY += dy
        } while (posY < map.size)

        return trees
    }
}
