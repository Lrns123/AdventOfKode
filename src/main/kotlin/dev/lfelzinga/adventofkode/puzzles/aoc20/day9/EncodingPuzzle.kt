package dev.lfelzinga.adventofkode.puzzles.aoc20.day9

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class EncodingPuzzle : Puzzle<Sequence<String>>(2020, 9, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return findInvalidNumber(input.map { it.toLong() }.toList()).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val numbers = input.map { it.toLong() }.toList()
        val target = findInvalidNumber(numbers) ?: return "???"

        var lBound = 0
        var uBound = 1
        var sum = numbers[0] + numbers[1]

        while (true) {
            when {
                sum == target -> {
                    val subRange = numbers.subList(lBound, uBound + 1)
                    return ((subRange.minOrNull() ?: -1) + (subRange.maxOrNull() ?: -1)).toString()
                }
                sum < target && uBound == numbers.lastIndex ->
                    break
                sum < target -> {
                    uBound++
                    sum += numbers[uBound]
                }
                else -> {
                    sum -= numbers[lBound]
                    lBound++

                    if (lBound == uBound) {
                        if (uBound == numbers.lastIndex) {
                            break
                        } else {
                            uBound++
                            sum += numbers[uBound]
                        }
                    }
                }
            }
        }

        return "???"
    }

    private fun findInvalidNumber(input: List<Long>): Long? {
        val set = mutableSetOf(*input.subList(0, 25).toTypedArray())

        for (i in 25 until input.lastIndex) {
            val number = input[i]

            if (set.none { number - it in set }) {
                return number
            }

            set -= input[i - 25]
            set += number
        }

        return null
    }
}
