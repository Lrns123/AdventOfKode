package dev.lfelzinga.adventofkode.puzzles.aoc20.day24

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2

class HexTilePuzzle : Puzzle<Sequence<String>>(2020, 24, LinesTransformer()) {
    companion object {
        private val EAST = Vector2(1, 0)
        private val NORTHEAST = Vector2(1, -1)
        private val SOUTHEAST = Vector2(0, 1)
        private val WEST = Vector2(-1, 0)
        private val NORTHWEST = Vector2(0, -1)
        private val SOUTHWEST = Vector2(-1, 1)
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return readInput(input).size.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var blackTiles = readInput(input)

        repeat(100) {
            blackTiles = step(blackTiles)
        }

        return blackTiles.size.toString()
    }

    private fun step(blackTiles: Set<Vector2>): Set<Vector2> {
        return blackTiles
            .flatMap { it.hexNeighbours() }
            .distinct()
            .filter { tile ->
                val adjacentBlackTiles = tile.hexNeighbours().count { it in blackTiles }
                if (tile in blackTiles) {
                    adjacentBlackTiles in 1..2
                } else {
                    adjacentBlackTiles == 2
                }
            }
            .toSet()
    }

    private fun readInput(input: Sequence<String>): Set<Vector2> {
        val blackTiles = mutableSetOf<Vector2>()

        input
            .map { it.toDirections().fold(Vector2(0, 0)) { acc, dir -> acc + dir } }
            .forEach {
                if (it in blackTiles) {
                    blackTiles -= it
                } else {
                    blackTiles += it
                }
            }

        return blackTiles
    }

    private fun Vector2.hexNeighbours() = listOf(
        this + WEST,
        this + EAST,
        this + NORTHWEST,
        this + NORTHEAST,
        this + SOUTHWEST,
        this + SOUTHEAST
    )

    private fun String.toDirections() = sequence {
        val string = this@toDirections
        var position = 0
        while (position in indices) {
            when (string[position++]) {
                'w' -> yield(WEST)
                'e' -> yield(EAST)
                'n' -> when (string[position++]) {
                    'w' -> yield(NORTHWEST)
                    'e' -> yield(NORTHEAST)
                    else -> error("Invalid direction")
                }
                's' -> when (string[position++]) {
                    'w' -> yield(SOUTHWEST)
                    'e' -> yield(SOUTHEAST)
                    else -> error("Invalid direction")
                }
                else -> error("Invalid direction")
            }
        }
    }
}
