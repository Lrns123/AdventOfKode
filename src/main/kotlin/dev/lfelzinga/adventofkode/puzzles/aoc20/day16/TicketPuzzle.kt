package dev.lfelzinga.adventofkode.puzzles.aoc20.day16

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class TicketPuzzle : Puzzle<Sequence<String>>(2020, 16, LinesTransformer()) {
    companion object {
        val rangeRegex = Regex("^([\\w\\s]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)$")
    }

    data class TicketInput(
        val fields: Map<String, List<IntRange>>,
        val ticket: List<Int>,
        val otherTickets: List<List<Int>>
    )

    override fun solveFirstPart(input: Sequence<String>): String {
        val ticketInput = parseInput(input)
        val allRanges = ticketInput.fields.flatMap { it.value }

        return ticketInput.otherTickets.flatten().filter { allRanges.none { range -> it in range } }.sum().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val ticketInput = parseInput(input)
        val allRanges = ticketInput.fields.flatMap { it.value }

        val validTickets = ticketInput.otherTickets.filter { it.none { allRanges.none { range -> it in range } } }
        val potentialFieldNames = List(ticketInput.ticket.size) { ticketInput.fields.keys.toMutableSet() }

        // Prune potential fields
        for (ticket in validTickets) {
            for ((index, fieldNames) in potentialFieldNames.withIndex()) {
                if (fieldNames.size == 1) continue
                val iterator = fieldNames.iterator()

                while (iterator.hasNext()) {
                    val fieldName = iterator.next()

                    if (ticketInput.fields[fieldName]!!.none { ticket[index] in it }) {
                        iterator.remove()

                        if (fieldNames.size == 1) {
                            deduplicate(potentialFieldNames, fieldNames.first())
                        }
                    }
                }
            }
        }

        return potentialFieldNames
            .mapIndexed { index, names -> names.first() to index }
            .toMap()
            .filterKeys { it.startsWith("departure") }
            .values
            .fold(1L) { acc, index -> acc * ticketInput.ticket[index] }
            .toString()
    }

    private fun deduplicate(potentialFieldNames: List<MutableSet<String>>, fieldName: String) {
        for (names in potentialFieldNames) {
            if (names.size > 1) {
                names.remove(fieldName)
                if (names.size == 1) {
                    deduplicate(potentialFieldNames, names.first())
                }
            }
        }
    }

    private fun parseInput(input: Sequence<String>): TicketInput {
        val fields = mutableMapOf<String, List<IntRange>>()
        val ticket = mutableListOf<Int>()
        val otherTickets = mutableListOf<List<Int>>()

        val iterator = input.iterator()

        for (line in iterator) {
            if (line.isEmpty()) break
            rangeRegex.matchEntire(line)?.destructured?.let { (name, min1, max1, min2, max2) ->
                fields[name] = listOf(min1.toInt()..max1.toInt(), min2.toInt()..max2.toInt())
            } ?: error("Cannot parse line $line")
        }

        for (line in iterator) {
            if (line.isEmpty()) break
            if (line != "your ticket:") {
                ticket += line.split(',').map { it.toInt() }
            }
        }

        for (line in iterator) {
            if (line != "nearby tickets:") {
                otherTickets += line.split(',').map { it.toInt() }
            }
        }

        return TicketInput(fields, ticket, otherTickets)
    }
}
