package dev.lfelzinga.adventofkode.puzzles.aoc20.day23

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class CupsPuzzle : Puzzle<String>(2020, 23, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        val game = CupsGame(input.map { it - '0' }.toList())

        game.runGame(100)

        return game.cupsAfter(1).take(input.length - 1).joinToString("")
    }

    override fun solveSecondPart(input: String): String {
        val game = CupsGame(
            input.map { it - '0' }.toList() + List(1_000_000 - input.length) { 10 + it }
        )

        game.runGame(10_000_000)

        return game.cupsAfter(1).take(2).fold(1L) { acc, item -> acc * item }.toString()
    }
}

class CupsGame(startingCups: List<Int>) {
    private val cupAfter = IntArray(startingCups.size + 1)
    private var currentCup = startingCups.first()

    init {
        for (i in startingCups.indices) {
            cupAfter[startingCups[i]] = startingCups[(i + 1) % startingCups.size]
        }
    }

    fun runGame(moves: Int) {
        repeat(moves) {
            doMove()
        }
    }

    fun cupsAfter(cup: Int) = generateSequence(cupAfter[cup]) { cupAfter[it] }

    private fun doMove() {
        val picks = doPick()
        insertPicks(picks, findDestination(picks))

        currentCup = cupAfter[currentCup]
    }

    private fun doPick(): List<Int> {
        val firstPick = cupAfter[currentCup]
        val secondPick = cupAfter[firstPick]
        val thirdPick = cupAfter[secondPick]

        cupAfter[currentCup] = cupAfter[thirdPick]

        return listOf(firstPick, secondPick, thirdPick)
    }

    private fun insertPicks(picks: List<Int>, destination: Int) {
        cupAfter[picks.last()] = cupAfter[destination]
        cupAfter[destination] = picks.first()
    }

    private fun findDestination(picks: List<Int>): Int {
        val min = calculateMin(picks)
        val max = calculateMax(picks)

        var candidate = currentCup - 1

        if (candidate < min) candidate = max

        while (candidate in picks) {
            candidate--
            if (candidate < min) {
                candidate = max
            }
        }

        return candidate
    }

    private fun calculateMin(picks: List<Int>): Int {
        var min = 1
        while (min in picks) {
            min++
        }

        return min
    }

    private fun calculateMax(picks: List<Int>): Int {
        var max = cupAfter.size - 1
        while (max in picks) {
            max--
        }

        return max
    }
}
