package dev.lfelzinga.adventofkode.puzzles.aoc20.day10

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.memoized

class AdapterPuzzle : Puzzle<Sequence<String>>(2020, 10, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val sums = (sequenceOf(0) + input.map { it.toInt() })
            .sorted()
            .zipWithNext { a, b -> b - a }
            .groupingBy { it }
            .eachCount()

        return (sums.getValue(1) * (1 + sums.getValue(3))).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val joltageSet = input.map { it.toInt() }.toSet()
        val target = joltageSet.maxOrNull() ?: error("No joltages in set")

        return countAdapterChains(0, target, joltageSet).toString()
    }

    private fun countAdapterChains(
        start: Int,
        target: Int,
        joltages: Set<Int>,
        cache: MutableMap<Int, Long> = mutableMapOf()
    ): Long = memoized(start, cache) {
        var paths = if (target - start in 1..3) 1L else 0L

        if (start + 1 in joltages) paths += countAdapterChains(start + 1, target, joltages, cache)
        if (start + 2 in joltages) paths += countAdapterChains(start + 2, target, joltages, cache)
        if (start + 3 in joltages) paths += countAdapterChains(start + 3, target, joltages, cache)

        paths
    }
}
