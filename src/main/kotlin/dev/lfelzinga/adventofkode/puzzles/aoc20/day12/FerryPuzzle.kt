package dev.lfelzinga.adventofkode.puzzles.aoc20.day12

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.rotateLeft
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.rotateRight

class FerryPuzzle : Puzzle<Sequence<String>>(2020, 12, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        var direction = Direction.EAST
        var position = Vector2(0, 0)

        for ((action, magnitude) in input.toInstructions()) {
            when (action) {
                'N' -> position += Direction.NORTH.delta * magnitude
                'S' -> position += Direction.SOUTH.delta * magnitude
                'E' -> position += Direction.EAST.delta * magnitude
                'W' -> position += Direction.WEST.delta * magnitude
                'F' -> position += direction.delta * magnitude
                'L' -> repeat(magnitude / 90) { direction = direction.left() }
                'R' -> repeat(magnitude / 90) { direction = direction.right() }
            }
        }

        return position.magnitude.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var waypoint = Vector2(10, -1)
        var position = Vector2(0, 0)

        for ((action, magnitude) in input.toInstructions()) {
            when (action) {
                'N' -> waypoint += Direction.NORTH.delta * magnitude
                'S' -> waypoint += Direction.SOUTH.delta * magnitude
                'E' -> waypoint += Direction.EAST.delta * magnitude
                'W' -> waypoint += Direction.WEST.delta * magnitude
                'F' -> position += waypoint * magnitude
                'L' -> repeat(magnitude / 90) { waypoint = waypoint.rotateLeft() }
                'R' -> repeat(magnitude / 90) { waypoint = waypoint.rotateRight() }
            }
        }

        return position.magnitude.toString()
    }

    private fun Sequence<String>.toInstructions() = map { it[0] to it.substring(1).toInt() }
}
