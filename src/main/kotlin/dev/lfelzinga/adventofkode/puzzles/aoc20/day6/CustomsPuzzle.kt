package dev.lfelzinga.adventofkode.puzzles.aoc20.day6

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class CustomsPuzzle : Puzzle<Sequence<String>>(2020, 6, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return transformInput(input, emptySet(), Iterable<Char>::union)
            .sumOf { it.size }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return transformInput(input, ('a'..'z').toSet(), Iterable<Char>::intersect)
            .sumOf { it.size }
            .toString()
    }

    private fun transformInput(
        input: Sequence<String>,
        initialSet: Set<Char>,
        combinator: (Iterable<Char>, Iterable<Char>) -> Set<Char>
    ): Sequence<Set<Char>> = sequence {
        var answersInGroup = initialSet

        for (line in input) {
            answersInGroup = if (line.isEmpty()) {
                yield(answersInGroup)
                initialSet
            } else {
                combinator(answersInGroup, line.toSet())
            }
        }

        if (answersInGroup.isNotEmpty()) {
            yield(answersInGroup)
        }
    }

}
