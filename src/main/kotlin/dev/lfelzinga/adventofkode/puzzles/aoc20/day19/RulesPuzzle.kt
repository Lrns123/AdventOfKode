package dev.lfelzinga.adventofkode.puzzles.aoc20.day19

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class RulesPuzzle : Puzzle<Sequence<String>>(2020, 19, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val (ruleLines, messages) = splitInput(input)
        val rules = ruleLines.associate { parseRule(it) }

        return messages.count { parse(it, rules, listOf(0)) }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val (ruleLines, messages) = splitInput(input)
        val rules = ruleLines.associate { parseRule(it) }.toMutableMap()

        rules[8] = Rule.NestedRule(listOf(listOf(42), listOf(42, 8)))
        rules[11] = Rule.NestedRule(listOf(listOf(42, 31), listOf(42, 11, 31)))

        return messages.count { parse(it, rules, listOf(0)) }.toString()
    }

    private fun parse(message: String, rules: Map<Int, Rule>, ruleStack: List<Int>): Boolean {
        if (ruleStack.isEmpty()) {
            return message.isEmpty()
        }

        return when (val rule = rules[ruleStack.first()]) {
            is Rule.CharacterRule -> message.startsWith(rule.char) && parse(message.drop(1), rules, ruleStack.drop(1))
            is Rule.NestedRule -> rule.ruleSets.any { parse(message, rules, it + ruleStack.drop(1)) }
            null -> error("Rule ${ruleStack.first()} not found")
        }
    }

    private fun splitInput(input: Sequence<String>): Pair<List<String>, List<String>> {
        var readingRules = true
        val ruleLines = mutableListOf<String>()
        val messagesLines = mutableListOf<String>()

        for (line in input) {
            if (line.isBlank()) {
                readingRules = false
            } else {
                if (readingRules) ruleLines += line else messagesLines += line
            }
        }

        return ruleLines to messagesLines
    }

    private fun parseRule(line: String): Pair<Int, Rule> {
        val (ruleId, ruleText) = line.split(": ")

        return ruleId.toInt() to if (ruleText.startsWith('"')) {
            Rule.CharacterRule(ruleText[1])
        } else {
            Rule.NestedRule(ruleText.split(" | ").map { it.split(" ").map { num -> num.toInt() } })
        }
    }

    sealed class Rule {
        class CharacterRule(val char: Char): Rule()
        class NestedRule(val ruleSets: List<List<Int>>) : Rule()
    }
}
