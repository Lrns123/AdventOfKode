package dev.lfelzinga.adventofkode.puzzles.aoc20.day8

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class ConsolePuzzle : Puzzle<Sequence<String>>(2020, 8, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val vm = ConsoleVM(ConsoleVM.assemble(input.toList()))

        vm.checkHalt()

        return vm.accumulator.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val program = ConsoleVM.assemble(input.toList())

        for ((index, instruction) in program.withIndex()) {
            if (instruction is Instruction.Acc) continue

            val vm = ConsoleVM(program.patchOpcode(index))
            if (vm.checkHalt()) {
                return vm.accumulator.toString()
            }
        }

        return "???"
    }

    private fun List<Instruction>.patchOpcode(index: Int): List<Instruction> =
        toMutableList().apply { this[index] = this[index].flip() }

    private fun Instruction.flip() = when (this) {
        is Instruction.Nop -> Instruction.Jmp(operand)
        is Instruction.Jmp -> Instruction.Nop(operand)
        is Instruction.Acc -> this
    }

    private fun ConsoleVM.checkHalt(): Boolean {
        val ipSet = mutableSetOf<Int>()

        while (instructionPointer !in ipSet) {
            ipSet += instructionPointer
            if (!step()) {
                return true
            }
        }

        return false
    }
}

sealed class Instruction(val operand: Int) {
    class Nop(operand: Int) : Instruction(operand)
    class Acc(operand: Int) : Instruction(operand)
    class Jmp(operand: Int) : Instruction(operand)
}

class ConsoleVM(val program: List<Instruction>) {

    companion object {
        fun assemble(assembly: List<String>): List<Instruction> {
            return assembly.map {
                val (opcode, operand) = it.split(' ')

                when (opcode) {
                    "nop" -> Instruction.Nop(operand.toInt())
                    "acc" -> Instruction.Acc(operand.toInt())
                    "jmp" -> Instruction.Jmp(operand.toInt())
                    else -> error("Unknown opcode $opcode")
                }
            }
        }
    }

    var instructionPointer: Int = 0
    var accumulator: Int = 0

    fun step(): Boolean {
        if (instructionPointer !in program.indices) {
            return false
        }

        when (val instruction = program[instructionPointer++]) {
            is Instruction.Nop -> {}
            is Instruction.Acc -> accumulator += instruction.operand
            is Instruction.Jmp -> instructionPointer += instruction.operand - 1
        }
        return true
    }
}
