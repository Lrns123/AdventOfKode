package dev.lfelzinga.adventofkode.puzzles.aoc20.day18

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class ParserPuzzle : Puzzle<Sequence<String>>(2020, 18, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input.map { MathSolver(it).solve() }.sum().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input.map { MathSolver(it, 2, 1).solve() }.sum().toString()
    }
}

sealed class MathToken {
    class Number(val number: Long) : MathToken()
    object Add : MathToken()
    object Multiply : MathToken()
    object ParenthesisOpen : MathToken()
    object ParenthesisClose : MathToken()
    object EOF : MathToken()
}

class MathTokenizer(private val expression: String) {
    private var position = 0

    private var token: MathToken = readToken()

    fun next(): MathToken {
        return token.also { token = readToken() }
    }

    fun peek() = token

    private fun readToken(): MathToken {
        while (true) {
            if (position >= expression.length) {
                return MathToken.EOF
            }

            return when (val ch = expression[position++]) {
                ' ' -> continue
                in '0'..'9' -> {
                    val startPosition = position - 1

                    while (position < expression.length && expression[position] in '0'..'9') {
                        position++
                    }

                    MathToken.Number(expression.substring(startPosition, position).toLong())
                }
                '+' -> MathToken.Add
                '*' -> MathToken.Multiply
                '(' -> MathToken.ParenthesisOpen
                ')' -> MathToken.ParenthesisClose
                else -> error("Unexpected symbol '$ch'")
            }
        }
    }

    fun assertNext(expected: MathToken, block: () -> String) {
        if (next() != expected) {
            error("Syntax error: ${block()} in '$expression' at position $position")
        }
    }
}

class MathSolver(expression: String, private val addPrecedence: Int = 1, private val mulPrecedence: Int = 1) {
    private val tokenizer = MathTokenizer(expression)

    fun solve(): Long {
        return parseExpression(parsePrimitive()).also {
            tokenizer.assertNext(MathToken.EOF) { "Premature end of expression" }
        }
    }

    private fun parsePrimitive(): Long {
        return when (val token = tokenizer.next()) {
            is MathToken.Number -> token.number
            MathToken.ParenthesisOpen -> {
                parseExpression(parsePrimitive()).also {
                    tokenizer.assertNext(MathToken.ParenthesisClose) { "Unmatched parentheses" }
                }
            }
            else -> error("Unexpected token for primitive: $token")
        }
    }

    private fun parseExpression(lhs: Long, minPrecedence: Int = 0): Long {
        var result = lhs

        while (tokenizer.peek().isOperator() && tokenizer.peek().precedence >= minPrecedence) {
            val operator = tokenizer.next()
            var rhs = parsePrimitive()

            while (tokenizer.peek().isOperator() && tokenizer.peek().precedence > operator.precedence) {
                rhs = parseExpression(rhs, tokenizer.peek().precedence)
            }

            result = operator.applyOperation(result, rhs)
        }

        return result
    }

    private fun MathToken.isOperator() = when (this) {
        MathToken.Add, MathToken.Multiply -> true
        else -> false
    }

    private fun MathToken.applyOperation(lhs: Long, rhs: Long): Long = when (this) {
        MathToken.Add -> lhs + rhs
        MathToken.Multiply -> lhs * rhs
        else -> error("Cannot apply operation of non-operator token $this")
    }

    private val MathToken.precedence: Int
        get() = when (this) {
            MathToken.Add -> addPrecedence
            MathToken.Multiply -> mulPrecedence
            else -> error("Cannot determine precedence of non-operator token $this")
        }
}
