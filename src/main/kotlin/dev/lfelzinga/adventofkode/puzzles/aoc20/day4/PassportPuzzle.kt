package dev.lfelzinga.adventofkode.puzzles.aoc20.day4

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class PassportPuzzle : Puzzle<Sequence<String>>(2020, 4, LinesTransformer()) {
    companion object {
        private val requiredFields = listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")

        private val yearRegex = Regex("^\\d{4}$")
        private val heightRegex = Regex("^(\\d+)(cm|in)$")
        private val colorRegex = Regex("^#[0-9a-f]{6}$")
        private val validEyeColors = listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
        private val passportRegex = Regex("^\\d{9}$")

        private val validators = mapOf<String, (String) -> Boolean>(
            "byr" to { value -> yearRegex.matches(value) && value.toInt() in 1920..2002 },
            "iyr" to { value -> yearRegex.matches(value) && value.toInt() in 2010..2020 },
            "eyr" to { value -> yearRegex.matches(value) && value.toInt() in 2020..2030 },
            "hgt" to { value -> heightRegex.matchEntire(value)?.destructured?.let { (length, unit) ->
                when (unit) {
                    "cm" -> length.toInt() in 150..193
                    "in" -> length.toInt() in 59..76
                    else -> false
                }
            } ?: false },
            "hcl" to { value -> colorRegex.matches(value) },
            "ecl" to { value -> value in validEyeColors },
            "pid" to { value -> passportRegex.matches(value) }
        )
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val fields = mutableMapOf<String, String>()
        var validPassports = 0

        for (line in input) {
            if (line.isBlank()) {
                if (requiredFields.all { it in fields.keys }) {
                    validPassports++
                }
                fields.clear()
            } else {
                for (entry in line.split(" ")) {
                    val (key, value) = entry.split(":")
                    fields[key] = value
                }
            }
        }

        if (fields.isNotEmpty() && requiredFields.all { it in fields.keys }) {
            validPassports++
        }

        return validPassports.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val fields = mutableMapOf<String, String>()
        var validPassports = 0

        for (line in input) {
            if (line.isBlank()) {
                if (validatePasssport(fields)) {
                    validPassports++
                }
                fields.clear()
            } else {
                for (entry in line.split(" ")) {
                    val (key, value) = entry.split(":")
                    fields[key] = value
                }
            }
        }

        if (fields.isNotEmpty() && validatePasssport(fields)) {
            validPassports++
        }

        return validPassports.toString()
    }

    private fun validatePasssport(fields: Map<String, String>): Boolean {
        for (requiredField in requiredFields) {
            val value = fields[requiredField] ?: return false
            if (!validators.getValue(requiredField)(value)) {
                return false
            }
        }

        return true
    }
}
