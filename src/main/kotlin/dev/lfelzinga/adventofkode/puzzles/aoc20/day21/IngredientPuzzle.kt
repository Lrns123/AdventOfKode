package dev.lfelzinga.adventofkode.puzzles.aoc20.day21

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class IngredientPuzzle : Puzzle<Sequence<String>>(2020, 21, LinesTransformer()) {
    companion object {
        private val lineRegex = Regex("^([a-z\\s]+) \\(contains ([a-z,\\s]+)\\)$")
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val data = parseInput(input)
        val allergens = findAllergens(data)
        val nonAllergens = data.ingredients - allergens.values.toSet()

        return data.food.flatMap { it.ingredients }.count { it in nonAllergens }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val data = parseInput(input)
        val allergens = findAllergens(data)

        return data.allergens.sorted().joinToString(",") { allergens.getValue(it) }
    }


    private fun parseInput(input: Sequence<String>): FoodData {
        val ingredients = mutableSetOf<String>()
        val allergens = mutableSetOf<String>()
        val food = input
            .map { parseFood(it) }
            .onEach {
                ingredients += it.ingredients
                allergens += it.allergens
            }
            .toList()

        return FoodData(food, ingredients, allergens)
    }

    private fun findAllergens(data: FoodData): Map<String, String> {
        val possibleIngredientsForAllergen = data.allergens.associateWith { data.ingredients.toMutableSet() }

        fun finalize(ingredient: String) {
            possibleIngredientsForAllergen.values.forEach {
                if (it.size > 1) {
                    it.remove(ingredient)

                    if (it.size == 1) {
                        finalize(it.first())
                    }
                }
            }
        }

        for (entry in data.food) {
            for (allergen in entry.allergens) {
                val possibleIngredients = possibleIngredientsForAllergen.getValue(allergen)

                possibleIngredients.retainAll(entry.ingredients)

                if (possibleIngredients.size == 1) {
                    finalize(possibleIngredients.first())
                }
            }
        }

        require(possibleIngredientsForAllergen.all { it.value.size == 1 })

        return possibleIngredientsForAllergen.mapValues { it.value.first() }
    }

    private fun parseFood(line: String): Food {
        val (ingredients, allergens) = lineRegex.matchEntire(line)?.destructured ?: error("Could not parse $line")

        return Food(ingredients.split(' ').toSet(), allergens.split(", ").toSet())
    }

    class Food(val ingredients: Set<String>, val allergens: Set<String>)
    class FoodData(val food: List<Food>, val ingredients: Set<String>, val allergens: Set<String>)
}
