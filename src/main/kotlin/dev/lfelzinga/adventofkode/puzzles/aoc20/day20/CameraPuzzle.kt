package dev.lfelzinga.adventofkode.puzzles.aoc20.day20

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.*
import kotlin.math.min
import kotlin.math.sqrt

class CameraPuzzle : Puzzle<Sequence<String>>(2020, 20, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val tiles = input.tiles().toList()
        val borderSignatures = tiles.flatMap { it.edgeSignatures }.groupingBy { it }.eachCount().filterValues { it == 1 }.keys

        val cornerTiles = tiles.filter { (it.edgeSignatures - borderSignatures).size == 2 }

        return cornerTiles.fold(1L) { acc, tile -> acc * tile.id }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val tiles = input.tiles().toList()
        val borderSignatures = tiles.flatMap { it.edgeSignatures }.groupingBy { it }.eachCount().filterValues { it == 1 }.keys
        val startingCorner = alignTopLeftCorner(tiles.first { (it.edgeSignatures - borderSignatures).size == 2 }, borderSignatures)

        val image = renderImage(alignTiles(startingCorner, tiles))
        val pattern = listOf(
            "                  # ",
            "#    ##    ##    ###",
            " #  #  #  #  #  #   "
        ).toCharGrid()

        val orientations = sequence {
            yield(image)
            yield(image.rotateClockwise())
            yield(image.rotateCounterClockwise())
            yield(image.flipHorizontal())
            yield(image.flipVertical())
            yield(image.flipBoth())
        }

        for (rotatedImage in orientations) {
            val monstersFound = rotatedImage.findAll(pattern, ' ').count()
            if (monstersFound != 0) {
                val roughness = rotatedImage.array.count { it == '#' } - (pattern.array.count { it == '#' } * monstersFound)
                return roughness.toString()
            }
        }

        return "???"
    }

    private fun alignTopLeftCorner(tile: Tile, borderSignatures: Set<Int>): Tile {
        var result = tile

        while (result.topEdgeSignature() !in borderSignatures || result.leftEdgeSignature() !in borderSignatures) {
            result = result.rotateClockwise()
        }

        return result
    }

    private fun alignTiles(cornerTile: Tile, tiles: List<Tile>): Grid<Tile> {
        val imageSize = sqrt(tiles.size.toFloat()).toInt()
        val grid = Grid(imageSize, imageSize) { Tile(0, CharGrid(0, 0) ) }

        val tileLookup = tiles.associateBy { it.id }
        val signatureLookup = tiles
            .flatMap { it.edgeSignatures }
            .toSet()
            .associateWith { signature -> tiles.filter { signature in it.edgeSignatures }.map { it.id } }

        fun connectedTile(tileId: Int, fuzzyEdgeSignature: Int): Tile =
            tileLookup.getValue((signatureLookup.getValue(fuzzyEdgeSignature) - tileId).single())

        fun alignVertical(tile: Tile, topAdjacentTile: Tile): Tile {
            val rotatedTile = when (topAdjacentTile.bottomEdgeSignature()) {
                tile.topEdgeSignature() -> tile
                tile.bottomEdgeSignature() -> tile.flipVertical()
                tile.rightEdgeSignature() -> tile.rotateCounterClockwise()
                tile.leftEdgeSignature() -> tile.rotateClockwise()
                else -> error("Cannot vertically align tile ${tile.id} to ${topAdjacentTile.id}")
            }

            return if (topAdjacentTile.bottomEdgeSignature(false) != rotatedTile.topEdgeSignature(false)) {
                rotatedTile.flipHorizontal()
            } else {
                rotatedTile
            }
        }

        fun alignHorizontal(tile: Tile, rightAdjacentTile: Tile): Tile {
            val rotatedTile = when (rightAdjacentTile.rightEdgeSignature()) {
                tile.topEdgeSignature() -> tile.rotateCounterClockwise()
                tile.bottomEdgeSignature() -> tile.rotateClockwise()
                tile.rightEdgeSignature() -> tile.flipHorizontal()
                tile.leftEdgeSignature() -> tile
                else -> error("Cannot horizontally align tile ${tile.id} to ${rightAdjacentTile.id}")
            }

            return if (rightAdjacentTile.rightEdgeSignature(false) != rotatedTile.leftEdgeSignature(false)) {
                rotatedTile.flipVertical()
            } else {
                rotatedTile
            }
        }

        grid[0, 0] = cornerTile

        for (x in 0 until imageSize) {
            for (y in 0 until imageSize) {
                if (x == 0 && y == 0) continue

                grid[x, y] = if (x == 0) {
                    val adjacentTile = grid[x, y - 1]
                    val tile = connectedTile(adjacentTile.id, adjacentTile.bottomEdgeSignature())

                    alignVertical(tile, adjacentTile)
                } else {
                    val adjacentTile = grid[x - 1, y]
                    val tile = connectedTile(adjacentTile.id, adjacentTile.rightEdgeSignature())

                    alignHorizontal(tile, adjacentTile)
                }
            }
        }

        return grid
    }

    private fun renderImage(tileGrid: Grid<Tile>): CharGrid {
        val tileWidth = tileGrid[0, 0].image.width
        val tileHeight = tileGrid[0, 0].image.width
        val width = (tileWidth - 2) * tileGrid.width
        val height = (tileHeight - 2) * tileGrid.width
        val result = CharGrid(width, height)

        for (tx in 0 until tileGrid.width) {
            for (ty in 0 until tileGrid.height) {
                val tile = tileGrid[tx, ty]
                for (x in 1 until tileWidth - 1) {
                    for (y in 1 until tileHeight - 1) {
                        result[tx * (tileWidth - 2) + x - 1, ty * (tileHeight - 2) + y - 1] = tile.image[x, y]
                    }
                }
            }
        }

        return result
    }

    private fun Sequence<String>.tiles() = sequence {
        var id = -1
        val imageLines = mutableListOf<String>()

        for (line in this@tiles) {
            when {
                line.isBlank() -> {
                    yield(Tile(id, imageLines.toCharGrid()))
                    imageLines.clear()
                }
                line.startsWith("Tile") -> id = line.substring(5..8).toInt()
                else -> imageLines += line
            }
        }

        if (imageLines.isNotEmpty()) {
            yield(Tile(id, imageLines.toCharGrid()))
        }
    }

    data class Tile(val id: Int, val image: CharGrid)

    private fun Tile.flipHorizontal() = Tile(id, image.flipHorizontal())
    private fun Tile.flipVertical() = Tile(id, image.flipVertical())
    private fun Tile.rotateClockwise() = Tile(id, image.rotateClockwise())
    private fun Tile.rotateCounterClockwise() = Tile(id, image.rotateCounterClockwise())

    private inline fun Tile.computeEdgeSignature(fuzzy: Boolean = true, isSet: (Int) -> Boolean): Int {
        var signature = 0
        var inverseSignature = 0
        for (i in 0 until image.width) {
            if (isSet(i)) {
                signature = signature or (1 shl i)
                inverseSignature = inverseSignature or (1 shl (image.width - 1 - i))
            }
        }

        return if (fuzzy) min(signature, inverseSignature) else signature
    }

    private fun Tile.topEdgeSignature(fuzzy: Boolean = true): Int = computeEdgeSignature(fuzzy) { image[it, 0] == '#' }
    private fun Tile.bottomEdgeSignature(fuzzy: Boolean = true): Int = computeEdgeSignature(fuzzy) { image[it, image.height - 1] == '#' }
    private fun Tile.leftEdgeSignature(fuzzy: Boolean = true): Int = computeEdgeSignature(fuzzy) { image[0, it] == '#' }
    private fun Tile.rightEdgeSignature(fuzzy: Boolean = true): Int = computeEdgeSignature(fuzzy) { image[image.width - 1, it] == '#' }

    private val Tile.edgeSignatures: List<Int> get() = listOf(
        topEdgeSignature(),
        bottomEdgeSignature(),
        leftEdgeSignature(),
        rightEdgeSignature()
    )

}
