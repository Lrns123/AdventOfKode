package dev.lfelzinga.adventofkode.puzzles.aoc20.day14

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class MemoryPuzzle : Puzzle<Sequence<String>>(2020, 14, LinesTransformer()) {
    companion object {
        private val memRegex = Regex("mem\\[(\\d+)] = (\\d+)")
        private const val limitMask = (-1L shl 36).inv()
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        var andMask = -1L
        var orMask = 0L

        val memory = mutableMapOf<Int, Long>()

        for (line in input) {
            if (line.startsWith("mask")) {
                val mask = line.split("=")[1].trim()
                andMask = mask.replace('X', '1').toLong(2)
                orMask = mask.replace('X', '0').toLong(2)
            } else {
                memRegex.matchEntire(line)?.destructured?.let { (addr, value) ->
                    memory[addr.toInt()] = value.toLong() and andMask or orMask
                }
            }
        }

        return memory.values.sum().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var mask = ""
        var orMask = 0L
        var andMask = -1L

        val memory = mutableMapOf<Long, Long>()

        for (line in input) {
            if (line.startsWith("mask")) {
                mask = line.split("=")[1].trim()
                orMask = mask.replace('X', '0').toLong(2)
                andMask = mask.replace('0', '1').replace('X', '0').toLong(2)
            } else {
                memRegex.matchEntire(line)?.destructured?.let { (addr, valueText) ->
                    val baseAddr = addr.toLong() and andMask or orMask
                    val value = valueText.toLong()

                    for (bitMask in mask.floatingBitMasks()) {
                        memory[baseAddr or bitMask] = value
                    }
                }
            }
        }

        return memory.values.sum().toString()
    }

    private fun String.floatingBitMasks() = sequence {
        val baseMask = foldIndexed(0L) { index, acc, ch ->
            if (ch == 'X') acc or (1L shl (length - index - 1)) else acc
        }.inv() and limitMask

        var bitMask = baseMask

        while (true) {
            yield(bitMask.inv() and limitMask)

            if (bitMask == limitMask) {
                break
            }

            bitMask++
            bitMask = bitMask or baseMask
        }
    }
}
