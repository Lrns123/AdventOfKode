package dev.lfelzinga.adventofkode.puzzles.aoc20.day7

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.memoized

class BaggagePuzzle : Puzzle<Sequence<String>>(2020, 7, LinesTransformer()) {
    companion object {
        private val lineRegex = Regex("^(\\w+ \\w+) bags contain (.*)\\.$")
        private val bagRegex = Regex("(\\d+) (\\w+ \\w+) bags?")
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val tree = parseInput(input)
        val cache = mutableMapOf<String, Boolean>()

        return tree.keys
            .filter { it != "shiny gold" }
            .count { containsBag(it, "shiny gold", tree, cache) }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val tree = parseInput(input)
        val cache = mutableMapOf<String, Int>()

        return nestedBags("shiny gold", tree, cache).toString()
    }

    private fun containsBag(
        start: String,
        target: String,
        tree: Map<String, List<Pair<String, Int>>>,
        cache: MutableMap<String, Boolean>
    ): Boolean = memoized(start, cache) {
        val containedBags = tree.getValue(start)

        if (target in containedBags.map { it.first }) {
            true
        } else {
            containedBags.any {
                containsBag(it.first, target, tree, cache)
            }
        }
    }

    private fun nestedBags(
        start: String,
        tree: Map<String, List<Pair<String, Int>>>,
        cache: MutableMap<String, Int>
    ): Int = memoized(start, cache) {
        tree.getValue(start)
            .sumOf { it.second * (1 + nestedBags(it.first, tree, cache)) }
    }

    private fun parseInput(input: Sequence<String>): Map<String, List<Pair<String, Int>>> {
        return input
            .map { lineRegex.matchEntire(it)?.destructured ?: error("Could not parse line $it") }
            .map { (color, contentsText) ->
                val contents = when (contentsText) {
                    "no other bags" -> emptyList()
                    else -> contentsText
                        .split(',')
                        .map { bagRegex.matchEntire(it.trim())?.destructured ?: error("Could not parse contents $it") }
                        .map { (amount, color) -> color to amount.toInt() }
                        .toList()
                }

                color to contents
            }
            .toMap()
    }
}
