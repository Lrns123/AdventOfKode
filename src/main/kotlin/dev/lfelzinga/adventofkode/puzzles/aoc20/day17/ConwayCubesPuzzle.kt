package dev.lfelzinga.adventofkode.puzzles.aoc20.day17

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.Vector3
import dev.lfelzinga.adventofkode.toolbox.vector.Vector4

class ConwayCubesPuzzle : Puzzle<Sequence<String>>(2020, 17, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        var points = readInput(input).map { Vector3(it.x, it.y, 0) }.toSet()

        repeat(6) {
            points = step(points) { it.adjacentNeighbours() }
        }

        return points.size.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var points = readInput(input).map { Vector4(it.x, it.y, 0, 0) }.toSet()

        repeat(6) {
            points = step(points) { it.adjacentNeighbours() }
        }

        return points.size.toString()
    }

    private fun <T> step(points: Set<T>, neighbourFn: (T) -> List<T>): Set<T> {
        return points
            .asSequence()
            .flatMap { neighbourFn(it) + it }
            .distinct()
            .filter { candidate ->
                val adjacentCount = neighbourFn(candidate).count { it in points }
                if (candidate in points) {
                    adjacentCount in 2..3
                } else {
                    adjacentCount == 3
                }
            }
            .toSet()
    }

    private fun Vector3.adjacentNeighbours(): List<Vector3> {
        val neighbours = ArrayList<Vector3>(26)
        for (dx in -1..1) {
            for (dy in -1..1) {
                for (dz in -1..1) {
                    if (dx == 0 && dy == 0 && dz == 0) continue
                    neighbours += Vector3(this.x + dx, this.y + dy, this.z + dz)
                }
            }
        }

        return neighbours
    }

    private fun Vector4.adjacentNeighbours(): List<Vector4> {
        val neighbours = ArrayList<Vector4>(80)
        for (dx in -1..1) {
            for (dy in -1..1) {
                for (dz in -1..1) {
                    for (dw in -1..1) {
                        if (dx == 0 && dy == 0 && dz == 0 && dw == 0) continue
                        neighbours += Vector4(this.x + dx, this.y + dy, this.z + dz, this.w + dw)
                    }
                }
            }
        }

        return neighbours
    }

    private fun readInput(input: Sequence<String>): Set<Vector2> {
        val points = mutableSetOf<Vector2>()
        for ((y, row) in input.withIndex()) {
            for ((x, ch) in row.withIndex()) {
                if (ch == '#') points += Vector2(x, y)
            }
        }

        return points
    }
}
