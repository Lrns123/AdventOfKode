package dev.lfelzinga.adventofkode.puzzles.aoc20.day25

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class KeyCardPuzzle : Puzzle<Sequence<String>>(2020, 25, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val pubKeys = input.map { it.toLong() }.toList()
        val loopSize = findLoopSize(pubKeys[1], 7L)

        return transform(pubKeys[0], loopSize).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return "Merry Christmas!"
    }

    private fun transform(subject: Long, loopSize: Long): Long {
        return subject.toBigInteger().modPow(loopSize.toBigInteger(), 20201227.toBigInteger()).longValueExact()
    }

    private fun findLoopSize(pubKey: Long, subject: Long): Long {
        var value = 1L
        var loops = 0L
        while (value != pubKey) {
            value *= subject
            value %= 20201227
            loops++
        }

        return loops
    }

}
