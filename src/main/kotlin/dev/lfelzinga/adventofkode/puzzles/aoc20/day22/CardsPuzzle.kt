package dev.lfelzinga.adventofkode.puzzles.aoc20.day22

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.memoized
import java.util.*

class CardsPuzzle : Puzzle<String>(2020, 22, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        val (deck1, deck2) = readDecks(input)

        return playGame(deck1, deck2).toString()
    }

    override fun solveSecondPart(input: String): String {
        val (deck1, deck2) = readDecks(input)

        return when (playRecursiveGame(deck1, deck2)) {
            1 -> deck1.score.toString()
            2 -> deck2.score.toString()
            else -> error("Invalid winner")
        }
    }

    private fun playGame(player1: Queue<Int>, player2: Queue<Int>): Int {

        while (player1.isNotEmpty() && player2.isNotEmpty()) {
            val card1 = player1.poll()
            val card2 = player2.poll()

            if (card1 > card2) {
                player1.offer(card1)
                player1.offer(card2)
            } else {
                player2.offer(card2)
                player2.offer(card1)
            }
        }

        val winner = if (player1.isEmpty()) player2 else player1
        return winner.score
    }

    private fun playRecursiveGame(
        player1: Queue<Int>,
        player2: Queue<Int>,
        cache: MutableMap<Long, Int> = mutableMapOf()
    ): Int = memoized(player1.hashCode().toLong() * player2.hashCode(), cache) {
        val encounteredStates = mutableSetOf<Long>()

        while (player1.isNotEmpty() && player2.isNotEmpty()) {
            if (!encounteredStates.add(player1.hashCode().toLong() * player2.hashCode())) {
                return@memoized 1
            }

            val card1 = player1.poll()
            val card2 = player2.poll()

            val winner = if (card1 <= player1.size && card2 <= player2.size) {
                playRecursiveGame(player1.subQueue(card1), player2.subQueue(card2), cache)
            } else {
                if (card1 > card2) 1 else 2
            }

            when (winner) {
                1 -> {
                    player1.offer(card1)
                    player1.offer(card2)
                }
                2 -> {
                    player2.offer(card2)
                    player2.offer(card1)
                }
            }
        }

        if (player1.isEmpty()) 2 else 1
    }

    private fun Queue<Int>.subQueue(n: Int): Queue<Int> =
        LinkedList<Int>().also { forEachIndexed { index, item -> if (index < n) it.offer(item) } }

    private val Queue<Int>.score
        get() = foldIndexed(0) { index, acc, card -> acc + card * (size - index) }

    private fun readDecks(input: String): Pair<Queue<Int>, Queue<Int>> {
        val (deck1, deck2) = input
            .split("\n\n")
            .map { deck -> deck.split("\n").drop(1).map { it.toInt() } }

        return LinkedList(deck1) to LinkedList(deck2)
    }
}
