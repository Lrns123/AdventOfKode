package dev.lfelzinga.adventofkode.puzzles.aoc20.day13

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.ModularLong
import dev.lfelzinga.adventofkode.toolbox.chineseRemainder

class BusPuzzle : Puzzle<Sequence<String>>(2020, 13, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val inputLines = input.toList()
        val time = inputLines[0].toInt()
        val busses = inputLines[1].split(',').filter { it != "x" }.map { it.toInt() }

        val nextBus = busses.minByOrNull { time - (time % it) + it } ?: error("No busses found")

        val nextBusTime = time - (time % nextBus) + nextBus

        return (nextBus * (nextBusTime - time)).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val inputLines = input.toList()
        val busses = inputLines[1].split(',').mapIndexedNotNull { index, id ->
            if (id == "x") null else {
                val busNr = id.toLong()
                val remainder = if (index == 0) 0L else busNr - index
                ModularLong(remainder, busNr)
            }
        }

        return busses.chineseRemainder().toString()
    }
}
