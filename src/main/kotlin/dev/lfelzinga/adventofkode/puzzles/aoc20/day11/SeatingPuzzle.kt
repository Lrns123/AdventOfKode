package dev.lfelzinga.adventofkode.puzzles.aoc20.day11

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid

class SeatingPuzzle : Puzzle<Sequence<String>>(2020, 11, LinesTransformer()) {

    override fun solveFirstPart(input: Sequence<String>): String {
        var grid = input.toList().toCharGrid()

        while (true) {
            val oldGrid = grid
            grid = simulateStep(grid, 4, 1)

            if (grid.array contentEquals oldGrid.array) {
                break
            }
        }

        return grid.array.count { it == '#' }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var grid = input.toList().toCharGrid()

        while (true) {
            val oldGrid = grid
            grid = simulateStep(grid, 5, 999)

            if (grid.array contentEquals oldGrid.array) {
                break
            }
        }

        return grid.array.count { it == '#' }.toString()
    }

    private fun simulateStep(grid: CharGrid, neighbourThreshold: Int, maxMagnitude: Int): CharGrid {
        val result = grid.copy()

        for (x in 0 until grid.width) {
            for (y in 0 until grid.height) {
                when (grid[x, y]) {
                    'L' -> if (grid.neighbours(x, y, maxMagnitude) == 0) result[x, y] = '#'
                    '#' -> if (grid.neighbours(x, y, maxMagnitude) >= neighbourThreshold) result[x, y] = 'L'
                }
            }
        }

        return result
    }

    private fun CharGrid.neighbours(x: Int, y: Int, maxMagnitude: Int): Int {
        var neighbours = 0

        for (dx in -1..1) {
            for (dy in -1..1) {
                if (dx == 0 && dy == 0) continue

                var magnitude = 1
                while (magnitude <= maxMagnitude) {
                    val nx = x + (dx * magnitude)
                    val ny = y + (dy * magnitude)

                    if (nx !in 0 until width || ny !in 0 until height) break

                    when (this[nx, ny]) {
                        '.' -> magnitude++
                        '#' -> { neighbours++; break }
                        else -> break
                    }
                }
            }
        }

        return neighbours
    }

}
