package dev.lfelzinga.adventofkode.puzzles.aoc20.day5

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class BoardingPassPuzzle : Puzzle<Sequence<String>>(2020, 5, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { decodeSeat(it) }
            .map {  it.first * 8 + it.second }
            .maxOrNull()
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val seats = input
            .map { decodeSeat(it) }
            .toSet()

        for (row in 6..120) {
            for (col in 0..7) {
                if (row to col !in seats) {
                    return (row * 8 + col).toString()
                }
            }
        }

        return "???"
    }

    private fun decodeSeat(bsp: String): Pair<Int, Int> {
        var rowBound = Range(0, 128)
        var colBound = Range(0, 8)

        for (ch in bsp) {
            when (ch) {
                'F' -> rowBound = rowBound.lower()
                'B' -> rowBound = rowBound.upper()
                'L' -> colBound = colBound.lower()
                'R' -> colBound = colBound.upper()
                else -> error("Illegal BSP symbol: $ch")
            }
        }

        if (rowBound.size != 1 && colBound.size != -1) {
            error("BSP line $bsp does not encode a specific seat")
        }

        return rowBound.lowerBound to colBound.lowerBound
    }

    data class Range(val lowerBound: Int, val upperBound: Int) {
        val size get() = upperBound - lowerBound

        fun upper() = Range(lowerBound + size / 2, upperBound)
        fun lower() = Range(lowerBound, upperBound - size / 2)
    }
}
