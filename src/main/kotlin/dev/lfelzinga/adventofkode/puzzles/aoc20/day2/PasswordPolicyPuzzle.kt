package dev.lfelzinga.adventofkode.puzzles.aoc20.day2

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class PasswordPolicyPuzzle : Puzzle<Sequence<String>>(2020, 2, LinesTransformer()) {
    companion object {
        val inputRegex = Regex("(\\d+)-(\\d+) ([a-z]): ([a-z]+)")
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { inputRegex.matchEntire(it)?.destructured ?: error("Cannot parse line $it") }
            .count { (min, max, char, password) ->
                password.count { it == char[0] } in min.toInt()..max.toInt()
            }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { inputRegex.matchEntire(it)?.destructured ?: error("Cannot parse line $it") }
            .count { (first, second, char, password) ->
                (password[first.toInt() - 1] == char[0]) != (password[second.toInt() - 1] == char[0])
            }
            .toString()
    }
}
