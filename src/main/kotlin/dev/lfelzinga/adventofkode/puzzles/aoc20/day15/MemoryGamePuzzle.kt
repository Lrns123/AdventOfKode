package dev.lfelzinga.adventofkode.puzzles.aoc20.day15

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.TokenizingTransformer

class MemoryGamePuzzle : Puzzle<List<String>>(2020, 15, TokenizingTransformer(",")) {
    override fun solveFirstPart(input: List<String>): String {
        return runGame(2020, input.map { it.toInt() }).toString()
    }

    override fun solveSecondPart(input: List<String>): String {
        return runGame(30_000_000, input.map { it.toInt() }).toString()
    }

    private fun runGame(steps: Int, startingNumbers: List<Int>): Int {
        val lastPositions = IntArray(steps) { -1 }
        for (i in 0 until startingNumbers.lastIndex) {
            lastPositions[startingNumbers[i]] = i
        }

        var lastNumber = startingNumbers.last()

        for (turn in startingNumbers.size - 1 until steps - 1) {
            val lastPosition = lastPositions[lastNumber]
            lastPositions[lastNumber] = turn

            lastNumber = if (lastPosition == -1) 0 else turn - lastPosition
        }

        return lastNumber
    }
}
