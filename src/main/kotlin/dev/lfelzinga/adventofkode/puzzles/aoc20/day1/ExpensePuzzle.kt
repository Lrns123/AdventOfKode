package dev.lfelzinga.adventofkode.puzzles.aoc20.day1

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class ExpensePuzzle : Puzzle<Sequence<String>>(2020, 1, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val expenses = input.map { it.toInt() }.sorted().toList()

        var lPos = 0
        var rPos = expenses.size - 1

        while (lPos < rPos) {
            val sum = expenses[lPos] + expenses[rPos]

            when {
                sum == 2020 -> return (expenses[lPos] * expenses[rPos]).toString()
                sum < 2020 -> lPos++
                else -> rPos--
            }
        }

        return "???"
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val expenses = input.map { it.toInt() }.sorted().toList()

        for (pos1 in 0..expenses.size - 3) {
            if (expenses[pos1] > 2020) {
                break
            }

            for (pos2 in (pos1 + 1)..expenses.size - 2) {
                if (expenses[pos1] + expenses[pos2] > 2020) {
                    break
                }

                for (pos3 in (pos2 + 1) until expenses.size) {
                    val sum = expenses[pos1] + expenses[pos2] + expenses[pos3]

                    if (sum == 2020) {
                        return (expenses[pos1] * expenses[pos2] * expenses[pos3]).toString()
                    } else if (sum > 2020) {
                        break
                    }
                }
            }
        }

        return "???"
    }
}
