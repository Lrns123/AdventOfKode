package dev.lfelzinga.adventofkode.puzzles.aoc17.day25

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class TuringMachinePuzzle : Puzzle<String>(2017, 25, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        val tape = MutableList(100_001) { 0 }
        var position = 50_000

        var state = "A"

        repeat(12794428) {
            when (state) {
                "A" -> {
                    if (tape[position] == 0) {
                        tape[position++] = 1
                        state = "B"
                    } else {
                        tape[position--] = 0
                        state = "F"
                    }
                }
                "B" -> {
                    if (tape[position] == 0) {
                        tape[position++] = 0
                        state = "C"
                    } else {
                        tape[position++] = 0
                        state = "D"
                    }
                }
                "C" -> {
                    if (tape[position] == 0) {
                        tape[position--] = 1
                        state = "D"
                    } else {
                        tape[position++] = 1
                        state = "E"
                    }
                }
                "D" -> {
                    if (tape[position] == 0) {
                        tape[position--] = 0
                        state = "E"
                    } else {
                        tape[position--] = 0
                        state = "D"
                    }
                }
                "E" -> {
                    if (tape[position] == 0) {
                        tape[position++] = 0
                        state = "A"
                    } else {
                        tape[position++] = 1
                        state = "C"
                    }
                }
                "F" -> {
                    if (tape[position] == 0) {
                        tape[position--] = 1
                        state = "A"
                    } else {
                        tape[position++] = 1
                        state = "A"
                    }
                }
            }
        }

        return tape.asSequence().filter { it == 1 }.count().toString()
    }

    override fun solveSecondPart(input: String) = "Merry Christmas!"
}
