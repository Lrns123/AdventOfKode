package dev.lfelzinga.adventofkode.puzzles.aoc17.day24

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import kotlin.math.max

class BridgePuzzle : Puzzle<Sequence<String>>(2017, 24, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val components = input
            .map { line -> line.split("/").map { it.toInt() } }
            .map { (lhs, rhs) -> lhs to rhs }
            .toList()


        return findStrongestBridge(components, 0, 0).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val components = input
            .map { line -> line.split("/").map { it.toInt() } }
            .map { (lhs, rhs) -> lhs to rhs }
            .toList()


        return findLongestStrongestBridge(components, 0, 0, 0).first.toString()
    }

    private fun findStrongestBridge(components: List<Pair<Int, Int>>, pins: Int, strength: Int): Int {
        var strongest = strength

        for (component in components) {
            if (component.first == pins || component.second == pins) {
                val otherPins = if (component.first == pins) component.second else component.first
                strongest = max(strongest, findStrongestBridge(components - component, otherPins, strength + component.first + component.second))
            }
        }

        return strongest
    }

    private fun findLongestStrongestBridge(components: List<Pair<Int, Int>>, pins: Int, strength: Int, length: Int): Pair<Int, Int> {
        var strongest = strength
        var longest = length

        for (component in components) {
            if (component.first == pins || component.second == pins) {
                val otherPins = if (component.first == pins) component.second else component.first
                val result = findLongestStrongestBridge(components - component, otherPins, strength + component.first + component.second, length + 1)

                if (result.second >= longest) {
                    longest = result.second
                    strongest = max(strongest, result.first)
                }
            }
        }

        return strongest to longest
    }
}
