package dev.lfelzinga.adventofkode.puzzles.aoc17.day23

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import kotlin.math.roundToInt
import kotlin.math.sqrt
import kotlin.reflect.KClass

class CoprocessorPuzzle : Puzzle<Sequence<String>>(2017, 23, LinesTransformer()) {
    private val opcodeRegex = Regex("""(\w+) (\w+) ?([0-9a-z-]+)?""")

    sealed class Opcode(private val op: VM.VMInterface.() -> Unit) {
        class SET(register: String, value: String) : Opcode({ registers[register] = value.resolve })
        class SUB(register: String, amount: String) : Opcode({ registers[register] = registers.getOrDefault(register, 0) - amount.resolve })
        class MUL(register: String, factor: String) : Opcode({ registers[register] = registers.getOrDefault(register, 0) * factor.resolve })
        class JNZ(conditional: String, offset: String) : Opcode({ if (conditional.resolve != 0L) jump(offset.resolve.toInt()) })

        operator fun invoke(vmInterface: VM.VMInterface) = vmInterface.op()
    }

    class VM {
        private var debug = true
        val registers = mutableMapOf<String, Long>()
        var instructionPointer: Int = 0
        val opcodes = mutableListOf<Opcode>()

        val opcodesExecuted = mutableMapOf<KClass<*>, Int>(
            Opcode.SET::class to 0,
            Opcode.SUB::class to 0,
            Opcode.MUL::class to 0,
            Opcode.JNZ::class to 0
        )

        private val vmInterface = VMInterface()

        fun run() {
            try {
                while (true) {
                    val op = opcodes[instructionPointer++]

                    if (debug) {
                        opcodesExecuted[op::class] = (opcodesExecuted[op::class] ?: 0) + 1
                    }

                    op(vmInterface)
                }
            } catch (e: Throwable) {
                // Halt VM
            }
        }

        inner class VMInterface {
            val registers: MutableMap<String, Long> get() = this@VM.registers
            val String.resolve: Long
                get() = if (this[0] in 'a'..'z') this@VM.registers.getOrDefault(this, 0L) else this.toLong()

            fun jump(offset: Int) {
                instructionPointer += offset - 1
            }
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val vm = VM()

        input
            .map { opcodeRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Could not parse opcode $it") }
            .map { (opcode, op1, op2) -> when (opcode) {
                "set" -> Opcode.SET(op1, op2)
                "sub" -> Opcode.SUB(op1, op2)
                "mul" -> Opcode.MUL(op1, op2)
                "jnz" -> Opcode.JNZ(op1, op2)
                else -> throw IllegalArgumentException("Unknown opcode $opcode")
            } }
            .forEach {
                vm.opcodes.add(it)
            }

        vm.run()

        return vm.opcodesExecuted[Opcode.MUL::class]!!.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        // The bytecode contains a primality test for numbers between
        // 109900 and 126900 (+17000) with increments of 17 (see disassembly.md)

        return (109900..126900 step 17)
            .count { !isPrime(it) }
            .toString()
    }

    private fun isPrime(number: Int): Boolean {
        if (number != 2 && number and 1 == 0) {
            return false
        }

        return (3..sqrt(number.toDouble()).roundToInt() step 2)
            .none { number % it == 0 }
    }
}
