package dev.lfelzinga.adventofkode.puzzles.aoc17.day22

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Direction

private const val CLEAN = 0
private const val WEAKENED = 1
private const val INFECTED = 2
private const val FLAGGED = 3
private const val STATE_MAX = 4


class VirusPuzzle : Puzzle<Sequence<String>>(2017, 22, LinesTransformer()) {

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = BooleanGrid(1001, 1001)
        val startState = input.toList()

        val offset = (grid.height / 2) - (startState.size / 2) // Assume input is square

        startState.forEachIndexed { row, s ->
            s.forEachIndexed { col, ch ->
                grid[col + offset, row + offset] = ch == '#'
            }
        }

        var x = grid.width / 2
        var y = grid.height / 2

        var direction = Direction.UP

        var infected = 0

        repeat(10_000) {
            direction = if (grid[x, y]) ++direction else --direction

            grid[x, y] = !grid[x, y]

            if (grid[x, y]) {
                infected++
            }

            x += direction.dx
            y += direction.dy
        }

        return infected.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = IntGrid(1001, 1001)
        val startState = input.toList()

        val offset = (grid.height / 2) - (startState.size / 2) // Assume input is square

        startState.forEachIndexed { row, s ->
            s.forEachIndexed { col, ch ->
                grid[col + offset, row + offset] = if (ch == '#') INFECTED else CLEAN
            }
        }

        var x = grid.width / 2
        var y = grid.height / 2

        var direction = Direction.UP

        var infected = 0

        repeat(10_000_000) {
            when (grid[x, y]) {
                CLEAN -> --direction
                INFECTED -> ++direction
                FLAGGED -> direction = -direction
            }

            grid[x, y] = (grid[x, y] + 1) % STATE_MAX

            if (grid[x, y] == INFECTED) {
                infected++
            }

            x += direction.dx
            y += direction.dy
        }

        return infected.toString()
    }
}
