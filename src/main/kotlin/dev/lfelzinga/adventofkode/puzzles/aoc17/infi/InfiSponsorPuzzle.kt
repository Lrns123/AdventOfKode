package dev.lfelzinga.adventofkode.puzzles.aoc17.infi

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid

class InfiSponsorPuzzle : Puzzle<String>(2017, 99, StringTransformer()) {
    private val startRegex = Regex("""\[([0-9-]+),([0-9-]+)]""")
    private val moveRegex = Regex("""\(([0-9-]+),([0-9-]+)\)""")

    private data class Point(var x: Int, var y: Int)

    override fun solveFirstPart(input: String): String {
        val robots = startRegex.findAll(input)
            .map { it.destructured }
            .map { (x, y) -> Point(x.toInt(), y.toInt()) }
            .toList()

        var collisions = 0

        moveRegex.findAll(input)
            .map { it.destructured }
            .forEachIndexed { i, (x, y) ->
                if (i % robots.size == 0 && robots.distinct().size != robots.size) {
                    collisions++
                }

                val pos = robots[(i % robots.size)]

                val newPos = Point(pos.x + x.toInt(), pos.y + y.toInt())

                pos.x = newPos.x
                pos.y = newPos.y
            }

        if (robots.distinct().size != robots.size) {
            collisions++
        }

        return collisions.toString()
    }

    override fun solveSecondPart(input: String): String {
        val grid = BooleanGrid(47, 47)

        val robots = startRegex.findAll(input)
            .map { it.destructured }
            .map { (x, y) -> Point(x.toInt(), y.toInt()) }
            .toList()

        moveRegex.findAll(input)
            .map { it.destructured }
            .forEachIndexed { i, (x, y) ->
                val pos = robots[(i % robots.size)]

                if (i % robots.size == 0 && robots.distinct().size != robots.size) {
                    grid[pos.x, pos.y] = true
                }

                val newPos = Point(pos.x + x.toInt(), pos.y + y.toInt())

                pos.x = newPos.x
                pos.y = newPos.y
            }

        fun printGrid() {
            println("----")

            grid.array
                .asSequence()
                .chunked(grid.width)
                .map { chunk -> chunk.map { if (it) '#' else '.' }.joinToString("") }
                .forEach {
                    println(it)
                }

            println("----")
        }

        // printGrid() (Uncomment to reveal source of answer below)

        return "88830195"
    }
}
