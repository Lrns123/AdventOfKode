package dev.lfelzinga.adventofkode.puzzles.aoc17.day13

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class FirewallPuzzle : Puzzle<Sequence<String>>(2017, 13, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { it.split(": ") }
            .map { (layer, depth) -> layer.toInt() to depth.toInt() }
            .filter { it.first % (2 * it.second - 2) == 0 }
            .sumOf { it.first * it.second }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val scanners = input
            .map { it.split(": ") }
            .map { (layer, depth) -> layer.toInt() to depth.toInt() }
            .toMap()

        return generateSequence(0) { it + 1 }
            .first { offset -> scanners.none { (offset + it.key) % (it.value + it.value - 2) == 0 } }
            .toString()
    }
}
