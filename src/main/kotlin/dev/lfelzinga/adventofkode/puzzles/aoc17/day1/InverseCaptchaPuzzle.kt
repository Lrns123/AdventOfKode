package dev.lfelzinga.adventofkode.puzzles.aoc17.day1

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class InverseCaptchaPuzzle : Puzzle<String>(2017, 1, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        val n = input.length
        return (0 until n)
            .asSequence()
            .filter { input[it] == input[(it + 1) % n] }
            .sumOf { input[it] - '0' }
            .toString()
    }

    override fun solveSecondPart(input: String): String {
        val n = input.length
        return (0 until n)
            .asSequence()
            .filter { input[it] == input[(it + (n / 2)) % n] }
            .sumOf { input[it] - '0' }
            .toString()
    }
}
