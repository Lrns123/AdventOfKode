package dev.lfelzinga.adventofkode.puzzles.aoc17.day9

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class StreamProcessingPuzzle : Puzzle<String>(2017, 9, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        return parseInput(input.iterator()).first.toString()
    }

    override fun solveSecondPart(input: String): String {
       return parseInput(input.iterator()).second.toString()
    }

    private fun skipGarbage(stream: CharIterator): Int {
        var chars = 0
        while (stream.hasNext()) {
            when (stream.nextChar()) {
                '>' -> return chars
                '!' -> stream.nextChar()
                else -> ++chars
            }
        }

        return chars
    }

    private fun parseInput(stream: CharIterator): Pair<Int, Int> {
        var score = 0
        var nestLevel = 0
        var garbage = 0

        while (stream.hasNext()) {
            when (stream.nextChar()) {
                '{' -> nestLevel++
                '}' -> score += nestLevel--
                '<' -> garbage += skipGarbage(stream)
            }
        }

        return score to garbage
    }
}
