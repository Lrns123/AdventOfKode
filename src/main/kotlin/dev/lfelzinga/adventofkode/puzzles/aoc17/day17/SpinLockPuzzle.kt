package dev.lfelzinga.adventofkode.puzzles.aoc17.day17

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.NumberTransformer

class SpinLockPuzzle : Puzzle<Long>(2017, 17, NumberTransformer()) {
    override fun solveFirstPart(input: Long): String {
        val step = input.toInt()
        val list = mutableListOf(0)
        var position = 0

        repeat(2017) {
            position = (position + step) % list.size

            list.add(++position, it + 1)
        }

        return list[(list.indexOf(2017) + 1) % list.size].toString()
    }

    override fun solveSecondPart(input: Long): String {
        val step = input.toInt()
        var position = 0
        var result = 0

        for (n in 1..50_000_000) {
            position = (position + step) % n

            if (position++ == 0) {
                result = n
            }
        }

        return result.toString()
    }
}
