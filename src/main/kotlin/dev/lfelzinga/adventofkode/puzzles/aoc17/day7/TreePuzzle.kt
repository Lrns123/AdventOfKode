package dev.lfelzinga.adventofkode.puzzles.aoc17.day7

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class TreePuzzle : Puzzle<Sequence<String>>(2017, 7, LinesTransformer()) {
    data class Node(val name: String, val weight: Int, val childNames: List<String>) {
        private var memoizedWeight: Int? = null

        fun getCombinedWeight(tree: Map<String, Node>): Int {
            return memoizedWeight ?: (weight + childNames.sumOf { tree[it]?.getCombinedWeight(tree) ?: throw IllegalArgumentException("Cannot find child node $it") })
                .also { memoizedWeight = it }
        }
    }

    private val nodeRegex = Regex("""(\w+) \((\d+)\)(?: -> ([a-z, ]+))?""")

    override fun solveFirstPart(input: Sequence<String>): String {
        val nodes = input
            .map { nodeRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse node") }
            .map { (name, weight, children) -> Node(name, weight.toInt(), children.split(", ")) }
            .associateBy { it.name }

        val nodeNames = nodes.map { it.key }.toList()
        val childNames = nodes.flatMap { it.value.childNames }.toList()

        return (nodeNames - childNames)[0]
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val nodes = input
            .map { nodeRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse node") }
            .map { (name, weight, children) -> Node(name, weight.toInt(), children.split(", ").filter { it.isNotBlank() }) }
            .associateBy { it.name }

        val nodeNames = nodes.map { it.key }.toList()
        val childNames = nodes.flatMap { it.value.childNames }.toList()

        val rootNode = nodes[(nodeNames - childNames)[0]] ?: throw IllegalArgumentException("Cannot find root node")

        // Precache all combined weights
        rootNode.getCombinedWeight(nodes)

        return findImbalance(nodes, rootNode).toString()
    }

    private fun isSubTreeBalanced(tree: Map<String, Node>, node: Node): Boolean {
        return node.childNames.isEmpty() || node.childNames
            .distinctBy { tree.getValue(it).getCombinedWeight(tree) }
            .size == 1
    }

    private fun findImbalance(tree: Map<String, Node>, rootNode: Node): Int {
        val weights = rootNode.childNames
            .asSequence()
            .map { it to tree.getValue(it).getCombinedWeight(tree) }
            .groupBy({ it.second }, { it.first })

        if (weights.size != 1) {
            val desiredWeight = requireNotNull(weights.maxByOrNull { it.value.size })
            val unbalancedWeight = requireNotNull(weights.minByOrNull { it.value.size })

            if (unbalancedWeight.value.size != 1) {
                throw IllegalStateException("Invalid state, multiple nodes with unbalanced weight")
            }

            val unbalancedNode = tree.getValue(unbalancedWeight.value[0])

            return if (isSubTreeBalanced(tree, unbalancedNode)) {
                unbalancedNode.weight - (unbalancedWeight.key - desiredWeight.key)
            } else {
                findImbalance(tree, unbalancedNode)
            }
        }

        return 0
    }
}
