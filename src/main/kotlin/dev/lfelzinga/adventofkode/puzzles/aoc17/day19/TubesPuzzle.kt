package dev.lfelzinga.adventofkode.puzzles.aoc17.day19

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Direction

class TubesPuzzle : Puzzle<Sequence<String>>(2017, 19, LinesTransformer()) {

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = input.toList()
        var y = 0
        var x = grid[0].indexOf('|')
        var direction = Direction.SOUTH

        val visitedPoints = StringBuilder()

        while (true) {
            x += direction.dx
            y += direction.dy

            when (grid[y][x]) {
                in 'A'..'Z' -> visitedPoints.append(grid[y][x])
                ' ' -> return visitedPoints.toString()
                '+' -> direction = turn(grid, x, y, direction)
            }
        }
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = input.toList()
        var y = 0
        var x = grid[0].indexOf('|')
        var direction = Direction.SOUTH

        var steps = 0

        while (true) {
            x += direction.dx
            y += direction.dy
            steps++

            when (grid[y][x]) {
                ' ' -> return steps.toString()
                '+' -> direction = turn(grid, x, y, direction)
            }
        }
    }

    private fun turn(grid: List<String>, x: Int, y: Int, currentDirection: Direction): Direction {
        fun isHorizontal(ch: Char) = ch == '-' || ch == '+' || ch in 'A'..'Z'
        fun isVertical(ch: Char) = ch == '|' || ch == '+' || ch in 'A'..'Z'

        when (currentDirection) {
            Direction.NORTH, Direction.SOUTH -> {
                return when {
                    isHorizontal(grid[y][x - 1]) -> Direction.WEST
                    isHorizontal(grid[y][x + 1]) -> Direction.EAST
                    else -> throw IllegalStateException("Cannot determine turn direction")
                }
            }
            Direction.WEST, Direction.EAST -> {
                return when {
                    isVertical(grid[y + 1][x]) -> Direction.SOUTH
                    isVertical(grid[y - 1][x]) -> Direction.NORTH
                    else -> throw IllegalStateException("Cannot determine turn direction")
                }
            }
        }
    }
}
