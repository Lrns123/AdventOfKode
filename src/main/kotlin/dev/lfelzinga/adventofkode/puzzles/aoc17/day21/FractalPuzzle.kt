package dev.lfelzinga.adventofkode.puzzles.aoc17.day21

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid

class FractalPuzzle : Puzzle<Sequence<String>>(2017, 21, LinesTransformer()) {

    override fun solveFirstPart(input: Sequence<String>): String {
        val rules = parseRules(input)
        var grid = convertToGrid(listOf(
            ".#.",
            "..#",
            "###"
        ))

        repeat(5) {
            grid = applyRules(grid, rules)
        }

        return grid.array.asSequence().filter { it }.count().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val rules = parseRules(input)
        var grid = convertToGrid(listOf(
            ".#.",
            "..#",
            "###"
        ))

        repeat(18) {
            grid = applyRules(grid, rules)
        }

        return grid.array.asSequence().filter { it }.count().toString()
    }

    private fun parseRules(input: Sequence<String>): Map<BooleanGrid, BooleanGrid> {
        return input
            .map { line -> line.split(" => ").map { it.split("/") } }
            .map { (input, output) -> convertToGrid(input) to convertToGrid(output) }
            .flatMap { (input, output) ->
                val rot90 = input.rotateClockwise()
                val rot180 = rot90.rotateClockwise()
                val rot270 = rot180.rotateClockwise()

                val flipH = input.flipHorizontal()
                val flipV = input.flipVertical()

                val rot90flipH = rot90.flipHorizontal()
                val rot90flipV = rot90.flipVertical()

                sequenceOf(
                    input to output,
                    rot90 to output,
                    rot180 to output,
                    rot270 to output,
                    flipH to output,
                    flipV to output,

                    rot90flipH to output,
                    rot90flipV to output
                ).distinctBy { (input, _) -> input.array.map { it } }
            }
            .toMap()
    }

    private fun convertToGrid(pattern: List<String>): BooleanGrid {
        val grid = BooleanGrid(pattern.size, pattern.size)

        for (x in pattern.indices) {
            for (y in pattern.indices) {
                grid[x, y] = pattern[y][x] == '#'
            }
        }

        return grid
    }

    private fun applyRules(grid: BooleanGrid, rules: Map<BooleanGrid, BooleanGrid>): BooleanGrid {
        return when {
            grid.width % 2 == 0 -> {
                val newGrid = BooleanGrid(grid.width / 2 * 3, grid.height / 2 * 3)
                val applicableRules = rules.filter { it.key.width == 2 }

                for (chunkX in 0 until grid.width / 2) {
                    for (chunkY in 0 until grid.height / 2) {
                        applyRules(grid, chunkX * 2, chunkY * 2, newGrid, chunkX * 3, chunkY * 3, applicableRules)
                    }
                }

                newGrid
            }
            grid.width % 3 == 0 -> {
                val newGrid = BooleanGrid(grid.width / 3 * 4, grid.height / 3 * 4)
                val applicableRules = rules.filter { it.key.width == 3 }

                for (chunkX in 0 until grid.width / 3) {
                    for (chunkY in 0 until grid.height / 3) {
                        applyRules(grid, chunkX * 3, chunkY * 3, newGrid, chunkX * 4, chunkY * 4, applicableRules)
                    }
                }

                newGrid
            }
            else -> throw IllegalStateException("Invalid input grid size")
        }
    }

    private fun applyRules(inputGrid: BooleanGrid, inOffsetX: Int, inOffsetY: Int, outputGrid: BooleanGrid, outOffsetX: Int, outOffsetY: Int, rules: Map<BooleanGrid, BooleanGrid>) {
        for (rule in rules) {
            if (patternMatches(inputGrid, inOffsetX, inOffsetY, rule.key)) {
                for (x in 0 until rule.value.width) {
                    for (y in 0 until rule.value.height) {
                        outputGrid[x + outOffsetX, y + outOffsetY] = rule.value[x, y]
                    }
                }
                return
            }
        }

        throw IllegalStateException("Could not match any rule to input")
    }

    private fun patternMatches(grid: BooleanGrid, offsetX: Int, offsetY: Int, pattern: BooleanGrid): Boolean {
        for (x in 0 until pattern.width) {
            for (y in 0 until pattern.height) {
                if (grid[offsetX + x, offsetY + y] != pattern[x, y]) {
                    return false
                }
            }
        }

        return true
    }
}
