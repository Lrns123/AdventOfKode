package dev.lfelzinga.adventofkode.puzzles.aoc17.day8

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class RegisterPuzzle : Puzzle<Sequence<String>>(2017, 8, LinesTransformer()) {
    private val maxRegister = "_max"
    private val instructionRegex = Regex("""(\w+) (inc|dec) (-?\d+) if (\w+) (\S+) (-?\d+)""")

    override fun solveFirstPart(input: Sequence<String>): String {
        return runProgram(input)
            .filter { it.key != maxRegister }
            .map { it.value }
            .maxOrNull()
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return runProgram(input)
            .getValue(maxRegister)
            .toString()
    }

    private fun runProgram(input: Sequence<String>): Map<String, Int> {
        val registers = mutableMapOf(maxRegister to Int.MIN_VALUE).withDefault { 0 }

        input
            .map { instructionRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot read instruction $it") }
            .forEach { (register, operation, amount, conditionRegister, conditionOperation, conditionAmount) ->
                val conditionFunc: (lhs: Int, rhs: Int) -> Boolean = when(conditionOperation) {
                    "==" -> { lhs, rhs -> lhs == rhs }
                    ">=" -> { lhs, rhs -> lhs >= rhs }
                    "<=" -> { lhs, rhs -> lhs <= rhs }
                    "!=" -> { lhs, rhs -> lhs != rhs }
                    ">" -> { lhs, rhs -> lhs > rhs }
                    "<" -> { lhs, rhs -> lhs < rhs }
                    else -> throw IllegalArgumentException("Unknown operation $conditionOperation")
                }

                val mutatorFunc: (current: Int, amount: Int) -> Int = when(operation) {
                    "inc" -> { current, amt -> current + amt }
                    "dec" -> { current, amt -> current - amt }
                    else -> throw IllegalArgumentException("Unknown instruction $operation")
                }

                if (conditionFunc(registers.getValue(conditionRegister), conditionAmount.toInt())) {
                    registers[register] = mutatorFunc(registers.getValue(register), amount.toInt()).also {
                        registers[maxRegister] = registers.getValue(maxRegister).coerceAtLeast(registers.getValue(register))
                    }
                }
            }

        return registers
    }
}
