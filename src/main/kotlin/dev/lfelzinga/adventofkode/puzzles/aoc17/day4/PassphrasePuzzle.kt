package dev.lfelzinga.adventofkode.puzzles.aoc17.day4

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class PassphrasePuzzle : Puzzle<Sequence<String>>(2017, 4, LinesTransformer()) {
    private val whitespaceRegex = Regex("\\s+")

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { it.split(whitespaceRegex) }
            .count { it.distinct().size == it.size }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { line -> line.split(whitespaceRegex).map { it.toCharArray().sorted().joinToString("") } }
            .count { it.distinct().size == it.size }
            .toString()
    }
}
