package dev.lfelzinga.adventofkode.puzzles.aoc17.day12

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class DigitalPlumberPuzzle : Puzzle<Sequence<String>>(2017, 12, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val connections = input
            .map { it.split(" <-> ") }
            .map { (source, destinations) -> source.toInt() to destinations.split(", ").map { it.toInt() }}
            .toMap()

        val connectedPrograms = mutableSetOf(0)
        tracePipes(connections, connectedPrograms, 0)

        return connectedPrograms.size.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val connections = input
            .map { it.split(" <-> ") }
            .map { (source, destinations) -> source.toInt() to destinations.split(", ").map { it.toInt() }}
            .toMap()

        val unresolvedPrograms = ArrayList(connections.keys)
        var groups = 0

        while (unresolvedPrograms.isNotEmpty()) {
            val connectedPrograms = mutableSetOf(unresolvedPrograms[0])
            tracePipes(connections, connectedPrograms, unresolvedPrograms[0])

            unresolvedPrograms.removeAll(connectedPrograms)
            groups++
        }

        return groups.toString()
    }

    private fun tracePipes(connections: Map<Int, List<Int>>, visited: MutableSet<Int>, id: Int) {
        connections
            .getValue(id)
            .filter { visited.add(it) }
            .forEach { tracePipes(connections, visited, it) }
    }
}
