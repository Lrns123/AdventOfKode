package dev.lfelzinga.adventofkode.puzzles.aoc17.day3

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.NumberTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import kotlin.math.absoluteValue
import kotlin.math.ceil
import kotlin.math.sqrt

class SpiralMemoryPuzzle : Puzzle<Long>(2017, 3, NumberTransformer()) {
    override fun solveFirstPart(input: Long): String {
        val ring = ceil(sqrt(input.toDouble())).let { if (it % 2 == 0.0) it + 1 else it }.toLong()
        val edgeSteps = ring - 1
        val midSteps = edgeSteps / 2

        val midpointDistance = List(4) { ring * ring }
            .mapIndexed { index, value -> value - edgeSteps * index - midSteps }
            .minOfOrNull { (it - input).absoluteValue } ?: 0

        return (midpointDistance + midSteps).toString()
    }

    private val spiralSequence = sequence {
        var x = 0
        var y = 0
        var edgeLength = 0
        yield(x to y)

        while (true) {
            edgeLength += 2

            yield(++x to y)

            // Move up
            repeat(edgeLength - 1) { yield(x to --y) }

            // Move right
            repeat(edgeLength) { yield(--x to y) }

            // Move down
            repeat(edgeLength) { yield(x to ++y) }

            // Move left
            repeat(edgeLength) { yield(++x to y) }
        }
    }

    private val adjacentCells = arrayOf(
        -1 to -1,
        -1 to 0,
        -1 to 1,
        0 to 1,
        0 to -1,
        1 to -1,
        1 to 0,
        1 to 1
    )

    override fun solveSecondPart(input: Long): String {
        val radius = 14
        val grid = IntGrid(radius * 2 + 1, radius * 2 + 1)

        return spiralSequence
            .map { it.first + radius to it.second + radius }
            .map { coords -> adjacentCells
                .map { it.first + coords.first to it.second + coords.second }
                .sumOf { grid[it.first, it.second] }
                .let { if (it == 0) 1 else it }
                .also { grid[coords.first, coords.second] = it }
            }
            .filter { it > input }
            .first()
            .toString()
    }
}
