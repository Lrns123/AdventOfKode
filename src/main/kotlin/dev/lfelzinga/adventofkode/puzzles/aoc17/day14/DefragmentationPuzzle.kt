package dev.lfelzinga.adventofkode.puzzles.aoc17.day14

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import java.util.*

class DefragmentationPuzzle : Puzzle<String>(2017, 14, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        return (0..127)
            .map { knotHash("$input-$it") }
            .sumOf { hash -> hash.sumOf { Integer.bitCount(it) } }
            .toString()
    }

    override fun solveSecondPart(input: String): String {
        val grid = IntGrid(128, 128)
        (0..127)
            .map { knotHash("$input-$it") }
            .forEachIndexed { row, list ->
                list.forEachIndexed { colSet, i ->
                    (0..7).forEach {
                        if (i and (1 shl it) == 0) {
                            grid[row, colSet * 8 + (7 - it)] = -1
                        }
                    }
                }
            }

        var region = 1
        for (row in 0..127) {
            for (col in 0..127) {
                if (grid[row, col] == 0) {
                    floodFill(grid, region, row, col)
                    region++
                }
            }
        }

        return (region - 1).toString()
    }

    private fun knotHash(input: String): List<Int> {
        val list = List(256) { it }
        var position = 0
        var skip = 0

        val lengths = input.trim().map { it.code } + listOf(17, 31, 73, 47, 23)

        repeat(64) {
            for (length in lengths) {
                reverse(list, position, position + length - 1)
                position += length + skip++
            }
        }

        return list.chunked(16) { it.reduce { lhs, rhs -> lhs xor rhs } }
    }

    private fun reverse(list: List<*>, startIndex: Int, endIndex: Int) {
        var i = startIndex
        var j = endIndex

        while (i < j) {
            Collections.swap(list, i++ % list.size, j-- % list.size)
        }
    }

    private fun floodFill(grid: IntGrid, value: Int, row: Int, col: Int) {
        if (row in 0..127 && col in 0..127 && grid[row, col] == 0) {
            grid[row, col] = value

            floodFill(grid, value, row + 1, col)
            floodFill(grid, value, row - 1, col)
            floodFill(grid, value, row, col + 1)
            floodFill(grid, value, row, col - 1)
        }
    }
}
