package dev.lfelzinga.adventofkode.puzzles.aoc17.day16

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.TokenizingTransformer
import java.util.Collections.swap

class DancePuzzle : Puzzle<List<String>>(2017, 16, TokenizingTransformer(",")) {
    private val spinRegex = Regex("s(\\d+)")
    private val exchangeRegex = Regex("x(\\d+)/(\\d+)")
    private val partnerRegex = Regex("p(\\w)/(\\w)")

    sealed class Move(val operation: (positions: MutableList<Char>) -> Unit) {
        class SpinMove(size: Int): Move({ positions ->
            repeat(size) {
                val program = positions.removeAt(15)
                positions.add(0, program)
            }
        })

        class ExchangeMove(pos1: Int, pos2: Int): Move({
            swap(it, pos1, pos2)
        })

        class PartnerMove(program1: Char, program2: Char): Move({
            swap(it, it.indexOf(program1), it.indexOf(program2))
        })

        operator fun invoke(positions: MutableList<Char>) {
            operation(positions)
        }
    }

    override fun solveFirstPart(input: List<String>): String {
        val positions = MutableList(16) { 'a' + it }
        val dance = parseDance(input)

        dance.forEach {
            it(positions)
        }

        return positions.joinToString("")
    }

    override fun solveSecondPart(input: List<String>): String {
        val seenPositions = mutableListOf<String>()
        val positions = MutableList(16) { 'a' + it }
        val dance = parseDance(input)

        repeat(1_000_000_000) {
            val positionString = positions.joinToString("")
            if (seenPositions.contains(positionString)) {
                val index = seenPositions.indexOf(positionString)
                val cycleSize = seenPositions.size - index

                return seenPositions[index + (1_000_000_000 - index) % cycleSize]
            }

            seenPositions.add(positionString)

            dance.forEach {
                it(positions)
            }
        }

        return positions.joinToString("")
    }

    private fun parseDance(input: List<String>): List<Move> {
        return input
            .map {
                spinRegex.matchEntire(it)?.destructured?.let { (size) ->
                    Move.SpinMove(size.toInt())
                } ?:

                exchangeRegex.matchEntire(it)?.destructured?.let { (pos1, pos2) ->
                    Move.ExchangeMove(pos1.toInt(), pos2.toInt())
                } ?:

                partnerRegex.matchEntire(it)?.destructured?.let { (program1, program2) ->
                    Move.PartnerMove(program1[0], program2[0])
                } ?:

                throw IllegalArgumentException("Cannot parse dance move")
            }
    }
}
