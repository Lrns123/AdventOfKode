package dev.lfelzinga.adventofkode.puzzles.aoc17.day18

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.util.*

class DuetPuzzle : Puzzle<Sequence<String>>(2017, 18, LinesTransformer()) {
    private val opcodeRegex = Regex("""(\w+) (\w+) ?([0-9a-z-]+)?""")

    class VM {
        private class VMHaltException: Exception()

        lateinit var inboundQueue: Queue<Long>
        lateinit var outboundQueue: Queue<Long>

        var valuesSent: Long = 0

        val playedSounds = mutableListOf<Long>()
        val registers = mutableMapOf<String, Long>()
        var instructionPointer: Int = 0
        val opcodes = mutableListOf<Opcode>()

        private val vmInterface = VMInterface()

        fun run(): Int {
            var opcodesExecuted = 0
            try {
                while (true) {
                    opcodes[instructionPointer++](vmInterface)
                    opcodesExecuted++
                }
            } catch (e: Throwable) {
                // Halt VM
            }

            return opcodesExecuted
        }

        inner class VMInterface {
            val registers: MutableMap<String, Long> get() = this@VM.registers
            val String.resolve: Long
                get() = if (this[0] in 'a'..'z') this@VM.registers.getOrDefault(this, 0L) else this.toLong()

            fun playSound(frequency: Long) {
                playedSounds += frequency
            }

            fun recover(): Nothing = throw VMHaltException()

            fun jump(offset: Int) {
                instructionPointer += offset - 1
            }

            fun send(value: Long) {
                outboundQueue.add(value)
                valuesSent++
            }

            fun receive(): Long {
                if (inboundQueue.isEmpty()) {
                    instructionPointer--
                    throw VMHaltException()
                } else {
                    return inboundQueue.poll()
                }
            }
        }
    }

    sealed class Opcode(private val op: VM.VMInterface.() -> Unit) {
        class SND(frequency: String) : Opcode({ playSound(frequency.resolve) })
        class SND2(value: String) : Opcode({ send(value.resolve) })
        class SET(register: String, value: String) : Opcode({ registers[register] = value.resolve })
        class ADD(register: String, amount: String) : Opcode({ registers[register] = registers.getOrDefault(register, 0) + amount.resolve })
        class MUL(register: String, factor: String) : Opcode({ registers[register] = registers.getOrDefault(register, 0) * factor.resolve })
        class MOD(register: String, divisor: String) : Opcode({ registers[register] = registers.getOrDefault(register, 0) % divisor.resolve })
        class RCV(conditional: String) : Opcode({ if (conditional.resolve != 0L) recover() })
        class RCV2(register: String) : Opcode({  registers[register] = receive() })
        class JGZ(conditional: String, offset: String) : Opcode({ if (conditional.resolve > 0L) jump(offset.resolve.toInt()) })

        operator fun invoke(vmInterface: VM.VMInterface) = vmInterface.op()
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val vm = VM()

        input
            .map { opcodeRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Could not parse opcode $it") }
            .map { (opcode, op1, op2) -> when (opcode) {
                "snd" -> Opcode.SND(op1)
                "set" -> Opcode.SET(op1, op2)
                "add" -> Opcode.ADD(op1, op2)
                "mul" -> Opcode.MUL(op1, op2)
                "mod" -> Opcode.MOD(op1, op2)
                "rcv" -> Opcode.RCV(op1)
                "jgz" -> Opcode.JGZ(op1, op2)
                else -> throw IllegalArgumentException("Unknown opcode $opcode")
            } }
            .forEach {
                vm.opcodes.add(it)
            }

        vm.run()

        return vm.playedSounds.last().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val vm1 = VM()
        val vm2 = VM()

        vm1.inboundQueue = ArrayDeque()
        vm1.outboundQueue = ArrayDeque()

        vm2.inboundQueue = vm1.outboundQueue
        vm2.outboundQueue = vm1.inboundQueue

        input
            .map { opcodeRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Could not parse opcode $it") }
            .map { (opcode, op1, op2) -> when (opcode) {
                "snd" -> Opcode.SND2(op1)
                "set" -> Opcode.SET(op1, op2)
                "add" -> Opcode.ADD(op1, op2)
                "mul" -> Opcode.MUL(op1, op2)
                "mod" -> Opcode.MOD(op1, op2)
                "rcv" -> Opcode.RCV2(op1)
                "jgz" -> Opcode.JGZ(op1, op2)
                else -> throw IllegalArgumentException("Unknown opcode $opcode")
            } }
            .forEach {
                vm1.opcodes.add(it)
                vm2.opcodes.add(it)
            }

        vm2.registers["p"] = 1

        while (vm1.run() + vm2.run() != 0) {
            /* Do nothing */
        }

        return vm2.valuesSent.toString()
    }
}
