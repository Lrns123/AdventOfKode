package dev.lfelzinga.adventofkode.puzzles.aoc17.day6

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class ReallocationPuzzle : Puzzle<String>(2017, 6, StringTransformer()) {
    private val whitespaceRegex = Regex("\\s+")

    override fun solveFirstPart(input: String): String {
        val banks = input.split(whitespaceRegex).map { it.toInt() }.toMutableList()
        val seenConfigurations = mutableSetOf<String>()
        var reconfigurations = 0

        while (seenConfigurations.add(banks.joinToString())) {
            val redistributionBank = banks.indexOf(banks.maxOrNull())
            val blocks = banks[redistributionBank]

            banks[redistributionBank] = 0

            for (i in 1..blocks) {
                banks[(redistributionBank + i) % banks.size]++
            }

            reconfigurations++
        }

        return reconfigurations.toString()
    }

    override fun solveSecondPart(input: String): String {
        val banks = input.split(whitespaceRegex).map { it.toInt() }.toMutableList()
        val seenConfigurations = mutableMapOf<String, Int>()
        var reconfigurations = 0

        while (true) {
            seenConfigurations.put(banks.joinToString(), reconfigurations)?.let {
                return (reconfigurations - it).toString()
            }

            val redistributionBank = banks.indexOf(banks.maxOrNull())
            val blocks = banks[redistributionBank]

            banks[redistributionBank] = 0

            for (i in 1..blocks) {
                banks[(redistributionBank + i) % banks.size]++
            }

            reconfigurations++
        }
    }
}
