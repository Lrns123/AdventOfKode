Bytecode, annotated with labels:

        set b 99
        set c b
        jnz a label1
        jnz 1 label2
    label1:
        mul b 100
        sub b -100000
        set c b
        sub c -17000
    label2:
        set f 1
        set d 2
    label4a:
        set e 2
    label4b:
        set g d
        mul g e
        sub g b
        jnz g label3
        set f 0
    label3:
        sub e -1
        set g e
        sub g b
        jnz g label4b
        sub d -1
        set g d
        sub g b
        jnz g label4a
        jnz f label5
        sub h -1
    label5:
        set g b
        sub g c
        jnz g label6
        jnz 1 end
    label6:
        sub b -17
        jnz 1 label2
    end:

Direct translation to Kotlin (collapsing register g into expressions):

    fun function(debug: Boolean) {
        var b = 99
        var c = 99
    
        if (!debug) {
            b = b * 100 + 100000
            c = b + 17000
        }
    
        while (true) {
            var f = 1
            var d = 2
            var e = 2
    
            while (true) {
                if (d * e == b) {
                    f = 0
                }
    
                e++
                if (e != b) {
                    continue
                }
    
                e = 2
    
                d++
                if (d != b) {
                    continue;
                }
    
                if (f == 0) {
                    h++
                }
    
                if (b == c) {
                    return
                }
                break;
            }
            b += 17
        }
    }
    
Simplified:

    fun findComposites(debug: Boolean): Int {
        var b = 99
        var c = 99
    
        if (!debug) {
            b = b * 100 + 100000
            c = b + 17000
        }
    
        while (true) {
            var isPrime = true
            
            for (d in 2 until b) {
                for (e in 2 until b) {
                    if (d * e == b) {
                        isPrime = false
                    }
                }
            }
            
            if (!isPrime) { 
                composite++
            }
           
            if (b == c) {
                return composite
            }
            
            b += 17
        }
    }
