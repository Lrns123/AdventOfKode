package dev.lfelzinga.adventofkode.puzzles.aoc17.day5

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class JumpPuzzle : Puzzle<Sequence<String>>(2017, 5, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val jumps = input.map { it.toInt() }.toList().toTypedArray()

        var position = 0
        var steps = 0

        while (position in jumps.indices) {
            position += jumps[position]++
            steps++
        }

        return steps.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val jumps = input.map { it.toInt() }.toList().toTypedArray()

        var position = 0
        var steps = 0

        while (position in jumps.indices) {
            val offset = jumps[position]

            if (offset >= 3) {
                --jumps[position]
            } else {
                ++jumps[position]
            }

            position += offset
            steps++
        }

        return steps.toString()
    }
}
