package dev.lfelzinga.adventofkode.puzzles.aoc17.day11

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.TokenizingTransformer
import kotlin.math.absoluteValue

class HexGridPuzzle : Puzzle<List<String>>(2017, 11, TokenizingTransformer(",")) {
    enum class Direction(val dx: Int, val dy: Int, val dz: Int) {
        NORTH(0, 1 ,-1),
        NORTHEAST(1, 0, -1),
        SOUTHEAST(1, -1, 0),
        SOUTH(0, -1, 1),
        SOUTHWEST(-1,0,1),
        NORTHWEST(-1, 1, 0)
    }

    override fun solveFirstPart(input: List<String>): String {
        var x = 0
        var y = 0
        var z = 0

        input.map { when(it) {
            "n" -> Direction.NORTH
            "ne" -> Direction.NORTHEAST
            "se" -> Direction.SOUTHEAST
            "s" -> Direction.SOUTH
            "sw" -> Direction.SOUTHWEST
            "nw" -> Direction.NORTHWEST
            else -> throw IllegalArgumentException("Unknown direction $it")
        }}.forEach {
            x += it.dx
            y += it.dy
            z += it.dz
        }

        return maxOf(x.absoluteValue, y.absoluteValue, z.absoluteValue).toString()
    }

    override fun solveSecondPart(input: List<String>): String {
        var x = 0
        var y = 0
        var z = 0
        var maxDist = 0

        input.map { when(it) {
            "n" -> Direction.NORTH
            "ne" -> Direction.NORTHEAST
            "se" -> Direction.SOUTHEAST
            "s" -> Direction.SOUTH
            "sw" -> Direction.SOUTHWEST
            "nw" -> Direction.NORTHWEST
            else -> throw IllegalArgumentException("Unknown direction $it")
        }}.forEach {
            x += it.dx
            y += it.dy
            z += it.dz

            maxDist = maxOf(maxDist, maxOf(x.absoluteValue, y.absoluteValue, z.absoluteValue))
        }

        return maxDist.toString()
    }
}
