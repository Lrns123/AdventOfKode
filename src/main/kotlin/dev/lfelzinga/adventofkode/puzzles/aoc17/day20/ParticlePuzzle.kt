package dev.lfelzinga.adventofkode.puzzles.aoc17.day20

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Vector3L

class ParticlePuzzle : Puzzle<Sequence<String>>(2017, 20, LinesTransformer()) {
    private val particleRegex =
        Regex("""p=<([0-9-]+),([0-9-]+),([0-9-]+)>, v=<([0-9-]+),([0-9-]+),([0-9-]+)>, a=<([0-9-]+),([0-9-]+),([0-9-]+)>""")

    class Particle(var position: Vector3L, private var velocity: Vector3L, private val acceleration: Vector3L) {
        fun estimatePosition(steps: Long) = position + (velocity * steps + (acceleration * steps).squared() / 2)

        fun step(): Vector3L {
            velocity += acceleration
            position += velocity

            return position
        }
    }

    private fun parseInput(input: Sequence<String>) = input
        .map {
            particleRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse particle $it")
        }
        .map { (px, py, pz, vx, vy, vz, ax, ay, az) ->
            Particle(
                Vector3L(px.toLong(), py.toLong(), pz.toLong()),
                Vector3L(vx.toLong(), vy.toLong(), vz.toLong()),
                Vector3L(ax.toLong(), ay.toLong(), az.toLong())
            )
        }

    override fun solveFirstPart(input: Sequence<String>): String {
        return parseInput(input)
            .map { it.estimatePosition(1_000_000).magnitude }
            .withIndex()
            .minByOrNull { (_, position) -> position }
            ?.index.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val particles = parseInput(input).toMutableList()

        repeat(50) {
            particles.groupBy { it.position }.forEach { (_, list) ->
                if (list.size > 1) {
                    particles.removeAll(list)
                }
            }

            particles.forEach { it.step() }
        }

        return particles.size.toString()
    }
}
