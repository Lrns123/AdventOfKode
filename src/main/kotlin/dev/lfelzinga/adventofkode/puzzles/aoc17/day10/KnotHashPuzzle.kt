package dev.lfelzinga.adventofkode.puzzles.aoc17.day10

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import java.util.Collections.swap

class KnotHashPuzzle : Puzzle<String>(2017, 10, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        val list = List(256) { it }
        var position = 0

        for ((skip, length) in input.split(Regex(",")).map { it.toInt() }.withIndex()) {
            reverse(list, position, position + length - 1)
            position += length + skip
        }

        return (list[0] * list[1]).toString()
    }

    override fun solveSecondPart(input: String): String {
        val list = List(256) { it }
        var position = 0
        var skip = 0

        val lengths = input.trim().map { it.code } + listOf(17, 31, 73, 47, 23)

        repeat(64) {
            for (length in lengths) {
                reverse(list, position, position + length - 1)
                position += length + skip++
            }
        }

        return list.chunked(16) { it.reduce { lhs, rhs -> lhs xor rhs } }.joinToString("") {
            it.toString(16).padStart(2, '0')
        }
    }

    private fun reverse(list: List<*>, startIndex: Int, endIndex: Int) {
        var i = startIndex
        var j = endIndex

        while (i < j) {
            swap(list, i++ % list.size, j-- % list.size)
        }
    }
}
