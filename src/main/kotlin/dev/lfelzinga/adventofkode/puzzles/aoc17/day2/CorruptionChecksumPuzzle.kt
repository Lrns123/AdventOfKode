package dev.lfelzinga.adventofkode.puzzles.aoc17.day2

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.combinations

class CorruptionChecksumPuzzle : Puzzle<Sequence<String>>(2017, 2, LinesTransformer()) {
    private val whitespaceRegex = Regex("\\s+")

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { line -> line.split(whitespaceRegex).map { it.toInt() } }
            .sumOf { (it.maxOrNull() ?: 0) - (it.minOrNull() ?: 0) }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { line -> line.split(whitespaceRegex).map { it.toInt() } }
            .map { findDivisors(it) }
            .sumOf { it.first / it.second }
            .toString()
    }

    private fun findDivisors(numbers: List<Int>): Pair<Int, Int> {
        return numbers.combinations(2)
            .flatMap { sequenceOf(it[0] to it[1], it[1] to it[0]) }
            .first { it.first % it.second == 0 }
    }
}
