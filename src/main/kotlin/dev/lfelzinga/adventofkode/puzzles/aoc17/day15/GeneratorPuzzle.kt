package dev.lfelzinga.adventofkode.puzzles.aoc17.day15

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class GeneratorPuzzle : Puzzle<Sequence<String>>(2017, 15, LinesTransformer()) {
    private val generatorRegex = Regex("""Generator (\w) starts with (\d+)""")

    class Generator(seed: Long, private val factor: Long) {
        var value = seed

        fun next(): Long {
            value = value * factor % 2147483647L
            return value
        }
    }

    class FilteredGenerator(seed: Long, private val factor: Long, private val filter: Long) {
        var value = seed

        fun next(): Long {
            do {
                value = value * factor % 2147483647L
            } while (value % filter != 0L)

            return value
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val seeds = input
            .map { generatorRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse $it") }
            .map { (gen, startingValue) -> gen to startingValue.toLong() }
            .toMap()

        val generatorA = Generator(seeds.getValue("A"), 16807)
        val generatorB = Generator(seeds.getValue("B"), 48271)

        var score = 0

        repeat(40_000_000) {
            if (generatorA.next() and 0xFFFF == generatorB.next() and 0xFFFF) {
                score++
            }
        }

        return score.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val seeds = input
            .map { generatorRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse $it") }
            .map { (gen, startingValue) -> gen to startingValue.toLong() }
            .toMap()

        val generatorA = FilteredGenerator(seeds.getValue("A"), 16807, 4)
        val generatorB = FilteredGenerator(seeds.getValue("B"), 48271, 8)

        var score = 0

        repeat(5_000_000) {
            if (generatorA.next() and 0xFFFF == generatorB.next() and 0xFFFF) {
                score++
            }
        }

        return score.toString()
    }
}
