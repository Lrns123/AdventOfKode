package dev.lfelzinga.adventofkode.puzzles.aoc18.day8

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.TokenizingTransformer

class LicensePuzzle : Puzzle<List<String>>(2018, 8, TokenizingTransformer(" ")) {

    class Node {
        val children = mutableListOf<Node>()
        val metadata = mutableListOf<Int>()

        fun nodeSum(): Int {
            return children.fold(metadata.sum()) { acc, node -> acc + node.nodeSum() }
        }

        fun nodeValue(): Int {
            return when {
                children.isEmpty() -> metadata.sum()
                else -> metadata
                    .mapNotNull { children.getOrNull(it - 1) }
                    .sumOf { it.nodeValue() }
            }
        }
    }

    private fun readNode(iterator: Iterator<Int>): Node {
        val children = iterator.next()
        val metadata = iterator.next()
        val node = Node()

        repeat(children) {
            node.children += readNode(iterator)
        }

        repeat(metadata) {
            node.metadata += iterator.next()
        }

        return node
    }

    override fun solveFirstPart(input: List<String>): String {
        return readNode(input.map { it.toInt() }.iterator()).nodeSum().toString()
    }

    override fun solveSecondPart(input: List<String>): String {
        return readNode(input.map { it.toInt() }.iterator()).nodeValue().toString()
    }
}
