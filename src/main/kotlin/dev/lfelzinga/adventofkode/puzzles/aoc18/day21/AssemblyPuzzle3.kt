package dev.lfelzinga.adventofkode.puzzles.aoc18.day21

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.lang.IllegalArgumentException

class AssemblyPuzzle3 : Puzzle<Sequence<String>>(2018, 21, LinesTransformer()) {
    private val instructionRegex = Regex("""(\w+) (\d+) (\d+) (\d+)""")

    private val opcodes = mapOf<String, (operands: List<Int>, registers: MutableList<Int>) -> Unit>(
        "addr" to { op, reg -> reg[op[2]] = reg[op[0]] + reg[op[1]] },
        "addi" to { op, reg -> reg[op[2]] = reg[op[0]] + op[1] },

        "mulr" to { op, reg -> reg[op[2]] = reg[op[0]] * reg[op[1]] },
        "muli" to { op, reg -> reg[op[2]] = reg[op[0]] * op[1] },

        "banr" to { op, reg -> reg[op[2]] = reg[op[0]] and reg[op[1]] },
        "bani" to { op, reg -> reg[op[2]] = reg[op[0]] and op[1] },

        "borr" to { op, reg -> reg[op[2]] = reg[op[0]] or reg[op[1]] },
        "bori" to { op, reg -> reg[op[2]] = reg[op[0]] or op[1] },

        "setr" to { op, reg -> reg[op[2]] = reg[op[0]] },
        "seti" to { op, reg -> reg[op[2]] = op[0] },

        "gtir" to { op, reg -> reg[op[2]] = if (op[0] > reg[op[1]]) 1 else 0 },
        "gtri" to { op, reg -> reg[op[2]] = if (reg[op[0]] > op[1]) 1 else 0 },
        "gtrr" to { op, reg -> reg[op[2]] = if (reg[op[0]] > reg[op[1]]) 1 else 0 },

        "eqir" to { op, reg -> reg[op[2]] = if (op[0] == reg[op[1]]) 1 else 0 },
        "eqri" to { op, reg -> reg[op[2]] = if (reg[op[0]] == op[1]) 1 else 0 },
        "eqrr" to { op, reg -> reg[op[2]] = if (reg[op[0]] == reg[op[1]]) 1 else 0 }
    )

    class Instruction(val operation: (operands: List<Int>, registers: MutableList<Int>) -> Unit, val operands: List<Int>) {
        operator fun invoke(registers: MutableList<Int>) {
            operation(operands, registers)
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        var instructionRegister = 0

        val instructions = input
            .mapNotNull {
                if (it.startsWith("#ip")) {
                    instructionRegister = it.split(" ")[1].toInt()
                    null
                } else {
                    instructionRegex.matchEntire(it)?.destructured
                        ?: throw IllegalArgumentException("Cannot parse line $it")
                }
            }
            .map { (opcode, op1, op2, op3) -> Instruction(
                operation = opcodes[opcode] ?: error("Unknown opcode $opcode"),
                operands = listOf(op1.toInt(), op2.toInt(), op3.toInt())
            ) }
            .toList()

        val registers = mutableListOf(0, 0, 0, 0, 0, 0)

        while (registers[instructionRegister] in instructions.indices) {
            val ip = registers[instructionRegister]
            if (ip == 28) {
                return registers[4].toString()
            }

            instructions[ip](registers)
            registers[instructionRegister]++
        }

        return "???"
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var instructionRegister = 0

        val instructions = input
            .mapNotNull {
                if (it.startsWith("#ip")) {
                    instructionRegister = it.split(" ")[1].toInt()
                    null
                } else {
                    instructionRegex.matchEntire(it)?.destructured
                        ?: throw IllegalArgumentException("Cannot parse line $it")
                }
            }
            .map { (opcode, op1, op2, op3) -> Instruction(
                operation = opcodes[opcode] ?: error("Unknown opcode $opcode"),
                operands = listOf(op1.toInt(), op2.toInt(), op3.toInt())
            ) }
            .toList()

        val registers = mutableListOf(0, 0, 0, 0, 0, 0)
        var lastFound = -1
        val visited = mutableSetOf<Int>()

        while (registers[instructionRegister] in instructions.indices) {
            val ip = registers[instructionRegister]
            if (ip == 28) {
                if (registers[4] in visited) {
                    return lastFound.toString()
                }

                lastFound = registers[4]
                visited += lastFound
            }

            if (ip == 17) {
                // Div optimization
                registers[3] /= 256
                registers[instructionRegister] = 8
            } else {
                instructions[ip](registers)
                registers[instructionRegister]++
            }
        }

        return "???"
    }
}
