package dev.lfelzinga.adventofkode.puzzles.aoc18.day18

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import java.lang.IllegalStateException

class LumberPuzzle : Puzzle<Sequence<String>>(2018, 18, LinesTransformer()) {

    private fun runGeneration(current: CharGrid) : CharGrid {
        val nextGeneration = CharGrid(current.width, current.height)

        for (x in 0 until current.width) {
            for (y in 0 until current.height) {
                val currentState = current[x, y]
                var adjacentTrees = 0
                var adjacentLumberyards = 0

                for (dx in -1..1) {
                    for (dy in -1..1) {
                        if (!(dx == 0 && dy == 0) && x + dx in 0 until current.width && y + dy in 0 until current.height) {
                            when (current[x + dx, y + dy]) {
                                '|' -> adjacentTrees++
                                '#' -> adjacentLumberyards++
                            }
                        }
                    }
                }


                val newState = when (currentState) {
                    '.' -> if (adjacentTrees >= 3) '|' else currentState
                    '|' -> if (adjacentLumberyards >= 3) '#' else currentState
                    '#' -> if (adjacentLumberyards < 1 || adjacentTrees < 1) '.' else currentState
                    else -> throw IllegalStateException("Invalid state $currentState")
                }

                nextGeneration[x, y] = newState
            }
        }

        return nextGeneration
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        var generation = CharGrid(50, 50)
        val initialState = input.toList()

        for (x in 0 until 50) {
            for (y in 0 until 50) {
                generation[x, y] = initialState[y][x]
            }
        }

        repeat(10) {
            generation = runGeneration(generation)
        }

        return (generation.array.count { it == '#' } * generation.array.count { it == '|' }).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var generation = CharGrid(50, 50)
        val initialState = input.toList()

        for (x in 0 until 50) {
            for (y in 0 until 50) {
                generation[x, y] = initialState[y][x]
            }
        }

        val encounteredStates = mutableSetOf(generation)
        val states = mutableListOf(generation)

        repeat(1000) { gen ->
            generation = runGeneration(generation)

            if (!encounteredStates.add(generation)) {
                val previousGen = states.indexOf(generation)
                val loopLength = (gen + 1) - previousGen
                val finalState = previousGen + (1_000_000_000 - previousGen) % loopLength
                val finalGen = states[finalState]

                return (finalGen.array.count { it == '#' } * finalGen.array.count { it == '|' }).toString()
            }

            states += generation
        }

        return "???"
    }
}
