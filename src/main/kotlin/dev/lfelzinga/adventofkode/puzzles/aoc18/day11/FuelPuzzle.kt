package dev.lfelzinga.adventofkode.puzzles.aoc18.day11

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.NumberTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import kotlin.math.max

class FuelPuzzle : Puzzle<Long>(2018, 11, NumberTransformer()) {
    override fun solveFirstPart(input: Long): String {
        val grid = getFuelGrid(input)
        val (rowSums, _) = computeRunningSums(grid)

        var maxFuel = Int.MIN_VALUE
        var maxX = 0
        var maxY = 0

        for (x in 0..297) {
            for (y in 0..297) {
                val fuel = rowSums[y][x + 3] - rowSums[y][x] +
                    rowSums[y + 1][x + 3] - rowSums[y + 1][x] +
                    rowSums[y + 2][x + 3] - rowSums[y + 2][x]

                if (fuel > maxFuel) {
                    maxFuel = fuel
                    maxX = x
                    maxY = y
                }

            }
        }

        return "${maxX + 1},${maxY + 1}"
    }

    override fun solveSecondPart(input: Long): String {
        val grid = getFuelGrid(input)
        val (rowSums, colSums) = computeRunningSums(grid)

        var maxFuel = Int.MIN_VALUE
        var maxX = 0
        var maxY = 0
        var maxSize = 0

        for (x in 0..299) {
            for (y in 0..299) {
                val maxAllowedSize = (299 - max(x, y))
                var fuel = 0
                for (size in 0 until maxAllowedSize) {
                    fuel += rowSums[y + size][x + size + 1] - rowSums[y + size][x]
                    fuel += colSums[x + size][y + size] - colSums[x + size][y]

                    if (fuel > maxFuel) {
                        maxFuel = fuel
                        maxX = x
                        maxY = y
                        maxSize = size
                    }

                }
            }
        }


        return "${maxX + 1},${maxY + 1},${maxSize + 1}"
    }

    private fun getFuelGrid(serial: Long): IntGrid {
        val grid = IntGrid(300, 300)

        for (x in 1..300) {
            for (y in 1..300) {
                val rackId = x + 10

                grid[x - 1, y - 1] = ((((rackId * y + serial) * rackId) / 100) % 10).toInt() - 5
            }
        }

        return grid
    }

    private fun computeRunningSums(grid: IntGrid): Pair<Array<IntArray>, Array<IntArray>> {
        val rowSums = Array(grid.height) { IntArray(grid.width + 1) }
        val colSums = Array(grid.width) { IntArray(grid.height + 1) }

        for (row in 0 until grid.height) {
            var sum = 0
            val sums = rowSums[row]

            for (x in 0 until grid.width) {
                sum += grid[x, row]
                sums[x + 1] = sum
            }
        }

        for (col in 0 until grid.width) {
            var sum = 0
            val sums = colSums[col]

            for (y in 0 until grid.height) {
                sum += grid[col, y]
                sums[y + 1] = sum
            }
        }

        return rowSums to colSums
    }
}
