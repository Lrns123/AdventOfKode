package dev.lfelzinga.adventofkode.puzzles.aoc18.day20

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import java.util.ArrayDeque

class MapPuzzle : Puzzle<String>(2018, 20, StringTransformer()) {
    private val doorSymbols = setOf('|', '-')


    private fun createRoom(grid: CharGrid, x: Int, y: Int) {
        fun addDoor(x: Int, y: Int) {
            if (grid[x, y] !in doorSymbols) {
                grid[x, y] = '?'
            }
        }

        grid[x - 1, y - 1] = '#'
        grid[x - 1, y + 1] = '#'
        grid[x + 1, y - 1] = '#'
        grid[x + 1, y + 1] = '#'

        addDoor(x - 1, y)
        addDoor(x + 1, y)
        addDoor(x, y - 1)
        addDoor(x, y + 1)
    }

    private fun finalizeRooms(grid: CharGrid) {
        for (i in grid.array.indices) {
            if (grid.array[i] == '?') {
                grid.array[i] = '#'
            }
        }
    }

    private fun explorePath(grid: CharGrid, startX: Int, startY: Int, tokens: CharIterator) {
        var x = startX
        var y = startY

        while (true) {
            when (tokens.nextChar()) {
                'N' -> {
                    grid[x, y - 1] = '-'
                    y -= 2
                    createRoom(grid, x, y)
                }
                'S' -> {
                    grid[x, y + 1] = '-'
                    y += 2
                    createRoom(grid, x, y)
                }
                'E' -> {
                    grid[x + 1, y] = '|'
                    x += 2
                    createRoom(grid, x, y)
                }
                'W' -> {
                    grid[x - 1, y] = '|'
                    x -= 2
                    createRoom(grid, x, y)
                }
                '(' -> explorePath(grid, x, y, tokens)
                ')', '$' -> return
                '|' -> {
                    x = startX
                    y = startY
                }
                '^' -> { /* do nothing */ }
            }
        }
    }

    private fun findLongestPath(grid: CharGrid, startX: Int, startY: Int): Int {
        data class Node(val x: Int, val y: Int, val distance: Int) {
            val position = x to y
        }

        val queue = ArrayDeque<Node>()
        val visited = mutableSetOf<Pair<Int, Int>>()

        queue += Node(startX, startY, 0)

        var maxDistance = 0

        while (queue.isNotEmpty()) {
            val node = queue.pop()

            if (!visited.add(node.position)) {
                continue
            }

            if (node.distance > maxDistance) {
                maxDistance = node.distance
            }

            if (grid[node.x - 1, node.y] != '#') {
                queue += Node(node.x - 2, node.y, node.distance + 1)
            }

            if (grid[node.x + 1, node.y] != '#') {
                queue += Node(node.x + 2, node.y, node.distance + 1)
            }

            if (grid[node.x, node.y - 1] != '#') {
                queue += Node(node.x, node.y - 2, node.distance + 1)
            }

            if (grid[node.x, node.y + 1] != '#') {
                queue += Node(node.x, node.y + 2, node.distance + 1)
            }
        }

        return maxDistance
    }

    private fun findRoomsWithMinimalShortestPath(grid: CharGrid, startX: Int, startY: Int, minDistance: Int): Int {
        data class Node(val x: Int, val y: Int, val distance: Int) {
            val position = x to y
        }

        val queue = ArrayDeque<Node>()
        val visited = mutableSetOf<Pair<Int, Int>>()

        queue += Node(startX, startY, 0)

        var numRooms = 0

        while (queue.isNotEmpty()) {
            val node = queue.pop()

            if (!visited.add(node.position)) {
                continue
            }

            if (node.distance >= minDistance) {
                numRooms++
            }

            if (grid[node.x - 1, node.y] != '#') {
                queue += Node(node.x - 2, node.y, node.distance + 1)
            }

            if (grid[node.x + 1, node.y] != '#') {
                queue += Node(node.x + 2, node.y, node.distance + 1)
            }

            if (grid[node.x, node.y - 1] != '#') {
                queue += Node(node.x, node.y - 2, node.distance + 1)
            }

            if (grid[node.x, node.y + 1] != '#') {
                queue += Node(node.x, node.y + 2, node.distance + 1)
            }
        }

        return numRooms
    }

    override fun solveFirstPart(input: String): String {
        val grid = CharGrid(250, 250).filledWith('.')

        createRoom(grid, 125, 125)
        explorePath(grid, 125, 125, input.iterator())
        finalizeRooms(grid)

        return findLongestPath(grid, 125, 125).toString()
    }

    override fun solveSecondPart(input: String): String {
        val grid = CharGrid(250, 250).filledWith('.')

        createRoom(grid, 125, 125)
        explorePath(grid, 125, 125, input.iterator())
        finalizeRooms(grid)

        return findRoomsWithMinimalShortestPath(grid, 125, 125, 1000).toString()
    }
}
