package dev.lfelzinga.adventofkode.puzzles.aoc18.day19

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.lang.IllegalArgumentException

class AssemblyPuzzle2 : Puzzle<Sequence<String>>(2018, 19, LinesTransformer()) {
    private val instructionRegex = Regex("""(\w+) (\d+) (\d+) (\d+)""")

    private val opcodes = mapOf<String, (operands: List<Int>, registers: MutableList<Int>) -> Unit>(
        "addr" to { op, reg -> reg[op[2]] = reg[op[0]] + reg[op[1]] },
        "addi" to { op, reg -> reg[op[2]] = reg[op[0]] + op[1] },

        "mulr" to { op, reg -> reg[op[2]] = reg[op[0]] * reg[op[1]] },
        "muli" to { op, reg -> reg[op[2]] = reg[op[0]] * op[1] },

        "banr" to { op, reg -> reg[op[2]] = reg[op[0]] and reg[op[1]] },
        "bani" to { op, reg -> reg[op[2]] = reg[op[0]] and op[1] },

        "borr" to { op, reg -> reg[op[2]] = reg[op[0]] or reg[op[1]] },
        "bori" to { op, reg -> reg[op[2]] = reg[op[0]] or op[1] },

        "setr" to { op, reg -> reg[op[2]] = reg[op[0]] },
        "seti" to { op, reg -> reg[op[2]] = op[0] },

        "gtir" to { op, reg -> reg[op[2]] = if (op[0] > reg[op[1]]) 1 else 0 },
        "gtri" to { op, reg -> reg[op[2]] = if (reg[op[0]] > op[1]) 1 else 0 },
        "gtrr" to { op, reg -> reg[op[2]] = if (reg[op[0]] > reg[op[1]]) 1 else 0 },

        "eqir" to { op, reg -> reg[op[2]] = if (op[0] == reg[op[1]]) 1 else 0 },
        "eqri" to { op, reg -> reg[op[2]] = if (reg[op[0]] == op[1]) 1 else 0 },
        "eqrr" to { op, reg -> reg[op[2]] = if (reg[op[0]] == reg[op[1]]) 1 else 0 }
    )

    class Instruction(val operation: (operands: List<Int>, registers: MutableList<Int>) -> Unit, val operands: List<Int>) {
        operator fun invoke(registers: MutableList<Int>) {
            operation(operands, registers)
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        var instructionRegister = 0

        val instructions = input
            .mapNotNull {
                if (it.startsWith("#ip")) {
                    instructionRegister = it.split(" ")[1].toInt()
                    null
                } else {
                    instructionRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it")
                }
            }
            .map { (opcode, op1, op2, op3) -> Instruction(
                operation = opcodes[opcode] ?: error("Unknown opcode $opcode"),
                operands = listOf(op1.toInt(), op2.toInt(), op3.toInt())
            ) }
            .toList()

        val registers = mutableListOf(0, 0, 0, 0, 0, 0)

        while (registers[instructionRegister] in instructions.indices) {
            instructions[registers[instructionRegister]](registers)
            registers[instructionRegister]++
        }

        return registers[0].toString()
    }

    /*
        Analysis of program for part 2:

        // Jump to setup:
        addi 3 16 3 -> ip += 16

        // Compute factors of r2, storing the sum in r0 (using r1 and r4 as factors to test)
        seti 1 0 4  -> r4 = 1
        seti 1 0 1  -> r1 = 1
        mulr 4 1 5  -> r5 = r4 * r1
        eqrr 5 2 5  -> r5 = r5 == r2
        addr 5 3 3  -> ip += r5
        addi 3 1 3  -> ip += 3
        addr 4 0 0  -> r0 += r4
        addi 1 1 1  -> r1 += 1
        gtrr 1 2 5  -> r5 = r1 > r2
        addr 3 5 3  -> ip += r5
        seti 2 9 3  -> ip = 2
        addi 4 1 4  -> r4 += 1
        gtrr 4 2 5  -> r5 = r4 > r2
        addr 5 3 3  -> ip += r5
        seti 1 2 3  -> ip = 1
        mulr 3 3 3  -> ip *= ip (exit)

        // Generate number to factorize
        addi 2 2 2  -> r2 += 2
        mulr 2 2 2  -> r2 *= r2
        mulr 3 2 2  -> r2 *= ip
        muli 2 11 2 -> r2 *= 11
        addi 5 4 5  -> r5 += 4
        mulr 5 3 5  -> r5 *= ip
        addi 5 16 5 -> r5 += 16
        addr 2 5 2  -> r2 += r5
        addr 3 0 3  -> ip += r0 (part 2)

        // Increase number to factorize if r0 == 1
        seti 0 8 3	-> ip = 0
        setr 3 2 5  -> r5 = ip
        mulr 5 3 5  -> r5 *= ip
        addr 3 5 5  -> r5 += ip
        mulr 3 5 5  -> r5 *= ip
        muli 5 14 5 -> r5 *= 14
        mulr 5 3 5  -> r5 *= ip
        addr 2 5 2  -> r2 += r5
        seti 0 0 0  -> r0 = 0
        seti 0 0 3  -> ip = 0
     */

    private fun getFactors(number: Int): List<Int> {
        val factors = mutableListOf<Int>()

        for (i in 1..number) {
            if (number % i == 0) {
                factors += i
            }
        }

        return factors
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var instructionRegister = 0

        val instructions = input
            .mapNotNull {
                if (it.startsWith("#ip")) {
                    instructionRegister = it.split(" ")[1].toInt()
                    null
                } else {
                    instructionRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it")
                }
            }
            .map { (opcode, op1, op2, op3) -> Instruction(
                operation = opcodes[opcode] ?: error("Unknown opcode $opcode"),
                operands = listOf(op1.toInt(), op2.toInt(), op3.toInt())
            ) }
            .toList()

        val registers = mutableListOf(1, 0, 0, 0, 0, 0)

        // The program computes the factors of a number, generated based on register 0
        // The number to computer the primes of is stored in r2, and the resulting factor sum is stored in r0

        // Running the program for a few (50) instructions, will populate r2 with the number to factorize

        repeat(50) {
            instructions[registers[instructionRegister]](registers)
            registers[instructionRegister]++
        }

        return getFactors(registers[2]).sum().toString()
    }
}
