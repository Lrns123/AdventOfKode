package dev.lfelzinga.adventofkode.puzzles.aoc18.day16

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.lang.IllegalArgumentException

class AssemblyPuzzle : Puzzle<Sequence<String>>(2018, 16, LinesTransformer()) {
    private val sampleRegex = Regex("""\w+:\s+\[(\d+), (\d+), (\d+), (\d+)]""")
    private val opcodeRegex = Regex("""(\d+) (\d+) (\d+) (\d+)""")

    private val opcodes = mapOf<String, (operands: List<Int>, registers: MutableList<Int>) -> Unit>(
        "addr" to { op, reg -> reg[op[2]] = reg[op[0]] + reg[op[1]] },
        "addi" to { op, reg -> reg[op[2]] = reg[op[0]] + op[1] },

        "mulr" to { op, reg -> reg[op[2]] = reg[op[0]] * reg[op[1]] },
        "muli" to { op, reg -> reg[op[2]] = reg[op[0]] * op[1] },

        "banr" to { op, reg -> reg[op[2]] = reg[op[0]] and reg[op[1]] },
        "bani" to { op, reg -> reg[op[2]] = reg[op[0]] and op[1] },

        "borr" to { op, reg -> reg[op[2]] = reg[op[0]] or reg[op[1]] },
        "bori" to { op, reg -> reg[op[2]] = reg[op[0]] or op[1] },

        "setr" to { op, reg -> reg[op[2]] = reg[op[0]] },
        "seti" to { op, reg -> reg[op[2]] = op[0] },

        "gtir" to { op, reg -> reg[op[2]] = if (op[0] > reg[op[1]]) 1 else 0 },
        "gtri" to { op, reg -> reg[op[2]] = if (reg[op[0]] > op[1]) 1 else 0 },
        "gtrr" to { op, reg -> reg[op[2]] = if (reg[op[0]] > reg[op[1]]) 1 else 0 },

        "eqir" to { op, reg -> reg[op[2]] = if (op[0] == reg[op[1]]) 1 else 0 },
        "eqri" to { op, reg -> reg[op[2]] = if (reg[op[0]] == op[1]) 1 else 0 },
        "eqrr" to { op, reg -> reg[op[2]] = if (reg[op[0]] == reg[op[1]]) 1 else 0 }
    )

    class Instruction(val opcode: Int, val operands: List<Int>)
    class Sample(val before: List<Int>, val instruction: Instruction, val after: List<Int>)

    private fun eligibleOpcodesForSample(sample: Sample): Set<String> {
        val eligible = mutableSetOf<String>()

        for ((opcode, func) in opcodes) {
            try {
                val registers = ArrayList(sample.before)

                func(sample.instruction.operands, registers)

                if (registers == sample.after) {
                    eligible += opcode
                }
            } catch (ex: Exception) {
                // Ignore
            }
        }

        return eligible
    }

    private fun resolveOpcodes(samples: List<Sample>): Map<Int, String> {
        val candidates = mutableMapOf<Int, MutableSet<String>>()

        for (sample in samples) {
            val remaining = candidates.computeIfAbsent(sample.instruction.opcode) { opcodes.keys.toMutableSet() }
            remaining.retainAll(eligibleOpcodesForSample(sample))
        }

        while (candidates.any { it.value.size > 1 }) {
            val identified = candidates.filter { it.value.size == 1 }.map { it.value.first() }.toSet()

            for ((_, value) in candidates) {
                if (value.size > 1) {
                    value.removeAll(identified)
                }
            }
        }

        return candidates.mapValues { it.value.first() }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val iterator = input.iterator()
        val samples = mutableListOf<Sample>()

        while (iterator.hasNext()) {
            val line = iterator.next()
            if (line.startsWith("Before:")) {
                val before = sampleRegex.matchEntire(line)
                    ?.destructured
                    ?.let { (r1, r2, r3, r4) -> listOf(r1.toInt(), r2.toInt(), r3.toInt(), r4.toInt())}
                    ?: throw IllegalArgumentException("Cannot parse Before: line ($line)")

                val instructionLine = iterator.next()
                val instruction = opcodeRegex.matchEntire(instructionLine)
                    ?.destructured
                    ?.let { (opcode, op1, op2, op3) -> Instruction(opcode.toInt(), listOf(op1.toInt(), op2.toInt(), op3.toInt())) }
                    ?: throw IllegalArgumentException("Cannot parse instruction line ($instructionLine)")

                val afterLine = iterator.next()
                val after = sampleRegex.matchEntire(afterLine)
                    ?.destructured
                    ?.let { (r1, r2, r3, r4) -> listOf(r1.toInt(), r2.toInt(), r3.toInt(), r4.toInt())}
                    ?: throw IllegalArgumentException("Cannot parse After: line ($afterLine)")

                samples += Sample(before, instruction, after)
            }
        }

        return samples.count { eligibleOpcodesForSample(it).size >= 3 }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val iterator = input.iterator()
        val samples = mutableListOf<Sample>()
        val instructions = mutableListOf<Instruction>()

        while (iterator.hasNext()) {
            val line = iterator.next()
            if (line.startsWith("Before:")) {
                val before = sampleRegex.matchEntire(line)
                    ?.destructured
                    ?.let { (r1, r2, r3, r4) -> listOf(r1.toInt(), r2.toInt(), r3.toInt(), r4.toInt())}
                    ?: throw IllegalArgumentException("Cannot parse Before: line ($line)")

                val instructionLine = iterator.next()
                val instruction = opcodeRegex.matchEntire(instructionLine)
                    ?.destructured
                    ?.let { (opcode, op1, op2, op3) -> Instruction(opcode.toInt(), listOf(op1.toInt(), op2.toInt(), op3.toInt())) }
                    ?: throw IllegalArgumentException("Cannot parse instruction line ($instructionLine)")

                val afterLine = iterator.next()
                val after = sampleRegex.matchEntire(afterLine)
                    ?.destructured
                    ?.let { (r1, r2, r3, r4) -> listOf(r1.toInt(), r2.toInt(), r3.toInt(), r4.toInt())}
                    ?: throw IllegalArgumentException("Cannot parse After: line ($afterLine)")

                samples += Sample(before, instruction, after)
            } else if (line.isNotBlank()) {
                val instruction = opcodeRegex.matchEntire(line)
                    ?.destructured
                    ?.let { (opcode, op1, op2, op3) -> Instruction(opcode.toInt(), listOf(op1.toInt(), op2.toInt(), op3.toInt())) }
                    ?: throw IllegalArgumentException("Cannot parse instruction line ($line)")

                instructions += instruction
            }
        }

        val opcodeMapping = resolveOpcodes(samples)
        val registers = mutableListOf(0, 0, 0, 0)

        instructions.forEach {
            val opcode = opcodes[opcodeMapping[it.opcode]] ?: throw IllegalArgumentException("Unknown opcode $it")
            opcode(it.operands, registers)
        }

        return registers[0].toString()
    }
}
