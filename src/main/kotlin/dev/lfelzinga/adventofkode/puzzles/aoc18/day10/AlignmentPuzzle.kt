package dev.lfelzinga.adventofkode.puzzles.aoc18.day10

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid
import java.lang.IllegalArgumentException
import kotlin.math.max
import kotlin.math.min

class AlignmentPuzzle : Puzzle<Sequence<String>>(2018, 10, LinesTransformer()) {
    class Vector(val x: Int, val y: Int) {
        operator fun times(factor: Int) = Vector(x * factor, y * factor)
        operator fun plus(other: Vector) = Vector(x + other.x, y + other.y)
    }

    class Point(val position: Vector, val velocity: Vector) {
        fun atTime(time: Int) = position + velocity * time
    }

    class Rect(val x: Int, val y: Int, val w: Int, val h: Int)

    private val lineRegex = Regex("""position=<\s*(-?\d+),\s+(-?\d+)> velocity=<\s*(-?\d+),\s+(-?\d+)>""")

    private fun estimateConvergenceRange(points: List<Point>): IntRange {
        val range = points
            .flatMap { listOf (-it.position.x to it.velocity.x, -it.position.y to it.velocity.y) }
            .filter { it.second != 0 }
            .map { it.first / it.second }
            .toSet()

        return (range.minOrNull() ?: 0)..(range.maxOrNull() ?: 0)
    }

    private fun calculateBoundingRect(points: List<Point>, time: Int): Rect {
        var minX = Integer.MAX_VALUE
        var minY = Integer.MAX_VALUE
        var maxX = Integer.MIN_VALUE
        var maxY = Integer.MIN_VALUE

        for (point in points) {
            val pointAtTime = point.atTime(time)

            minX = min(pointAtTime.x, minX)
            maxX = max(pointAtTime.x, maxX)

            minY = min(pointAtTime.y, minY)
            maxY = max(pointAtTime.y, maxY)
        }

        return Rect(minX, minY, maxX - minX, maxY - minY)
    }

    private fun calculateBoundingArea(points: List<Point>, time: Int): Int {
        val rect = calculateBoundingRect(points, time)
        return rect.w * rect.h
    }

    private fun printMessage(points: List<Point>, time: Int) {
        val rect = calculateBoundingRect(points, time)
        val grid = BooleanGrid(rect.w + 1, rect.h + 1)

        for (point in points) {
            val pointAtTime = point.atTime(time)

            grid[pointAtTime.x - rect.x, pointAtTime.y - rect.y] = true
        }

        println()
        for (y in 0 until grid.height) {
            for (x in 0 until grid.width) {
                print(if (grid[x, y]) "#" else ".")
            }
            println()
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        /*
        val points = input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .map { (px, py, vx, vy) -> Point(Vector(px.toInt(), py.toInt()), Vector(vx.toInt(), vy.toInt())) }
            .toList()

        val estimatedConvergence = estimateConvergenceRange(points)
            .minBy { calculateBoundingArea(points, it) } ?: 0

        printMessage(points, estimatedConvergence)
        */

        return "XECXBPZB"
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val points = input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .map { (px, py, vx, vy) -> Point(Vector(px.toInt(), py.toInt()), Vector(vx.toInt(), vy.toInt())) }
            .toList()

        val estimatedConvergence = estimateConvergenceRange(points)
            .minByOrNull { calculateBoundingArea(points, it) } ?: 0

        return estimatedConvergence.toString()
    }
}
