package dev.lfelzinga.adventofkode.puzzles.aoc18.day22

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import java.util.*
import kotlin.math.abs

class ModeMazePuzzle : Puzzle<Sequence<String>>(2018, 22, LinesTransformer()) {

    override fun solveFirstPart(input: Sequence<String>): String {
        val data = input.toList()
        val depth = data[0].split(": ")[1].toInt()
        val (tx, ty) = data[1].split(": ")[1].split(",").map { it.toInt() }

        val erosionGrid = IntGrid(tx + 1, ty + 1)

        for (x in 0..tx) {
            for (y in 0..ty) {
                val geologicIndex = if ((x == 0 && y == 0) || (x == tx && y == ty)) {
                    0
                } else if (y == 0) {
                    x * 16807
                } else if (x == 0) {
                    y * 48271
                } else {
                    erosionGrid[x - 1, y] * erosionGrid[x, y - 1]
                }

                erosionGrid[x, y] = (geologicIndex + depth) % 20183
            }
        }

        return erosionGrid.array.sumOf { it % 3 }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val data = input.toList()
        val depth = data[0].split(": ")[1].toInt()
        val (tx, ty) = data[1].split(": ")[1].split(",").map { it.toInt() }

        val erosionGrid = IntGrid(tx + 200, ty + 200)

        for (x in 0 until tx + 200) {
            for (y in 0 until ty + 200) {
                val geologicIndex = if ((x == 0 && y == 0) || (x == tx && y == ty)) {
                    0
                } else if (y == 0) {
                    x * 16807
                } else if (x == 0) {
                    y * 48271
                } else {
                    erosionGrid[x - 1, y] * erosionGrid[x, y - 1]
                }

                erosionGrid[x, y] = (geologicIndex + depth) % 20183
            }
        }

        return findShortestPath(erosionGrid, tx, ty).toString()
    }

    enum class TerrainType {
        ROCKY,
        WET,
        NARROW
    }

    enum class Gear(val usableIn: List<TerrainType>) {
        TORCH(listOf(TerrainType.ROCKY, TerrainType.NARROW)),
        CLIMBING_GEAR(listOf(TerrainType.ROCKY, TerrainType.WET)),
        NOTHING(listOf(TerrainType.WET, TerrainType.NARROW));
    }

    private fun findShortestPath(erosionGrid: IntGrid, targetX: Int, targetY: Int): Int {
        data class VisitedNode(val x: Int, val y: Int, val gear: Gear)

        class Node(val x: Int, val y: Int, val gear: Gear, val timeSpent: Int) : Comparable<Node> {
            override fun compareTo(other: Node): Int = score - other.score

            val heuristic = abs(x - targetX) + abs(y - targetY)
            val score = timeSpent + heuristic
        }

        fun terrainAt(x: Int, y: Int) = TerrainType.values()[erosionGrid[x, y] % 3]

        val queue = PriorityQueue<Node>()
        val visited = mutableSetOf<VisitedNode>()

        queue += Node(0, 0, Gear.TORCH, 0)

        while (queue.isNotEmpty()) {
            val node = queue.poll()

            if (!visited.add(VisitedNode(node.x, node.y, node.gear))) {
                continue
            }

            if (node.x == targetX && node.y == targetY && node.gear == Gear.TORCH) {
                return node.timeSpent
            }

            // Add movements
            if (node.x > 0 && terrainAt(node.x - 1, node.y) in node.gear.usableIn) {
                queue += Node(node.x - 1, node.y, node.gear, node.timeSpent + 1)
            }

            if (node.x < erosionGrid.width - 1 && terrainAt(node.x + 1, node.y) in node.gear.usableIn) {
                queue += Node(node.x + 1, node.y, node.gear, node.timeSpent + 1)
            }

            if (node.y > 0 && terrainAt(node.x, node.y - 1) in node.gear.usableIn) {
                queue += Node(node.x, node.y - 1, node.gear, node.timeSpent + 1)
            }

            if (node.y < erosionGrid.height - 1 && terrainAt(node.x, node.y + 1) in node.gear.usableIn) {
                queue += Node(node.x, node.y + 1, node.gear, node.timeSpent + 1)
            }

            // Add gear changes
            for (gear in Gear.values()) {
                if (gear != node.gear && terrainAt(node.x, node.y) in gear.usableIn) {
                    queue += Node(node.x, node.y, gear, node.timeSpent + 7)
                }
            }
        }

        return -1
    }
}
