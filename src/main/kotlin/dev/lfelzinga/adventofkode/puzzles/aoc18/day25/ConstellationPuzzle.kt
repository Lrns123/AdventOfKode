package dev.lfelzinga.adventofkode.puzzles.aoc18.day25

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.lang.IllegalArgumentException
import java.util.*
import kotlin.math.abs

class ConstellationPuzzle : Puzzle<Sequence<String>>(2018, 25, LinesTransformer()) {
    private val coordRegex = Regex("""(-?\d+),(-?\d+),(-?\d+),(-?\d+)""")

    data class Coordinate(val x: Int, val y: Int, val z: Int, val w: Int, var constellation: Int = -1) {
        fun distanceTo(other: Coordinate) = abs(x - other.x) + abs(y - other.y) + abs(z - other.z) + abs(w - other.w)
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val points = input
            .map { coordRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .map { (x, y, z, w) -> Coordinate(x.toInt(), y.toInt(), z.toInt(), w.toInt()) }
            .toList()

        var constellationsFound = 0

        val mappedPoints = mutableSetOf<Coordinate>()
        val queue = ArrayDeque<Coordinate>()

        while (mappedPoints.size != points.size) {

            queue.add((points - mappedPoints).first())

            while (queue.isNotEmpty()) {
                val point = queue.poll()

                if (!mappedPoints.add(point)) {
                    continue
                }

                val pointsInRange = points.filter { point.distanceTo(it) <= 3 }

                queue.addAll(pointsInRange)
            }

            constellationsFound++
        }

        return constellationsFound.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return "Merry Christmas!"
    }
}
