package dev.lfelzinga.adventofkode.puzzles.aoc18.day9

import java.util.*

class CircularDeque<T> : ArrayDeque<T>() {
    fun rotate(numPositions: Int) {
        when {
            numPositions == 0 -> return
            numPositions > 0 -> repeat(numPositions) { addFirst(removeLast()) }
            else -> repeat(-numPositions - 1) { addLast(removeFirst()) }
        }
    }
}
