package dev.lfelzinga.adventofkode.puzzles.aoc18.day12

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.util.BitSet

class PlantPuzzle : Puzzle<Sequence<String>>(2018, 12, LinesTransformer()) {
    private val initialStateRegex = Regex("""initial state: ([.#]+)""")
    private val transitionRegex = Regex("""([.#]{5}) => ([.#])""")

    class Transition(val pattern: String, val result: String)

    private fun patternMatches(pattern: String, pots: BitSet, offset: Int): Boolean {
        for (i in pattern.indices) {
            if (pots[i + offset - 2] != (pattern[i] == '#')) {
                return false
            }
        }

        return true
    }

    private fun runGeneration(currentGeneration: BitSet, transitions: List<Transition>): BitSet {
        val nextGeneration = BitSet(currentGeneration.size())

        for (offset in 2 until nextGeneration.size() - 2) {
            for (transition in transitions) {
                if (patternMatches(transition.pattern, currentGeneration, offset)) {
                    nextGeneration[offset] = (transition.result == "#")
                    break
                }
            }
        }

        return nextGeneration
    }

    private fun BitSet.potScore(offset: Int): Long {
        return (0 until size()).sumOf { if (this[it]) (it - offset).toLong() else 0L }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val offset = 250
        var pots = BitSet(500)
        val lines = input.toList()

        initialStateRegex.matchEntire(lines[0])?.destructured?.let { (initialState) ->
            initialState.forEachIndexed { index, char -> pots[index + offset] = char == '#' }
        }

        val transitions = lines
            .asSequence()
            .drop(2)
            .map { transitionRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .map { (pattern, result) -> Transition(pattern, result) }
            .toList()

        repeat(20) {
            pots = runGeneration(pots, transitions)
        }

        return pots.potScore(offset).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val offset = 500
        var pots = BitSet(1000)
        val generations = mutableListOf(pots)
        val lines = input.toList()

        initialStateRegex.matchEntire(lines[0])?.destructured?.let { (initialState) ->
            initialState.forEachIndexed { index, char -> pots[index + offset] = char == '#' }
        }

        val transitions = lines
            .asSequence()
            .drop(2)
            .map { transitionRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .map { (pattern, result) -> Transition(pattern, result) }
            .toList()

        for (generation in 1..300) {
            pots = runGeneration(pots, transitions)
            generations += pots
        }

        val baseScore = generations[298].potScore(offset)
        val scoreDelta = generations[299].potScore(offset) - baseScore

        return (baseScore + (50_000_000_000L - 298) * scoreDelta).toString()
    }
}
