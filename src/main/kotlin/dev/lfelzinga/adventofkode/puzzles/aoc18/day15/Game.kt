package dev.lfelzinga.adventofkode.puzzles.aoc18.day15

import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import java.util.*
import kotlin.math.abs

data class Vector(val x: Int, val y: Int) {
    fun distanceTo(other: Vector) = abs(x - other.x) + abs(y - other.y)

    operator fun plus(other: Vector) = Vector(x + other.x, y + other.y)
    operator fun minus(other: Vector) = Vector(x + other.x, y + other.y)
    operator fun times(factor: Int) = Vector(x * factor, y * factor)
}

fun Vector.adjacentPoints(): List<Vector> = listOf(
    this + Vector(0, -1),
    this + Vector(-1, 0),
    this + Vector(1, 0),
    this + Vector(0, 1)
)

sealed class Entity(var position: Vector, val symbol: Char) {
    class Goblin(position: Vector) : Entity(position, 'G')
    class Elf(position: Vector) : Entity(position, 'E')

    var dead: Boolean = false
    var health: Int = 200
    var attack: Int = 3
}

class Game(input: List<String>) {
    val grid: CharGrid = createGrid(input)
    val entities = createEntitiesFromGrid()

    class GameOver : RuntimeException()

    private fun createGrid(input: List<String>): CharGrid {
        val grid = CharGrid(input.first().length, input.size)

        input.forEachIndexed { y, line ->
            line.forEachIndexed { x, char ->
                grid[x, y] = char
            }
        }

        return grid
    }

    private fun createEntitiesFromGrid(): List<Entity> {
        val entities = mutableListOf<Entity>()

        for (y in 0 until grid.height) {
            for (x in 0 until grid.width) {
                when (grid[x, y]) {
                    'G' -> entities += Entity.Goblin(Vector(x, y))
                    'E' -> entities += Entity.Elf(Vector(x, y))
                }
            }
        }

        return entities
    }

    fun playRound() {
        entities
            .sortedBy { it.position.y * grid.width + it.position.x }
            .forEach { doTurn(it) }
    }

    private fun doTurn(entity: Entity) {
        if (entity.dead) {
            return
        }

        val targets = entities.filter { it.symbol != entity.symbol && !it.dead }

        if (targets.isEmpty()) {
            throw GameOver()
        }

        if (targets.none { entity.position.distanceTo(it.position) == 1}) {
            doMovement(entity, targets)
        }

        doAttack(entity, targets.filter { entity.position.distanceTo(it.position) == 1 })
    }

    private fun doMovement(entity: Entity, targets: List<Entity>) {
        if (targets.isEmpty()) {
            return
        }

        val destinations = targets
            .flatMap { it.position.adjacentPoints() }
            .filter { grid[it.x, it.y] == '.' }

        if (destinations.isEmpty()) {
            return
        }

        findNextStep(entity.position, destinations)?.let { nextPosition ->
            grid[entity.position.x, entity.position.y] = '.'
            entity.position = nextPosition
            grid[entity.position.x, entity.position.y] = entity.symbol
        }
    }

    private fun findNextStep(from: Vector, to: List<Vector>): Vector? {
        class Node(val location: Vector, val initialPoint: Vector, val distance: Int)
        class Path(val destination: Vector, val initialPoint: Vector, val distance: Int)

        val visited = mutableSetOf<Vector>()
        val queue = ArrayDeque<Node>()
        val paths = mutableListOf<Path>()

        queue.addAll(from.adjacentPoints().map { Node(it, it, 1) })

        var maxDistance = Int.MAX_VALUE

        while (queue.isNotEmpty()) {
            val node = queue.pop()

            if (node.location in visited || node.distance > maxDistance) {
                continue
            }

            visited += node.location

            if (grid[node.location.x, node.location.y] != '.') {
                continue
            }

            if (node.location in to) {
                paths += Path(node.location, node.initialPoint, node.distance)
                maxDistance = node.distance
            }

            queue.addAll(node.location.adjacentPoints().map { Node(it, node.initialPoint, node.distance + 1) })
        }

        return paths.minByOrNull {
            (it.destination.y * grid.width + it.destination.x) * 10000 + (it.initialPoint.y * grid.width + it.initialPoint.x)
        }?.initialPoint
    }

    private fun doAttack(entity: Entity, targets: List<Entity>) {
        if (targets.isEmpty()) {
            return
        }

        val weakestHP = targets.minOfOrNull { it.health } ?: 0
        val target = targets
            .filter { it.health == weakestHP }
            .minByOrNull { it.position.y * grid.width + it.position.x }!!

        target.health -= entity.attack
        if (target.health <= 0) {
            target.dead = true
            grid[target.position.x, target.position.y] = '.'
        }
    }
}
