package dev.lfelzinga.adventofkode.puzzles.aoc18.day7

class Worker(private val onComplete: (String) -> Unit) {
    private var currentStep: String = ""

    var remainingDuration: Int = 0
        private set

    fun advanceTime(amount: Int) {
        if (remainingDuration != 0) {
            remainingDuration -= amount
            if (remainingDuration <= 0) {
                remainingDuration = 0
                onComplete(currentStep)
            }
        }
    }

    fun isIdle(): Boolean = remainingDuration == 0

    fun startWork(step: String) {
        currentStep = step
        remainingDuration = (step[0] - 'A') + 61
    }
}
