package dev.lfelzinga.adventofkode.puzzles.aoc18.day5

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import java.util.*

class PolymerPuzzle : Puzzle<String>(2018, 5, StringTransformer()) {

    private fun reducePolymer(polymer: List<Char>): List<Char> {
        val units = LinkedList(polymer)
        val iter = units.listIterator()

        while (iter.hasNext()) {
            val char1 = iter.next()
            if (!iter.hasNext()) {
                break
            }

            val char2 = iter.next()

            if (char1 != char2 && char1.equals(char2, ignoreCase = true)) {
                iter.remove()
                iter.previous()
                iter.remove()

                if (iter.hasPrevious()) {
                    iter.previous()
                }
            } else {
                iter.previous()
            }
        }

        return units
    }

    override fun solveFirstPart(input: String): String {
        val reduced = reducePolymer(input.toList())

        return reduced.size.toString()
    }

    override fun solveSecondPart(input: String): String {
        val candidatesForRemoval = input.lowercase().toList().distinct()
        val polymer = input.toList()

        val minLength = candidatesForRemoval
            .map { unit ->
                val result = polymer.toMutableList()
                result.removeIf { it.lowercaseChar() == unit }
                result
            }
            .minOfOrNull { reducePolymer(it).size } ?: 0

        return minLength.toString()
    }
}
