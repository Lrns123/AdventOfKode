package dev.lfelzinga.adventofkode.puzzles.aoc18.day15

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class GoblinPuzzle : Puzzle<Sequence<String>>(2018, 15, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val game = Game(input.toList())

        var rounds = 0
        try {
            while(true) {
                game.playRound()
                rounds++
            }
        } catch (ex: Game.GameOver) {
        }

        return (rounds * game.entities.filter { !it.dead }.sumOf { it.health }).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val board = input.toList()
        var elfAttack = 4

        while(true) {
            val game = Game(board)

            game.entities.filterIsInstance<Entity.Elf>().forEach { it.attack = elfAttack }

            var rounds = 0
            try {
                while(true) {
                    game.playRound()
                    rounds++
                }
            } catch (ex: Game.GameOver) {
            }

            if (game.entities.filterIsInstance<Entity.Elf>().none { it.dead }) {
                return (rounds * game.entities.filter { !it.dead }.sumOf { it.health }).toString()
            } else {
                elfAttack++
            }
        }
    }
}
