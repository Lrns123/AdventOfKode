package dev.lfelzinga.adventofkode.puzzles.aoc18.day14

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.NumberTransformer

class RecipePuzzle : Puzzle<Long>(2018, 14, NumberTransformer()) {
    override fun solveFirstPart(input: Long): String {
        val recipeScores = mutableListOf(3, 7)
        val activeRecipes = IntArray(2) { it }

        repeat(input.toInt() + 8) {
            val combinedScore = recipeScores[activeRecipes[0]] + recipeScores[activeRecipes[1]]

            if (combinedScore >= 10) {
                recipeScores += combinedScore / 10
            }
            recipeScores += combinedScore % 10

            activeRecipes[0] = (activeRecipes[0] + 1 + recipeScores[activeRecipes[0]]) % recipeScores.size
            activeRecipes[1] = (activeRecipes[1] + 1 + recipeScores[activeRecipes[1]]) % recipeScores.size
        }

        return recipeScores.asSequence().drop(input.toInt()).take(10).joinToString("")
    }

    private fun splitDigits(number: Long): List<Int> {
        var remainder = number
        val digits = mutableListOf<Int>()

        while (remainder != 0L) {
            digits += (remainder % 10).toInt()
            remainder /= 10L
        }

        return digits.reversed()
    }

    override fun solveSecondPart(input: Long): String {
        val digits = splitDigits(input)

        val recipeScores = mutableListOf(3, 7)
        val activeRecipes = IntArray(2) { it }

        fun checkMatch() = recipeScores.last() == digits.last() && digits == recipeScores.takeLast(digits.size)

        while(true) {
            val combinedScore = recipeScores[activeRecipes[0]] + recipeScores[activeRecipes[1]]

            if (combinedScore >= 10) {
                recipeScores += combinedScore / 10

                if (checkMatch()) {
                    return (recipeScores.size - digits.size).toString()
                }
            }
            recipeScores += combinedScore % 10

            if (checkMatch()) {
                return (recipeScores.size - digits.size).toString()
            }

            activeRecipes[0] = (activeRecipes[0] + 1 + recipeScores[activeRecipes[0]]) % recipeScores.size
            activeRecipes[1] = (activeRecipes[1] + 1 + recipeScores[activeRecipes[1]]) % recipeScores.size
        }
    }
}
