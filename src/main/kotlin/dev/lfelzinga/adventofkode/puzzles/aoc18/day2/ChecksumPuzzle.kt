package dev.lfelzinga.adventofkode.puzzles.aoc18.day2

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.lang.StringBuilder

class ChecksumPuzzle : Puzzle<Sequence<String>>(2018, 2, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { line -> line.toCharArray().groupBy { it }.map { it.value.size } }
            .map { (if (2 in it) 1 else 0) to (if (3 in it) 1 else 0) }
            .reduce { acc, pair -> acc.first + pair.first to acc.second + pair.second }
            .let { it.first * it.second }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val ids = input.toList()

        for (firstId in ids) {
            for (secondId in ids) {
                if (firstId charDiffCount secondId == 1) {
                   return firstId charUnion secondId
                }
            }
        }

        return "Not found"
    }

    private infix fun String.charDiffCount(other: String): Int {
        var differences = 0

        for (i in indices) {
            if (this[i] != other[i]) {
                differences++
            }
        }

        return differences
    }

    private infix fun String.charUnion(other: String): String {
        val builder = StringBuilder()

        for (i in indices) {
            if (this[i] == other[i]) {
                builder.append(this[i])
            }
        }

        return builder.toString()
    }
}
