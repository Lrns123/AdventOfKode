package dev.lfelzinga.adventofkode.puzzles.aoc18.day17

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException

class WaterClayPuzzle : Puzzle<Sequence<String>>(2018, 17, LinesTransformer()) {
    private val lineRegex = Regex("""(\w)=(\d+), \w=(\d+)..(\d+)""")

    private fun readGrid(input: Sequence<String>): CharGrid {
        val grid = CharGrid(2500, 2500).filledWith('.')

        input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .forEach { (axis, pos, min, max) ->

                fun verticalBarrier(x: Int, y1: Int, y2: Int) { for (y in y1..y2) grid[x, y] = '#' }
                fun horizontalBarrier(y: Int, x1: Int, x2: Int) { for (x in x1..x2) grid[x, y] = '#' }

                when (axis) {
                    "x" -> verticalBarrier(pos.toInt(), min.toInt(), max.toInt())
                    "y" -> horizontalBarrier(pos.toInt(), min.toInt(), max.toInt())
                    else -> throw IllegalStateException("Illegal axis: $axis")
                }
            }

        return grid
    }


    private fun findHighestPoint(grid: CharGrid): Int {
        for (y in 0 until grid.height) {
            if ((0 until grid.width).any { grid[it, y] == '#' }) {
                return y
            }
        }

        return -1
    }

    private fun findLowestPoint(grid: CharGrid): Int {
        for (y in grid.height downTo 0) {
            if ((0 until grid.width).any { grid[it, y - 1] == '#' }) {
                return y
            }
        }

        return -1
    }

    private fun simulateFlow(grid: CharGrid, sourceX: Int, sourceY: Int, bottomY: Int) {
        var y = sourceY + 1

        while (y < bottomY) {
            when (grid[sourceX, y]) {
                '.' -> grid[sourceX, y++] = '|'
                '#', '~' -> {
                    while (tryFill(grid, sourceX, --y, bottomY)) { /* Do nothing */ }
                    return
                }
                '|' -> return
            }
        }
    }

    private fun tryFill(grid: CharGrid, startX: Int, startY: Int, bottomY: Int): Boolean {
        val validBottom = setOf('#', '~')

        when (grid[startX, startY]) {
            '#', '~' -> return true
        }

        val (minX, maxX) = findSpreadBounds(grid, startX, startY)

        if (grid[minX, startY + 1] in validBottom && grid[maxX, startY + 1] in validBottom) {
            for (x in minX..maxX) {
                grid[x, startY] = '~'
            }
            return true
        } else {
            for (x in minX..maxX) {
                grid[x, startY] = '|'
            }

            if (grid[minX, startY + 1] !in validBottom) {
                simulateFlow(grid, minX, startY, bottomY)
            }

            if (grid[maxX, startY + 1] !in validBottom) {
                simulateFlow(grid, maxX, startY, bottomY)
            }

            return false
        }
    }

    private fun findSpreadBounds(grid: CharGrid, x: Int, y: Int): Pair<Int, Int> {
        val solid = setOf('#', '~')

        val minX = (x downTo 0).first { grid[it - 1, y] in solid || grid[it, y + 1] !in solid }
        val maxX = (x until grid.width).first { grid[it + 1, y] in solid || grid[it, y + 1] !in solid }

        return minX to maxX
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = readGrid(input)
        val highestY = findHighestPoint(grid)
        val lowestY = findLowestPoint(grid)

        grid[500, 0] = '+'

        simulateFlow(grid, 500, 0, lowestY)

        return (highestY..lowestY)
            .sumOf { y -> (0 until grid.width).count { x -> grid[x, y] == '|' || grid[x, y] == '~' } }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = readGrid(input)
        val highestY = findHighestPoint(grid)
        val lowestY = findLowestPoint(grid)

        grid[500, 0] = '+'

        simulateFlow(grid, 500, 0, lowestY)

        return (highestY..lowestY)
            .sumOf { y -> (0 until grid.width).count { x -> grid[x, y] == '~' } }
            .toString()
    }
}
