package dev.lfelzinga.adventofkode.puzzles.aoc18.day1

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class FrequencyDriftPuzzle : Puzzle<Sequence<String>>(2018, 1, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .sumOf { it.toInt() }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val reached = mutableSetOf(0)
        var frequency = 0
        val deltas = input.map { it.toInt() }.toList()

        while (true) {
            for (delta in deltas) {
                frequency += delta
                if (!reached.add(frequency)) {
                    return "$frequency"
                }
            }
        }
    }
}
