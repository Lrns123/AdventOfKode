package dev.lfelzinga.adventofkode.puzzles.aoc18.day7

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.util.TreeSet

class BuildStepsPuzzle : Puzzle<Sequence<String>>(2018, 7, LinesTransformer()) {
    private val lineRegex = Regex("""Step (\w) must be finished before step (\w) can begin\.""")

    override fun solveFirstPart(input: Sequence<String>): String {
        val prerequisites = parsePrerequisites(input)
        val remainingSteps = TreeSet(prerequisites.flatMap { it.value + it.key })
        val order = StringBuilder()

        while (remainingSteps.isNotEmpty()) {
            for (step in remainingSteps) {
                if (prerequisites[step]?.all { it !in remainingSteps } != false) {
                    order.append(step)
                    remainingSteps.remove(step)
                    break
                }
            }
        }

        return order.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val prerequisites = parsePrerequisites(input)
        val remainingSteps = TreeSet(prerequisites.flatMap { it.value + it.key })
        val completedSteps = mutableSetOf<String>()

        val workerPool = WorkerPool(5) { completedSteps += it }

        while (remainingSteps.isNotEmpty()) {
            val stepsQueued = mutableSetOf<String>()
            for (step in remainingSteps) {
                if (prerequisites[step]?.all { it in completedSteps } != false) {
                    workerPool.queueWork(step)
                    stepsQueued += step
                }
            }

            remainingSteps -= stepsQueued

            workerPool.finishOne()
        }

        workerPool.finishAll()

        return workerPool.timeElapsed.toString()
    }

    private fun parsePrerequisites(input: Sequence<String>): Map<String, Set<String>> {
        val prerequisites = mutableMapOf<String, MutableSet<String>>()

        input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .forEach { (prerequisite, step) -> prerequisites.computeIfAbsent(step) { mutableSetOf() } += prerequisite }

        return prerequisites
    }
}
