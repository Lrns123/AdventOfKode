package dev.lfelzinga.adventofkode.puzzles.aoc18.day23

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Vector3
import java.util.*

class NanobotPuzzle : Puzzle<Sequence<String>>(2018, 23, LinesTransformer()) {
    private val lineRegex = Regex("""pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)""")

    class NanoBot(val position: Vector3, val range: Int) {
        fun isInRangeOf(other: NanoBot) = position.distanceTo(other.position) <= range
    }

    data class Box(val min: Vector3, val max: Vector3) {
        operator fun contains(vector: Vector3): Boolean {
            return vector.x in min.x..max.x &&
                   vector.y in min.y..max.y &&
                   vector.z in min.z..max.z
        }

        val width = max.x - min.x
        val height = max.y - min.y
        val depth = max.z - min.z

        init {
            if (width < 0 || height < 0 || depth < 0) {
                throw IllegalStateException("Invalid box bounds! $min $max ($width, $height, $depth)")
            }
        }

        fun centerPoint() = Vector3(
            min.x + (max.x - min.x) / 2,
            min.y + (max.y - min.y) / 2,
            min.z + (max.z - min.z) / 2
        )

        fun cubed(): Box {
            val size = listOf(width, height, depth).maxOrNull()!! / 2
            val center = centerPoint()

            return Box(
                Vector3(center.x - size, center.y - size, center.z - size),
                Vector3(center.x + size, center.y + size, center.z + size)
            )
        }

        fun pointInBox(vector: Vector3) = Vector3(
            vector.x.coerceIn(min.x, max.x),
            vector.y.coerceIn(min.y, max.y),
            vector.z.coerceIn(min.z, max.z)
        )

        fun octants(): List<Box> {
            if (width == 0 || height == 0 || depth == 0) {
                return listOf()
            }

            val center = centerPoint()

            return listOf(
                Box(Vector3(min.x, min.y, min.z), Vector3(center.x, center.y, center.z)),
                Box(Vector3(center.x + 1, min.y, min.z), Vector3(max.x, center.y, center.z)),
                Box(Vector3(min.x, center.y + 1, min.z), Vector3(center.x, max.y, center.z)),
                Box(Vector3(center.x + 1, center.y + 1, min.z), Vector3(max.x, max.y, center.z)),

                Box(Vector3(min.x, min.y, center.z + 1), Vector3(center.x, center.y, max.z)),
                Box(Vector3(center.x + 1, min.y, center.z + 1), Vector3(max.x, center.y, max.z)),
                Box(Vector3(min.x, center.y + 1, center.z + 1), Vector3(center.x, max.y, max.z)),
                Box(Vector3(center.x + 1, center.y + 1, center.z + 1), Vector3(max.x, max.y, max.z))
            )
        }
    }

    private fun Iterable<Vector3>.boundingBox(): Box {
        val xs = this.map { it.x }
        val ys = this.map { it.y }
        val zs = this.map { it.z }

        return Box(
            Vector3(xs.minOrNull() ?: 0, ys.minOrNull() ?: 0, zs.minOrNull() ?: 0),
            Vector3(xs.maxOrNull() ?: 0, ys.maxOrNull() ?: 0, zs.maxOrNull() ?: 0)
        )
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val nanobots = input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .map { (x, y, z, range) -> NanoBot(Vector3(x.toInt(), y.toInt(), z.toInt()), range.toInt()) }
            .toList()

        val strongest = nanobots.maxByOrNull { it.range }!!

        return nanobots.count { strongest.isInRangeOf(it) }.toString()
    }

    private fun findStrongestSignal(nanoBots: List<NanoBot>, searchBox: Box): Vector3 {
        class Node(val box: Box) {
            val botsInRange = nanoBots.count { bot -> bot.position.distanceTo(box.pointInBox(bot.position)) <= bot.range }
            val distanceFromOrigin = box.centerPoint().magnitude
        }

        val queue = PriorityQueue(
            compareBy<Node> { -it.botsInRange }
                .thenBy { it.distanceFromOrigin }
                .thenBy { it.box.width + it.box.height + it.box.depth }
        )

        queue.add(Node(searchBox))

        while (queue.isNotEmpty()) {
            val node = queue.poll()

            if (node.box.width + node.box.height + node.box.depth == 0) {
                return node.box.centerPoint()
            }

            node.box.octants().forEach {
                queue.add(Node(it))
            }
        }

        throw IllegalStateException()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val nanobots = input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .map { (x, y, z, range) -> NanoBot(Vector3(x.toInt(), y.toInt(), z.toInt()), range.toInt()) }
            .toList()

        val boundingCube = nanobots.map { it.position }.boundingBox().cubed()

        return findStrongestSignal(nanobots, boundingCube).magnitude.toString()
    }
}
