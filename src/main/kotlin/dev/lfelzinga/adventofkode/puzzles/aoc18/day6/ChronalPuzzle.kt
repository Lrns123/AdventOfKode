package dev.lfelzinga.adventofkode.puzzles.aoc18.day6

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2

class ChronalPuzzle : Puzzle<Sequence<String>>(2018, 6, LinesTransformer()) {
    private fun findClosestPoint(x: Int, y: Int, points: List<Vector2>): Int {
        val distances = points
            .map { it.distanceTo(Vector2(x, y)) }
            .toList()

        val minDistance = distances.minOrNull() ?: -1

        val index1 = distances.indexOf(minDistance)
        val index2 = distances.lastIndexOf(minDistance)

        return if (index1 == index2) index1 else -1
    }

    private fun calculateTotalDistance(x: Int, y: Int, points: List<Vector2>): Int {
        return points.sumOf { it.distanceTo(Vector2(x, y)) }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val locations = IntGrid(500, 500)
        val points = input
            .map { line -> line.split(",").map { it.trim().toInt() } }
            .map { (x, y) -> Vector2(x, y) }
            .toList()

        for (x in 0 until 500) {
            for (y in 0 until 500) {
                locations[x, y] = findClosestPoint(x, y, points)
            }
        }

        val infiniteSet = mutableSetOf(-1)

        for (x in 0 until 500) infiniteSet += locations[x, 0]
        for (x in 0 until 500) infiniteSet += locations[x, 499]
        for (y in 0 until 500) infiniteSet += locations[0, y]
        for (y in 0 until 500) infiniteSet += locations[499, y]

        val areas = locations.array.filter { it !in infiniteSet }.groupBy { it }.mapValues { it.value.size }

        return areas.maxByOrNull { it.value }?.value?.toString() ?: "???"
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val locations = IntGrid(500, 500)
        val points = input
            .map { line -> line.split(",").map { it.trim().toInt() } }
            .map { (x, y) -> Vector2(x, y) }
            .toList()

        for (x in 0 until 500) {
            for (y in 0 until 500) {
                locations[x, y] = calculateTotalDistance(x, y, points)
            }
        }

        return locations.array.count { it < 10000 }.toString()
    }
}
