package dev.lfelzinga.adventofkode.puzzles.aoc18.day9

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class MarblePuzzle : Puzzle<String>(2018, 9, StringTransformer()) {
    private val lineRegex = Regex("""(\d+) players; last marble is worth (\d+) points""")

    override fun solveFirstPart(input: String): String {
        val tokens = lineRegex.matchEntire(input)?.destructured ?: throw IllegalArgumentException("Could not parse input")
        val players: Int = tokens.component1().toInt()
        val lastMarble: Long = tokens.component2().toLong()

        val marbles = CircularDeque<Long>()
        marbles.add(0)

        val scores = LongArray(players) { 0 }

        for (value in 1..lastMarble) {
            if (value % 23L == 0L) {
                val currentPlayer = (value - 1) % players

                marbles.rotate(-7)
                scores[currentPlayer.toInt()] += marbles.pop() + value
            } else {
                marbles.rotate(2)
                marbles.addLast(value)
            }
        }

        return scores.maxOrNull().toString()
    }

    override fun solveSecondPart(input: String): String {
        val tokens = lineRegex.matchEntire(input)?.destructured ?: throw IllegalArgumentException("Could not parse input")
        val players: Int = tokens.component1().toInt()
        val lastMarble: Long = tokens.component2().toLong() * 100L

        val marbles = CircularDeque<Long>()
        marbles.add(0)

        val scores = LongArray(players) { 0 }

        for (value in 1..lastMarble) {
            if (value % 23L == 0L) {
                val currentPlayer = (value - 1) % players

                marbles.rotate(-7)
                scores[currentPlayer.toInt()] += marbles.pop() + value
            } else {
                marbles.rotate(2)
                marbles.addLast(value)
            }
        }

        return scores.maxOrNull().toString()
    }
}
