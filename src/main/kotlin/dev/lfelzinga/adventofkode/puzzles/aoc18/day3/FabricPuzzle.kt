package dev.lfelzinga.adventofkode.puzzles.aoc18.day3

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import java.lang.IllegalArgumentException


class FabricPuzzle : Puzzle<Sequence<String>>(2018, 3, LinesTransformer()) {
    private val lineRegex = Regex("""#(\d+) @ (\d+),(\d+): (\d+)x(\d+)""")

    override fun solveFirstPart(input: Sequence<String>): String {
        val fabric = IntGrid(1000, 1000)

        input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .forEach { (id, x, y, w, h) ->
                fabric.fillClaim(id.toInt(), x.toInt(), y.toInt(), w.toInt(), h.toInt())
            }

        return fabric.array.count { it == -1 }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val fabric = IntGrid(1000, 1000)
        val claims = mutableSetOf<Int>()
        val overlappedClaims = mutableSetOf<Int>()

        input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .forEach { (id, x, y, w, h) ->
                claims += id.toInt()

                fabric.fillClaimAndCheckOverlaps(id.toInt(), x.toInt(), y.toInt(), w.toInt(), h.toInt(), overlappedClaims)
            }

        return (claims - overlappedClaims).first().toString()
    }

    private fun IntGrid.fillClaim(id: Int, x: Int, y: Int, w: Int, h: Int) {
        for (xPos in x until x + w) {
            for (yPos in y until y + h) {
                this[xPos, yPos] = if (this[xPos, yPos] == 0) id else -1
            }
        }
    }

    private fun IntGrid.fillClaimAndCheckOverlaps(id: Int, x: Int, y: Int, w: Int, h: Int, overlaps: MutableSet<Int>) {
        for (xPos in x until x + w) {
            for (yPos in y until y + h) {
                if (this[xPos, yPos] > 0) {
                    overlaps += this[xPos, yPos]
                    overlaps += id

                    this[xPos, yPos] = -1
                } else {
                    this[xPos, yPos] = id
                }
            }
        }
    }
}
