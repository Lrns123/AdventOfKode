package dev.lfelzinga.adventofkode.puzzles.aoc18.day24

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.extensions.filterIf
import java.lang.IllegalArgumentException

class ImmuneSystemPuzzle : Puzzle<Sequence<String>>(2018, 24, LinesTransformer()) {
    data class Group(var units: Int, val hitPoints: Int, val attack: Int, val attackType: String, val initiative: Int, val immunities: List<String>, val weaknesses: List<String>) {
        val effectivePower
            get() = units * attack
    }

    private val unitRegex = Regex("""(\d+) units each with (\d+) hit points(?: \((.*)\))? with an attack that does (\d+) (\w+) damage at initiative (\d+)""")

    private fun parseImmunityAndWeakness(input: String): Pair<List<String>, List<String>> {
        val immunities = mutableListOf<String>()
        val weaknesses = mutableListOf<String>()

        val sanitizedInput = input.trim('(', ')').split("; ")

        for (entry in sanitizedInput) {
            if (entry.startsWith("immune to")) {
                immunities += entry.substring(10).split(", ")
            } else if (entry.startsWith("weak to")) {
                weaknesses += entry.substring(8).split(", ")
            }
        }

        return immunities to weaknesses
    }

    private fun parseArmies(input: Sequence<String>): Pair<List<Group>, List<Group>> {
        val immuneSystem = mutableListOf<Group>()
        val infection = mutableListOf<Group>()

        lateinit var current: MutableList<Group>

        for (line in input) {
            when (line) {
                "Immune System:" -> current = immuneSystem
                "Infection:" -> current = infection
                "" -> {}
                else -> (unitRegex.matchEntire(line)?.destructured ?: throw IllegalArgumentException("Could not parse line $line"))
                    .also { (units, hp, immunityAndWeakness, attack, attackType, initiative ) ->
                        val (immunities, weaknesses) = parseImmunityAndWeakness(immunityAndWeakness)
                        current.add(Group(units.toInt(), hp.toInt(), attack.toInt(), attackType, initiative.toInt(), immunities, weaknesses))
                    }

            }
        }

        return immuneSystem to infection
    }

    private val targetSelectionOrder = compareByDescending<Group> { it.effectivePower }.thenByDescending { it.initiative }

    private fun attackDamage(attacker: Group, defender: Group): Int {
        return when (attacker.attackType) {
            in defender.immunities -> 0
            in defender.weaknesses -> attacker.effectivePower * 2
            else -> attacker.effectivePower
        }
    }

    private fun doTargetSelection(attackers: List<Group>, defenders: List<Group>, requireCasualties: Boolean): List<Pair<Group, Group>> {
        val remainingDefenders = ArrayList(defenders)
        val targets = mutableListOf<Pair<Group, Group>>()

        attackers
            .asSequence()
            .filter { it.units != 0 }
            .sortedWith(targetSelectionOrder)
            .forEach { attacker ->
                val selected = remainingDefenders
                    .asSequence()
                    .filter { it.units != 0 }
                    .filterIf(requireCasualties) { attackDamage(attacker, it) >= it.hitPoints }
                    .sortedWith(
                        compareByDescending<Group> { attackDamage(attacker, it) }
                            .thenByDescending { it.effectivePower }
                            .thenByDescending { it.initiative }
                    )
                    .firstOrNull()

                if (selected != null) {
                    targets += attacker to selected
                    remainingDefenders.remove(selected)
                }
            }

        return targets
    }

    private fun List<Group>.isDefeated() = this.none { it.units > 0 }

    private fun simulateCombat(immuneSystem: List<Group>, infection: List<Group>, requireCasualties: Boolean) {
        while (!immuneSystem.isDefeated() && !infection.isDefeated()) {
            val targets = mutableListOf<Pair<Group, Group>>()

            targets += doTargetSelection(immuneSystem, infection, requireCasualties)
            targets += doTargetSelection(infection, immuneSystem, requireCasualties)

            if (targets.isEmpty()) {
                // Stalemate
                return
            }

            targets
                .sortedByDescending { it.first.initiative }
                .forEach { (attacker, defender) ->
                    if (attacker.units != 0) {
                        val unitsKilled = attackDamage(attacker, defender) / defender.hitPoints
                        defender.units = (defender.units - unitsKilled).coerceAtLeast(0)
                    }
                }
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val (immuneSystem, infection) = parseArmies(input)

        simulateCombat(immuneSystem, infection, false)

        return if (immuneSystem.isDefeated()) {
            infection.sumOf { it.units }.toString()
        } else {
            immuneSystem.sumOf { it.units }.toString()
        }
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val (sourceImmuneSystem, sourceInfection) = parseArmies(input)


        val sequence = generateSequence(1) { it + 1 }

        for (boost in sequence) {
            val immuneSystem = sourceImmuneSystem.map { it.copy(attack = it.attack + boost) }
            val infection = sourceInfection.map { it.copy() }

            simulateCombat(immuneSystem, infection, true)

            if (infection.isDefeated()) {
                return immuneSystem.sumOf { it.units }.toString()
            }
        }

        return "???"
    }
}
