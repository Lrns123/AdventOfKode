package dev.lfelzinga.adventofkode.puzzles.aoc18.day13

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class TrainPuzzle : Puzzle<Sequence<String>>(2018, 13, LinesTransformer()) {

    enum class Direction(val dx: Int, val dy: Int) {
        NORTH(0, -1),
        SOUTH(0, 1),
        EAST(1, 0),
        WEST(-1, 0);

        fun left() = when (this) {
            NORTH -> WEST
            SOUTH -> EAST
            EAST -> NORTH
            WEST -> SOUTH
        }

        fun right() = when (this) {
            NORTH -> EAST
            SOUTH -> WEST
            EAST -> SOUTH
            WEST -> NORTH
        }

        fun cornerSWtoNE() = when (this) {
            NORTH -> EAST
            SOUTH -> WEST
            EAST -> NORTH
            WEST -> SOUTH
        }

        fun cornerSEtoNW() = when (this) {
            NORTH -> WEST
            SOUTH -> EAST
            EAST -> SOUTH
            WEST -> NORTH
        }
    }

    class Train(var x: Int, var y: Int, var direction: Direction, var intersections: Int = 0)

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = input.toList()
        val trains = mutableListOf<Train>()

        grid.forEachIndexed { y, tracks ->
            tracks.forEachIndexed { x, char ->
                when (char) {
                    '>' -> trains += Train(x, y, Direction.EAST)
                    '<' -> trains += Train(x, y, Direction.WEST)
                    '^' -> trains += Train(x, y, Direction.NORTH)
                    'v' -> trains += Train(x, y, Direction.SOUTH)
                }
            }
        }

        while (true) {
            trains.sortBy { it.y * 150 + it.x }

            for (train in trains) {
                train.x += train.direction.dx
                train.y += train.direction.dy

                if (trains.any { it != train && it.x == train.x && it.y == train.y }) {
                    return "${train.x},${train.y}"
                }

                when (grid[train.y][train.x]) {
                    '/' -> train.direction = train.direction.cornerSWtoNE()
                    '\\' -> train.direction = train.direction.cornerSEtoNW()
                    '+' -> when(train.intersections++ % 3) {
                        0 -> train.direction = train.direction.left()
                        2 -> train.direction = train.direction.right()
                    }
                }
            }
        }
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = input.toList()
        val trains = mutableListOf<Train>()

        grid.forEachIndexed { y, tracks ->
            tracks.forEachIndexed { x, char ->
                when (char) {
                    '>' -> trains += Train(x, y, Direction.EAST)
                    '<' -> trains += Train(x, y, Direction.WEST)
                    '^' -> trains += Train(x, y, Direction.NORTH)
                    'v' -> trains += Train(x, y, Direction.SOUTH)
                }
            }
        }

        while (true) {
            trains.sortBy { it.y * 150 + it.x }
            val crashedTrains = mutableSetOf<Train>()

            for (train in trains) {
                train.x += train.direction.dx
                train.y += train.direction.dy


                trains.find { it != train && it.x == train.x && it.y == train.y }?.let {
                    crashedTrains += train
                    crashedTrains += it
                }

                when (grid[train.y][train.x]) {
                    '/' -> train.direction = train.direction.cornerSWtoNE()
                    '\\' -> train.direction = train.direction.cornerSEtoNW()
                    '+' -> when(train.intersections++ % 3) {
                        0 -> train.direction = train.direction.left()
                        2 -> train.direction = train.direction.right()
                    }
                }
            }

            trains -= crashedTrains

            if (trains.size < 2) {
                val lastTrain = trains.first()
                return "${lastTrain.x},${lastTrain.y}"
            }
        }
    }
}
