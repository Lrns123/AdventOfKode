package dev.lfelzinga.adventofkode.puzzles.aoc18.day7

class WorkerPool(poolSize: Int, private val onComplete: (String) -> Unit) {
    private val workers = Array(poolSize) { Worker(onComplete) }

    var timeElapsed = 0
        private set

    private fun getIdleWorker(): Worker? = workers.find { it.isIdle() }

    tailrec fun queueWork(step: String) {
        val worker = getIdleWorker()
        if (worker == null) {
            finishOne()
            queueWork(step)
        } else {
            worker.startWork(step)
        }
    }

    fun finishOne() {
        val timeToSkip = workers
            .filter { !it.isIdle() }
            .minOfOrNull { it.remainingDuration } ?: 0

        advanceTime(timeToSkip)
    }

    fun finishAll() {
        val timeToSkip = workers
            .filter { !it.isIdle() }
            .maxOfOrNull { it.remainingDuration } ?: 0

        advanceTime(timeToSkip)
    }

    private fun advanceTime(amount: Int) {
        timeElapsed += amount
        for (worker in workers) {
            worker.advanceTime(amount)
        }
    }
}
