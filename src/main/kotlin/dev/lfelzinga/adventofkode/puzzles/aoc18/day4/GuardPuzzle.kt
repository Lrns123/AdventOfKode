package dev.lfelzinga.adventofkode.puzzles.aoc18.day4

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.lang.IllegalStateException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class GuardPuzzle : Puzzle<Sequence<String>>(2018, 4, LinesTransformer()) {
    private val lineRegex = Regex("""\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})] (.+)""")
    private val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
    private val guardRegex = Regex("""Guard #(\d+) begins shift""")

    sealed class Event(val timestamp: LocalDateTime) {
        class StartShift(val id: Int, timestamp: LocalDateTime) : Event(timestamp)
        class Sleep(timestamp: LocalDateTime) : Event(timestamp)
        class Wake(timestamp: LocalDateTime) : Event(timestamp)
    }

    private fun convertEvent(timeString: String, event: String): Event {
        val timestamp = LocalDateTime.from(dateFormat.parse(timeString))

        return when (event) {
            "wakes up" -> Event.Wake(timestamp)
            "falls asleep" -> Event.Sleep(timestamp)
            else -> {
                val match = guardRegex.matchEntire(event) ?: throw IllegalArgumentException("Cannot parse event $event")
                val id = match.groupValues[1].toInt()

                Event.StartShift(id, timestamp)
            }
        }
    }

    private fun convertInputToChronologicalEvents(input: Sequence<String>): List<Event> {
        return input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .map { (time, event) -> convertEvent(time, event) }
            .sortedBy { it.timestamp }
            .toList()
    }

    private fun computeSleepPatterns(events: List<Event>) : Map<Int, Map<Int, Int>> {
        val guardSleepPatterns = mutableMapOf<Int, MutableMap<Int, Int>>()
        var currentGuard = -1
        var sleepTime = -1

        for (event in events) {
            when (event) {
                is Event.StartShift -> currentGuard = event.id
                is Event.Sleep -> sleepTime = event.timestamp.minute
                is Event.Wake -> {
                    if (currentGuard == -1 || sleepTime == -1) {
                        throw IllegalStateException("Cannot handle wake event, no prior start shift and sleep found")
                    }

                    val sleepPatterns = guardSleepPatterns.computeIfAbsent(currentGuard) { mutableMapOf() }

                    for (minute in sleepTime until event.timestamp.minute) {
                        sleepPatterns.compute(minute) { _, currentCount -> 1 + (currentCount ?: 0) }
                    }
                }
            }
        }

        return guardSleepPatterns
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val events = convertInputToChronologicalEvents(input)
        val sleepPatterns = computeSleepPatterns(events)

        val mostSleepingGuard = sleepPatterns.maxByOrNull { it.value.values.sum() } ?: return "???"

        return (mostSleepingGuard.key * (mostSleepingGuard.value.maxByOrNull { it.value }?.key ?: -1)).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val events = convertInputToChronologicalEvents(input)
        val sleepPatterns = computeSleepPatterns(events)

        val mostSleepingGuard = sleepPatterns.maxByOrNull { it.value.values.maxOrNull() ?: 0 } ?: return "???"

        return (mostSleepingGuard.key * (mostSleepingGuard.value.maxByOrNull { it.value }?.key ?: -1)).toString()
    }
}
