package dev.lfelzinga.adventofkode.puzzles.aoc21.day20

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds

class ImageEnhancementPuzzle : Puzzle<Sequence<String>>(2021, 20, LinesTransformer()) {

    companion object {
        private val pixelNeighbours = listOf(
            Vector2(-1, -1),
            Vector2(0, -1),
            Vector2(1, -1),
            Vector2(-1, 0),
            Vector2(0, 0),
            Vector2(1, 0),
            Vector2(-1, 1),
            Vector2(0, 1),
            Vector2(1, 1),
        )
    }

    private fun parseInput(input: Sequence<String>): Pair<BooleanArray, Set<Vector2>> {
        val iterator = input.iterator()
        val algorithm = iterator.next().let { line -> BooleanArray(line.length) { line[it] == '#' } }

        iterator.next() // Skip empty line

        val image = mutableSetOf<Vector2>()

        var y = 0
        while (iterator.hasNext()) {
            iterator.next().forEachIndexed { x, ch -> if (ch == '#') image += Vector2(x, y) }
            y++
        }

        return algorithm to image
    }

    private fun indexForPixel(pixel: Vector2, image: Set<Vector2>, step: Int): Int = pixelNeighbours
        .map { if (pixel + it in image) (step + 1) % 2 else step % 2 }
        .reduce { acc, i -> acc shl 1 or i }

    private fun enhance(algorithm: BooleanArray, image: Set<Vector2>, step: Int): Set<Vector2> {
        val newPixels = mutableSetOf<Vector2>()
        val (min, max) = image.bounds()

        for (x in (min.x - 1)..(max.x + 1)) {
            for (y in (min.y - 1)..(max.y + 1)) {
                if (algorithm[indexForPixel(Vector2(x, y), image, step)] == (step % 2 == 1)) {
                    newPixels += Vector2(x, y)
                }
            }
        }

        return newPixels
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val (algorithm, image) = parseInput(input)

        val finalImage = (0 until 2).fold(image) { acc, step -> enhance(algorithm, acc, step) }

        return finalImage.size.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val (algorithm, image) = parseInput(input)

        val finalImage = (0 until 50).fold(image) { acc, step -> enhance(algorithm, acc, step) }

        return finalImage.size.toString()
    }
}
