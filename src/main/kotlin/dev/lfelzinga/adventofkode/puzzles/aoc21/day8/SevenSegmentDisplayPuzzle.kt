package dev.lfelzinga.adventofkode.puzzles.aoc21.day8

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class SevenSegmentDisplayPuzzle : Puzzle<Sequence<String>>(2021, 8, LinesTransformer()) {
    private data class SegmentNote(val uniquePatterns: List<String>, val outputValue: List<String>)

    private fun String.sorted() = toCharArray().sorted().joinToString("")

    private fun parseInput(input: Sequence<String>): Sequence<SegmentNote> {
        return input
            .map { it.split(" | ") }
            .map { (patterns, output) -> SegmentNote(
                patterns.split(" ").map { it.sorted() },
                output.split(" ").map { it.sorted() }
            ) }
    }

    private fun deduceDigits(input: SegmentNote): Map<String, Int> {
        val digitMapping = mutableMapOf<Int, String>()

        // Unique patterns
        for (pattern in input.uniquePatterns) {
            when (pattern.length) {
                2 -> digitMapping[1] = pattern
                3 -> digitMapping[7] = pattern
                4 -> digitMapping[4] = pattern
                7 -> digitMapping[8] = pattern
            }
        }

        fun String.overlap(digit: Int): Int {
            val signals = digitMapping[digit] ?: error("No pattern for digit $digit")
            return count { it in signals }
        }

        // Ambiguous patterns
        for (pattern in input.uniquePatterns) {
            when (pattern.length) {
                5 -> when {
                    pattern.overlap(1) == 2 -> digitMapping[3] = pattern
                    pattern.overlap(4) == 2 -> digitMapping[2] = pattern
                    pattern.overlap(4) == 3 -> digitMapping[5] = pattern
                }
                6 -> when {
                    pattern.overlap(4) == 4 -> digitMapping[9] = pattern
                    pattern.overlap(1) == 1 -> digitMapping[6] = pattern
                    pattern.overlap(1) == 2 -> digitMapping[0] = pattern
                }
            }
        }

        return digitMapping.map { (key, value) -> value to key }.toMap()
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val uniqueLengths = listOf(2, 3, 4, 7)

        return parseInput(input)
            .sumOf { it.outputValue.count { output -> output.length in uniqueLengths } }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return parseInput(input)
            .sumOf { note ->
                val mapping = deduceDigits(note)
                note.outputValue
                    .map { mapping[it] ?: error("No mapping for $it") }
                    .reduce { acc, number -> acc * 10 + number }
            }.toString()
    }
}
