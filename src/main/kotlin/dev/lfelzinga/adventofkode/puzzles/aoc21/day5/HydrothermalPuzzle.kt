package dev.lfelzinga.adventofkode.puzzles.aoc21.day5

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import kotlin.math.abs
import kotlin.math.max

class HydrothermalPuzzle : Puzzle<Sequence<String>>(2021, 5, LinesTransformer()) {
    companion object {
        private val lineRegex = Regex("(\\d+),(\\d+) -> (\\d+),(\\d+)")
    }

    private fun parseInput(input: Sequence<String>): Sequence<Pair<Vector2, Vector2>> {
        return input
            .map { lineRegex.matchEntire(it)?.destructured ?: error("Could not parse line '$it'") }
            .map { (x1, y1, x2, y2) -> Vector2(x1.toInt(), y1.toInt()) to Vector2(x2.toInt(), y2.toInt()) }
    }

    private fun drawLine(grid: IntGrid, p1: Vector2, p2: Vector2, includeDiagonal: Boolean) {
        val delta = p2 - p1
        if (delta.x != 0 && delta.y != 0 && !includeDiagonal)
            return

        val length = max(abs(delta.x), abs(delta.y))
        val direction = delta.normalized

        for (i in 0..length) {
            val point = p1 + direction * i
            grid[point.x, point.y]++
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = IntGrid(1000, 1000)

        for ((p1, p2) in parseInput(input)) {
           drawLine(grid, p1, p2, false)
        }

        return grid.array.count { it > 1 }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = IntGrid(1000, 1000)

        for ((p1, p2) in parseInput(input)) {
            drawLine(grid, p1, p2, true)
        }

        return grid.array.count { it > 1 }.toString()
    }
}
