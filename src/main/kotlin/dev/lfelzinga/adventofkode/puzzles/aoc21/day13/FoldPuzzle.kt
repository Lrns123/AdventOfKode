package dev.lfelzinga.adventofkode.puzzles.aoc21.day13

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.puzzles.aoc21.day13.FoldPuzzle.FoldDirection.HORIZONTAL
import dev.lfelzinga.adventofkode.puzzles.aoc21.day13.FoldPuzzle.FoldDirection.VERTICAL
import dev.lfelzinga.adventofkode.toolbox.ocr.ocr
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2

class FoldPuzzle : Puzzle<Sequence<String>>(2021, 13, LinesTransformer()) {

    companion object {
        private val foldRegex = Regex("fold along ([xy])=(\\d+)")
    }

    private enum class FoldDirection { HORIZONTAL, VERTICAL }
    private data class Fold(val position: Int, val direction: FoldDirection)
    private data class Manual(val points: Set<Vector2>, val folds: List<Fold>)

    private fun parseManual(input: Sequence<String>): Manual {
        val instructions = input.toList()
        val separator = instructions.indexOf("")

        val points = instructions.subList(0, separator)
            .map { it.split(',').let { (x, y) -> Vector2(x.toInt(), y.toInt()) } }
            .toSet()

        val folds = instructions.subList(separator + 1, instructions.size)
            .map { foldRegex.matchEntire(it)?.destructured ?: error("Cannot parse line $it") }
            .map { (axis, position) -> Fold(position.toInt(), if (axis == "x") HORIZONTAL else VERTICAL) }

        return Manual(points, folds)
    }

    private fun applyFold(points: Set<Vector2>, fold: Fold): Set<Vector2> {
        val foldedPoints = mutableSetOf<Vector2>()

        when (fold.direction) {
            HORIZONTAL -> points.forEach {
                foldedPoints += if (it.x > fold.position) it.copy(x = fold.position - (it.x - fold.position)) else it
            }
            VERTICAL -> points.forEach {
                foldedPoints += if (it.y > fold.position) it.copy(y = fold.position - (it.y - fold.position)) else it
            }
        }

        return foldedPoints
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val manual = parseManual(input)

        return applyFold(manual.points, manual.folds.first()).size.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val manual = parseManual(input)
        return manual.folds.fold(manual.points) { points, fold -> applyFold(points, fold) }.ocr()
    }
}
