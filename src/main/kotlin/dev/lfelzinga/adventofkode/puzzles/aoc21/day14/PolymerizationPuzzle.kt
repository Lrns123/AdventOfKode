package dev.lfelzinga.adventofkode.puzzles.aoc21.day14

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.nonNull

private typealias PairMap = Map<Pair<Char, Char>, Long>
private typealias RuleMap = Map<Pair<Char, Char>, Char>

class PolymerizationPuzzle : Puzzle<Sequence<String>>(2021, 14, LinesTransformer()) {

    private fun parseInput(input: List<String>): Pair<PairMap, RuleMap> {
        val pairs = input.first()
            .windowedSequence(2)
            .map { it[0] to it[1] }
            .groupingBy { it }
            .eachCount()
            .mapValues { it.value.toLong() }

        val rules = input.subList(2, input.size).associate { line ->
            line.split(" -> ").let { (match, inserted) ->
                (match[0] to match[1]) to inserted[0]
            }
        }

        return pairs to rules
    }

    private fun polymerize(pairs: PairMap, rules: RuleMap): PairMap {
        val newPairs = mutableMapOf<Pair<Char, Char>, Long>().nonNull { 0L }

        for ((pair, quantity) in pairs) {
            val insertion = rules[pair] ?: error("No rule for pair $pair")

            newPairs[pair.first to insertion] += quantity
            newPairs[insertion to pair.second] += quantity
        }

        return newPairs
    }

    private fun countElements(pairs: PairMap): Map<Char, Long> {
        val quantities = mutableMapOf<Char, Long>().nonNull { 0L }

        for ((pair, quantity) in pairs) {
            quantities[pair.first] += quantity
            quantities[pair.second] += quantity
        }

        // All elements except the outer ones are counted twice
        return quantities.mapValues { (it.value + 1) / 2 }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val (template, rules) = parseInput(input.toList())

        val polymerPairs = (0 until 10).fold(template) { pairs, _ -> polymerize(pairs, rules) }
        val elementCount = countElements(polymerPairs).values.sorted()

        return (elementCount.last() - elementCount.first()).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val (template, rules) = parseInput(input.toList())

        val polymerPairs = (0 until 40).fold(template) { pairs, _ -> polymerize(pairs, rules) }
        val elementCount = countElements(polymerPairs).values.sorted()

        return (elementCount.last() - elementCount.first()).toString()
    }
}
