package dev.lfelzinga.adventofkode.puzzles.aoc21.day24

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.alu.SubmarineALU
import dev.lfelzinga.adventofkode.toolbox.vm.alu.SubmarineALU.ALUOpcode.*
import java.util.Stack

class SubmarineALUPuzzle : Puzzle<Sequence<String>>(2021, 24, LinesTransformer()) {

    private fun parseInput(input: Sequence<String>): List<SubmarineALU> {
        val alus = mutableListOf<SubmarineALU>()
        val program = mutableListOf<String>()

        for (line in input) {
            if (line == "inp w" && program.isNotEmpty()) {
                alus += SubmarineALU.fromAssembly(program)
                program.clear()
            }

            program += line
        }

        if (program.isNotEmpty()) {
            alus += SubmarineALU.fromAssembly(program)
        }

        return alus
    }

    /*
        All ALU's perform the same operation with a few differences:
        * Exactly half the ALU's divide z by 26
        * All ALU's perform a (z % 26 + x) == w check
            * If false: z = z * 26 + w + y.
            * If true: z is unchanged.
            * On half the ALU's (the half that don't divide by 26), this check always fails as x >= 10.
        * To solve, the ALUs that divide and multiply need to be paired up and canceled out, by ensuring the check
          always passes on ALU's that divide.
     */
    private fun solveOptimized(alus: List<SubmarineALU>, minimize: Boolean = false): Long {
        data class ALUParameters(val index: Int, val x: Long, val y: Long)
        val aluParameters = alus.mapIndexed { index, alu ->
            ALUParameters(
                index,
                (alu.instructions[5] as AddConst).value,    // x parameter
                (alu.instructions[15] as AddConst).value    // y parameter
            )
        }

        val digits = IntArray(14)
        val unpaired = Stack<ALUParameters>()

        for (current in aluParameters) {
            if (current.x >= 10) {
                unpaired.push(current)
            } else {
                val prev: ALUParameters = unpaired.pop()

                val diff = current.x + prev.y

                if (diff > 0) { // Current digit must be higher
                    if (minimize) {
                        digits[prev.index] = 1
                        digits[current.index] = 1 + diff.toInt()
                    } else {
                        digits[current.index] = 9
                        digits[prev.index] = 9 - diff.toInt()
                    }
                } else {
                    if (minimize) {
                        digits[current.index] = 1
                        digits[prev.index] = 1 - diff.toInt()
                    } else {
                        digits[prev.index] = 9
                        digits[current.index] = 9 + diff.toInt()
                    }
                }
            }
        }

        return digits.fold(0) { num, digit -> num * 10 + digit }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val alus = parseInput(input)

        return solveOptimized(alus, false).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val alus = parseInput(input)

        return solveOptimized(alus, true).toString()
    }
}
