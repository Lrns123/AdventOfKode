package dev.lfelzinga.adventofkode.puzzles.aoc21.day2

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class SubmarinePositionPuzzle : Puzzle<Sequence<String>>(2021, 2, LinesTransformer()) {
    private fun parseInput(input: Sequence<String>) = input
        .map { it.split(' ') }
        .map { (dir, amount) -> dir to amount.toInt() }

    override fun solveFirstPart(input: Sequence<String>): String {
        var pos = 0
        var depth = 0

        for ((dir, amount) in parseInput(input)) {
            when (dir) {
                "forward" -> pos += amount
                "up" -> depth -= amount
                "down" -> depth += amount
                else -> error("Unknown direction $dir")
            }
        }

        return (pos * depth).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        var aim = 0
        var pos = 0
        var depth = 0

        for ((dir, amount) in parseInput(input)) {
            when (dir) {
                "forward" -> {
                    pos += amount
                    depth += aim * amount
                }
                "up" -> aim -= amount
                "down" -> aim += amount
                else -> error("Unknown direction $dir")
            }
        }

        return (pos * depth).toString()
    }
}
