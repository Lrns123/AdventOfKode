package dev.lfelzinga.adventofkode.puzzles.aoc21.day3

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class DiagnosticPuzzle : Puzzle<Sequence<String>>(2021, 3, LinesTransformer()) {

    private fun getOneBitCounts(measurements: List<String>): IntArray {
        val oneCounts = IntArray(measurements.first().length) { 0 }

        for (measurement in measurements) {
            for ((index, bit) in measurement.withIndex()) {
                if (bit == '1') oneCounts[index]++
            }
        }

        return oneCounts
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val measurements = input.toList()
        val oneCounts = getOneBitCounts(measurements)

        val gammaRate = oneCounts.foldIndexed(0) { index, acc, count ->
            if (count >= measurements.size / 2) acc or (1 shl (oneCounts.size - index - 1)) else acc
        }

        val epsilonRate = gammaRate.inv() and ((1 shl oneCounts.size) - 1)

        return (gammaRate * epsilonRate).toString()
    }

    private fun getBitCounts(measurements: List<String>, position: Int): Pair<Int, Int> {
        var oneCounts = 0
        var zeroCounts = 0

        for (measurement in measurements) {
            if (measurement[position] == '0') zeroCounts++ else oneCounts++
        }

        return oneCounts to zeroCounts
    }

    private fun reduce(measurements: List<String>, predicate: (oneCounts: Int, zeroCounts: Int) -> Char): Int {
        val numBits = measurements.first().length
        var reducedList = measurements

        for (i in 0 until numBits) {
            val (oneCount, zeroCount) = getBitCounts(reducedList, i)
            val bitToKeep = predicate(oneCount, zeroCount)
            reducedList = reducedList.filter { it[i] == bitToKeep }

            if (reducedList.size == 1) {
                return reducedList.first().toInt(2)
            }
        }

        error("Could not reduce to one value")
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val measurements = input.toList()

        val oxygenGeneratorRating = reduce(measurements) { oneCount, zeroCount -> if (oneCount >= zeroCount) '1' else '0' }
        val c02ScrubberRating = reduce(measurements) { oneCount, zeroCount -> if (oneCount < zeroCount) '1' else '0' }

        return (oxygenGeneratorRating * c02ScrubberRating).toString()
    }
}
