package dev.lfelzinga.adventofkode.puzzles.aoc21.day19

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.combinations
import dev.lfelzinga.adventofkode.toolbox.vector.Vector3

class ScannerPuzzle : Puzzle<Sequence<String>>(2021, 19, LinesTransformer()) {
    companion object {
        private val beaconRegex = Regex("(-?\\d+),(-?\\d+),(-?\\d+)")

        private val projections = listOf<Vector3.() -> Vector3>(
            { Vector3(x, y, z) },
            { Vector3(y, -x, z) },
            { Vector3(-x, -y, z) },
            { Vector3(-y, x, z) },

            { Vector3(-x, y, -z) },
            { Vector3(y, x, -z) },
            { Vector3(x, -y, -z) },
            { Vector3(-y, -x, -z) },

            { Vector3(-z, y, x) },
            { Vector3(y, z, x) },
            { Vector3(z, -y, x) },
            { Vector3(-y, -z, x) },

            { Vector3(z, y, -x) },
            { Vector3(y, -z, -x) },
            { Vector3(-z, -y, -x) },
            { Vector3(-y, z, -x) },

            { Vector3(x, -z, y) },
            { Vector3(-z, -x, y) },
            { Vector3(-x, z, y) },
            { Vector3(z, x, y) },

            { Vector3(x, z, -y) },
            { Vector3(z, -x, -y) },
            { Vector3(-x, -z, -y) },
            { Vector3(-z, x, -y) },
        )

        fun projections(beacons: List<Vector3>) = sequence {
            for (projection in projections) {
                yield(beacons.map { it.projection() })
            }
        }
    }

    private fun parseInput(input: Sequence<String>): List<List<Vector3>> {
        val scanners = mutableListOf<List<Vector3>>()
        val beacons = mutableListOf<Vector3>()

        for (line in input) {
            if (line.isEmpty()) continue

            if (line.startsWith("---")) {
                if (beacons.isNotEmpty()) {
                    scanners += beacons.toList()
                }
                beacons.clear()
            } else {
                val (x, y, z) = beaconRegex.matchEntire(line)?.destructured ?: error("Cannot parse beacon '$line'")

                beacons += Vector3(x.toInt(), y.toInt(), z.toInt())
            }
        }

        if (beacons.isNotEmpty()) {
            scanners += beacons
        }

        return scanners
    }

    private fun resolveLocations(measurements: List<List<Vector3>>): Pair<Set<Vector3>, Set<Vector3>> {
        val remainingScanners = measurements.toMutableList()
        val knownBeacons = measurements.first().toMutableSet()
        val knownScanners = mutableSetOf(Vector3(0, 0 ,0))

        remainingScanners.removeAt(0)

        while (remainingScanners.isNotEmpty()) {
            val remaining = remainingScanners.size
            val iterator = remainingScanners.listIterator()
            while (iterator.hasNext()) {
                for (projection in projections(iterator.next())) {
                    val delta = projection
                        .asSequence()
                        .flatMap { beacon -> knownBeacons.map { it - beacon } }
                        .groupingBy { it }
                        .eachCount()
                        .maxByOrNull { it.value }
                        ?.takeIf { it.value >= 12 }
                        ?.key
                        ?: continue

                    for (beacon in projection) {
                        knownBeacons += beacon + delta
                    }
                    knownScanners += delta
                    iterator.remove()
                    break
                }
            }

            if (remaining == remainingScanners.size) {
                error("Could not reduce unknown scanners")
            }
        }

        return knownBeacons to knownScanners
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val measurements = parseInput(input)
        return resolveLocations(measurements).first.size.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val measurements = parseInput(input)
        return resolveLocations(measurements).second.toList()
            .combinations(2)
            .maxOf { (first, second) -> (first - second).magnitude }
            .toString()
    }
}
