package dev.lfelzinga.adventofkode.puzzles.aoc21.day25

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.coordinates
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get

class SeaCucumberPuzzle : Puzzle<Sequence<String>>(2021, 25, LinesTransformer()) {

    private data class Environment(
        val rightMovingCucumbers: Set<Vector2>,
        val downMovingCucumbers: Set<Vector2>,
        val width: Int,
        val height: Int
    ) {
        operator fun contains(location: Vector2): Boolean {
            return location in rightMovingCucumbers || location in downMovingCucumbers
        }
    }

    private fun parseInput(input: Sequence<String>): Environment {
        val grid = input.toList().toCharGrid()
        val rightPositions = mutableSetOf<Vector2>()
        val downPositions = mutableSetOf<Vector2>()

        for (point in grid.coordinates) {
            if (grid[point] == '>') rightPositions += point
            else if (grid[point] == 'v') downPositions += point
        }

        return Environment(rightPositions, downPositions, grid.width, grid.height)
    }

    private fun advance(environment: Environment): Pair<Boolean, Environment> {
        var noMovement = true
        val newRightPositions = mutableSetOf<Vector2>()
        val newDownPositions = mutableSetOf<Vector2>()

        for (cucumber in environment.rightMovingCucumbers) {
            val newPos = cucumber.copy(x = (cucumber.x + 1) % environment.width)
            newRightPositions += if (newPos !in environment) {
                noMovement = false
                newPos
            } else {
                cucumber
            }
        }

        for (cucumber in environment.downMovingCucumbers) {
            val newPos = cucumber.copy(y = (cucumber.y + 1) % environment.height)
            newDownPositions += if (newPos !in environment.downMovingCucumbers && newPos !in newRightPositions) {
                noMovement = false
                newPos
            } else {
                cucumber
            }
        }

        return noMovement to Environment(newRightPositions, newDownPositions, environment.width, environment.height)
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        var environment = parseInput(input)
        var steps = 0

        do {
            val (finished, newEnv) = advance(environment)
            steps++
            environment = newEnv
        } while (!finished)

        return steps.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return "Merry Christmas!"
    }
}
