package dev.lfelzinga.adventofkode.puzzles.aoc21.day7

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.TokenizingTransformer
import dev.lfelzinga.adventofkode.toolbox.median
import kotlin.math.abs

class CrabPuzzle : Puzzle<List<String>>(2021, 7, TokenizingTransformer(",")) {

    private val Int.fuelCost
        get() = (this * (this + 1)) / 2

    override fun solveFirstPart(input: List<String>): String {
        val positions = input.map { it.toInt() }
        val targetPosition = positions.sorted().median()

        return positions.sumOf { abs(it - targetPosition) }.toString()
    }

    override fun solveSecondPart(input: List<String>): String {
        val positions = input.map { it.toInt() }.sorted()

        val minFuel = (positions.first()..positions.last()).minOf { target ->
            positions.sumOf { abs(it - target).fuelCost }
        }

        return minFuel.toString()
    }
}
