package dev.lfelzinga.adventofkode.puzzles.aoc21.day18

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.combinations

class SnailfishPuzzle : Puzzle<Sequence<String>>(2021, 18, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { SnailfishNumber.parse(it) }
            .reduce { acc, number -> acc + number }
            .magnitude()
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { SnailfishNumber.parse(it) }
            .toList()
            .combinations(2)
            .flatMap { (first, second) -> sequenceOf(first + second, second + first) }
            .maxOf { it.magnitude() }
            .toString()
    }
}
