package dev.lfelzinga.adventofkode.puzzles.aoc21.day17

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Rect
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import kotlin.math.absoluteValue
import kotlin.math.sign

class LauncherPuzzle : Puzzle<String>(2021, 17, StringTransformer()) {
    companion object {
        private val inputRegex = Regex("target area: x=(-?\\d+)..(-?\\d+), y=(-?\\d+)..(-?\\d+)")
    }

    private fun parseInput(input: String): Rect {
        return inputRegex.matchEntire(input)?.destructured?.let { (x1, x2, y1, y2) ->
            Rect(Vector2(x1.toInt(), y1.toInt()), Vector2(x2.toInt(), y2.toInt()))
        } ?: error("Could not parse input")
    }

    private fun findTrajectoryRange(target: Rect): Pair<IntRange, IntRange> {
        // The min X velocity is the lowest velocity that will come to a standstill at the start of the target.
        val minX = (1..target.min.x).first { it * (it + 1) / 2 >= target.min.x }

        // The max X velocity is the velocity that shoots it to the far end of the target is 1 step.
        val maxX = target.max.x

        // The max downward velocity is the velocity that shoots it to the far bottom of the target in 1 step.
        val minY = target.min.y

        // The max upward velocity is the velocity one less than the max downward velocity (with sign removed).
        // Shots fired upward follow a parabolic trajectory and reach Y=0 at the same speed that were initially fired upward with.
        // The next step down further increases the speed by 1, hence the subtraction of 1.
        val maxY = -minY - 1

        return minX..maxX to minY..maxY
    }

    private fun Vector2.applyDragAndGravity() = copy(
        x = (x.absoluteValue - 1).coerceAtLeast(0) * x.sign,
        y = y - 1
    )

    private fun trajectoryHitsTarget(launchVector: Vector2, target: Rect): Boolean {
        var velocity = launchVector
        var position = launchVector

        while (position.x <= target.max.x && position.y >= target.min.y ) {
            if (position in target) {
                return true
            }

            velocity = velocity.applyDragAndGravity()
            position += velocity
        }

        return false
    }

    override fun solveFirstPart(input: String): String {
        val target = parseInput(input)
        val (_, yRange) = findTrajectoryRange(target)

        return (yRange.last * (yRange.last + 1) / 2).toString()
    }

    override fun solveSecondPart(input: String): String {
        val target = parseInput(input)
        val (xRange, yRange) = findTrajectoryRange(target)

        return sequence { for (x in xRange) for (y in yRange) yield(Vector2(x, y)) }
            .count { trajectoryHitsTarget(it, target) }
            .toString()
    }
}
