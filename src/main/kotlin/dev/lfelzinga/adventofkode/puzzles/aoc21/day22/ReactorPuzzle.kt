package dev.lfelzinga.adventofkode.puzzles.aoc21.day22

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Cuboid
import dev.lfelzinga.adventofkode.toolbox.vector.Vector3

class ReactorPuzzle : Puzzle<Sequence<String>>(2021, 22, LinesTransformer()) {
    companion object {
        private val lineRegex = Regex("(on|off) x=(-?\\d++)..(-?\\d++),y=(-?\\d++)..(-?\\d++),z=(-?\\d++)..(-?\\d++)")
    }

    private class Reactor {
        private val regions = mutableListOf<Region>()

        fun addRegion(newRegion: Region) {
            regions += regions
                .mapNotNull { region -> (region.cube intersect newRegion.cube)?.let { Region(!region.additive, it) } }

            if (newRegion.additive)
                regions += newRegion
        }

        fun enabledCuboids(): Long {
            return regions.fold(0L) { acc, region -> acc + region.cube.volume * if (region.additive) 1 else -1 }
        }
    }

    private data class Region(val additive: Boolean, val cube: Cuboid)

    private fun parseInput(input: Sequence<String>) = input
        .map { lineRegex.matchEntire(it)?.destructured ?: error("Could not parse line $it") }
        .map { (mode, x1, x2, y1, y2, z1, z2) -> Region(
            mode == "on",
            Cuboid(
                Vector3(x1.toInt(), y1.toInt(), z1.toInt()),
                Vector3(x2.toInt(), y2.toInt(), z2.toInt())
            )
        )}

    override fun solveFirstPart(input: Sequence<String>): String {
        val reactor = Reactor()
        val initCube = Cuboid(Vector3(-50, -50, -50), Vector3(50, 50, 50))

        parseInput(input)
            .filter { it.cube.intersectsWith(initCube) }
            .forEach { reactor.addRegion(it) }

        return reactor.enabledCuboids().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val reactor = Reactor()

        parseInput(input)
            .forEach { reactor.addRegion(it) }

        return reactor.enabledCuboids().toString()
    }
}
