package dev.lfelzinga.adventofkode.puzzles.aoc21.day16

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.bitstream.BitStream
import kotlin.math.max
import kotlin.math.min

class PacketPuzzle : Puzzle<String>(2021, 16, StringTransformer()) {

    private fun BitStream.readVarLengthLong(): Long {
        var value = 0L

        do {
            val hasMore = readInt(1) == 1
            value = (value shl 4) or readLong(4)
        } while (hasMore)

        return value
    }

    private sealed class Packet(val version: Int) {
        class LiteralPacket(version: Int, val value: Long) : Packet(version) {
            override fun eval(): Long = value
            override fun versionSum(): Int = version
        }

        sealed class OperatorPacket(version: Int, val operands: List<Packet>) : Packet(version) {
            override fun versionSum(): Int = version + operands.sumOf { it.versionSum() }
        }

        class SumPacket(version: Int, operands: List<Packet>) : OperatorPacket(version, operands) {
            override fun eval(): Long = operands.sumOf { it.eval() }
        }

        class ProductPacket(version: Int, operands: List<Packet>) : OperatorPacket(version, operands) {
            override fun eval(): Long = operands.fold(1L) { acc, it -> acc * it.eval() }
        }

        class MinPacket(version: Int, operands: List<Packet>) : OperatorPacket(version, operands) {
            override fun eval(): Long = operands.fold(Long.MAX_VALUE) { acc, it -> min(acc, it.eval()) }
        }

        class MaxPacket(version: Int, operands: List<Packet>) : OperatorPacket(version, operands) {
            override fun eval(): Long = operands.fold(Long.MIN_VALUE) { acc, it -> max(acc, it.eval()) }
        }

        class GtPacket(version: Int, operands: List<Packet>) : OperatorPacket(version, operands) {
            override fun eval(): Long = if (operands[0].eval() > operands[1].eval()) 1L else 0L
        }

        class LtPacket(version: Int, operands: List<Packet>) : OperatorPacket(version, operands) {
            override fun eval(): Long = if (operands[0].eval() < operands[1].eval()) 1L else 0L
        }

        class EqPacket(version: Int, operands: List<Packet>) : OperatorPacket(version, operands) {
            override fun eval(): Long = if (operands[0].eval() == operands[1].eval()) 1L else 0L
        }

        abstract fun eval(): Long
        abstract fun versionSum(): Int
    }

    private fun readPacket(bitStream: BitStream): Packet {
        val version = bitStream.readInt(3)

        fun readOperands(): List<Packet> = when (bitStream.readInt(1)) {
            0 -> {
                val length = bitStream.readInt(15)
                val pos = bitStream.position

                buildList {
                    while (bitStream.position - pos < length) add(readPacket(bitStream))
                }
            }
            1 -> (0 until bitStream.readInt(11)).map { readPacket(bitStream) }
            else -> error("Unreachable")
        }

        return when (bitStream.readInt(3)) {
            0 -> Packet.SumPacket(version, readOperands())
            1 -> Packet.ProductPacket(version, readOperands())
            2 -> Packet.MinPacket(version, readOperands())
            3 -> Packet.MaxPacket(version, readOperands())
            4 -> Packet.LiteralPacket(version, bitStream.readVarLengthLong())
            5 -> Packet.GtPacket(version, readOperands())
            6 -> Packet.LtPacket(version, readOperands())
            7 -> Packet.EqPacket(version, readOperands())
            else -> error("Unknown type id")
        }
    }

    override fun solveFirstPart(input: String): String {
        return readPacket(BitStream.fromHex(input)).versionSum().toString()
    }

    override fun solveSecondPart(input: String): String {
        return readPacket(BitStream.fromHex(input)).eval().toString()
    }
}
