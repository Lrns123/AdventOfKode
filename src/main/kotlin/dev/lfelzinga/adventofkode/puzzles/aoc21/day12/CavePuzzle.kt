package dev.lfelzinga.adventofkode.puzzles.aoc21.day12

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class CavePuzzle : Puzzle<Sequence<String>>(2021, 12, LinesTransformer()) {

    private fun parseInput(input: Sequence<String>): Map<String, List<String>> {
        return input
            .flatMap { it.split('-').let { (first, second) -> listOf(first to second, second to first) } }
            .groupBy({ it.first }, { it.second })
    }

    data class Route(val location: String, val path: List<String>, val didDoubleVisit: Boolean)

    private fun findPaths(connections: Map<String, List<String>>, allowDoubleVisit: Boolean): Int {
        val queue = ArrayDeque<Route>()
        var routes = 0

        queue += Route("start", emptyList(), false)

        while (queue.isNotEmpty()) {
            val route = queue.removeFirst()

            if (route.location == "end") {
                routes++
                continue
            }

            for (cave in connections.getValue(route.location)) {
                if (cave == "start") continue
                if (cave.first().isLowerCase() && cave in route.path) {
                    if (allowDoubleVisit && !route.didDoubleVisit) {
                        queue.addLast(Route(cave, route.path + route.location, true))
                    }

                    continue
                }

                queue.addLast(Route(cave, route.path + route.location, route.didDoubleVisit))
            }
        }

        return routes
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return findPaths(parseInput(input), false).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return findPaths(parseInput(input), true).toString()
    }
}
