package dev.lfelzinga.adventofkode.puzzles.aoc21.day9

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.explore
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.contains
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.coordinates
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get

class BasinPuzzle : Puzzle<Sequence<String>>(2021, 9, LinesTransformer()) {

    private class BasinGraph(private val grid: CharGrid) : Graph<Vector2> {
        override fun neighbours(node: Vector2) = node.neighbours().filter { it in grid && grid[it] != '9' }
    }

    private fun exploreBasin(grid: CharGrid, start: Vector2): Set<Vector2> = buildSet {
        BasinGraph(grid).explore(start) {
            this += it.node
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()

        return grid.coordinates
            .filter { coord -> coord.neighbours().filter { it in grid }.all { grid[it] > grid[coord] } }
            .sumOf { (grid[it] - '0') + 1 }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()
        val visited = mutableSetOf<Vector2>()
        val basinSizes = mutableListOf<Int>()

        grid.coordinates
            .filter { grid[it] != '9' && it !in visited }
            .forEach {
                val basinNodes = exploreBasin(grid, it)
                visited += basinNodes
                basinSizes += basinNodes.size
            }

        return basinSizes.sortedDescending().take(3).reduce { acc, i -> acc * i }.toString()
    }
}
