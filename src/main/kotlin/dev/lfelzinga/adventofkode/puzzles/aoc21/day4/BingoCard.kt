package dev.lfelzinga.adventofkode.puzzles.aoc21.day4

import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid

class BingoCard(val grid: IntGrid) {
    private val marked = BooleanGrid(grid.width, grid.height)

    fun markNumber(num: Int) {
        grid.array.forEachIndexed { pos, value -> if (value == num) marked.array[pos] = true }
    }

    fun hasWon(): Boolean {
        for (row in 0 until marked.height)
            if (hasFullRow(row))
                return true

        for (col in 0 until marked.width)
            if (hasFullColumn(col))
                return true

        return false
    }

    private fun hasFullRow(row: Int): Boolean {
        for (col in 0 until marked.width)
            if (!marked[col, row])
                return false

        return true
    }

    private fun hasFullColumn(column: Int): Boolean {
        for (row in 0 until marked.height)
            if (!marked[column, row])
                return false

        return true
    }

    fun getUnmarkedNumbers() = sequence {
        marked.array.forEachIndexed { pos, marked -> if (!marked) yield(grid.array[pos]) }
    }
}
