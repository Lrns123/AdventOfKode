package dev.lfelzinga.adventofkode.puzzles.aoc21.day23

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Edge
import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.dijkstraSPF
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class AmphipodPuzzle : Puzzle<Sequence<String>>(2021, 23, LinesTransformer()) {

    companion object {
        private val movementCost = mapOf('A' to 1, 'B' to 10, 'C' to 100, 'D' to 1000)
    }

    private data class Environment(
        val roomSize: Int,
        val hallwaySize: Int,
        val roomPositions: List<Int>
    )

    private data class State(val hallway: List<Char>, val rooms: List<List<Char>>) {
        fun isFinalState(): Boolean {
            if (hallway.any { it != ' ' }) return false

            for ((i, room) in rooms.withIndex()) {
                if (!room.all { it == 'A' + i }) return false
            }

            return true
        }

        fun moveToHallway(room: Int, hallwayPosition: Int): State {
            require(rooms[room].isNotEmpty()) { "Cannot move from empty room" }
            require(hallway[hallwayPosition] == ' ') { "Cannot move to occupied hallway location" }

            return State(
                hallway = hallway.toMutableList().apply { this[hallwayPosition] = rooms[room].first() },
                rooms = List(rooms.size) {
                    if (room == it) {
                        rooms[it].subList(1, rooms[it].size)
                    } else {
                        rooms[it]
                    }
                }
            )
        }

        fun moveFromHallway(hallwayPosition: Int, room: Int): State {
            require(hallway[hallwayPosition] != ' ') { "Cannot move from empty hallway location" }

            return State(
                hallway = hallway.toMutableList().apply { this[hallwayPosition] = ' ' },
                rooms = List(rooms.size) {
                    if (room == it) {
                        listOf(hallway[hallwayPosition]) + rooms[it]
                    } else {
                        rooms[it]
                    }
                }
            )
        }
    }

    private class HallwayMaze(private val env: Environment) : WeightedGraph<State> {

        private val hallwayPositions = (0 until env.hallwaySize).filter { it !in env.roomPositions }

        private fun State.pathClear(fromX: Int, toX: Int): Boolean {
            for (x in min(fromX, toX)..max(fromX, toX)) {
                if (x != fromX && hallway[x] != ' ') return false
            }

            return true
        }

        override fun edges(node: State): List<Edge<State>> = buildList {
            for ((index, room) in node.rooms.withIndex()) {
                if (room.any { it != 'A' + index }) {
                    val roomX = env.roomPositions[index]
                    hallwayPositions
                        .filter { node.pathClear(roomX, it) }
                        .forEach {
                            val cost = (abs(roomX - it) + (env.roomSize + 1 - room.size)) * movementCost[room.first()]!!
                            add(Edge(node.moveToHallway(index, it), cost))
                        }
                }
            }

            for ((position, ch) in node.hallway.withIndex()) {
                if (ch == ' ') continue

                val targetRoom = ch - 'A'
                val room = node.rooms[targetRoom]
                val roomX = env.roomPositions[targetRoom]

                if (node.pathClear(position, roomX) && (room.isEmpty() || room.all { it == ch })) {
                    val cost = (abs(roomX - position) + (env.roomSize - room.size)) * movementCost[ch]!!
                    add(Edge(node.moveFromHallway(position, targetRoom), cost))
                }
            }
        }
    }

    private fun parseInput(input: List<String>): Pair<Environment, State> {
        data class Amphipod(val kind: Char, val position: Vector2)

        val hallwayY = input.indexOfFirst { it.any { ch -> ch == '.' } }
        val hallwayMinX = input[hallwayY].indexOfFirst { it == '.' }
        val hallwayMaxX = input[hallwayY].indexOfLast { it == '.' }
        val hallwaySize = hallwayMaxX - hallwayMinX + 1

        val amphipods = mutableSetOf<Amphipod>()

        for (y in hallwayY + 1 until input.size) {
            for ((x, ch) in input[y].withIndex()) {
                if (ch in 'A'..'D') {
                    amphipods += Amphipod(ch, Vector2(x, y))
                }
            }
        }

        val roomPositions = amphipods.map { it.position.x - hallwayMinX }.distinct().sorted()
        val rooms = roomPositions.map { x ->
            amphipods.filter { it.position.x == x + 1 }.sortedBy { it.position.y }.map { it.kind }
        }

        return Environment(
            rooms.first().size,
            hallwaySize,
            roomPositions
        ) to State(List(hallwaySize) { ' ' }, rooms)
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val (environment, initialState) = parseInput(input.toList())
        val maze = HallwayMaze(environment)

        val path = maze.dijkstraSPF(initialState) { it.node.isFinalState() } ?: error("Could not find path")

        return path.cost.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val (environment, initialState) = parseInput(input.toMutableList().apply {
            addAll(3, listOf("  #D#C#B#A#", "  #D#B#A#C#"))
        })

        val maze = HallwayMaze(environment)

        val path = maze.dijkstraSPF(initialState) { it.node.isFinalState() } ?: error("Could not find path")

        return path.cost.toString()
    }
}
