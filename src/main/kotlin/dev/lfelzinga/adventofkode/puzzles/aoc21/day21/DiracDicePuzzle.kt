package dev.lfelzinga.adventofkode.puzzles.aoc21.day21

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.memoized

class DiracDicePuzzle : Puzzle<Sequence<String>>(2021, 21, LinesTransformer()) {

    companion object {
        private val lineRegex = Regex("Player (\\d+) starting position: (\\d+)")
    }

    private class DeterministicDie {
        private var value = 1
        var rolls = 0
            private set

        fun roll(): Int = (1 + (value++ - 1) % 100).also { rolls++ }
    }

    private fun runPracticeGame(startPositions: List<Int>, die: DeterministicDie): List<Int> {
        val positions = startPositions.toMutableList()
        val scores = MutableList(startPositions.size) { 0 }
        var turn = 0

        while (scores.none { it >= 1000 }) {
            val roll = die.roll() + die.roll() + die.roll()

            positions[turn % positions.size] = 1 + (positions[turn % positions.size] + roll - 1) % 10
            scores[turn % scores.size] += positions[turn % positions.size]

            turn++
        }

        return scores
    }

    private fun runQuantumGame(startPositions: List<Int>): List<Long> {
        data class State(val positions: List<Int>, val scores: List<Int>, val turn: Int) {
            fun applyRoll(roll: Int): State {
                val newPosition = 1 + (positions[turn] + roll - 1) % 10
                return State(
                    positions = positions.toMutableList().apply { this[turn] = newPosition },
                    scores = scores.toMutableList().apply { this[turn] += newPosition },
                    turn = (turn + 1) % positions.size
                )
            }
        }

        fun playTurn(state: State, cache: MutableMap<State, List<Long>> = mutableMapOf()): List<Long> = memoized(state, cache) {
            if (state.scores.any { it >= 21 }) {
                List(state.scores.size) { if (state.scores[it] >= 21) 1L else 0L }
            } else {
                val wins = MutableList(state.positions.size) { 0L }
                for (roll in 1..3) {
                    for (roll2 in 1..3) {
                        for (roll3 in 1..3) {
                            playTurn(state.applyRoll(roll + roll2 + roll3), cache)
                                .forEachIndexed { index, winCount -> wins[index] += winCount }
                        }
                    }
                }
                wins
            }
        }

        return playTurn(State(startPositions, List(startPositions.size) { 0 }, 0))
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val startingPositions = input
            .map { lineRegex.matchEntire(it)?.destructured ?: error("Could not parse line $it") }
            .map { (_, start) -> start.toInt() }
            .toList()

        val die = DeterministicDie()
        val finalScores = runPracticeGame(startingPositions, die)

        return (finalScores.minOrNull()!! * die.rolls).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val startingPositions = input
            .map { lineRegex.matchEntire(it)?.destructured ?: error("Could not parse line $it") }
            .map { (_, start) -> start.toInt() }
            .toList()

        val wins = runQuantumGame(startingPositions)

        return wins.maxOrNull().toString()
    }
}
