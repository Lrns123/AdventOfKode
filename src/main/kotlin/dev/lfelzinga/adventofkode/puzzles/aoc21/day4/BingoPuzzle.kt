package dev.lfelzinga.adventofkode.puzzles.aoc21.day4

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid

class BingoPuzzle : Puzzle<Sequence<String>>(2021, 4, LinesTransformer()) {

    private fun parseInput(input: Sequence<String>): Pair<List<Int>, List<BingoCard>> {
        val iterator = input.iterator()
        val numbers = iterator.next().split(',').map { it.toInt() }
        val bingoCards = mutableListOf<BingoCard>()

        while (iterator.hasNext()) {
            iterator.next() // Skip empty line
            val grid = IntGrid(5, 5)
            repeat(5) { y ->
                iterator.next().split(' ').filter { it.isNotEmpty() }.map { it.toInt() }.forEachIndexed { x, num -> grid[x, y] = num }
            }

            bingoCards += BingoCard(grid)
        }

        return numbers to bingoCards
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val (numbers, cards) = parseInput(input)

        for (number in numbers) {
            for (card in cards) {
                card.markNumber(number)
                if (card.hasWon()) {
                    return (card.getUnmarkedNumbers().sum() * number).toString()
                }
            }
        }

        return "???"
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val (numbers, cards) = parseInput(input)
        val remainingCards = cards.toMutableList()

        for (number in numbers) {
            val iterator = remainingCards.listIterator()
            while (iterator.hasNext()) {
                val card = iterator.next()
                card.markNumber(number)
                if (card.hasWon()) {
                    iterator.remove()
                    if (remainingCards.isEmpty()) {
                        return (card.getUnmarkedNumbers().sum() * number).toString()
                    }
                }
            }
        }

        return "???"
    }
}
