package dev.lfelzinga.adventofkode.puzzles.aoc21.day15

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Edge
import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.dijkstraSPFTo
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2

class ChitonPuzzle : Puzzle<Sequence<String>>(2021, 15, LinesTransformer()) {

    private class ChitonMaze(val grid: CharGrid, val size: Int): WeightedGraph<Vector2> {
        override fun edges(node: Vector2): List<Edge<Vector2>> {
            return node.neighbours()
                .filter { it.x in 0 until grid.width * size && it.y in 0 until grid.height * size }
                .map {
                    val risk = grid[it.x % grid.width, it.y % grid.height] - '0'

                    val xTile = it.x / grid.width
                    val yTile = it.y / grid.height
                    val offset = xTile + yTile

                    Edge(it, 1 + (risk + offset - 1) % 9)
                }
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()

        val path = ChitonMaze(grid, 1)
            .dijkstraSPFTo(Vector2(0, 0), Vector2(grid.width - 1, grid.height - 1))
            ?: error("Could not find path")

        return path.cost.toString()

    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()

        val path = ChitonMaze(grid, 5)
            .dijkstraSPFTo(Vector2(0, 0), Vector2((grid.width * 5) - 1, (grid.height * 5) - 1))
            ?: error("Could not find path")

        return path.cost.toString()
    }
}
