package dev.lfelzinga.adventofkode.puzzles.aoc21.day11

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.contains
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.coordinates
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.set

class DumboOctopusPuzzle : Puzzle<Sequence<String>>(2021, 11, LinesTransformer()) {

    private fun Vector2.neighbours8() = listOf(
        this + Vector2(0, 1),
        this + Vector2(1, 1),
        this + Vector2(1, 0),
        this + Vector2(1, -1),
        this + Vector2(0, -1),
        this + Vector2(-1, -1),
        this + Vector2(-1, 0),
        this + Vector2(-1, 1)
    )

    private fun flash(grid: CharGrid, vec: Vector2) {
        grid[vec] = 'F'

        for (neighbour in vec.neighbours8().filter { it in grid }) {
            when (grid[neighbour]) {
                '9' -> flash(grid, neighbour)
                in '0'..'8' -> grid[neighbour]++
            }
        }
    }

    private fun runStep(grid: CharGrid): Int {
        for (vec in grid.coordinates) {
            when (grid[vec]) {
                '9' -> flash(grid, vec)
                in '0'..'8' -> grid[vec]++
            }
        }

        var flashes = 0
        for ((i, ch) in grid.array.withIndex()) {
            if (ch == 'F') {
                grid.array[i] = '0'
                flashes++
            }

        }

        return flashes
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()

        return (0 until 100).fold(0) { flashes, _ -> flashes + runStep(grid) }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()
        var steps = 0

        while (!grid.array.all { it == '0' }) {
            runStep(grid)
            steps++
        }

        return steps.toString()
    }
}
