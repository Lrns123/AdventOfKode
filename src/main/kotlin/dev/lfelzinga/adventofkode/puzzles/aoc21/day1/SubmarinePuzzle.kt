package dev.lfelzinga.adventofkode.puzzles.aoc21.day1

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class SubmarinePuzzle : Puzzle<Sequence<String>>(2021, 1, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { it.toInt() }
            .windowed(2)
            .count { it[0] < it[1] }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { it.toInt() }
            .windowed(4)
            .count { it.first() < it.last() }
            .toString()
    }
}
