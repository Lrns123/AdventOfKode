package dev.lfelzinga.adventofkode.puzzles.aoc21.day10

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.median

class SyntaxCheckerPuzzle : Puzzle<Sequence<String>>(2021, 10, LinesTransformer()) {

    private fun getSyntaxErrorScore(line: String): Int {
        val nesting = ArrayDeque<Char>()

        for (ch in line) {
            when (ch) {
                '(', '[', '{', '<' -> nesting.addFirst(ch)
                ')' -> if (nesting.removeFirst() != '(') return 3
                ']' -> if (nesting.removeFirst() != '[') return 57
                '}' -> if (nesting.removeFirst() != '{') return 1197
                '>' -> if (nesting.removeFirst() != '<') return 25137
            }
        }

        return 0
    }

    private fun getAutoCompleteScore(line: String): Long {
        val nesting = ArrayDeque<Char>()

        for (ch in line) {
            when (ch) {
                '(', '[', '{', '<' -> nesting.addFirst(ch)
                ')' -> if (nesting.removeFirst() != '(') return -1L
                ']' -> if (nesting.removeFirst() != '[') return -1L
                '}' -> if (nesting.removeFirst() != '{') return -1L
                '>' -> if (nesting.removeFirst() != '<') return -1L
            }
        }

        return nesting.fold(0L) { acc, ch ->
            acc * 5L + when(ch) {
                '(' -> 1L
                '[' -> 2L
                '{' -> 3L
                '<' -> 4L
                else -> 0L
            }
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .sumOf { getSyntaxErrorScore(it) }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { getAutoCompleteScore(it) }
            .filter { it != -1L }
            .sorted()
            .toList()
            .median()
            .toString()
    }
}
