package dev.lfelzinga.adventofkode.puzzles.aoc21.day18

sealed class SnailfishNumber(var parent: Compound? = null) {

    companion object {
        fun parse(str: String): SnailfishNumber {
            return parseValue(str.iterator())
        }

        private fun parseValue(iterator: Iterator<Char>): SnailfishNumber {
            return when (val ch = iterator.next()) {
                in '0'..'9' -> Leaf(ch - '0')
                '[' -> parseCompound(iterator)
                else -> error("Syntax error: expected digit or '[', got '$ch'")
            }
        }

        private fun Iterator<Char>.requireNext(expected: Char) {
            val ch = next()
            if (ch != expected) error("Syntax error: expected '$expected', got '$ch")
        }

        private fun parseCompound(iterator: Iterator<Char>): Compound {
            val left = parseValue(iterator)

            iterator.requireNext(',')

            val right = parseValue(iterator)

            iterator.requireNext(']')

            return Compound(left, right)
        }
    }

    operator fun plus(other: SnailfishNumber) = Compound(this.copy(), other.copy()).reduced()

    abstract fun copy(): SnailfishNumber
    abstract fun magnitude(): Long

    protected abstract fun tryExplode(depth: Int): Boolean
    protected abstract fun trySplit(): Boolean

    protected abstract fun leftMostLeaf(): Leaf
    protected abstract fun rightMostLeaf(): Leaf

    protected fun reduced() = apply {
        while (tryExplode(0) || trySplit()) {
            // Continue
        }
    }

    protected fun replaceSelf(replacement: SnailfishNumber) {
        val parent = parent ?: return

        if (parent.left === this) {
            parent.left = replacement
        } else {
            parent.right = replacement
        }

        replacement.parent = parent
    }

    protected fun getLeftLeaf(): Leaf? {
        val parent = parent ?: return null
        return if (parent.right === this) parent.left.rightMostLeaf() else parent.getLeftLeaf()
    }

    protected fun getRightLeaf(): Leaf? {
        val parent = parent ?: return null
        return if (parent.left === this) parent.right.leftMostLeaf() else parent.getRightLeaf()
    }

    class Leaf(val value: Int) : SnailfishNumber() {
        override fun tryExplode(depth: Int): Boolean = false

        override fun trySplit(): Boolean {
            if (value >= 10) {
                val half = value / 2
                val otherHalf = if (half * 2 < value) half + 1 else half

                replaceSelf(Compound(Leaf(half), Leaf(otherHalf)))
                return true
            }

            return false
        }

        override fun leftMostLeaf(): Leaf = this
        override fun rightMostLeaf(): Leaf = this

        override fun magnitude(): Long = value.toLong()

        override fun copy(): SnailfishNumber = Leaf(value)

        override fun toString(): String = value.toString()
    }

    class Compound(var left: SnailfishNumber, var right: SnailfishNumber) : SnailfishNumber() {
        init {
            require(left.parent == null) { "Attempting to re-parent a snailfish number" }
            left.parent = this

            require(right.parent == null) { "Attempting to re-parent a snailfish number" }
            right.parent = this
        }

        override fun tryExplode(depth: Int): Boolean {
            if (depth == 4) {
                require(left is Leaf && right is Leaf) { "Explode with compound branches" }

                getLeftLeaf()?.let { it.replaceSelf(Leaf(it.value + (left as Leaf).value)) }
                getRightLeaf()?.let { it.replaceSelf(Leaf(it.value + (right as Leaf).value)) }

                replaceSelf(Leaf(0))

                return true
            }

            return left.tryExplode(depth + 1) || right.tryExplode(depth + 1)
        }

        override fun trySplit(): Boolean {
            return left.trySplit() || right.trySplit()
        }

        override fun leftMostLeaf(): Leaf = left.leftMostLeaf()
        override fun rightMostLeaf(): Leaf = right.rightMostLeaf()

        override fun magnitude(): Long = 3L * left.magnitude() + 2L * right.magnitude()

        override fun copy(): SnailfishNumber = Compound(left.copy(), right.copy())

        override fun toString() = "[$left,$right]"
    }
}
