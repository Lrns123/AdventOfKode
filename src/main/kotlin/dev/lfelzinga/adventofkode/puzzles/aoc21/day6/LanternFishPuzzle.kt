package dev.lfelzinga.adventofkode.puzzles.aoc21.day6

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.TokenizingTransformer

class LanternFishPuzzle : Puzzle<List<String>>(2021, 6, TokenizingTransformer(",")) {
    private fun parseInput(input: List<Int>): LongArray {
        return LongArray(9) { timer -> input.count { it == timer }.toLong() }
    }

    private fun simulateDays(initialTimers: LongArray, days: Int): LongArray {
        return (0 until days).fold(initialTimers) { timers, _ ->
            LongArray(9) { timers[(it + 1) % 9] }.also { it[6] += timers[0] }
        }
    }

    override fun solveFirstPart(input: List<String>): String {
        return simulateDays(parseInput(input.map { it.toInt() }), 80).sum().toString()
    }

    override fun solveSecondPart(input: List<String>): String {
        return simulateDays(parseInput(input.map { it.toInt() }), 256).sum().toString()
    }
}
