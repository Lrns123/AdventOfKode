package dev.lfelzinga.adventofkode.puzzles.aoc19.day3

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.plus

class WirePuzzle : Puzzle<Sequence<String>>(2019, 3, LinesTransformer()) {
    private fun readPaths(input: Sequence<String>): List<Map<Vector2, Int>> {
        return input
            .map { line -> line.split(',').map { it[0] to it.substring(1).toInt() } }
            .map { tracePath(it) }
            .toList()
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val paths = readPaths(input)

        return (paths[0].keys intersect paths[1].keys)
            .minOfOrNull { it.magnitude }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val paths = readPaths(input)

        return (paths[0].keys intersect paths[1].keys)
            .minOfOrNull { paths[0].getValue(it) + paths[1].getValue(it) }
            .toString()
    }

    private fun tracePath(steps: List<Pair<Char, Int>>): Map<Vector2, Int> {
        val path = mutableMapOf<Vector2, Int>()

        var position = Vector2(0, 0)
        var length = 0

        for (step in steps) {
            val direction = Direction.fromChar(step.first)

            repeat(step.second) {
                position += direction
                length++

                path.putIfAbsent(position, length)
            }
        }

        return path
    }
}
