package dev.lfelzinga.adventofkode.puzzles.aoc19.day16

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import kotlin.math.absoluteValue

class FrequencyPuzzle : Puzzle<String>(2019, 16, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        val phase = MutableList(input.length) { input[it] - '0' }

        repeat(100) {
            fft(phase)
        }

        return phase.take(8).joinToString("")
    }

    override fun solveSecondPart(input: String): String {
        val phase = MutableList(10_000 * input.length) { input[it % input.length] - '0' }
        val offset = phase.take(7).joinToString("").toInt()

        repeat(100) {
            fft(phase, offset)
        }

        return phase.subList(offset, offset + 8).joinToString("")
    }

    private fun fft(phase: MutableList<Int>, offset: Int = 0) {
        if (offset > phase.size / 2) {
            // Optimized path
            var sum = phase.last()
            for (position in phase.size - 2 downTo offset) {
                sum = (sum + phase[position]) % 10
                phase[position] = sum
            }
        } else {
            for (position in offset until phase.size) {
                var sum = 0
                for (index in position until phase.size) {
                    sum += phase[index] * pattern[((1 + index) / (position + 1)) % pattern.size]
                }

                phase[position] = sum.absoluteValue % 10
            }
        }
    }

    companion object {
        private val pattern = listOf(0, 1, 0, -1)
    }
}
