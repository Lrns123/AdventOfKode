package dev.lfelzinga.adventofkode.puzzles.aoc19.day10

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2D
import java.util.*

class AsteroidPuzzle : Puzzle<Sequence<String>>(2019, 10, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val asteroids = readInput(input)

        return asteroids.asSequence().map { candidateLocation ->
            asteroids.asSequence().filter { it != candidateLocation }.distinctBy { candidateLocation.angleToDeg(it) }.count()
        }.maxOrNull().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val asteroids = readInput(input)

        val stationLocation = asteroids.maxByOrNull { candidateLocation ->
            asteroids.asSequence().filter { it != candidateLocation }.distinctBy { candidateLocation.angleToDeg(it) }.count()
        } ?: throw IllegalStateException("Could not find station location")

        val destructionOrder = TreeMap<Double, Vector2D>()

        asteroids.asSequence()
            .filter { it != stationLocation }
            .groupBy { stationLocation.angleToDeg(it) }
            .forEach { (angle, asteroids) ->
                asteroids
                    .sortedBy { it.distanceTo(stationLocation) }
                    .forEachIndexed { index, point ->
                        destructionOrder[index * 360 + angle] = point
                    }
            }

        val asteroid = destructionOrder.values.toList()[199]
        return (asteroid.x * 100 + asteroid.y).toInt().toString()
    }

    private fun readInput(input: Sequence<String>): List<Vector2D> {
        return input.mapIndexed { y, row ->
            row.mapIndexedNotNull { x, char ->
                when (char) {
                    '#' -> Vector2D(x.toDouble(), y.toDouble())
                    else -> null
                }
            }
        }.flatten().toList()
    }
}
