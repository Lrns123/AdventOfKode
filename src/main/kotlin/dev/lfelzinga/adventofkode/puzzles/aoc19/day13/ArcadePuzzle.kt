package dev.lfelzinga.adventofkode.puzzles.aoc19.day13

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.runBlocking
import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter
import kotlin.math.sign

class ArcadePuzzle : Puzzle<List<Long>>(2019, 13, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String {
        return IntCodeInterpreter(input, memorySize = 3000)().chunked(3).count { it[2] == 2L }.toString()
    }

    override fun solveSecondPart(input: List<Long>): String = runBlocking {
        val interpreter = IntCodeInterpreter(input, Channel(1), memorySize = 3000).also {
            it.memory[0] = 2
            it.run(this)
        }

        var ballX: Long
        var paddleX = -1L
        var score = 0L

        for (message in messages(interpreter.output)) {
            when (message) {
                is Message.ScoreMessage -> score = message.score
                is Message.BlockMessage -> when (message.type) {
                    3L -> paddleX = message.x
                    4L -> {
                        ballX = message.x
                        interpreter.input.send(ballX.compareTo(paddleX).sign.toLong())
                    }
                }
            }
        }

        score.toString()
    }

    private sealed class Message {
        class BlockMessage(val x: Long, val y: Long, val type: Long): Message()
        class ScoreMessage(val score: Long): Message()
    }

    private fun CoroutineScope.messages(channel: Channel<Long>) = produce {
        while (true) {
            val x = channel.receiveCatching().getOrNull() ?: break
            val y = channel.receive()
            val arg = channel.receive()

            when (x) {
                -1L -> send(Message.ScoreMessage(arg))
                else -> send(Message.BlockMessage(x, y, arg))
            }
        }
    }
}
