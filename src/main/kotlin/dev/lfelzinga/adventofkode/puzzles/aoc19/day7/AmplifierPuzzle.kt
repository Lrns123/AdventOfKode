package dev.lfelzinga.adventofkode.puzzles.aoc19.day7

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.permutations
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class AmplifierPuzzle : Puzzle<List<Long>>(2019, 7, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String {
        return listOf(0, 1, 2, 3, 4).permutations().map { thrusterOutputFor(input, it) }.maxOrNull().toString()
    }

    override fun solveSecondPart(input: List<Long>): String {
        return listOf(5, 6, 7, 8, 9).permutations().map { loopedThrusterOutputFor(input, it) }.maxOrNull().toString()
    }

    private fun thrusterOutputFor(program: List<Long>, phases: List<Int>): Long {
        val channels = List(phases.size + 1) { Channel<Long>() }
        val amplifiers = List(phases.size) { IntCodeInterpreter(program, channels[it], channels[it + 1]) }

        return runBlocking {
            launch {
                phases.forEachIndexed { index, phase -> channels[index].send(phase.toLong()) }
                channels[0].send(0)
            }

            amplifiers.forEach { it.run(this) }

            channels.last().receive()
        }
    }

    private fun loopedThrusterOutputFor(program: List<Long>, phases: List<Int>): Long {
        val channels = List(phases.size) { Channel<Long>(1) }
        val amplifiers = List(phases.size) { IntCodeInterpreter(program, channels[it], channels[(it + 1) % phases.size]) }

        return runBlocking {
            coroutineScope {
                launch {
                    phases.forEachIndexed { index, phase -> channels[index].send(phase.toLong()) }
                    channels[0].send(0)
                }
                amplifiers.forEach { it.run(this) }
            }

            channels.first().receive()
        }
    }
}
