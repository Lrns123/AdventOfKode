package dev.lfelzinga.adventofkode.puzzles.aoc19.day2

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class IntCodePuzzle : Puzzle<List<Long>>(2019, 2, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String {
        val interpreter = IntCodeInterpreter(input)
        interpreter.memory[1] = 12
        interpreter.memory[2] = 2

        interpreter()

        return interpreter.memory[0].toString()
    }

    override fun solveSecondPart(input: List<Long>): String {
        for (noun in 0..99L) {
            for (verb in 0..99L) {
                val interpreter = IntCodeInterpreter(input)
                interpreter.memory[1] = noun
                interpreter.memory[2] = verb

                interpreter()

                if (interpreter.memory[0] == 19690720L) {
                    return (100 * noun + verb).toString()
                }
            }
        }

        return "???"
    }
}
