package dev.lfelzinga.adventofkode.puzzles.aoc19.day4

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class PasswordPuzzle : Puzzle<String>(2019, 4, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        val range = input.split('-').map { it.toInt() }

        return passwordSequence(range[0], range[1]).count { passwordValid(it) }.toString()
    }

    override fun solveSecondPart(input: String): String {
        val range = input.split('-').map { it.toInt() }

        return passwordSequence(range[0], range[1]).count { passwordValid2(it) }.toString()
    }

    private fun passwordSequence(from: Int, to: Int) = sequence {
        val password = from.toString().toCharArray()
        val end = to.toString()

        correctInput(password)

        while (true) {
            val candidate = String(password)
            if (candidate > end) break

            yield(candidate)
            increment(password, password.lastIndex)
        }
    }

    private fun correctInput(digits: CharArray) {
        var highest = '0'
        for (i in digits.indices) when {
            digits[i] < highest -> digits[i] = highest
            else -> highest = digits[i]
        }
    }

    private fun increment(digits: CharArray, position: Int) {
        if (digits[position] == '9') {
            increment(digits, position - 1)
            digits[position] = digits[position - 1]
        } else {
            digits[position]++
        }
    }

    private fun passwordValid(password: String): Boolean = hasPair(password)

    private fun passwordValid2(password: String): Boolean = hasProperPair(password)

    private fun hasPair(password: String): Boolean {
        for (i in 0 until password.length - 1) {
            if (password[i] == password[i + 1]) {
                return true
            }
        }

        return false
    }

    private fun hasProperPair(password: String): Boolean {
        for (i in 0 until password.length - 1) {
            if (password[i] == password[i + 1]) {
                if ((i - 1 >= 0 && password[i - 1] == password[i]) || (i + 2 < password.length && password[i + 2] == password[i + 1])) {
                    continue
                }

                return true
            }
        }

        return false
    }
}
