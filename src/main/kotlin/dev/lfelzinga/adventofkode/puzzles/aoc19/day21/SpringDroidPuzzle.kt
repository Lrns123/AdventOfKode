package dev.lfelzinga.adventofkode.puzzles.aoc19.day21

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class SpringDroidPuzzle : Puzzle<List<Long>>(2019, 21, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String {
        val program = "NOT A J\nNOT B T\nOR T J\nNOT C T\nOR T J\nAND D J\nWALK\n"

        val interpreter = IntCodeInterpreter(input, memorySize = 3000)
        return interpreter(*program.map { it.code.toLong() }.toLongArray()).last().toString()
    }

    override fun solveSecondPart(input: List<Long>): String {
        val program = "NOT A J\nNOT B T\nOR T J\nNOT C T\nOR T J\nAND D J\nAND E T\nOR H T\nAND T J\nRUN\n"

        val interpreter = IntCodeInterpreter(input, memorySize = 3000)
        return interpreter(*program.map { it.code.toLong() }.toLongArray()).last().toString()
    }
}
