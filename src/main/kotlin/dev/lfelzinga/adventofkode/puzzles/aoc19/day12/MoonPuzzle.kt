package dev.lfelzinga.adventofkode.puzzles.aoc19.day12

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.combinations
import dev.lfelzinga.adventofkode.toolbox.lcm
import dev.lfelzinga.adventofkode.toolbox.vector.Vector3

class MoonPuzzle : Puzzle<Sequence<String>>(2019, 12, LinesTransformer()) {
    companion object {
        private val coordRegex = Regex("""<x=(-?\d+), y=(-?\d+), z=(-?\d+)>""")
    }

    data class Moon(var position: Vector3, var velocity: Vector3)

    override fun solveFirstPart(input: Sequence<String>): String {
        val moons = readMoons(input)

        repeat(1000) {
            simulationStep(moons)
        }

        return moons.sumOf { it.position.magnitude * it.velocity.magnitude }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val moons = readMoons(input)

        var xCycle = -1L
        var yCycle = -1L
        var zCycle = -1L
        val initialState = moons.map { it.position }
        var steps = 0L

        while (xCycle == -1L || yCycle == -1L || zCycle == -1L) {
            steps++
            simulationStep(moons)

            val deltas = moons.mapIndexed { index, moon ->
                listOf((moon.position - initialState[index]), moon.velocity)
            }.flatten()

            if (xCycle == -1L && deltas.all { it.x == 0 }) xCycle = steps
            if (yCycle == -1L && deltas.all { it.y == 0 }) yCycle = steps
            if (zCycle == -1L && deltas.all { it.z == 0 }) zCycle = steps
        }

        return listOf(xCycle, yCycle, zCycle).reduce(::lcm).toString()
    }

    private fun readMoons(input: Sequence<String>): List<Moon> {
        return input
            .map { coordRegex.matchEntire(it)?.destructured ?: throw IllegalStateException("Could not read input $it") }
            .map { (x, y, z) -> Moon(Vector3(x.toInt(), y.toInt(), z.toInt()), Vector3(0, 0, 0)) }
            .toList()
    }

    private fun simulationStep(moons: List<Moon>) {
        for ((moon1, moon2) in moons.combinations(2)) {
            calculateGravity(moon1, moon2, { it.position.x }, { Vector3(it, 0, 0) })
            calculateGravity(moon1, moon2, { it.position.y }, { Vector3(0, it, 0) })
            calculateGravity(moon1, moon2, { it.position.z }, { Vector3(0, 0, it) })
        }

        for (moon in moons) {
            moon.position += moon.velocity
        }
    }

    private inline fun calculateGravity(moon1: Moon, moon2: Moon, coordGetter: (Moon) -> Int, vectorBuilder: (Int) -> Vector3) {
        val deltaVector = vectorBuilder(coordGetter(moon1).compareTo(coordGetter(moon2)))

        moon1.velocity -= deltaVector
        moon2.velocity += deltaVector
    }
}
