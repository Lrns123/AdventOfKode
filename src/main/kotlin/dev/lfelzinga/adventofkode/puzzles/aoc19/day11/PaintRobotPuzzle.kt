package dev.lfelzinga.adventofkode.puzzles.aoc19.day11

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.ocr.ocr
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.plus
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.set
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class PaintRobotPuzzle : Puzzle<List<Long>>(2019, 11, IntCodeTransformer()) {

    override fun solveFirstPart(input: List<Long>): String = runBlocking {
        val panels = mutableMapOf<Vector2, Long>()
        val interpreter = IntCodeInterpreter(input, Channel(1), memorySize = 2000).also { it.run(this) }
        var position = Vector2(0, 0)
        var direction = Direction.NORTH

        while (true) {
            interpreter.input.send(panels.getOrDefault(position, 0L))
            val color = interpreter.output.receiveCatching().getOrNull() ?: break
            val turnDirection = interpreter.output.receive()

            panels[position] = color
            direction = when (turnDirection) {
                0L -> direction.left()
                1L -> direction.right()
                else -> error("Unknown turn direction $turnDirection")
            }

            position += direction.delta
        }

        panels.size.toString()
    }

    override fun solveSecondPart(input: List<Long>): String = runBlocking {
        val panels = CharGrid(42, 6).filledWith(' ')
        val interpreter = IntCodeInterpreter(input, Channel(1), memorySize = 2000).also { it.run(this) }
        var position = Vector2(0, 0)
        var direction = Direction.NORTH

        panels[0, 0] = '\u2591'

        while (true) {
            interpreter.input.send(if (panels[position] == ' ') 0 else 1)
            val color = interpreter.output.receiveCatching().getOrNull() ?: break
            val turnDirection = interpreter.output.receive()

            panels[position] = if (color == 0L) ' ' else '\u2591'
            direction = when (turnDirection) {
                0L -> direction.left()
                1L -> direction.right()
                else -> error("Unknown turn direction $turnDirection")
            }

            position += direction
        }

        panels.ocr('\u2591', 1)
    }
}
