package dev.lfelzinga.adventofkode.puzzles.aoc19.day6

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.breadthFirstSearchTo

class OrbitPuzzle : Puzzle<Sequence<String>>(2019, 6, LinesTransformer()) {
    class OrbitingBody(val id: String, var orbit: OrbitingBody? = null, val orbiters: MutableList<OrbitingBody> = mutableListOf())
    class SolarSystem : Graph<OrbitingBody> {
        val bodies = mutableMapOf<String, OrbitingBody>()

        override fun neighbours(node: OrbitingBody): List<OrbitingBody> = mutableListOf<OrbitingBody>().apply {
            addAll(node.orbiters)
            node.orbit?.let { this += it }
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val system = readOrbits(input)

        return calculateOrbits(system.bodies["COM"] ?: error("COM not found"), 0).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val system = readOrbits(input)

        val yourOrbit = system.bodies["YOU"]?.orbit ?: error("Orbit for YOU not found")
        val santaOrbit = system.bodies["SAN"]?.orbit ?: error("Orbit for SAN not found")

        val path = system.breadthFirstSearchTo(yourOrbit, santaOrbit)

        return path?.cost.toString()
    }

    private fun readOrbits(input: Sequence<String>): SolarSystem {
        val system = SolarSystem()

        input.map { it.split(')') }
            .forEach { (orbitedId, orbiterId) ->
                val orbited = system.bodies.computeIfAbsent(orbitedId) { OrbitingBody(it) }
                val orbiter = system.bodies.computeIfAbsent(orbiterId) { OrbitingBody(it, orbited) }

                orbited.orbiters += orbiter
                if (orbiter.orbit == null) {
                    orbiter.orbit = orbited
                }
            }

        return system
    }

    private fun calculateOrbits(node: OrbitingBody, depth: Int): Int {
        var orbits = depth

        for (orbiter in node.orbiters) {
            orbits += calculateOrbits(orbiter, depth + 1)
        }

        return orbits
    }
}
