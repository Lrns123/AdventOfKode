package dev.lfelzinga.adventofkode.puzzles.aoc19.day8

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.ocr.ocr

class PasswordImagePuzzle : Puzzle<String>(2019, 8, StringTransformer()) {
    companion object {
        private const val imageWidth = 25
        private const val imageHeight = 6
    }

    override fun solveFirstPart(input: String): String {
        val layers = input.chunked(imageWidth * imageHeight)
        val bestLayer = layers.minByOrNull { layer -> layer.count { it == '0' } } ?: error("No layers")

        return (bestLayer.count { it == '1' } * bestLayer.count { it == '2' }).toString()
    }

    override fun solveSecondPart(input: String): String {
        val layers = input.chunked(imageWidth * imageHeight)
        val image = CharGrid(imageWidth, imageHeight).filledWith(' ')

        for (layer in layers) {
            for ((index, value) in layer.withIndex()) {
                if (image.array[index] != ' ') continue

                when (value) {
                    '0' -> image.array[index] = '\u2591'
                    '1' -> image.array[index] = '\u2588'
                }
            }
        }

        return image.ocr('\u2588')
    }
}
