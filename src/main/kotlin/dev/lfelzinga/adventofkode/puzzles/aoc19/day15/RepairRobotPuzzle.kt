package dev.lfelzinga.adventofkode.puzzles.aoc19.day15

import kotlinx.coroutines.runBlocking
import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.breadthFirstSearchTo
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.maxDepth
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.toDirection
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter
import java.util.*

class RepairRobotPuzzle : Puzzle<List<Long>>(2019, 15, IntCodeTransformer()) {
    class Maze(val map: Map<Vector2, Int>): Graph<Vector2> {
        override fun neighbours(node: Vector2) = node.neighbours().filter { map[it] != 0 }

        fun locationOfOxygenSystem(): Vector2 =
            map.entries.find { it.value == 2 }?.key ?: error("Oxygen system not found")
    }

    override fun solveFirstPart(input: List<Long>): String {
        val maze = exploreMaze(input)
        val path = maze.breadthFirstSearchTo(Vector2(0, 0), maze.locationOfOxygenSystem()) ?: error("Could not find path")

        return path.cost.toString()
    }

    override fun solveSecondPart(input: List<Long>): String {
        val maze = exploreMaze(input)
        val path = maze.maxDepth(maze.locationOfOxygenSystem())

        return path.cost.toString()
    }

    private fun exploreMaze(program: List<Long>): Maze = runBlocking {
        val interpreter = IntCodeInterpreter(program).also { it.run(this) }

        val map = mutableMapOf<Vector2, Int>()
        val backtrackStack = Stack<Vector2>()
        var pos = Vector2(0, 0)

        while (true) {
            val unvisited = pos.neighbours().firstOrNull { it !in map }
            if (unvisited == null) {
                if (backtrackStack.isEmpty()) break

                val backtrackPos = backtrackStack.pop()
                val direction = (backtrackPos - pos).toDirection() ?: error("Invalid backtrack position")
                interpreter.input.send(direction.toRobotInput())

                check(interpreter.output.receive() != 0L) { "Backtrack failed: Hit a wall" }
                pos = backtrackPos
            } else {
                val direction = (unvisited - pos).toDirection() ?: error("Invalid move position")
                interpreter.input.send(direction.toRobotInput())
                when (interpreter.output.receive().also { map[unvisited] = it.toInt() }) {
                    1L, 2L -> {
                        backtrackStack.push(pos)
                        pos = unvisited
                    }
                }
            }
        }

        interpreter.input.send(0L)
        Maze(map)
    }

    private fun Direction.toRobotInput() = when (this) {
        Direction.NORTH -> 1L
        Direction.SOUTH -> 2L
        Direction.WEST -> 3L
        Direction.EAST -> 4L
    }
}
