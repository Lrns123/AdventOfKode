package dev.lfelzinga.adventofkode.puzzles.aoc19.day1

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class FuelRequirementPuzzle : Puzzle<Sequence<String>>(2019, 1, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input.map { (it.toInt() / 3) - 2 }.sum().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input.map { fuelForMassRecursive(it.toInt()) }.sum().toString()
    }

    private fun fuelForMassRecursive(mass: Int): Int {
        val fuel = (mass / 3) - 2
        if (fuel <= 0) {
            return 0
        }

        return fuel + fuelForMassRecursive(fuel)
    }
}
