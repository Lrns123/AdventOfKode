package dev.lfelzinga.adventofkode.puzzles.aoc19.day14

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.binarySearch
import java.util.*

class FuelPuzzle : Puzzle<Sequence<String>>(2019, 14, LinesTransformer()) {
    data class Chemical(val name: String, val quantity: Long)
    data class Reaction(val input: List<Chemical>, val output: Chemical)

    override fun solveFirstPart(input: Sequence<String>): String {
        val reactions = readReactions(input)

        return oreRequiredForFuel(reactions, 1).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val reactions = readReactions(input)
        val totalOre = 1_000_000_000_000L
        val upperBound = totalOre / (oreRequiredForFuel(reactions, 1) / 4)

        val maxFuelForOre = binarySearch(1, upperBound) { oreRequiredForFuel(reactions, it) >= totalOre } - 1

        return maxFuelForOre.toString()
    }

    private fun readReactions(input: Sequence<String>): List<Reaction> {
        return input
            .map { it.split(" => ") }
            .map { (input, output) -> Reaction(input.split(", ").map(::readChemical), readChemical(output)) }
            .toList()
    }

    private fun readChemical(str: String): Chemical {
        val tokens = str.split(" ")
        return Chemical(tokens[1], tokens[0].toLong())
    }

    private fun oreRequiredForFuel(reactions: List<Reaction>, fuel: Long): Long {
        val reactionLookup = reactions.associateBy { it.output.name }
        val surplus = mutableMapOf<String, Long>()
        val queue = ArrayDeque<Chemical>()
        var ore = 0L

        queue += Chemical("FUEL", fuel)

        while (queue.isNotEmpty()) {
            val chemical = queue.poll()

            if (chemical.name == "ORE") {
                ore += chemical.quantity
                continue
            }

            val missing = chemical.quantity - (surplus[chemical.name] ?: 0L)
            if (missing <= 0L) {
                surplus[chemical.name] = -missing
                continue
            }

            val reaction = reactionLookup[chemical.name] ?: error("No recipe for ${chemical.name}")
            val batches = batchesRequired(missing, reaction.output.quantity)

            for (input in reaction.input) {
                queue += Chemical(input.name, input.quantity * batches)
            }

            surplus[chemical.name] = reaction.output.quantity * batches - missing
        }

        return ore
    }

    private fun batchesRequired(requested: Long, perBatch: Long): Long {
        return requested / perBatch + if (requested % perBatch == 0L) 0L else 1L
    }
}
