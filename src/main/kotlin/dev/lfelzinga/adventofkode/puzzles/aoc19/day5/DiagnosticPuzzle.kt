package dev.lfelzinga.adventofkode.puzzles.aoc19.day5

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class DiagnosticPuzzle : Puzzle<List<Long>>(2019, 5, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String {
        return IntCodeInterpreter(input)(1).last().toString()
    }

    override fun solveSecondPart(input: List<Long>): String {
        return IntCodeInterpreter(input)(5).last().toString()
    }
}
