package dev.lfelzinga.adventofkode.puzzles.aoc19.day23

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.produce
import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.nonNull
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class NetworkingPuzzle : Puzzle<List<Long>>(2019, 23, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String = runBlocking {
        val devices = createDevices(input)
        val switch = Switch(this)
        devices.forEachIndexed { address, device -> switch.registerDevice(address, NIC(device.input, device.output)) }

        for (packet in switch.nat) {
            coroutineContext[Job]?.cancelChildren()
            return@runBlocking packet.y.toString()
        }

        "???"
    }

    override fun solveSecondPart(input: List<Long>): String = runBlocking {
        val devices = createDevices(input)
        val switch = Switch(this)
        devices.forEachIndexed { address, device -> switch.registerDevice(address, NIC(device.input, device.output)) }

        var lastPacket: Packet? = null
        for (packet in switch.nat) {
            if (lastPacket?.y == packet.y) {
                coroutineContext[Job]?.cancelChildren()
                return@runBlocking packet.y.toString()
            }
            lastPacket = packet
        }

        "???"
    }

    private suspend fun CoroutineScope.createDevices(program: List<Long>): List<IntCodeInterpreter> {
        return List(50) { address ->
            IntCodeInterpreter(program, Channel(Channel.UNLIMITED), memorySize = 3000).also {
                it.input.send(address.toLong())
                it.run(this)
            }
        }
    }

    data class NIC(val inbound: Channel<Long>, val outbound: Channel<Long>)
    inner class Switch(scope: CoroutineScope): CoroutineScope by scope {
        private val idleScore = mutableMapOf<Int, Int>().nonNull { 0 }
        private val devices = mutableMapOf<Int, NIC>()
        val nat = produce {
            val natDevice = NIC(Channel(Channel.UNLIMITED), Channel())
            lateinit var lastPacket: Packet

            registerDevice(255, natDevice)

            launch {
                for (packet in natDevice.inboundPackets(this)) {
                    lastPacket = packet
                }
            }

            while (true) {
                if (idleScore.isNotEmpty() && idleScore.values.all { it > 2 }) {
                    devices[0]?.let {
                        it.inbound.send(lastPacket.x)
                        it.inbound.send(lastPacket.y)
                    } ?: error("No device at address zero")

                    idleScore[0] = 0
                    send(lastPacket)
                }
                yield()
            }
        }

        fun registerDevice(address: Int, nic: NIC) {
            require(address !in devices) { "Address $address already in use" }
            devices[address] = nic

            launch {
                while (true) {
                    if (nic.inbound.isEmpty) {
                        nic.inbound.send(-1L)
                        idleScore[address] += 1
                    } else {
                        idleScore[address] = 0
                    }
                    yield()
                }
            }

            launch {
                for (packet in nic.outboundPackets(this)) {
                    idleScore[address] = 0
                    devices[packet.destination]?.let {
                        it.inbound.send(packet.x)
                        it.inbound.send(packet.y)
                    } ?: error("Unknown destination ${packet.destination}")
                }
            }
        }
    }

    data class Packet(val destination: Int, val x: Long, val y: Long)
    private fun NIC.outboundPackets(scope: CoroutineScope) = scope.produce {
        while (true) {
            send(Packet(
                outbound.receiveCatching().getOrNull()?.toInt() ?: break,
                outbound.receive(),
                outbound.receive()
            ))
        }
    }

    private fun NIC.inboundPackets(scope: CoroutineScope) = scope.produce {
        while (true) {
            send(Packet(
                -1,
                (inbound.receiveCatching().getOrNull() ?: break).takeIf { it != -1L } ?: continue,
                inbound.receive()
            ))
        }
    }
}

