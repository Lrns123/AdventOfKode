package dev.lfelzinga.adventofkode.puzzles.aoc19.day17

import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.plus
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class ScaffoldPuzzle : Puzzle<List<Long>>(2019, 17, IntCodeTransformer()) {

    private class Scaffolding(
        val scaffolds: Set<Vector2>,
        val robotPosition: Vector2,
        val robotDirection: Direction
    )

    override fun solveFirstPart(input: List<Long>): String {
        val scaffolds = readScaffolding(input)

        return scaffolds.scaffolds
            .filter { position -> position.neighbours().all { it in scaffolds.scaffolds } }
            .sumOf { it.x * it.y }
            .toString()
    }

    override fun solveSecondPart(input: List<Long>): String {
        val subPaths = solveSubPaths(generatePath(readScaffolding(input)))

        return runBlocking {
            val interpreter = IntCodeInterpreter(input, memorySize = 5000).also { it.memory[0] = 2; it.run(this) }
            val instructions = "${subPaths.main}\n${subPaths.funcA}\n${subPaths.funcB}\n${subPaths.funcC}\nn\n"

            launch {
                for (char in instructions) {
                    interpreter.input.send(char.code.toLong())
                }
            }

            var output = 0L
            interpreter.output.consumeEach { output = it }

            output.toString()
        }
    }

    private fun readScaffolding(program: List<Long>): Scaffolding = runBlocking {
        val interpreter = IntCodeInterpreter(program, memorySize = 5000).also { it.run(this) }
        val scaffolds = mutableSetOf<Vector2>()
        lateinit var robotPosition: Vector2
        lateinit var robotDirection: Direction
        var x = 0
        var y = 0

        for (code in interpreter.output) when (code.toInt().toChar()) {
            '#' -> scaffolds += Vector2(x++, y)
            '.' -> x++
            '\n' -> { y++; x = 0 }
            '^', '<', '>', 'v' -> {
                robotPosition = Vector2(x, y)
                robotDirection = Direction.fromChar(code.toInt().toChar())
                scaffolds += Vector2(x++, y)
            }
            else -> error("Unrecognised output '$code'")
        }

        Scaffolding(scaffolds, robotPosition, robotDirection)
    }

    private fun generatePath(scaffolding: Scaffolding): List<String> {
        val path = mutableListOf<String>()
        var position = scaffolding.robotPosition
        var direction = scaffolding.robotDirection
        var pathLength = 0

        while (true) {
            if (position + direction in scaffolding.scaffolds) {
                position += direction
                pathLength++
            } else {
                if (pathLength > 0) path += pathLength.toString()
                pathLength = 0

                path += when {
                    position + direction.left() in scaffolding.scaffolds -> { direction--; "L" }
                    position + direction.right() in scaffolding.scaffolds -> { direction++; "R" }
                    else -> return path
                }
            }
        }
    }

    data class SubPaths(val main: String, val funcA: String, val funcB: String, val funcC: String)

    private fun <K, V> Map<K, V>.reversed(): Map<V, K> = mutableMapOf<V, K>().also {
        for (entry in entries) {
            it[entry.value] = entry.key
        }
    }

    private inline fun prefixSubstitution(name: String, input: String, block: (prefix: String, substituted: String) -> Unit) {
        val offset = input.indexOfFirst { it.isLowerCase() }
        if (offset == -1) return

        val maxLength = (input.length - offset - 1).coerceAtMost(5)

        for (length in 2..maxLength) {
            val prefix = input.substring(offset, offset + length)
            if (prefix.any { it.isUpperCase() }) break

            block(prefix, input.replace(prefix, name))
        }
    }

    /**
     * Sub paths algorithm:
     *
     * 1. Encode all turn-move pairs (e.g. L,8) into lower case characters (a, b, c, etc...)
     * 2. Take a (lower-case) prefix between 2 and (at most) 5 characters long (since more would exceed the memory limit)
     * 3. Substitute the prefix string with an upper case character (representing the function A, B, or C)
     * 4. Repeat step 3 for functions B and C
     * 5. Check if the final substitution has no more lower case characters left, if so, it is valid and represents the main program
     * 6. Expand prefixes A, B and C (i.e. a -> L,8) and return the result
     */
    private fun solveSubPaths(fullPath: List<String>): SubPaths {
        val dictionary = mutableMapOf<Pair<String, String>, Char>()
        val packed = fullPath
            .chunked(2) { dictionary.getOrPut(it[0] to it[1]) { 'a' + dictionary.size } }
            .joinToString("")

        val lookup = dictionary.reversed()
        fun String.unpack() = map { char -> lookup.getValue(char).let { "${it.first},${it.second}" } }.joinToString(",")

        prefixSubstitution("A", packed) { prefixA, substA ->
            prefixSubstitution("B", substA) { prefixB, substB ->
                prefixSubstitution("C", substB) { prefixC, substC ->
                    if (substC.none { it.isLowerCase() }) {
                        return SubPaths(
                            main = substC.toCharArray().joinToString(","),
                            funcA = prefixA.unpack(),
                            funcB = prefixB.unpack(),
                            funcC = prefixC.unpack()
                        )
                    }
                }
            }
        }

        error("Could not find paths")
    }
}
