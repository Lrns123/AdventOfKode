package dev.lfelzinga.adventofkode.puzzles.aoc19.day22

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import java.math.BigInteger

class CardShufflePuzzle : Puzzle<Sequence<String>>(2019, 22, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map(this::readShuffleTechnique)
            .fold(List(10007) { it }) { deck, technique -> technique.shuffle(deck) }
            .indexOf(2019)
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val size = BigInteger.valueOf(119315717514047L)
        val shuffles = BigInteger.valueOf(101741582076661L)

        val (offset, increment) = input
            .map(this::readShuffleTechnique)
            .fold(BigInteger.ZERO to BigInteger.ONE) { coeffs, technique -> technique.updateCoefficients(coeffs.first, coeffs.second, size) }

        val finalIncrement = increment.modPow(shuffles, size)
        val finalOffset = offset * (BigInteger.ONE - finalIncrement) * (BigInteger.ONE - increment).modInverse(size)

        val card = (finalOffset + finalIncrement * 2020.toBigInteger()) % size

        return card.toString()
    }

    private fun readShuffleTechnique(technique: String): ShuffleTechnique = when {
        technique == "deal into new stack" -> ShuffleTechnique.Deal
        technique.startsWith("cut ") -> ShuffleTechnique.Cut(technique.substring(4).toInt())
        technique.startsWith("deal with increment ") -> ShuffleTechnique.DealIncrement(technique.substring(20).toInt())
        else -> error("Unknown shuffle technique '$technique'")
    }

    sealed class ShuffleTechnique {
        abstract fun shuffle(deck: List<Int>): List<Int>
        abstract fun updateCoefficients(offset: BigInteger, increment: BigInteger, size: BigInteger): Pair<BigInteger, BigInteger>

        object Deal : ShuffleTechnique() {
            override fun shuffle(deck: List<Int>) = deck.reversed()
            override fun updateCoefficients(offset: BigInteger, increment: BigInteger, size: BigInteger): Pair<BigInteger, BigInteger> {
                val newIncrement = (-increment) % size
                val newOffset = (offset + newIncrement).mod(size)
                return newOffset to newIncrement
            }
        }

        class Cut(val n: Int) : ShuffleTechnique() {
            override fun shuffle(deck: List<Int>): List<Int> {
                return if (n < 0) {
                    deck.subList(deck.size + n, deck.size) + deck.subList(0, deck.size + n)
                } else {
                    deck.subList(n, deck.size) + deck.subList(0, n)
                }
            }

            override fun updateCoefficients(offset: BigInteger, increment: BigInteger, size: BigInteger): Pair<BigInteger, BigInteger> {
                return (offset + (n.toBigInteger() * increment)) % size to increment
            }
        }

        class DealIncrement(private val increment: Int): ShuffleTechnique() {
            override fun shuffle(deck: List<Int>): List<Int> {
                val result = MutableList(deck.size) { 0 }
                for (i in deck.indices) {
                    result[i * increment % deck.size] = deck[i]
                }

                return result
            }

            override fun updateCoefficients(offset: BigInteger, increment: BigInteger, size: BigInteger): Pair<BigInteger, BigInteger> {
                return offset to increment * this.increment.toBigInteger().modInverse(size)
            }
        }
    }
}
