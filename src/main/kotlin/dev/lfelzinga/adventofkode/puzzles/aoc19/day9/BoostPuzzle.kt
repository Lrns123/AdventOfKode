package dev.lfelzinga.adventofkode.puzzles.aoc19.day9

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class BoostPuzzle : Puzzle<List<Long>>(2019, 9, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String {
        return IntCodeInterpreter(input, memorySize = 2000)(1).last().toString()
    }

    override fun solveSecondPart(input: List<Long>): String {
        return IntCodeInterpreter(input, memorySize = 2000)(2).last().toString()
    }
}
