package dev.lfelzinga.adventofkode.puzzles.aoc19.day25

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.produce
import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class AirlockPuzzle : Puzzle<List<Long>>(2019, 25, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String = playAutomated(input)

    private fun playAutomated(program: List<Long>) = runBlocking {
        val interpreter = IntCodeInterpreter(program, memorySize = 6000).also { it.run(this) }

        launch {
            for (char in steps) {
                interpreter.input.send(char.code.toLong())
            }
        }

        for (line in interpreter.output.lines(this)) {
            if (line.startsWith("\"Oh, hello!")) {
                answerRegex.matchEntire(line)?.destructured?.let { (code) ->
                    coroutineContext[Job]?.cancelChildren()
                    return@runBlocking code
                } ?: error("Could not interpret answer line")
            }
        }

        "???"
    }

    private fun playInteractive(program: List<Long>) = runBlocking {
        val interpreter = IntCodeInterpreter(program, memorySize = 6000).also { it.run(this) }

        val inputJob = launch(Dispatchers.IO) {
            while (true) {
                interpreter.input.send(System.`in`.read().toLong())
            }
        }

        println()
        for (char in interpreter.output) {
            print(char.toInt().toChar())
        }

        inputJob.cancel()
    }

    override fun solveSecondPart(input: List<Long>): String {
        return "Merry Christmas!"
    }

    private fun Channel<Long>.lines(scope: CoroutineScope) = scope.produce {
        val builder = StringBuilder()

        for (ch in this@lines) {
            if (ch == 10L) {
                send(builder.toString())
                builder.setLength(0)
            } else {
                builder.append(ch.toInt().toChar())
            }
        }
    }

    companion object {
        // Required items: fixed point, candy cane, polygon, shell
        private const val steps = "south\ntake fixed point\nnorth\nnorth\ntake candy cane\nwest\nwest\ntake shell\neast\neast\nnorth\nnorth\ntake polygon\nsouth\nwest\nwest\nwest\n"
        private val answerRegex = Regex("\"Oh, hello! You should be able to get in by typing (\\d+) on the keypad at the main airlock.\"")
    }
}
