package dev.lfelzinga.adventofkode.puzzles.aoc19.day20

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.puzzles.aoc19.day20.PortalMazePuzzle.PortalMaze.Node
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.nonNull
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Edge
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.dijkstraSPF
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.explore
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get

class PortalMazePuzzle : Puzzle<Sequence<String>>(2019, 20, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val maze = PortalMaze(input.toList().toCharGrid())
        val target = Node(maze.exit, 0)

        val path = maze.dijkstraSPF(Node(maze.entrance, 0)) { it.node == target } ?: error("No path found")
        return path.cost.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val maze = PortalMaze(input.toList().toCharGrid(), recursive = true)
        val target = Node(maze.exit, 0)

        val path = maze.dijkstraSPF(Node(maze.entrance, 0)) { it.node == target } ?: error("No path found")
        return path.cost.toString()
    }

    class PortalMaze(private val maze: CharGrid, val recursive: Boolean = false): WeightedGraph<Node> {
        data class Node(val position: Vector2, val depth: Int)
        private val portals = scanPortals()
        private val portalLookup = portals.flatMap { (name, portals) -> portals.map { it to name } }.toMap()
        private val edges = exploreEdges()

        override fun edges(node: Node): List<Edge<Node>> {
            return edges.getValue(node.position)
                .mapNotNull { (position, distance) ->
                    val portal = portalLookup[position] ?: error("Position is not a valid portal")
                    val portalTarget = portals[portal]?.firstOrNull { endpoint -> endpoint != position }
                    val innerPortal = isInnerPortal(position)
                    when {
                        portal == "ZZ" && node.depth == 0 -> Edge(Node(position, node.depth), distance)
                        portalTarget == null -> null
                        !recursive -> Edge(Node(portalTarget, node.depth), distance + 1)
                        !innerPortal && node.depth == 0 -> null
                        innerPortal -> Edge(Node(portalTarget, node.depth + 1), distance + 1)
                        else -> Edge(Node(portalTarget, node.depth - 1), distance + 1)
                    }
                }
        }

        val entrance: Vector2 = portals["AA"]?.first() ?: error("Could not find entrance")
        val exit: Vector2 = portals["ZZ"]?.first() ?: error("Could not find exit")

        private fun isInnerPortal(position: Vector2) = position.x in (maze.width / 4)..(maze.width * 3 / 4) && position.y in (maze.height / 4)..(maze.height * 3 / 4)

        private fun scanPortals(): Map<String, List<Vector2>> {
            val portals = mutableMapOf<String, MutableList<Vector2>>().nonNull { mutableListOf() }

            for (x in 0 until maze.width) {
                for (y in 0 until maze.height) {
                    if (maze[x, y] in 'A'..'Z') {
                        if (x < maze.width - 1 && maze[x + 1, y] in 'A'..'Z') {
                            val name = "${maze[x, y]}${maze[x + 1, y]}"
                            val position = when {
                                x > 0 && maze[x - 1, y] == '.' -> Vector2(x - 1, y)
                                x < maze.width - 1 && maze[x + 2, y] == '.' -> Vector2(x + 2, y)
                                else -> error("Portal without adjacent space")
                            }

                            portals[name].add(position)
                        } else if (y < maze.height - 1 && maze[x, y + 1] in 'A'..'Z') {
                            val name = "${maze[x, y]}${maze[x, y + 1]}"
                            val position = when {
                                y > 0 && maze[x, y - 1] == '.' -> Vector2(x, y - 1)
                                y < maze.height - 1 && maze[x, y + 2] == '.' -> Vector2(x, y + 2)
                                else -> error("Portal without adjacent space")
                            }

                            portals[name].add(position)
                        }
                    }
                }
            }

            return portals
        }

        private fun exploreEdges(): Map<Vector2, Map<Vector2, Int>> {
            val reachablePoints = mutableMapOf<Vector2, MutableMap<Vector2, Int>>().nonNull { mutableMapOf() }
            val exploreGraph = object : Graph<Vector2> {
                override fun neighbours(node: Vector2) = node.neighbours().filter { maze[it] == '.' }
            }

            for (start in portalLookup.keys) {
                exploreGraph.explore(start) {
                    if (it.node != start && it.node in portalLookup.keys) {
                        reachablePoints[start][it.node] = it.cost
                    }
                }
            }

            return reachablePoints
        }
    }
}
