package dev.lfelzinga.adventofkode.puzzles.aoc19.day19

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.IntCodeTransformer
import dev.lfelzinga.adventofkode.toolbox.vm.intcode.IntCodeInterpreter

class TractorBeamPuzzle : Puzzle<List<Long>>(2019, 19, IntCodeTransformer()) {
    override fun solveFirstPart(input: List<Long>): String {
        var positionsInBeam = 0

        for (x in 0..49L) {
            for (y in 0..49L) {
                if(IntCodeInterpreter(input, memorySize = 500)(x, y).last() == 1L) {
                    positionsInBeam++
                }
            }
        }

        return positionsInBeam.toString()
    }

    override fun solveSecondPart(input: List<Long>): String {
        var y = 99L
        var x = 0L

        while (true) {
            when (IntCodeInterpreter(input, memorySize = 500)(x, y).last()) {
                0L -> x++
                1L -> when (IntCodeInterpreter(input, memorySize = 500)(x + 99, y - 99).last()) {
                    0L -> y++
                    1L -> return ((x * 10000) + (y - 99)).toString()
                }
            }
        }
    }
}
