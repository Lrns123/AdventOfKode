package dev.lfelzinga.adventofkode.puzzles.aoc19.day18

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.puzzles.aoc19.day18.VaultPuzzle.VaultMaze.Node
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Edge
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.backtrack
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.dijkstraSPF
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.explore
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.set

class VaultPuzzle : Puzzle<Sequence<String>>(2019, 18, LinesTransformer()) {
    private class VaultMaze(private val maze: CharGrid): WeightedGraph<Node> {
        data class Node(val position: Vector2, val keys: Int)
        private data class Path(val length: Int, val doors: Int)

        val edges = exploreEdges()

        override fun edges(node: Node): List<Edge<Node>> {
            return edges.getValue(node.position).mapNotNull { (target, path) ->
                when {
                    path.doors and node.keys.inv() != 0 -> null // Path contains doors with missing key
                    node.keys and (1 shl maze[target] - 'a') != 0 -> null // Destination is key already obtained
                    else -> Edge(Node(target, node.keys or (1 shl maze[target] - 'a')), path.length)
                }
            }
        }

        fun findEntrances() = sequence {
            for (pos in maze.array.indices) {
                if (maze.array[pos] == '@') {
                    yield(Vector2(pos % maze.width, pos / maze.width))
                }
            }
        }

        private fun findKeys() = sequence {
            for (pos in maze.array.indices) {
                when (maze.array[pos]) {
                    in 'a'..'z' -> yield(Vector2(pos % maze.width, pos / maze.width))
                }
            }
        }

        private fun exploreEdges(): Map<Vector2, Map<Vector2, Path>> {
            val paths = mutableMapOf<Vector2, MutableMap<Vector2, Path>>()
            val exploreGraph = object : Graph<Vector2> {
                override fun neighbours(node: Vector2) = node.neighbours().filter { maze[it] != '#' }
            }

            for (start in findKeys() + findEntrances()) {
                exploreGraph.explore(start) {
                    if (it.node != start) {
                        when (maze[it.node]) {
                            in 'a'..'z' -> paths.getOrPut(start) { mutableMapOf() }[it.node] = Path(
                                it.cost,
                                it.backtrack().fold(0) { acc, node -> when (val ch = maze[node.node]) {
                                    in 'A'..'Z' -> acc or (1 shl ch - 'A')
                                    else -> acc
                                } }
                            )
                        }
                    }
                }
            }

            return paths
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val optimized = VaultMaze(input.toList().toCharGrid())
        val path = optimized.dijkstraSPF(Node(optimized.findEntrances().first(), 0)) { pathNode -> pathNode.node.keys == targetMask } ?: error("Pathfinding failed")

        return path.cost.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val field = input.toList().toCharGrid()
        val start = field.array.indexOf('@').let { Vector2(it % field.width, it / field.width) }

        field[start] = '#'
        start.neighbours().forEach { field[it] = '#' }
        start.diagonals().forEach { field[it] = '@' }

        val maze = VaultMaze(field)

        return maze
            .findEntrances()
            .map {
                val reachableKeys = maze.edges.getValue(it)
                    .map { path -> field[path.key] - 'a' }
                    .fold(0) { acc, key -> acc or (1 shl key) }

                Node(it, targetMask xor reachableKeys)
            }
            .map { startPoint -> maze.dijkstraSPF(startPoint) { it.node.keys == targetMask }?.cost ?: error("No path found") }
            .sum()
            .toString()
    }

    private fun Vector2.diagonals() = diagonals.map { this + it }

    companion object {
        private val diagonals = listOf(Vector2(-1, -1), Vector2(1, -1), Vector2(-1, 1), Vector2(1, 1))
        private const val targetMask = 0x3FFFFFF
    }
}
