package dev.lfelzinga.adventofkode.puzzles.aoc19.day24

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.toCharGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Direction
import dev.lfelzinga.adventofkode.toolbox.vector.Rect
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.Vector3
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.get
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.plus
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.set

class BugsPuzzle : Puzzle<Sequence<String>>(2019, 24, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        var grid = input.toList().toCharGrid()
        val visited = mutableSetOf(grid.asBits())

        do {
            grid = update(grid)
        } while (visited.add(grid.asBits()))

        return grid.asBits().toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = input.toList().toCharGrid()
        var bugs = grid.bounds().points().filter { grid[it] == '#' }.map { Vector3(it.x, it.y, 0) }.toSet()

        repeat(200) {
            bugs = updateRecursive(bugs)
        }

        return bugs.size.toString()
    }

    private fun update(grid: CharGrid): CharGrid {
        val result = CharGrid(grid.width, grid.height)
        val rect = grid.bounds()

        for (pos in rect.points()) {
            val adjacentBugs = pos.neighbours().count { it in rect && grid[it] == '#' }
            result[pos] = when (grid[pos]) {
                '#' -> if (adjacentBugs == 1) '#' else '.'
                '.' -> if (adjacentBugs in 1..2) '#' else '.'
                else -> error("Unknown character")
            }
        }

        return result
    }

    private fun updateRecursive(bugs: Set<Vector3>): Set<Vector3> {
        return bugs
            .asSequence()
            .flatMap { it.adjacentCells() }
            .groupingBy { it }
            .eachCount()
            .filter { (vec, neighbours) -> (vec in bugs && neighbours == 1) || vec !in bugs && neighbours in 1..2 }
            .keys
    }

    private fun Vector3.adjacentCells() = sequence {
        for (dir in Direction.values()) {
            when (val vec2 = Vector2(x, y) + dir) {
                center -> when (dir) {
                    Direction.NORTH -> for (x in 0..4) yield(Vector3(x, 4, z + 1))
                    Direction.SOUTH -> for (x in 0..4) yield(Vector3(x, 0, z + 1))
                    Direction.WEST -> for (y in 0..4) yield(Vector3(4, y, z + 1))
                    Direction.EAST -> for (y in 0..4) yield(Vector3(0, y, z + 1))
                }
                !in cellRect -> yield((center + dir).withZ(z - 1))
                else -> yield(vec2.withZ(z))
            }
        }
    }

    private fun Vector2.withZ(z: Int) = Vector3(x, y, z)

    private fun CharGrid.asBits() = array.foldIndexed(0) { index, acc, ch -> if (ch == '#') acc or (1 shl index) else acc }

    companion object {
        private val center = Vector2(2, 2)
        private val cellRect = Rect(Vector2(0, 0), Vector2(4, 4))
    }
}
