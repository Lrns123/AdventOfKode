package dev.lfelzinga.adventofkode.puzzles.aoc15.day14

class Reindeer(private val speed: Int, private val flyDuration: Int, private val restDuration: Int) {

    private var secondsRemaining: Int = flyDuration
    private var isFlying = true

    var distanceTravelled = 0
        private set

    fun step() {
        if (secondsRemaining-- == 0) {
            isFlying = !isFlying
            secondsRemaining = (if (isFlying) flyDuration else restDuration) - 1
        }
        if (isFlying) {
            distanceTravelled += speed
        }
    }
}
