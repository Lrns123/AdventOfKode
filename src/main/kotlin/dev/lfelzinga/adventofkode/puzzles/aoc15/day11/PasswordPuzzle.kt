package dev.lfelzinga.adventofkode.puzzles.aoc15.day11

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class PasswordPuzzle : Puzzle<String>(2015, 11, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        return nextPassword(input)
    }

    override fun solveSecondPart(input: String): String {
        return nextPassword(nextPassword(input))
    }

    private fun nextPassword(password: String): String {
        val passwordArray = password.toCharArray()

        do {
            incrementPassword(passwordArray)
        } while (!isValidPassword(passwordArray))

        return String(passwordArray)
    }

    private fun incrementPassword(password: CharArray): CharArray {
        for (i in password.indices.reversed()) {
            when (password[i]) {
                'z' -> password[i] = 'a'
                'h', 'k', 'n' -> {
                    ++password[i]
                    ++password[i]
                    return password
                }
                else -> {
                    ++password[i]
                    return password
                }
            }
        }

        return password
    }

    private fun isValidPassword(password: CharArray): Boolean {
        return findStraight(password) && !hasBannedCharacters(password) && hasTwoPairs(password)
    }

    private fun findStraight(password: CharArray): Boolean {
        for (i in 0 until password.size - 2) {
            if (password[i + 2] == password[i + 1] + 1 && password[i + 1] == password[i] + 1) {
                return true
            }
        }

        return false
    }

    private fun hasBannedCharacters(password: CharArray): Boolean = password.any { it == 'i' || it == 'o' || it == 'l' }

    private fun hasTwoPairs(password: CharArray): Boolean {
        var firstPair: Char? = null

        for (i in 0 until password.size - 1) {
            if (password[i] == password[i + 1]) {
                if (firstPair == null) {
                    firstPair = password[i]
                } else if (firstPair != password[i]) {
                    return true
                }
            }
        }

        return false
    }
}
