package dev.lfelzinga.adventofkode.puzzles.aoc15.day13

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.permutations
import java.lang.IllegalArgumentException

class SeatingPuzzle : Puzzle<Sequence<String>>(2015, 13, LinesTransformer()) {
    private val lineRegex = Regex("""(\w+) would (gain|lose) (\d+) happiness units by sitting next to (\w+).""")

    override fun solveFirstPart(input: Sequence<String>): String {
        val happinessTable = readHappinessTable(input)

        return happinessTable.keys.toList().permutations()
            .map { happinessForArrangement(it, happinessTable) }
            .maxOrNull()
            .toString()

    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val happinessTable = readHappinessTable(input)

        return (happinessTable.keys + "Me").toList().permutations()
            .map { happinessForArrangement(it, happinessTable) }
            .maxOrNull()
            .toString()
    }

    private fun readHappinessTable(input: Sequence<String>): MutableMap<String, MutableMap<String, Int>> {
        val happinessTable = mutableMapOf<String, MutableMap<String, Int>>()

        input
            .map { lineRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line $it") }
            .forEach { (person, direction, happiness, other) ->
                happinessTable.computeIfAbsent(person) { mutableMapOf() }[other] = when (direction) {
                    "gain" -> happiness.toInt()
                    "lose" -> -happiness.toInt()
                    else -> throw IllegalArgumentException()
                }
            }

        return happinessTable
    }

    private fun happinessForArrangement(arrangement: List<String>, happinessTable: Map<String, Map<String, Int>>): Int {
        var happiness = 0

        for (i in arrangement.indices) {
            val self = happinessTable[arrangement[i]] ?: continue
            val leftNeighbour = arrangement[(i + arrangement.size - 1) % arrangement.size]
            val rightNeighbour = arrangement[(i + arrangement.size + 1) % arrangement.size]

            happiness += self[leftNeighbour] ?: 0
            happiness += self[rightNeighbour] ?: 0
        }

        return happiness
    }
}
