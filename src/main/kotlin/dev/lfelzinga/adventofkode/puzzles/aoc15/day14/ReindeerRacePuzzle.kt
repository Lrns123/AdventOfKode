package dev.lfelzinga.adventofkode.puzzles.aoc15.day14

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class ReindeerRacePuzzle : Puzzle<Sequence<String>>(2015, 14, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val reindeerMap = input
            .map { pattern.matchEntire(it)?.destructured ?: throw IllegalStateException("Cannot parse line $it") }
            .map { (name, speed, flyDuration, restDuration) -> name to Reindeer(speed.toInt(), flyDuration.toInt(), restDuration.toInt()) }
            .toMap()

        runSteps(2503, reindeerMap)

        return reindeerMap.values.maxOfOrNull { it.distanceTravelled }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val reindeerMap = input
            .map { pattern.matchEntire(it)?.destructured ?: throw IllegalStateException("Cannot parse line $it") }
            .map { (name, speed, flyDuration, restDuration) -> name to Reindeer(speed.toInt(), flyDuration.toInt(), restDuration.toInt()) }
            .toMap()

        val scores = mutableMapOf<String, Int>()

        runStepsAndScore(2503, scores, reindeerMap)

        return scores.values.maxOrNull().toString()
    }

    private fun runSteps(numSteps: Int, reindeerMap: Map<String, Reindeer>) {
        repeat(numSteps) {
            for (reindeer in reindeerMap.values) {
                reindeer.step()
            }
        }
    }

    private fun runStepsAndScore(numSteps: Int, scores: MutableMap<String, Int>, reindeerMap: Map<String, Reindeer>) {
        repeat(numSteps) {
            reindeerMap.values.forEach { it.step() }
            val maxDistance = reindeerMap.values.maxOfOrNull { it.distanceTravelled }
            for ((name, _) in reindeerMap.entries.filter { it.value.distanceTravelled == maxDistance }) {
                scores[name] = scores.getOrDefault(name, 0) + 1
            }
        }
    }

    companion object {
        private val pattern = Regex("""(\w+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.""")
    }
}
