package dev.lfelzinga.adventofkode.puzzles.aoc15.day3

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class PresentDeliveryPuzzle : Puzzle<String>(2015, 3, StringTransformer()) {
    data class Point(var x: Int, var y: Int) {
        fun add(dx: Int, dy: Int) {
            x += dx
            y += dy
        }
    }

    override fun solveFirstPart(input: String): String {
        val visited = mutableSetOf(Point(0, 0))
        val santaLocation = Point(0, 0)


        for (ch in input) {
            val (dx, dy) = directionForSymbol(ch)

            santaLocation.add(dx, dy)

            visited += santaLocation.copy()
        }

        return visited.size.toString()
    }

    override fun solveSecondPart(input: String): String {
        val visited = mutableSetOf(Point(0, 0))
        val actorSequence = sequence {
            val santaLocation = Point(0, 0)
            val roboLocation = Point(0, 0)
            while (true) {
                yield(santaLocation)
                yield(roboLocation)
            }
        }.iterator()

        for (ch in input) {
            val actor = actorSequence.next()
            val (dx, dy) = directionForSymbol(ch)

            actor.add(dx, dy)

            visited += actor.copy()
        }

        return visited.size.toString()
    }

    private fun directionForSymbol(symbol: Char) = when(symbol) {
        '<' -> Pair(-1, 0)
        '>' -> Pair(1, 0)
        '^' -> Pair(0, -1)
        'v' -> Pair(0, 1)
        else -> Pair(0, 0)
    }
}
