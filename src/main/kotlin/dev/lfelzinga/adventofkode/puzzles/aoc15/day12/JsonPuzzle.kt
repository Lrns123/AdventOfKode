package dev.lfelzinga.adventofkode.puzzles.aoc15.day12

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener

class JsonPuzzle : Puzzle<String>(2015, 12, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        val json = JSONObject(JSONTokener(input))

        return walkJsonObject(json).toString()
    }

    override fun solveSecondPart(input: String): String {
        val json = JSONObject(JSONTokener(input))

        return walkJsonObject(json, true).toString()
    }

    private fun walkJsonObject(obj: JSONObject, checkRed: Boolean = false): Int {
        var sum = 0
        for (key in obj.keys()) {
            when (val value = obj[key]) {
                "red" -> if (checkRed) return 0
                is JSONObject -> sum += walkJsonObject(value, checkRed)
                is JSONArray -> sum += walkJsonArray(value, checkRed)
                is Int -> sum += value
            }
        }
        return sum
    }

    private fun walkJsonArray(array: JSONArray, checkRed: Boolean = false): Int {
        var sum = 0
        for (value in array) {
            when (value) {
                is JSONObject -> sum += walkJsonObject(value, checkRed)
                is JSONArray -> sum += walkJsonArray(value, checkRed)
                is Int -> sum += value
            }
        }
        return sum
    }
}
