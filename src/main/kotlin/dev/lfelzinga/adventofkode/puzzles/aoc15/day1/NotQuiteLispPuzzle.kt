package dev.lfelzinga.adventofkode.puzzles.aoc15.day1

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class NotQuiteLispPuzzle : Puzzle<String>(2015, 1, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        return input.asSequence()
            .sumOf { if (it == '(') 1L else -1L }
            .toString()
    }

    override fun solveSecondPart(input: String): String {
        var floor = 0

        input.forEachIndexed { index, ch ->
            floor += if (ch == '(') 1 else -1

            if (floor < 0) {
                return (index + 1).toString()
            }
        }

        return "?"
    }
}
