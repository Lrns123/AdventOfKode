package dev.lfelzinga.adventofkode.puzzles.aoc15.day2

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class WrappingPaperPuzzle : Puzzle<Sequence<String>>(2015, 2, LinesTransformer()) {
    private class Present(length: Int, width: Int, height: Int) {
        val dimensions = arrayOf(length, width, height).sortedArray()

        val area: Int
            get() = 2 * dimensions[0] * dimensions[1] +
                    2 * dimensions[1] * dimensions[2] +
                    2 * dimensions[2] * dimensions[0]

        val slack: Int
            get() = dimensions[0] * dimensions[1]

        val wrappingPaperSize: Int
            get() = area + slack

        val ribbonLength: Int
            get() = 2 * dimensions[0] + 2 * dimensions[1] + dimensions[0] * dimensions[1] * dimensions[2]
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .map { parsePresent(it) }
            .map { it.wrappingPaperSize }
            .sum()
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .map { parsePresent(it) }
            .map { it.ribbonLength }
            .sum()
            .toString()
    }

    private fun parsePresent(measurements: String): Present {
        val (length, width, height) = measurements.split("x").map { it.toInt() }

        return Present(length, width, height)
    }
}
