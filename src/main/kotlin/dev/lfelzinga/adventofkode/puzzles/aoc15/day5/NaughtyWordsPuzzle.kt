package dev.lfelzinga.adventofkode.puzzles.aoc15.day5

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class NaughtyWordsPuzzle : Puzzle<Sequence<String>>(2015, 5, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .filter { line -> line.count { isVowel(it) } >= 3 }
            .filter(Regex("(.)\\1")::containsMatchIn)
            .filterNot(Regex("(ab)|(cd)|(pq)|(xy)")::containsMatchIn)
            .count()
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .filter(Regex("(..).*?\\1")::containsMatchIn)
            .filter(Regex("(.).\\1")::containsMatchIn)
            .count()
            .toString()
    }

    private fun isVowel(ch: Char) = when (ch) {
        'a', 'e', 'i', 'o', 'u' -> true
        else -> false
    }
}
