package dev.lfelzinga.adventofkode.puzzles.aoc15.day9

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.permutations

class PathFinderPuzzle : Puzzle<Sequence<String>>(2015, 9, LinesTransformer()) {
    private val distanceDescriptorRegex = Regex("""(\w+) to (\w+) = (\d+)""")

    override fun solveFirstPart(input: Sequence<String>): String {
       return processInput(input)
            .minOrNull()
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return processInput(input)
            .maxOrNull()
            .toString()
    }

    private fun processInput(input: Sequence<String>): Sequence<Int> {
        val distances = input
            .map { distanceDescriptorRegex.matchEntire(it)?.destructured ?: throw IllegalArgumentException("Cannot parse line '$it'") }
            .flatMap { (origin, destination, distance) -> sequenceOf((origin to destination) to distance.toInt(), (destination to origin) to distance.toInt()) }
            .associateBy({ it.first }, { it.second })

        return distances.keys
            .map { it.first }
            .distinct()
            .permutations()
            .map {
                it
                    .windowed(2) { (first, second) -> distances[first to second] ?: throw IllegalStateException("No distance known for $first to $second") }
                    .sum()
            }
    }
}
