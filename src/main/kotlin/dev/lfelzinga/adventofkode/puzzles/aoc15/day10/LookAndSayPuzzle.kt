package dev.lfelzinga.adventofkode.puzzles.aoc15.day10

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class LookAndSayPuzzle : Puzzle<String>(2015, 10, StringTransformer()) {
    override fun solveFirstPart(input: String): String {
        var result = input

        repeat(40) {
            result = lookAndSay(result)
        }

        return result.length.toString()
    }

    override fun solveSecondPart(input: String): String {
        var result = input

        repeat(50) {
            result = lookAndSay(result)
        }

        return result.length.toString()
    }

    private fun lookAndSay(input: String): String {
        val stringBuilder = StringBuilder()

        var currentChar: Char = 0.toChar()
        var sequence = 0

        for (ch in input.toCharArray()) {
            if (ch != currentChar) {
                if (currentChar.code != 0) {
                    stringBuilder.append(sequence)
                    stringBuilder.append(currentChar)
                }

                currentChar = ch
                sequence = 1
            } else {
                sequence++
            }
        }

        if (currentChar.code != 0) {
            stringBuilder.append(sequence)
            stringBuilder.append(currentChar)
        }

        return stringBuilder.toString()
    }
}
