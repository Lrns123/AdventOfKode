package dev.lfelzinga.adventofkode.puzzles.aoc15.day16

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class AuntSuePuzzle : Puzzle<Sequence<String>>(2015, 16, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val sueIndex = input
            .map { parseAunt(it) }
            .indexOfFirst { it.matchesCriteria(sueCriteria, defaultMatchers) }

        return (sueIndex + 1).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val sueIndex = input
            .map { parseAunt(it) }
            .indexOfFirst { it.matchesCriteria(sueCriteria, extraMatchers) }

        return (sueIndex + 1).toString()
    }

    private fun parseAunt(line: String): AuntSue {
        val (index) = suePattern.matchEntire(line)?.destructured
            ?: throw IllegalStateException("Could not parse line $line")

        val properties = propertyPattern.findAll(line)
            .map { it.destructured }
            .map { (key, value) -> key to value.toInt() }
            .toMap()

        return AuntSue(index.toInt(), properties)
    }

    companion object {
        private val suePattern = Regex("Sue (\\d+):.*")
        private val propertyPattern = Regex("(\\w+): (\\d+)")
        private val sueCriteria = mapOf(
            "children" to 3,
            "cats" to 7,
            "samoyeds" to 2,
            "pomeranians" to 3,
            "akitas" to 0,
            "vizslas" to 0,
            "goldfish" to 5,
            "trees" to 3,
            "cars" to 2,
            "perfumes" to 1
        )

        private val defaultMatchers = emptyMap<String, (Int, Int) -> Boolean>()
            .withDefault { { value, expected -> value == expected } }

        private val extraMatchers = mapOf(
            "cats" to { value: Int, expected: Int -> value > expected },
            "trees" to { value: Int, expected: Int -> value > expected },
            "pomeranians" to { value: Int, expected: Int -> value < expected },
            "goldfish" to { value: Int, expected: Int -> value < expected }
        ).withDefault { { value, expected -> value == expected } }
    }
}
