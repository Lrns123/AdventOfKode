package dev.lfelzinga.adventofkode.puzzles.aoc15.day25

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer

class CodePuzzle : Puzzle<String>(2015, 25, StringTransformer()) {
    companion object {
        private val inputRegex = Regex("To continue, please consult the code grid in the manual.  Enter the code at row (\\d+), column (\\d+).")

        private const val initial = 20151125L
        private const val multiplicand = 252533L
        private const val modulus = 33554393L
    }

    // Computes (initial * multiplicand ^ exponent) % modulus
    private fun modExp(exponent: Long): Long {
        var exp = exponent
        var mul = multiplicand
        var result = initial

        while (exp != 0L) {
            if (exp and 1L != 0L) {
                result = (result * mul) % modulus
            }

            exp = exp ushr 1
            mul = (mul * mul) % modulus
        }

        return result
    }

    override fun solveFirstPart(input: String): String {
        val (row, col) = readInput(input)

        val index = (col + row - 1 downTo 0).sum() - row

        return modExp(index.toLong()).toString()
    }

    override fun solveSecondPart(input: String): String {
        return "Merry Christmas!"
    }

    private fun readInput(input: String): Pair<Int, Int> {
        val (row, col) = inputRegex.matchEntire(input)?.destructured ?: error("Could not parse input")

        return row.toInt() to col.toInt()
    }
}
