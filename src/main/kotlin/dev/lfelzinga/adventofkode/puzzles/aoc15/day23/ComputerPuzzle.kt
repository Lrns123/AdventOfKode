package dev.lfelzinga.adventofkode.puzzles.aoc15.day23

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class ComputerPuzzle : Puzzle<Sequence<String>>(2015, 23, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val program = parseInput(input)
        val computer = Computer(program)
        computer.run()

        return computer.registers[1].toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val program = parseInput(input)
        val computer = Computer(program)

        computer.registers[0] = 1

        computer.run()

        return computer.registers[1].toString()
    }

    sealed class Instruction {
        class Hlf(val register: Int) : Instruction()
        class Tpl(val register: Int) : Instruction()
        class Inc(val register: Int) : Instruction()
        class Jmp(val offset: Int) : Instruction()
        class Jie(val register: Int, val offset: Int) : Instruction()
        class Jio(val register: Int, val offset: Int) : Instruction()
    }

    class Computer(val program: List<Instruction>) {
        val registers = IntArray(2)
        private var programCounter: Int = 0

        fun step() {
            when (val instruction = program[programCounter++]) {
                is Instruction.Hlf -> registers[instruction.register] /= 2
                is Instruction.Tpl -> registers[instruction.register] *= 3
                is Instruction.Inc -> registers[instruction.register]++
                is Instruction.Jmp -> programCounter += (instruction.offset - 1)
                is Instruction.Jie -> if (registers[instruction.register] % 2 == 0) programCounter += (instruction.offset - 1)
                is Instruction.Jio -> if (registers[instruction.register] == 1) programCounter += (instruction.offset - 1)
            }
        }

        fun run() {
            while (programCounter < program.size) {
                step()
            }
        }
    }

    private fun parseInput(input: Sequence<String>): List<Instruction> {
        val program = mutableListOf<Instruction>()

        for (line in input) {
            val opcode = line.substring(0, 3)
            val args = line.substring(4).split(", ")

            program += when (opcode) {
                "hlf" -> Instruction.Hlf(args[0][0] - 'a')
                "tpl" -> Instruction.Tpl(args[0][0] - 'a')
                "inc" -> Instruction.Inc(args[0][0] - 'a')
                "jmp" -> Instruction.Jmp(args[0].toInt())
                "jie" -> Instruction.Jie(args[0][0] - 'a', args[1].toInt())
                "jio" -> Instruction.Jio(args[0][0] - 'a', args[1].toInt())
                else -> error("Unknown opcode $opcode")
            }
        }

        return program
    }
}
