package dev.lfelzinga.adventofkode.puzzles.aoc15.day20

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.NumberTransformer
import kotlin.math.min

class PresentsPuzzle : Puzzle<Long>(2015, 20, NumberTransformer()) {
    override fun solveFirstPart(input: Long): String {
        return findFirstHouseWithNPresents(input.toInt(), 10, Int.MAX_VALUE).toString()
    }

    override fun solveSecondPart(input: Long): String {
        return findFirstHouseWithNPresents(input.toInt(), 11, 50).toString()
    }

    private fun findFirstHouseWithNPresents(threshold: Int, presentsPerElf: Int, maxVisits: Int = Int.MAX_VALUE, maxHouses: Int = 1_000_000): Int {
        var result = maxHouses
        val presents = IntArray(maxHouses)

        for (elf in 1 until maxHouses) {
            val elfPresents = elf * presentsPerElf
            val maxHouse = if (maxVisits != Int.MAX_VALUE) {
                maxHouses.coerceAtMost(elf + maxVisits * elf)
            } else {
                maxHouses
            }

            for (house in elf until maxHouse step elf) {
                presents[house] += elfPresents
                if (presents[house] >= threshold) {
                    result = min(result, house)
                }
            }
        }

        return result
    }
}
