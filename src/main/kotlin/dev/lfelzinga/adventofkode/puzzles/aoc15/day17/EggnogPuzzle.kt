package dev.lfelzinga.adventofkode.puzzles.aoc15.day17

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class EggnogPuzzle : Puzzle<Sequence<String>>(2015, 17, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val containers = input.map { it.toInt() }.toList()
        return (1 until (1 shl containers.size))
            .count { checkVolume(containers, it) }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val containers = input.map { it.toInt() }.toList()
        return (1 until (1 shl containers.size))
                .filter { checkVolume(containers, it) }
                .groupingBy { it.countOneBits() }
                .eachCount()
                .minByOrNull { it.key }
                ?.value
            .toString()
    }

    private fun checkVolume(containers: List<Int>, mask: Int): Boolean {
        var volume = 0
        for (i in containers.indices) {
            if (mask and (1 shl i) != 0) {
                volume += containers[i]
                if (volume > 150) {
                    return false
                }
            }

        }

        return volume == 150
    }
}
