package dev.lfelzinga.adventofkode.puzzles.aoc15.day22

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.pathfinding.Edge
import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.dijkstraSPF

class WizardPuzzle : Puzzle<Sequence<String>>(2015, 22, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val settings = parseInput(input, false)
        val result = TurnGraph().dijkstraSPF(settings.initialState()) {
            it.node.bossHP <= 0 && it.node.playerHP > 0
        } ?: error("No solution found")

        return result.cost.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val settings = parseInput(input, true)

        val result = TurnGraph().dijkstraSPF(settings.initialState()) {
            it.node.bossHP <= 0 && it.node.playerHP > 0
        } ?: error("No solution found")

        return result.cost.toString()
    }

    data class GameSettings(val bossStartingHP: Int, val bossDamage: Int, val hardMode: Boolean)

    data class GameState(
        val settings: GameSettings,
        val playerTurn: Boolean,
        val playerHP: Int,
        val bossHP: Int,
        val playerMana: Int,
        val shieldTicks: Int,
        val poisonTicks: Int,
        val rechargeTicks: Int
    )

    private fun GameState.applyEffects(): GameState = copy(
        shieldTicks = (shieldTicks - 1).coerceAtLeast(0),
        poisonTicks = (poisonTicks - 1).coerceAtLeast(0),
        rechargeTicks = (rechargeTicks - 1).coerceAtLeast(0),
        bossHP = if (poisonTicks > 0) bossHP - 3 else bossHP,
        playerMana = if (rechargeTicks > 0) playerMana + 101 else playerMana,
        playerHP = if (settings.hardMode && playerTurn) playerHP - 1 else playerHP
    )

    private fun GameSettings.initialState() = GameState(
        settings = this,
        playerTurn = true,
        playerHP = 50,
        bossHP = bossStartingHP,
        playerMana = 500,
        shieldTicks = 0,
        poisonTicks = 0,
        rechargeTicks = 0
    ).applyEffects()

    fun GameState.bossAttack() = copy(
        playerTurn = !playerTurn,
        playerHP = playerHP - (settings.bossDamage - if (shieldTicks > 0) 7 else 0).coerceAtLeast(1)
    ).applyEffects()

    fun GameState.magicMissile() = copy(
        playerTurn = !playerTurn,
        bossHP = bossHP - 4,
        playerMana = playerMana - 53
    ).applyEffects()

    fun GameState.drain() = copy(
        playerTurn = !playerTurn,
        bossHP = bossHP - 2,
        playerHP = playerHP + 2,
        playerMana = playerMana - 73
    ).applyEffects()

    fun GameState.shield() = copy(
        playerTurn = !playerTurn,
        playerMana = playerMana - 113,
        shieldTicks = 6
    ).applyEffects()

    fun GameState.poison() = copy(
        playerTurn = !playerTurn,
        playerMana = playerMana - 173,
        poisonTicks = 6
    ).applyEffects()

    fun GameState.recharge() = copy(
        playerTurn = !playerTurn,
        playerMana = playerMana - 229,
        rechargeTicks = 5
    ).applyEffects()

    inner class TurnGraph : WeightedGraph<GameState> {
        override fun edges(node: GameState): List<Edge<GameState>> = when {
            node.playerHP <= 0 || node.bossHP <= 0 -> emptyList()
            !node.playerTurn -> listOf(Edge(node.bossAttack(), 0))
            else -> buildList {
                if (node.playerMana >= 53) add(Edge(node.magicMissile(), 53))
                if (node.playerMana >= 73) add(Edge(node.drain(), 73))
                if (node.playerMana >= 113 && node.shieldTicks == 0) add(Edge(node.shield(), 113))
                if (node.playerMana >= 173 && node.poisonTicks == 0) add(Edge(node.poison(), 173))
                if (node.playerMana >= 229 && node.rechargeTicks == 0) add(Edge(node.recharge(), 229))
            }
        }
    }

    private fun parseInput(input: Sequence<String>, hardMode: Boolean): GameSettings {
        var hp = 0
        var damage = 0

        for (line in input) {
            val (item, value) = line.split(": ")

            when (item) {
                "Hit Points" -> hp = value.toInt()
                "Damage" -> damage = value.toInt()
            }
        }

        return GameSettings(hp, damage, hardMode)
    }
}
