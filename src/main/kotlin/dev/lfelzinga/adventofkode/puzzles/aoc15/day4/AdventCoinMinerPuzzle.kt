package dev.lfelzinga.adventofkode.puzzles.aoc15.day4

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.StringTransformer
import java.security.MessageDigest
import java.util.stream.IntStream

class AdventCoinMinerPuzzle : Puzzle<String>(2015, 4, StringTransformer()) {
    companion object {
        private val md5 = ThreadLocal.withInitial { MessageDigest.getInstance("MD5") }
    }

    class HashCandidate(val index: Int, base: String, number: Int) {
        private val hash: ByteArray = md5.get().digest((base + number).toByteArray())

        val hasFiveLeadingZeroes: Boolean
            get() = hash[0].toInt() == 0 && hash[1].toInt() == 0 && (hash[2].toInt() and 0xF0) == 0

        val hasSixLeadingZeroes: Boolean
            get() = hash[0].toInt() == 0 && hash[1].toInt() == 0 && hash[2].toInt() == 0
    }

    override fun solveFirstPart(input: String): String {
        return IntStream.iterate(1) { it + 1 }
            .parallel()
            .mapToObj { HashCandidate(it, input, it) }
            .filter { it.hasFiveLeadingZeroes }
            .findFirst()
            .map { it.index }
            .orElse(-1)
            .toString()
    }

    override fun solveSecondPart(input: String): String {
        return IntStream.iterate(1) { it + 1 }
            .parallel()
            .mapToObj { HashCandidate(it, input, it) }
            .filter { it.hasSixLeadingZeroes }
            .findFirst()
            .map { it.index }
            .orElse(-1)
            .toString()
    }
}
