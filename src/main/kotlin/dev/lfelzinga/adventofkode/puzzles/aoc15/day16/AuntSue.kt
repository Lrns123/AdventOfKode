package dev.lfelzinga.adventofkode.puzzles.aoc15.day16

class AuntSue(val index: Int, private val properties: Map<String, Int>) {
    fun matchesCriteria(criteria: Map<String, Int>, customMatchers: Map<String, (Int, Int) -> Boolean>): Boolean {
        for ((key, expected) in criteria) {
            val value = properties[key] ?: continue

            if (!customMatchers.getValue(key)(value, expected)) {
                return false
            }
        }
        return true
    }
}
