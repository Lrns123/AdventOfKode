package dev.lfelzinga.adventofkode.puzzles.aoc15.day19

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class MoleculePuzzle : Puzzle<Sequence<String>>(2015, 19, LinesTransformer()) {
    data class Rule(val input: String, val output: String)

    override fun solveFirstPart(input: Sequence<String>): String {
        val (rules, molecule) = readInput(input)
        val substitutions = mutableSetOf<String>()

        for (rule in rules) {
            for (match in rule.input.toRegex().findAll(molecule)) {
                substitutions += molecule.replaceRange(match.range, rule.output)
            }
        }

        return substitutions.size.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val (rules, initialMolecule) = readInput(input)
        val reversedRules = rules.map { Rule(it.output, it.input) }
        var molecule = initialMolecule
        var steps = 0

        while (molecule != "e") {
            for (rule in reversedRules) {
                if (molecule.contains(rule.input)) {
                    steps++
                    molecule = molecule.replaceFirst(rule.input, rule.output)
                }
            }
        }

        return steps.toString()
    }

    private fun readInput(input: Sequence<String>): Pair<List<Rule>, String> {
        val rules = mutableListOf<Rule>()
        val iterator = input.iterator()
        while (iterator.hasNext()) {
            val line = iterator.next()
            if (line.isEmpty()) break

            val tokens = line.split((" => "))
            rules += Rule(tokens[0], tokens[1])
        }

        return rules to iterator.next()
    }
}
