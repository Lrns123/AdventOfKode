package dev.lfelzinga.adventofkode.puzzles.aoc15.day7

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class LogicGatePuzzle : Puzzle<Sequence<String>>(2015, 7, LinesTransformer()) {

    private sealed class LogicGate(private val evalFunc: EvaluationContext.() -> Int) {
        class EvaluationContext(private val network: Map<String, LogicGate>) {
            fun evaluate(gateName: String): Int {
                return if (gateName[0].isDigit()) {
                    gateName.toInt()
                } else {
                    network[gateName]?.evaluate(this) ?: throw IllegalArgumentException("Unknown gate $gateName")
                }
            }
        }

        class ConstantGate(inputGate: String) : LogicGate({ evaluate(inputGate) })
        class NotGate(inputGate: String) : LogicGate({ evaluate(inputGate).inv() and 0xFFFF })
        class AndGate(lhsGate: String, rhsGate: String) : LogicGate({ evaluate(lhsGate) and evaluate(rhsGate) })
        class OrGate(lhsGate: String, rhsGate: String) : LogicGate({ evaluate(lhsGate) or evaluate(rhsGate) })
        class LShiftGate(inputGate: String, shiftGate: String) : LogicGate({ evaluate(inputGate) shl evaluate(shiftGate) })
        class RShiftGate(inputGate: String, shiftGate: String) : LogicGate({ evaluate(inputGate) shr evaluate(shiftGate) })

        private var memoized: Int? = null

        fun evaluate(context: EvaluationContext): Int = memoized ?: evalFunc(context).also { memoized = it }

        fun reset() {
            memoized = null
        }
    }

    private fun evaluate(network: Map<String, LogicGate>, name: String) = LogicGate.EvaluationContext(network).evaluate(name)

    override fun solveFirstPart(input: Sequence<String>): String {
        val network = input
            .map { parseGate(it) }
            .associateBy({ it.first }, { it.second })

        return evaluate(network, "a").toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val network = mutableMapOf<String, LogicGate>()
        input
            .map { parseGate(it) }
            .associateByTo(network, { it.first }, { it.second })

        val result = evaluate(network, "a")

        network["b"] = LogicGate.ConstantGate(result.toString())
        network.forEach { it.value.reset() }

        return evaluate(network, "a").toString()
    }

    private val constantGateRegex = Regex("""(\w+) -> (\w+)""")
    private val functionGateRegex = Regex("""(\w+) (\w+) (\w+) -> (\w+)""")
    private val unaryGateRegex = Regex("""(\w+) (\w+) -> (\w+)""")

    private fun parseGate(descriptor: String): Pair<String, LogicGate> {
        constantGateRegex.matchEntire(descriptor)?.destructured?.let { (input, name) ->
            return name to LogicGate.ConstantGate(input)
        }

        functionGateRegex.matchEntire(descriptor)?.destructured?.let { (lhsOperand, operation, rhsOperand, name) ->
            return when (operation) {
                "AND" -> name to LogicGate.AndGate(lhsOperand, rhsOperand)
                "OR" -> name to LogicGate.OrGate(lhsOperand, rhsOperand)
                "LSHIFT" -> name to LogicGate.LShiftGate(lhsOperand, rhsOperand)
                "RSHIFT" -> name to LogicGate.RShiftGate(lhsOperand, rhsOperand)
                else -> throw IllegalArgumentException("Unknown operation $operation")
            }
        }

        unaryGateRegex.matchEntire(descriptor)?.destructured?.let { (operation, operand, name) ->
            return when (operation) {
                "NOT" -> name to LogicGate.NotGate(operand)
                else -> throw IllegalArgumentException("Unknown operation $operation")
            }
        }

        throw IllegalArgumentException("Cannot parse gate descriptor '$descriptor'")
    }
}
