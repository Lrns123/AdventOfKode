package dev.lfelzinga.adventofkode.puzzles.aoc15.day8

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class StringEncodingPuzzle : Puzzle<Sequence<String>>(2015, 8, LinesTransformer()) {

    override fun solveFirstPart(input: Sequence<String>): String {
        return input
            .sumOf { it.length - unescape(it).length }
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        return input
            .sumOf { escape(it).length - it.length }
            .toString()
    }

    private fun unescape(input: String): String {
        return buildString {
            var i = 0
            val n = input.length

            while (i < n) {
                var ch = input[i]
                when (ch) {
                    '"' -> if (i != 0 && i != n - 1) {
                        throw IllegalStateException("Unexpected '\"' in input")
                    }
                    '\\' -> {
                        ch = input[++i]
                        when (ch) {
                            '\\', '"' -> append(ch)
                            'x' -> {
                                append(input.substring(i + 1, i + 3).toInt(16).toChar())
                                i += 2
                            }
                            else -> throw IllegalStateException("Unexpected escape: $ch")
                        }
                    }
                    else -> append(ch)
                }
                ++i
            }
        }
    }

    private fun escape(input: String): String {
        return buildString {
            append('"')
            for (ch in input.toCharArray()) {
                when (ch) {
                    '"' -> append("\\\"")
                    '\\' -> append("\\\\")
                    else -> append(ch)
                }
            }
            append('"')
        }
    }
}
