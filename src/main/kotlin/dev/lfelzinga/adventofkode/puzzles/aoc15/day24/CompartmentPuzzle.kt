package dev.lfelzinga.adventofkode.puzzles.aoc15.day24

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.combinations
import kotlin.math.min

class CompartmentPuzzle : Puzzle<Sequence<String>>(2015, 24, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val packages = input.map { it.toInt() }.sortedDescending().toList()
        val totalWeight = packages.sum()

        return findMinEntanglement(packages, totalWeight / 3).toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val packages = input.map { it.toInt() }.sortedDescending().toList()
        val totalWeight = packages.sum()

        return findMinEntanglement(packages, totalWeight / 4).toString()
    }

    private inline val List<Int>.entanglement: Long
        get() = fold(1L) { acc, item -> acc * item }

    private fun findMinEntanglement(packages: List<Int>, targetWeight: Int): Long {
        for (k in guessInitialK(packages, targetWeight)..packages.size) {
            var bestQE = Long.MAX_VALUE

            val minQE = packages.combinations(k)
                .filter { it.sum() == targetWeight }
                .filter { it.entanglement < bestQE }
                .filter { checkCompartments(packages - it, targetWeight) }
                .map { it.entanglement }
                .onEach { bestQE = min(bestQE, it) }
                .minOrNull()
                ?.toLong()

            if (minQE != null) {
                return minQE
            }
        }

        return -1
    }

    private fun checkCompartments(packages: List<Int>, targetWeight: Int): Boolean {
        if (packages.sum() == targetWeight) {
            return true
        }

        return (guessInitialK(packages, targetWeight)..packages.size)
            .asSequence()
            .flatMap { packages.combinations(it) }
            .filter { it.sum() == targetWeight }
            .any { checkCompartments(packages - it, targetWeight) }
    }

    private fun guessInitialK(packages: List<Int>, targetWeight: Int): Int {
        var sum = 0

        for ((index, weight) in packages.withIndex()) {
            sum += weight
            if (sum >= targetWeight) {
                return index + 1
            }
        }

        return 1
    }
}
