package dev.lfelzinga.adventofkode.puzzles.aoc15.day18

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid

class LightPuzzle : Puzzle<Sequence<String>>(2015, 18, LinesTransformer()) {
    override fun solveFirstPart(input: Sequence<String>): String {
        val grids = MutableList(2) { BooleanGrid(100, 100) }
        input.forEachIndexed { y, row -> row.forEachIndexed { x, char -> grids[0][x, y] = char == '#' } }

        repeat(100) {
            animateLights(grids[0], grids[1])
            grids.reverse()
        }

        return grids[0].array.count { it }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grids = MutableList(2) { BooleanGrid(100, 100) }
        input.forEachIndexed { y, row -> row.forEachIndexed { x, char -> grids[0][x, y] = char == '#' } }

        repeat(100) {
            animateLights(grids[0], grids[1])

            with(grids[1]) {
                this[0, 0] = true
                this[width - 1, 0] = true
                this[0, height - 1] = true
                this[width - 1, height - 1] = true
            }

            grids.reverse()
        }

        return grids[0].array.count { it }.toString()
    }

    private fun animateLights(source: BooleanGrid, target: BooleanGrid) {
        for (x in 0 until source.width) {
            for (y in 0 until source.height) {
                val litNeighbours = neighbours(source, x, y)
                target[x, y] = when (source[x, y]) {
                    true -> litNeighbours == 2 || litNeighbours == 3
                    false -> litNeighbours == 3
                }
            }
        }
    }

    private fun neighbours(grid: BooleanGrid, x: Int, y: Int): Int {
        var litNeighbours = if (grid[x, y]) -1 else 0
        for (dx in -1..1) {
            for (dy in -1..1) {
                if (x + dx in 0 until grid.width && y + dy in 0 until grid.height && grid[x + dx, y + dy]) {
                    litNeighbours++
                }
            }
        }

        return litNeighbours
    }
}
