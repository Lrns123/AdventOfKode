package dev.lfelzinga.adventofkode.puzzles.aoc15.day15

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.combinatorics.ratios

class IngredientPuzzle : Puzzle<Sequence<String>>(2015, 15, LinesTransformer()) {
    companion object {
        private val pattern = Regex("""(\w+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)""")
    }

    data class Ingredient(val capacity: Long, val durability: Long, val flavor: Long, val texture: Long, val calories: Long)

    override fun solveFirstPart(input: Sequence<String>): String {
        val ingredients = readIngredients(input)

        return ingredients.ratios(100)
                .map { getScore(it, ingredients) }
                .maxOrNull()
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val ingredients = readIngredients(input)

        return ingredients.ratios(100)
                .filter { getCalories(it, ingredients) == 500L }
                .map { getScore(it, ingredients) }
                .maxOrNull()
            .toString()
    }

    private fun getScore(ratios: IntArray, ingredients: List<Ingredient>): Long {
        require(ratios.size == ingredients.size) { "The number of ratios must equal the number of ingredients" }

        var capacity = 0L
        var durability = 0L
        var flavor = 0L
        var texture = 0L
        for (i in ingredients.indices) {
            val ingredient: Ingredient = ingredients[i]

            capacity += ingredient.capacity * ratios[i]
            durability += ingredient.durability * ratios[i]
            flavor += ingredient.flavor * ratios[i]
            texture += ingredient.texture * ratios[i]
        }
        return when {
            capacity <= 0 || durability <= 0 || flavor <= 0 || texture <= 0 -> 0
            else -> capacity * durability * flavor * texture
        }
    }

    private fun getCalories(ratios: IntArray, ingredients: List<Ingredient>): Long {
        require(ratios.size == ingredients.size) { "The number of ratios must equal the number of ingredients" }

        var calories = 0L
        for (i in ingredients.indices) {
            calories += ingredients[i].calories * ratios[i]
        }
        return calories
    }

    private fun readIngredients(input: Sequence<String>): List<Ingredient> {
        return input
            .map { pattern.matchEntire(it)?.destructured ?: throw IllegalStateException("Cannot parse line $it") }
            .map { (_, capacity, durability, flavor, texture, calories) ->
                Ingredient(capacity.toLong(), durability.toLong(), flavor.toLong(), texture.toLong(), calories.toLong())
            }
            .toList()
    }
}
