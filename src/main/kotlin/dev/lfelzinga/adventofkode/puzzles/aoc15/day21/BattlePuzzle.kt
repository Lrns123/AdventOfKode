package dev.lfelzinga.adventofkode.puzzles.aoc15.day21

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer

class BattlePuzzle : Puzzle<Sequence<String>>(2015, 21, LinesTransformer()) {

    data class Stats(val hp: Int, val damage: Int, val armor: Int)
    data class Gear(val name: String, val cost: Int, val damage: Int, val armor: Int)
    data class Equipment(val weapon: Gear, val armor: Gear, val ring1: Gear, val ring2: Gear) {
        fun stats(hp: Int) =  Stats(
            hp = hp,
            damage = weapon.damage + ring1.damage + ring2.damage,
            armor = armor.armor + ring1.armor + ring2.armor
        )

        fun cost() = weapon.cost + armor.cost + ring1.cost + ring2.cost
    }

    companion object {
        val none = Gear("None", 0, 0, 0)

        val weapons = listOf(
            Gear("Dagger", 8, 4, 0),
            Gear("Shortsword", 10, 5, 0),
            Gear("Warhammer", 25, 6, 0),
            Gear("Longsword", 40, 7, 0),
            Gear("Greataxe", 74, 8, 0)
        )

        val armor = listOf(
            none,
            Gear("Leather", 13, 0 , 1),
            Gear("Chainmail", 31, 0, 2),
            Gear("Splintmail", 53, 0, 3),
            Gear("Bandedmail", 75, 0, 4),
            Gear("Platemail", 102, 0, 5)
        )

        val rings = listOf(
            none,
            Gear("Damage +1", 25, 1, 0),
            Gear("Damage +2", 50, 2, 0),
            Gear("Damage +3", 100, 3, 0),
            Gear("Defense +1", 20, 0, 1),
            Gear("Defense +2", 40, 0, 2),
            Gear("Defense +3", 80, 0, 3)
        )
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val bossStats = readInput(input)

        return allEquipmentCombinations()
            .filter { willWinFight(it.stats(100), bossStats) }
            .map { it.cost() }
            .minOrNull()
            .toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val bossStats = readInput(input)

        return allEquipmentCombinations()
            .filter { !willWinFight(it.stats(100), bossStats) }
            .map { it.cost() }
            .maxOrNull()
            .toString()
    }

    private fun allEquipmentCombinations() = sequence {
        for (weapon in weapons) {
            for (armor in armor) {
                for (ring1 in rings) {
                    for (ring2 in rings) {
                        if (ring1 !== none && ring1 === ring2) continue

                        yield(Equipment(weapon, armor, ring1, ring2))
                    }
                }
            }
        }
    }

    private fun willWinFight(player: Stats, boss: Stats): Boolean {
        val netPlayerDamage = (player.damage - boss.armor).coerceAtLeast(1)
        val netBossDamage = (boss.damage - player.armor).coerceAtLeast(1)

        val playerTurns = boss.hp / netPlayerDamage + if (boss.hp % netPlayerDamage != 0) 1 else 0
        val bossTurns = player.hp / netBossDamage + if (player.hp % netBossDamage != 0) 1 else 0

        return bossTurns >= playerTurns
    }

    private fun readInput(input: Sequence<String>): Stats {
        var hp = 0
        var damage = 0
        var armor = 0

        for (line in input) {
            val (item, value) = line.split(": ")

            when (item) {
                "Hit Points" -> hp = value.toInt()
                "Damage" -> damage = value.toInt()
                "Armor" -> armor = value.toInt()
            }
        }

        return Stats(hp, damage, armor)
    }
}
