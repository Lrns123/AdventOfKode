package dev.lfelzinga.adventofkode.puzzles.aoc15.day6

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.framework.transformers.LinesTransformer
import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid

class LightGridPuzzle : Puzzle<Sequence<String>>(2015, 6, LinesTransformer()) {
    private val instructionRegex = Regex("""(turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+)""")

    sealed class Instruction(private val x1: Int, private val y1: Int, private val x2: Int, private val y2: Int, private val mutatorFirst: (Boolean) -> Boolean, private val mutatorSecond: (Int) -> Int) {
        class TurnOn(x1: Int, y1: Int, x2: Int, y2: Int) : Instruction(x1, y1, x2, y2, { true }, { it + 1 })
        class TurnOff(x1: Int, y1: Int, x2: Int, y2: Int) : Instruction(x1, y1, x2, y2, { false }, { (it - 1).coerceAtLeast(0) })
        class Toggle(x1: Int, y1: Int, x2: Int, y2: Int) : Instruction(x1, y1, x2, y2, { !it }, { it + 2 })

        fun applyFirst(grid: BooleanGrid) {
            for (x in x1..x2) {
                for (y in y1..y2) {
                    grid[x, y] = mutatorFirst(grid[x, y])
                }
            }
        }

        fun applySecond(grid: IntGrid) {
            for (x in x1..x2) {
                for (y in y1..y2) {
                    grid[x, y] = mutatorSecond(grid[x, y])
                }
            }
        }
    }

    override fun solveFirstPart(input: Sequence<String>): String {
        val grid = BooleanGrid(1000, 1000)

        input
            .map { parseInstruction(it) }
            .forEach { it.applyFirst(grid) }

        return grid.array.count { it }.toString()
    }

    override fun solveSecondPart(input: Sequence<String>): String {
        val grid = IntGrid(1000, 1000)

        input
            .map { parseInstruction(it) }
            .forEach { it.applySecond(grid) }

        return grid.array.sum().toString()
    }

    private fun parseInstruction(instruction: String): Instruction {
        return instructionRegex.matchEntire(instruction)?.destructured?.let { (operation, x1, y1, x2, y2) ->
            when (operation) {
                "turn on" -> Instruction.TurnOn(x1.toInt(), y1.toInt(), x2.toInt(), y2.toInt())
                "turn off" -> Instruction.TurnOff(x1.toInt(), y1.toInt(), x2.toInt(), y2.toInt())
                "toggle" -> Instruction.Toggle(x1.toInt(), y1.toInt(), x2.toInt(), y2.toInt())
                else -> null
            }
        } ?: throw IllegalArgumentException("Could not parse instruction $instruction")
    }
}
