package dev.lfelzinga.adventofkode.toolbox.matching

class MultiRegexMatcher {
    private val matchers = mutableListOf<Pair<Regex, (MatchResult.Destructured) -> Unit>>()
    private var failOnUnknown = false

    fun add(regex: Regex, block: (MatchResult.Destructured) -> Unit) = apply {
        matchers += regex to block
    }

    fun failOnUnknown() = apply {
        failOnUnknown = true
    }

    private fun match(input: Iterator<CharSequence>) {
        for (line in input) {
            var matched = false
            for ((regex, block) in matchers) {
                val match = regex.matchEntire(line)
                if (match != null) {
                    matched = true
                    block(match.destructured)
                    break
                }
            }

            if (!matched && failOnUnknown) {
                error("Could not match line '$line'")
            }
        }
    }

    fun match(input: Iterable<CharSequence>) = match(input.iterator())
    fun match(input: Sequence<CharSequence>) = match(input.iterator())
}
