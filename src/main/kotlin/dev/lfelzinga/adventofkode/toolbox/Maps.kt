package dev.lfelzinga.adventofkode.toolbox

class NonNullMap<K, V : Any>(private val underlying: Map<K, V>, private val defaultValue: (K) -> V): Map<K, V> by underlying {
    override fun get(key: K): V = underlying[key] ?: defaultValue(key)
}

class NonNullMutableMap<K, V : Any>(private val underlying: MutableMap<K, V>, private val defaultValue: (K) -> V): MutableMap<K, V> by underlying {
    override fun get(key: K): V = underlying.getOrPut(key) { defaultValue(key) }
}

fun <K, V : Any> Map<K, V>.nonNull(defaultValue: (K) -> V) = NonNullMap(this, defaultValue)

@JvmName("nonNullMutable")
fun <K, V : Any> MutableMap<K, V>.nonNull(defaultValue: (K) -> V) = NonNullMutableMap(this, defaultValue)
