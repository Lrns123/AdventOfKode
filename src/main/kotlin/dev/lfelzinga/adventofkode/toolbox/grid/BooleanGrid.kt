package dev.lfelzinga.adventofkode.toolbox.grid

class BooleanGrid(val width: Int, val height: Int) {
    val array = BooleanArray(width * height)

    operator fun set(x: Int, y: Int, value: Boolean) {
        array[y * width + x] = value
    }

    operator fun get(x: Int, y: Int) = array[y * width + x]

    fun fill(value: Boolean) = array.fill(value)

    fun filledWith(value: Boolean) = apply { fill(value) }

    fun copy() = BooleanGrid(width, height).also { array.copyInto(it.array) }

    private inline fun transpose(width: Int, height: Int, resolve: (x: Int, y: Int) -> Boolean): BooleanGrid =
        BooleanGrid(width, height).apply {
            for (x in 0 until width) {
                for (y in 0 until height) {
                    this[x, y] = resolve(x, y)
                }
            }
        }

    fun flipHorizontal() = transpose(width, height) { x, y -> this[width - 1 - x, y] }
    fun flipVertical() = transpose(width, height) { x, y -> this[x, height - 1 - y] }
    fun flipBoth() = transpose(width, height) { x, y -> this[width - 1 - x, height - 1 - y] }
    fun rotateClockwise() = transpose(height, width) { x, y -> this[y, width - 1 - x] }
    fun rotateCounterClockwise() = transpose(height, width) { x, y -> this[height - 1 - y, x] }

    override fun equals(other: Any?): Boolean {
        return this === other || (other is BooleanGrid && width == other.width && height == other.height && array contentEquals other.array)
    }

    override fun hashCode(): Int {
        var result = width
        result = 31 * result + height
        result = 31 * result + array.contentHashCode()
        return result
    }

    override fun toString(): String {
        val builder = StringBuilder()

        for (y in 0 until height) {
            for (x in 0 until width) {
                builder.append(if (this[x, y]) '#' else '.')
            }
            builder.append('\n')
        }

        return builder.toString()
    }
}
