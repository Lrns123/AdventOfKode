package dev.lfelzinga.adventofkode.toolbox.grid

import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds

fun <T> Map<Vector2, T>.toCharGrid(mapper: (T?) -> Char): CharGrid {
    val (min, max) = bounds()

    val width = max.x - min.x + 1
    val height = max.y - min.y + 1

    val grid = CharGrid(width, height)

    for (x in 0 until width) {
        for (y in 0 until height) {
            grid[x, y] = mapper(get(Vector2(x + min.x, y + min.y)))
        }
    }

    return grid
}

fun Set<Vector2>.toCharGrid(fillChar: Char, emptyChar: Char, xPadding: Int = 0, yPadding: Int = 0): CharGrid {
    val (min, max) = bounds()

    val width = max.x - min.x + 1 + xPadding
    val height = max.y - min.y + 1 + yPadding

    val grid = CharGrid(width, height)

    for (x in 0 until width) {
        for (y in 0 until height) {
            grid[x, y] = if (Vector2(x + min.x, y + min.y) in this) fillChar else emptyChar
        }
    }

    return grid
}

fun List<String>.toCharGrid(): CharGrid {
    require(distinctBy { it.length }.count() == 1) { "The input must be rectangular" }
    val maze = CharGrid(this[0].length, size )

    for ((y, row) in withIndex()) {
        for ((x, ch) in row.withIndex()) {
            maze[x, y] = ch
        }
    }

    return maze
}

fun CharGrid.findAll(pattern: CharGrid, mask: Char?) = sequence {
    require(pattern.width <= width) { "Pattern is too wide" }
    require(pattern.height <= height) { "Pattern is too tall" }

    for (offsetX in 0 until width - pattern.width) {
        for (offsetY in 0 until height - pattern.height) {
            if (tryMatch(pattern, mask, offsetX, offsetY)) {
                yield(offsetX to offsetY)
            }
        }
    }
}

private fun CharGrid.matchPattern(pattern: CharGrid, mask: Char?, offsetX: Int, offsetY: Int): Boolean {
    require(pattern.width <= width) { "Pattern is too wide" }
    require(pattern.height <= height) { "Pattern is too tall" }

    return tryMatch(pattern, mask, offsetX, offsetY)
}

private fun CharGrid.tryMatch(pattern: CharGrid, mask: Char?, offsetX: Int, offsetY: Int): Boolean {
    for (x in 0 until pattern.width) {
        for (y in 0 until pattern.height) {
            if (pattern[x, y] != mask && this[x + offsetX, y + offsetY] != pattern[x, y]) {
                return false
            }
        }
    }

    return true
}
