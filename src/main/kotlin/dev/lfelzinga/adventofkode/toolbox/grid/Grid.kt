package dev.lfelzinga.adventofkode.toolbox.grid

class Grid<T : Any>(val width: Int, val height: Int, private val default: () -> T) {
    val array = MutableList(width * height) { default() }

    operator fun set(x: Int, y: Int, value: T) {
        array[y * width + x] = value
    }

    operator fun get(x: Int, y: Int): T = array[y * width + x]

    fun fill(value: T) = array.fill(value)

    fun filledWith(value: T) = apply { fill(value) }

    fun copy() = Grid(width, height, default).also {
        it.array.clear()
        it.array.addAll(array)
    }

    private inline fun transpose(width: Int, height: Int, resolve: (x: Int, y: Int) -> T): Grid<T> =
        Grid(width, height, default).apply {
            for (x in 0 until width) {
                for (y in 0 until height) {
                    this[x, y] = resolve(x, y)
                }
            }
        }

    fun flipHorizontal() = transpose(width, height) { x, y -> this[width - 1 - x, y] }
    fun flipVertical() = transpose(width, height) { x, y -> this[x, height - 1 - y] }
    fun flipBoth() = transpose(width, height) { x, y -> this[width - 1 - x, height - 1 - y] }
    fun rotateClockwise() = transpose(height, width) { x, y -> this[y, width - 1 - x] }
    fun rotateCounterClockwise() = transpose(height, width) { x, y -> this[height - 1 - y, x] }

    override fun equals(other: Any?): Boolean {
        return this === other || (other is Grid<*> && width == other.width && height == other.height && array == other.array)
    }

    override fun hashCode(): Int {
        var result = width
        result = 31 * result + height
        result = 31 * result + array.hashCode()
        return result
    }
}
