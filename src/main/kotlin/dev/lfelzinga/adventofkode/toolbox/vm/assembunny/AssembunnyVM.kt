package dev.lfelzinga.adventofkode.toolbox.vm.assembunny

class AssembunnyVM {
    companion object {
        private const val REGISTER_MASK = 1L shl 34
    }

    enum class Opcode {
        CPY, INC, DEC, JNZ, TGL, OUT
    }

    class Instruction(var opcode: Opcode, val operand1: Long, val operand2: Long)

    class InvalidInstructionException(message: String) : RuntimeException(message)

    private val instructions = mutableListOf<Instruction>()
    private var instructionPointer = 0
    private val registers = IntArray(4)
    val output = mutableListOf<Int>()
    var outputLimit = 1

    fun setRegister(register: Char, value: Int) {
        val reg = register - 'a'
        require(reg in 0..3) { "Invalid register name" }
        registers[reg] = value
    }

    fun getRegister(register: Char): Int {
        val reg = register - 'a'
        require(reg in 0..3) { "Invalid register name" }
        return registers[reg]
    }

    fun step(): Boolean {
        if (instructionPointer >= instructions.size) return false

        val instruction: Instruction = instructions[instructionPointer++]
        try {
            when (instruction.opcode) {
                Opcode.CPY -> registers[instruction.operand2.register] = instruction.operand1.value
                Opcode.INC -> registers[instruction.operand1.register]++
                Opcode.DEC -> registers[instruction.operand1.register]--
                Opcode.JNZ -> if (instruction.operand1.value != 0) {
                    val offset = instruction.operand2.value

                    // Detect additions and multiplications
                    if (offset == -2 && optimizeAddition()) {
                        // Don't update IP
                    } else if (offset == -5 && optimizeMultiplication()) {
                        // Don't update IP
                    } else {
                        instructionPointer += offset - 1
                    }
                }
                Opcode.TGL -> {
                    val instructionIdx = instructionPointer + instruction.operand1.value - 1
                    if (instructionIdx in instructions.indices) {
                        val instructionToToggle = instructions[instructionIdx]

                        when (instructionToToggle.opcode) {
                            Opcode.INC -> instructionToToggle.opcode = Opcode.DEC
                            Opcode.DEC, Opcode.TGL, Opcode.OUT -> instructionToToggle.opcode = Opcode.INC
                            Opcode.JNZ -> instructionToToggle.opcode = Opcode.CPY
                            Opcode.CPY -> instructionToToggle.opcode = Opcode.JNZ
                        }
                    }
                }
                Opcode.OUT -> {
                    output += instruction.operand1.value
                    if (output.size >= outputLimit)
                        return false
                }
            }
        } catch (e: InvalidInstructionException) {
            // Skip instruction
        }
        return true
    }

    fun reset() {
        instructionPointer = 0
        registers.fill(0)
        output.clear()
    }

    fun run() {
        while (step()) {
            // Running
        }
    }

    fun addAssembly(assembly: String) {
        instructions.add(assemble(assembly))
    }

    private fun assemble(assembly: String): Instruction {
        val parts = assembly.split(" ")
        return when (parts[0]) {
            "cpy" -> Instruction(Opcode.CPY, parseOperand(parts[1]), parseOperand(parts[2]))
            "inc" -> Instruction(Opcode.INC, parseOperand(parts[1]), 0)
            "dec" -> Instruction(Opcode.DEC, parseOperand(parts[1]), 0)
            "jnz" -> Instruction(Opcode.JNZ, parseOperand(parts[1]), parseOperand(parts[2]))
            "tgl" -> Instruction(Opcode.TGL, parseOperand(parts[1]), 0)
            "out" -> Instruction(Opcode.OUT, parseOperand(parts[1]), 0)
            else -> throw IllegalArgumentException("Unknown opcode " + parts[0])
        }
    }

    private fun parseOperand(operand: String): Long = try {
        operand.toLong() and 0xFFFFFFFFL
    } catch (e: NumberFormatException) {
        REGISTER_MASK or (operand[0] - 'a').toLong()
    }

    private val Long.value: Int get() {
        return if (this and REGISTER_MASK != 0L) {
            registers[(this and REGISTER_MASK.inv()).toInt()]
        } else {
            this.toInt()
        }
    }

    private val Long.register: Int get() {
        if (this and REGISTER_MASK != 0L) {
            return (this and REGISTER_MASK.inv()).toInt()
        }
        throw InvalidInstructionException("Cannot resolve operand $this to register")
    }

    private fun optimizeAddition(): Boolean {
        // Checks for the following opcode pattern:
        // inc a
        // dec b
        // jnz b -2

        if (instructionPointer < 3)
            return false

        val incOpcode = instructions[instructionPointer - 3]
        val decOpcode = instructions[instructionPointer - 2]
        val jnzOpcode = instructions[instructionPointer - 1]

        if (incOpcode.opcode !== Opcode.INC || decOpcode.opcode !== Opcode.DEC || jnzOpcode.opcode !== Opcode.JNZ)
            return false

        if (jnzOpcode.operand2.toInt() != -2)
            return false

        if (incOpcode.operand1 == decOpcode.operand1)
            return false

        if (jnzOpcode.operand1 != decOpcode.operand1)
            return false

        registers[incOpcode.operand1.register] += registers[decOpcode.operand1.register]
        registers[decOpcode.operand1.register] = 0

        return true
    }

    private fun optimizeMultiplication(): Boolean {
        // Checks for the following opcode pattern:
        // cpy ? c <- (ignored)
        // cpy ? a
        // inc b
        // dec a
        // jnz a -2
        // dec c
        // jzn c -5

        if (instructionPointer < 6)
            return false

        val cpyOpcode = instructions[instructionPointer - 6]
        val incOpcode = instructions[instructionPointer - 5]
        val decOpcode = instructions[instructionPointer - 4]
        val jnzOpcode = instructions[instructionPointer - 3]
        val dec2Opcode = instructions[instructionPointer - 2]
        val jnz2Opcode = instructions[instructionPointer - 1]

        if (cpyOpcode.opcode !== Opcode.CPY || incOpcode.opcode !== Opcode.INC || decOpcode.opcode !== Opcode.DEC || jnzOpcode.opcode !== Opcode.JNZ || dec2Opcode.opcode !== Opcode.DEC || jnz2Opcode.opcode !== Opcode.JNZ)
            return false

        if (jnzOpcode.operand2.toInt() != -2 || jnz2Opcode.operand2.toInt() != -5)
            return false

        if (cpyOpcode.operand2 != decOpcode.operand1)
            return false

        if (incOpcode.operand1 == decOpcode.operand1 || incOpcode.operand1 == dec2Opcode.operand1 || decOpcode.operand1 == dec2Opcode.operand1)
            return false

        if (jnzOpcode.operand1 != decOpcode.operand1)
            return false

        if (jnz2Opcode.operand1 != dec2Opcode.operand1)
            return false

        val multiplier = registers[dec2Opcode.operand1.register]
        val multiplicand = cpyOpcode.operand1.value

        registers[incOpcode.operand1.register] += multiplier * multiplicand
        registers[decOpcode.operand1.register] = 0
        registers[dec2Opcode.operand1.register] = 0

        return true
    }
}
