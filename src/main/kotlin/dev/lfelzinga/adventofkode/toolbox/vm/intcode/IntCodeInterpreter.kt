package dev.lfelzinga.adventofkode.toolbox.vm.intcode

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class IntCodeInterpreter(
    private val program: List<Long>,
    val input: Channel<Long> = Channel(),
    val output: Channel<Long> = Channel(),
    memorySize: Int = program.size
) {
    val memory = LongArray(memorySize)
    var position = 0

    init {
        require(memorySize >= program.size) { "Insufficient memory to load program" }
        for (i in program.indices) {
            memory[i] = program[i]
        }
    }

    private var relativeBase: Int = 0
    private val opcode: Long get() = memory[position]
    private val operand = object {
        val mode = Array(3) { OperandMode.Pointer }

        operator fun get(index: Int) = when (mode[index - 1]) {
            OperandMode.Pointer -> memory[memory[position + index].toInt()]
            OperandMode.Immediate -> memory[position + index]
            OperandMode.Relative -> memory[relativeBase + memory[position + index].toInt()]
        }

        operator fun set(index: Int, value: Long) {
            when (mode[index - 1]) {
                OperandMode.Pointer,
                OperandMode.Immediate -> memory[memory[position + index].toInt()] = value
                OperandMode.Relative -> memory[relativeBase + memory[position + index].toInt()] = value
            }
        }
    }

    private fun decodeOpcode(): Int {
        var modifiers = opcode / 100

        for (i in operand.mode.indices) {
            operand.mode[i] = OperandMode.values()[(modifiers % 10L).toInt()]
            modifiers /= 10
        }

        return (opcode % 100L).toInt()
    }

    suspend fun step(): Boolean {
        val opcode = decodeOpcode()

        when (opcode) {
            ADD -> operand[3] = operand[1] + operand[2]
            MUL -> operand[3] = operand[1] * operand[2]
            IN -> operand[1] = input.receive()
            OUT -> output.send(operand[1])
            JT -> if (operand[1] != 0L) { position = operand[2].toInt(); return true }
            JF -> if (operand[1] == 0L) { position = operand[2].toInt(); return true }
            LT -> operand[3] = if (operand[1] < operand[2]) 1 else 0
            EQ -> operand[3] = if (operand[1] == operand[2]) 1 else 0
            RBO -> relativeBase += operand[1].toInt()
            HLT -> return false
            else -> throw IllegalStateException("Unknown opcode $opcode")
        }

        position += when (opcode) {
            ADD, MUL, LT, EQ -> 4
            IN, OUT, RBO -> 2
            JT, JF -> 3
            else -> 1
        }
        return true
    }

    suspend fun run() {
        while (step()) {
            /* Running */
        }

        output.close()
    }

    fun run(scope: CoroutineScope) = scope.launch { run() }

    operator fun invoke(vararg inputValues: Long): List<Long> = runBlocking {
        val inputJob = launch {
            inputValues.forEach { input.send(it) }
            input.close()
        }
        launch { run() }

        output.toList().also {
            if (inputJob.isActive) {
                inputJob.cancel()
            }
        }
    }

    private enum class OperandMode {
        Pointer,
        Immediate,
        Relative
    }

    companion object {
        private const val ADD = 1
        private const val MUL = 2
        private const val IN = 3
        private const val OUT = 4
        private const val JT = 5
        private const val JF = 6
        private const val LT = 7
        private const val EQ = 8
        private const val RBO = 9
        private const val HLT = 99
    }
}
