package dev.lfelzinga.adventofkode.toolbox.vm.intcode

class IntCodeDisassembler(private val program: List<Long>) {

    fun disassemble() {
        var position = 0
        while (position in program.indices) {
            println(disassembleInstruction(position))
            position += opcodeSize(position)
        }
    }

    private fun opcodeSize(pointer: Int): Int = 1 + argsForOpcode(program[pointer])

    private fun disassembleInstruction(pointer: Int): String {
        val opcode = program[pointer]
        val name = opcodeName(opcode)
        val args = List(argsForOpcode(opcode)) {
            when (argMode(opcode, it + 1)) {
                0 -> "[${program[pointer + it + 1]}]"
                1 -> "${program[pointer + it + 1]}"
                2 -> "R[${program[pointer + it + 1]}]"
                else -> "?[${program[pointer + it + 1]}]"
            }
        }

        val assembly = "$name ${args.joinToString(", ")}".padEnd(32, ' ')
        val raw = program.subList(pointer, pointer + argsForOpcode(opcode) + 1)

        return "${pointer.toString().padStart(6, '0')}: $assembly // ${raw.joinToString(", ")}"
    }

    private fun opcodeName(opcode: Long) = when (opcode.toInt() % 100) {
        1 -> "ADD"
        2 -> "MUL"
        3 -> "IN"
        4 -> "OUT"
        5 -> "JT"
        6 -> "JF"
        7 -> "LT"
        8 -> "EQ"
        9 -> "RBO"
        99 -> "HLT"
        else -> "D (${opcode.toInt()})"
    }

    private fun argsForOpcode(opcode: Long) = when (opcode.toInt() % 100) {
        1, 2, 7, 8 -> 3
        5, 6 -> 2
        3, 4, 9 -> 1
        else -> 0
    }

    private fun argMode(opcode: Long, arg: Int): Int {
        var modes = opcode / 100
        repeat(arg - 1)  { modes /= 10 }
        return (modes % 10).toInt()
    }
}
