package dev.lfelzinga.adventofkode.toolbox.vm.alu

class SubmarineALU(val instructions: List<ALUOpcode>) {

    companion object {
        fun fromAssembly(assembly: List<String>): SubmarineALU = SubmarineALU(assembly.map { parseOpcode(it) })

        private fun parseOpcode(assembly: String): ALUOpcode {
            val tokens = assembly.split(' ')

            return when (tokens[0]) {
                "inp" -> ALUOpcode.Input(tokens[1][0] - 'w')
                "add" -> {
                    if (tokens[2][0] in 'w'..'z')
                        ALUOpcode.AddRegister(tokens[1][0] - 'w', tokens[2][0] - 'w')
                    else
                        ALUOpcode.AddConst(tokens[1][0] - 'w', tokens[2].toLong())
                }
                "mul" -> {
                    if (tokens[2][0] in 'w'..'z')
                        ALUOpcode.MulRegister(tokens[1][0] - 'w', tokens[2][0] - 'w')
                    else
                        ALUOpcode.MulConst(tokens[1][0] - 'w', tokens[2].toLong())
                }
                "div" -> {
                    if (tokens[2][0] in 'w'..'z')
                        ALUOpcode.DivRegister(tokens[1][0] - 'w', tokens[2][0] - 'w')
                    else
                        ALUOpcode.DivConst(tokens[1][0] - 'w', tokens[2].toLong())
                }
                "mod" -> {
                    if (tokens[2][0] in 'w'..'z')
                        ALUOpcode.ModRegister(tokens[1][0] - 'w', tokens[2][0] - 'w')
                    else
                        ALUOpcode.ModConst(tokens[1][0] - 'w', tokens[2].toLong())
                }
                "eql" -> {
                    if (tokens[2][0] in 'w'..'z')
                        ALUOpcode.EqlRegister(tokens[1][0] - 'w', tokens[2][0] - 'w')
                    else
                        ALUOpcode.EqlConst(tokens[1][0] - 'w', tokens[2].toLong())
                }
                else -> error("Unknown opcode ${tokens[0]}")
            }
        }
    }

    val registers = LongArray(4)
    val input = ArrayDeque<Long>()

    sealed class ALUOpcode(val execute: SubmarineALU.() -> Unit) {
        class Input(val register: Int) : ALUOpcode({ registers[register] = input.removeFirst() })

        class AddConst(val register: Int, val value: Long) : ALUOpcode({ registers[register] += value })
        class AddRegister(val register1: Int, val register2: Int) : ALUOpcode({ registers[register1] += registers[register2] })

        class MulConst(val register: Int, val value: Long) : ALUOpcode({ registers[register] *= value })
        class MulRegister(val register1: Int, val register2: Int) : ALUOpcode({ registers[register1] *= registers[register2] })

        class DivConst(val register: Int, val value: Long) : ALUOpcode({ registers[register] /= value })
        class DivRegister(val register1: Int, val register2: Int) : ALUOpcode({ registers[register1] /= registers[register2] })

        class ModConst(val register: Int, val value: Long) : ALUOpcode({ registers[register] %= value })
        class ModRegister(val register1: Int, val register2: Int) : ALUOpcode({ registers[register1] %= registers[register2] })

        class EqlConst(val register: Int, val value: Long) : ALUOpcode({ registers[register] = if (registers[register] == value) 1 else 0 })
        class EqlRegister(val register1: Int, val register2: Int) : ALUOpcode({ registers[register1] = if (registers[register1] == registers[register2]) 1 else 0 })

        operator fun invoke(alu: SubmarineALU) = alu.execute()
    }

    fun reset() {
        registers.fill(0)
        input.clear()
    }

    fun run() {
        for (instruction in instructions) {
            instruction(this)
        }
    }
}
