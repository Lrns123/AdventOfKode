package dev.lfelzinga.adventofkode.toolbox

tailrec fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)
tailrec fun gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

fun lcm(a: Int, b: Int): Int = if(a == 0 || b == 0) 0 else a / gcd(a, b) * b
fun lcm(a: Long, b: Long): Long = if(a == 0L || b == 0L) 0 else a / gcd(a, b) * b

fun modInverse(number: Long, modulus: Long): Long {
    tailrec fun modInvRecursive(a: Long, b: Long, x: Long = 1L, y: Long = 0): Long =
        if (b == 0L) {
            if (a != 1L) error("$number % $modulus does not have a modular inverse") else x
        } else {
            modInvRecursive(b, a % b, y, x - y * (a / b))
        }

    val inverse = modInvRecursive(number, modulus)
    return if (inverse < 0) inverse + modulus else inverse
}

data class ModularLong(val remainder: Long, val modulus: Long)

fun Iterable<ModularLong>.chineseRemainder(): Long {
    val modulusProduct = fold(1L) { acc, value -> acc * value.modulus }
    val result = sumOf {
        it.remainder * (modulusProduct / it.modulus) * modInverse(modulusProduct / it.modulus, it.modulus)
    }

    return result % modulusProduct
}

fun <T> List<T>.median(): T = this[size / 2]
