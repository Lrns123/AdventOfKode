package dev.lfelzinga.adventofkode.toolbox.ocr

import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.vector.Vector2
import dev.lfelzinga.adventofkode.toolbox.vector.extensions.bounds

fun CharGrid.ocr(fillChar: Char = '#', xOffset: Int = 0, yOffset: Int = 0): String {
    require(height - yOffset >= 6 && width - xOffset >= 5) { "Grid area too small for OCR" }

    val grid = this

    return buildString {
        for (x in xOffset..width - 5 step 5) {
            append(OCRCharacter.findCharacter { px, py -> grid[x + px, yOffset + py] == fillChar })
        }
    }
}


fun Set<Vector2>.ocr(xOffset: Int = 0, yOffset: Int = 0): String {
    val set = this
    val bounds = bounds()
    var x = xOffset

    return buildString {
        while (x <= bounds.max.x) {
            append(OCRCharacter.findCharacter { px, py -> Vector2(x + px, yOffset + py) in set })
            x += 5
        }
    }
}
