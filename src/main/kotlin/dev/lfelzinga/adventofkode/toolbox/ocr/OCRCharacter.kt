package dev.lfelzinga.adventofkode.toolbox.ocr

enum class OCRCharacter(vararg lines: String) {
    A(
        " ##  ",
        "#  # ",
        "#  # ",
        "#### ",
        "#  # ",
        "#  # "
    ),
    B(
        "###  ",
        "#  # ",
        "###  ",
        "#  # ",
        "#  # ",
        "###  "
    ),
    C(
        " ##  ",
        "#  # ",
        "#    ",
        "#    ",
        "#  # ",
        " ##  "
    ),
    D(
        "###  ",
        "#  # ",
        "#  # ",
        "#  # ",
        "#  # ",
        "###  "
    ),
    E(
        "#### ",
        "#    ",
        "###  ",
        "#    ",
        "#    ",
        "#### "
    ),
    F(
        "#### ",
        "#    ",
        "###  ",
        "#    ",
        "#    ",
        "#    "
    ),
    G(
        " ##  ",
        "#    ",
        "#    ",
        "# ## ",
        "#  # ",
        " ##  "
    ),
    G2(
        " ##  ",
        "#  # ",
        "#    ",
        "# ## ",
        "#  # ",
        " ### "
    ),
    H(
        "#  # ",
        "#  # ",
        "#### ",
        "#  # ",
        "#  # ",
        "#  # "
    ),
    I(
        " ### ",
        "  #  ",
        "  #  ",
        "  #  ",
        "  #  ",
        " ### "
    ),
    J(
        "  ## ",
        "   # ",
        "   # ",
        "   # ",
        "#  # ",
        " ##  "
    ),
    K(
        "#  # ",
        "# #  ",
        "##   ",
        "# #  ",
        "# #  ",
        "#  # "
    ),
    L(
        "#    ",
        "#    ",
        "#    ",
        "#    ",
        "#    ",
        "#### "
    ),
    M(
        "#   #",
        "## ##",
        "# # #",
        "#   #",
        "#   #",
        "#   #"
    ),
    N(
        "#   #",
        "##  #",
        "# # #",
        "# # #",
        "#  ##",
        "#   #"
    ),
    O(
        " ##  ",
        "#  # ",
        "#  # ",
        "#  # ",
        "#  # ",
        " ##  "
    ),
    P(
        "###  ",
        "#  # ",
        "#  # ",
        "###  ",
        "#    ",
        "#    "
    ),
    Q(
        " ##  ",
        "#  # ",
        "#  # ",
        "# ## ",
        "#  # ",
        " ## #"
    ),
    R(
        "###  ",
        "#  # ",
        "#  # ",
        "###  ",
        "# #  ",
        "#  # "
    ),
    S(
        " ### ",
        "#    ",
        "#    ",
        " ##  ",
        "   # ",
        "###  "
    ),
    T(
        "#####",
        "  #  ",
        "  #  ",
        "  #  ",
        "  #  ",
        "  #  "
    ),
    U(
        "#  # ",
        "#  # ",
        "#  # ",
        "#  # ",
        "#  # ",
        " ##  "
    ),
    V(
        "#   #",
        "#   #",
        "#   #",
        " # # ",
        " # # ",
        "  #  "
    ),
    W(
        "#   #",
        "#   #",
        "# # #",
        "# # #",
        "# # #",
        " # # "
    ),
    X(
        "#   #",
        " # # ",
        "  #  ",
        " # # ",
        " # # ",
        "#   #"
    ),
    Y(
        "#   #",
        "#   #",
        " # # ",
        "  #  ",
        "  #  ",
        "  #  "
    ),
    Z(
        "#### ",
        "   # ",
        "  #  ",
        " #   ",
        "#    ",
        "#### "
    ),
    @Suppress("EnumEntryName")
    ` `(
        "     ",
        "     ",
        "     ",
        "     ",
        "     ",
        "     "
    );

    val pattern = Array(6) { BooleanArray(5) }

    init {
        require(lines.size == 6) { "OCR Characters must be 6 rows high" }
        for (y in 0..5) {
            val line = lines[y]
            require(line.length == 5) { "OCR Characters must be 5 columns wide" }
            for (x in 0..4) {
                pattern[y][x] = line[x] == '#'
            }
        }
    }

    companion object {
        inline fun findCharacter(present: (x: Int, y: Int) -> Boolean): Char? {
            return values().find { it.matches(present) }?.name?.first()
        }
    }

    inline fun matches(present: (x: Int, y: Int) -> Boolean): Boolean {
        for (y in pattern.indices) {
            val row = pattern[y]
            for (x in row.indices) {
                if (present(x, y) != row[x]) {
                    return false
                }
            }
        }

        return true
    }
}
