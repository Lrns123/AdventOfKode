package dev.lfelzinga.adventofkode.toolbox.extensions


fun <T> Sequence<T>.filterIf(condition: Boolean, predicate: (T) -> Boolean) =
    if (condition) filter(predicate) else this
