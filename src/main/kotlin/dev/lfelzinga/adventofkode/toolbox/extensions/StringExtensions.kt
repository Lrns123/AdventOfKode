package dev.lfelzinga.adventofkode.toolbox.extensions

fun String.rotate(shift: Int): String {
    if (isEmpty() || shift == 0 || shift % length == 0) return this

    val str = this
    val len = length
    val offset = -(shift % length)

    return buildString(length) {
        if (offset < 0) {
            append(str.substring(len + offset))
            append(str.substring(0, len + offset))
        } else {
            append(str.substring(offset))
            append(str.substring(0, offset))
        }
    }
}

fun String.fromHex(): ByteArray {
    require(length % 2 == 0) { "Hex string must be of even length" }

    return ByteArray(length / 2) {
        substring(it * 2, (it + 1) * 2).toInt(16).toByte()
    }
}
