package dev.lfelzinga.adventofkode.toolbox

inline fun <K, R : Any> memoized(key: K, cache: MutableMap<K, R>, block: () -> R): R {
    return cache[key] ?: block().also { cache[key] = it }
}
