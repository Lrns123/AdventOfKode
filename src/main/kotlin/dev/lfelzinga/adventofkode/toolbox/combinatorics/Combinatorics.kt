package dev.lfelzinga.adventofkode.toolbox.combinatorics

import java.util.Collections.swap

private fun <T : Comparable<T>> nextPermutation(list: List<T>): Boolean {
    var i = list.size - 1
    while (i > 0 && list[i - 1] >= list[i]) {
        i--
    }

    if (i <= 0) {
        return false
    }

    // Find new pivot
    var j = list.size - 1
    while (list[j] <= list[i - 1]) {
        j--
    }

    swap(list, i - 1, j)

    // Reverse suffix
    j = list.size - 1
    while (i < j) {
        swap(list, i++, j--)
    }

    return true
}

private fun <T : Comparable<T>> previousPermutation(list: List<T>): Boolean {
    // Find non-decreasing suffix
    var i = list.size - 1
    while (i > 0 && list[i - 1] <= list[i]) {
        i--
    }

    if (i <= 0)
        return false

    // Find new pivot
    var j = list.size - 1
    while (list[j] >= list[i - 1]) {
        j--
    }

    swap(list, i - 1, j)

    // Reverse suffix
    j = list.size - 1
    while (i < j) {
        swap(list, i++, j--)
    }

    return true
}

@JvmName("permutationsComparable")
fun <T : Comparable<T>> List<T>.permutations(): Sequence<List<T>> = sequence {
    val backingList = this@permutations.sorted()

    do {
        yield(backingList)
    } while (nextPermutation(backingList))
}

fun <T> List<T>.permutations(): Sequence<List<T>> = sequence {
    val backingList = List(size) { it }

    do {
        yield(backingList.map { this@permutations[it] })
    } while (nextPermutation(backingList))
}

fun <T> List<T>.combinations(amount: Int): Sequence<List<T>> = sequence {
    val backingList = List(size) { it < amount }

    do {
        yield(this@combinations.filterIndexed { index, _ -> backingList[index] })
    } while (previousPermutation(backingList))
}
