package dev.lfelzinga.adventofkode.toolbox.combinatorics

private fun nextRatio(separators: IntArray, max: Int): Boolean {
    for (i in separators.indices.reversed()) {
        if (separators[i]++ == max) {
            if (i == 0) return false
            separators[i] = 0
        } else {
            break
        }
    }

    // Ensure separators are consecutive
    var lowest = 0
    for (i in separators.indices) {
        separators[i] = separators[i].coerceAtLeast(lowest)
        lowest = separators[i]
    }
    return true
}

private fun separatorsToRatio(separators: IntArray, sum: Int): IntArray {
    val ratio = IntArray(separators.size + 1)
    ratio[0] = separators[0]
    for (i in 1 until separators.size) {
        ratio[i] = separators[i] - separators[i - 1]
    }
    ratio[separators.size] = sum - separators[separators.size - 1]
    return ratio
}

fun <T> List<T>.ratios(max: Int) = sequence {
    val separators = IntArray(size - 1)

    do {
        yield(separatorsToRatio(separators, max))
    } while (nextRatio(separators, max))
}
