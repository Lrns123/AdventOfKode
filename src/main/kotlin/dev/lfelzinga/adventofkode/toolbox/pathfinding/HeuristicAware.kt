package dev.lfelzinga.adventofkode.toolbox.pathfinding

interface HeuristicAware<T : Any> {
    fun heuristic(node: T, target: T): Int
}
