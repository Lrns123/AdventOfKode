package dev.lfelzinga.adventofkode.toolbox.pathfinding

interface Graph<T : Any> {
    fun neighbours(node: T): List<T>
}
