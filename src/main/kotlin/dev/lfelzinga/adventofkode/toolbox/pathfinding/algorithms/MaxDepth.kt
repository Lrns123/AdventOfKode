package dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms

import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.PathNode

fun <T : Any> Graph<T>.maxDepth(start: T): PathNode<T> {
    var maxPath = PathNode(start, 0, null)

    explore(start) { if (it.cost > maxPath.cost) maxPath = it }

    return maxPath
}

fun <T : Any> WeightedGraph<T>.maxCost(start: T): PathNode<T> {
    var maxPath = PathNode(start, 0, null)

    explore(start) { if (it.cost > maxPath.cost) maxPath = it }

    return maxPath
}
