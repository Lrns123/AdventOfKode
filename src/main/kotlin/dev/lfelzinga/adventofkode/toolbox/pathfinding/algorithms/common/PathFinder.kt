package dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common

import java.util.*

inline fun <T : Any> findPath(queue: Queue<PathNode<T>>, startState: T, costLimit: Int = Int.MAX_VALUE, targetReached: (PathNode<T>) -> Boolean, neighbours: (PathNode<T>) -> List<PathNode<T>>): PathNode<T>? {
    val visited = mutableMapOf<T, Int>()

    queue += PathNode(startState, 0, null)
    while (queue.isNotEmpty()) {
        val pathNode = queue.poll()

        if (pathNode.cost > costLimit || visited[pathNode.node]?.takeIf { it <= pathNode.cost } != null) {
            continue
        }

        visited[pathNode.node] = pathNode.cost

        if (targetReached(pathNode)) {
            return pathNode
        }

        queue.addAll(neighbours(pathNode))
    }

    return null
}
