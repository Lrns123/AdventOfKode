package dev.lfelzinga.adventofkode.toolbox.pathfinding

data class Edge<T>(val node: T, val cost: Int)
