package dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms

import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.HeuristicAware
import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.PathNode
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.findPath
import java.util.*

inline fun <G, T : Any> G.aStar(start: T, heuristicTarget: T, maxCost: Int = Int.MAX_VALUE, targetReached: (PathNode<T>) -> Boolean): PathNode<T>? where G : WeightedGraph<T>, G : HeuristicAware<T> {
    return findPath(
        queue = PriorityQueue { a, b -> (a.cost + heuristic(a.node, heuristicTarget)) - (b.cost + heuristic(b.node, heuristicTarget)) },
        startState = start,
        costLimit = maxCost,
        targetReached = targetReached
    ) { pathNode ->
        edges(pathNode.node).map { PathNode(it.node, pathNode.cost + it.cost, pathNode) }
    }
}

inline fun <G, T : Any> G.aStar(start: T, heuristicTarget: T, maxDistance: Int = Int.MAX_VALUE, targetReached: (PathNode<T>) -> Boolean): PathNode<T>? where G : Graph<T>, G : HeuristicAware<T> {
    return findPath(
        queue = PriorityQueue { a, b -> (a.cost + heuristic(a.node, heuristicTarget)) - (b.cost + heuristic(b.node, heuristicTarget)) },
        startState = start,
        costLimit = maxDistance,
        targetReached = targetReached
    ) { pathNode ->
        neighbours(pathNode.node).map { PathNode(it, pathNode.cost + 1, pathNode) }
    }
}

fun <G, T : Any> G.aStarTo(start: T, target: T, maxCost: Int = Int.MAX_VALUE): PathNode<T>? where G : WeightedGraph<T>, G : HeuristicAware<T> =
    aStar(start, target, maxCost) { it.node == target }

fun <G, T : Any> G.aStarTo(start: T, target: T, maxDistance: Int = Int.MAX_VALUE): PathNode<T>? where G : Graph<T>, G : HeuristicAware<T> =
    aStar(start, target, maxDistance) { it.node == target }
