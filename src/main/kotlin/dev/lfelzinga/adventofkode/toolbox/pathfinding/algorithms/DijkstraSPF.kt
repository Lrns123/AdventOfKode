package dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms

import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.PathNode
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.findPath
import java.util.*

inline fun <T : Any> WeightedGraph<T>.dijkstraSPF(start: T, maxCost: Int = Int.MAX_VALUE, targetReached: (PathNode<T>) -> Boolean): PathNode<T>? {
    return findPath(
        queue = PriorityQueue { a, b -> a.cost - b.cost },
        startState = start,
        costLimit = maxCost,
        targetReached = targetReached
    ) { pathNode ->
        edges(pathNode.node).map { PathNode(it.node, pathNode.cost + it.cost, pathNode) }
    }
}

fun <T : Any> WeightedGraph<T>.dijkstraSPFTo(start: T, target: T, maxCost: Int = Int.MAX_VALUE): PathNode<T>? =
    dijkstraSPF(start, maxCost) { it.node == target }
