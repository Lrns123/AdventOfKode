package dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms

import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.PathNode
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.findPath
import java.util.*

inline fun <T : Any> Graph<T>.breadthFirstSearch(start: T, maxDistance: Int = Int.MAX_VALUE, targetReached: (PathNode<T>) -> Boolean): PathNode<T>? {
    return findPath(
        queue = ArrayDeque(),
        startState = start,
        costLimit = maxDistance,
        targetReached = targetReached
    ) { pathNode ->
        neighbours(pathNode.node).map { PathNode(it, pathNode.cost + 1, pathNode) }
    }
}

fun <T : Any> Graph<T>.breadthFirstSearchTo(start: T, target: T, maxDistance: Int = Int.MAX_VALUE): PathNode<T>? =
    breadthFirstSearch(start, maxDistance) { it.node == target }
