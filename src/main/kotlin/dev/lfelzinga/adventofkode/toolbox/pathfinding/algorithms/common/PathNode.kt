package dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common

class PathNode<T : Any>(val node: T, val cost: Int, val origin: PathNode<T>?)

fun <T : Any> PathNode<T>.backtrack() = generateSequence(this) { it.origin }
fun <T : Any> PathNode<T>.path() = backtrack().map { it.node }.toList().asReversed()
fun <T : Any> PathNode<T>.length() = backtrack().count() - 1
