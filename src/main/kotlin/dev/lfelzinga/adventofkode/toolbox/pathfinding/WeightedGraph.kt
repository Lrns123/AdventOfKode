package dev.lfelzinga.adventofkode.toolbox.pathfinding

interface WeightedGraph<T : Any> : Graph<T> {
    fun edges(node: T): List<Edge<T>>

    override fun neighbours(node: T): List<T> = edges(node).map { it.node }
}
