package dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms

import dev.lfelzinga.adventofkode.toolbox.pathfinding.Graph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.WeightedGraph
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.PathNode
import dev.lfelzinga.adventofkode.toolbox.pathfinding.algorithms.common.findPath
import java.util.*

inline fun <T : Any> Graph<T>.explore(start: T, maxDistance: Int = Int.MAX_VALUE, nodeVisit: (PathNode<T>) -> Unit) {
    findPath(
        queue = ArrayDeque(),
        startState = start,
        costLimit = maxDistance,
        targetReached = { false }
    ) { pathNode ->
        nodeVisit(pathNode)
        neighbours(pathNode.node).map { PathNode(it, pathNode.cost + 1, pathNode) }
    }
}

inline fun <T : Any> WeightedGraph<T>.explore(start: T, maxCost: Int = Int.MAX_VALUE, nodeVisit: (PathNode<T>) -> Unit) {
    findPath(
        queue = ArrayDeque(),
        startState = start,
        costLimit = maxCost,
        targetReached = { false }
    ) { pathNode ->
        nodeVisit(pathNode)
        edges(pathNode.node).map { PathNode(it.node, pathNode.cost + it.cost, pathNode) }
    }
}
