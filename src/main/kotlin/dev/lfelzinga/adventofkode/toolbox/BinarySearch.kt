package dev.lfelzinga.adventofkode.toolbox

inline fun binarySearch(lowerBound: Long, upperBound: Long, predicate: (Long) -> Boolean): Long {
    var left = lowerBound
    var right = upperBound

    while (left + 1 < right) {
        val mid = (right + left) ushr 1
        when(predicate(mid)) {
            true -> right = mid
            false -> left = mid
        }
    }

    return right
}
