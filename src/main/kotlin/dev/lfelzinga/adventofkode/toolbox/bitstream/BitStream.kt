package dev.lfelzinga.adventofkode.toolbox.bitstream

import dev.lfelzinga.adventofkode.toolbox.extensions.fromHex

class BitStream(bits: ByteArray) {
    private val iterator = bits.iterator()

    private var bytesRead = -1
    private var octet = 0
    private var bitsAvailable = 0

    companion object {
        fun fromHex(hex: String): BitStream {
            require(hex.length % 2 == 0) { "Hex input must be an even length" }

            return BitStream(hex.fromHex())
        }

        private val masks = arrayOf(0x00, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F, 0xFF)
    }

    private fun loadByte() {
        require(iterator.hasNext()) { "End of bitstream reached" }

        octet = iterator.nextByte().toInt() and 0xFF
        bitsAvailable = 8
        bytesRead++
    }

    private fun readUnsigned(bits: Int): Int {
        require(bits in 0..8) { "readUnsigned can only read 0 to 8 bits" }

        if (bits == 0)
            return 0

        if (bitsAvailable == 0) {
            loadByte()
        }

        val additionalBitsRequired = bits - bitsAvailable
        if (additionalBitsRequired > 0) {
            return (readUnsigned(bitsAvailable) shl additionalBitsRequired) or readUnsigned(additionalBitsRequired)
        }

        bitsAvailable -= bits
        return (octet ushr bitsAvailable) and masks[bits]
    }

    fun readInt(bits: Int): Int {
        require(bits in 0..32) { "readInt can only read 0 to 32 bits" }

        var value = 0
        var remainingBits = bits

        while (remainingBits > 8) {
            value = (value shl 8) or readUnsigned(8)
            remainingBits -= 8
        }

        if (remainingBits > 0) {
            value = (value shl remainingBits) or readUnsigned(remainingBits)
        }

        return value
    }

    fun readLong(bits: Int): Long {
        require(bits in 0..64) { "readLong can only read 0 to 64 bits" }

        var value = 0L
        var remainingBits = bits

        while (remainingBits > 8) {
            value = (value shl 8) or readUnsigned(8).toLong()
            remainingBits -= 8
        }

        if (remainingBits > 0) {
            value = (value shl remainingBits) or readUnsigned(remainingBits).toLong()
        }

        return value
    }

    fun skip(bits: Int) {
        var remainingBits = bits

        while (remainingBits > 8) {
            readUnsigned(8)
            remainingBits -= 8
        }

        readUnsigned(remainingBits)
    }

    val position: Int
        get() = bytesRead * 8 + (8 - bitsAvailable)
}
