package dev.lfelzinga.adventofkode.toolbox.vector

data class Rect(val min: Vector2, val max: Vector2) {
    init {
        require(min.x <= max.x && min.y <= max.y) { "Invalid bounds" }
    }

    operator fun contains(vector: Vector2): Boolean = vector.x in min.x..max.x && vector.y in min.y..max.y

    fun points() = sequence {
        for (x in min.x..max.x) {
            for (y in min.y..max.y) {
                yield(Vector2(x, y))
            }
        }
    }

    val width: Int
        get() = (max.x - min.x + 1)

    val height: Int
        get() = (max.y - min.y + 1)

    val area: Long
        get() = width.toLong() * height.toLong()
}
