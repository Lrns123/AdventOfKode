package dev.lfelzinga.adventofkode.toolbox.vector

import dev.lfelzinga.adventofkode.toolbox.gcd
import kotlin.math.abs

data class Vector2(val x: Int, val y: Int) {
    companion object {
        val ORIGIN = Vector2(0, 0)
    }

    operator fun plus(other: Vector2) = Vector2(x + other.x, y + other.y)
    operator fun minus(other: Vector2) = Vector2(x - other.x, y - other.y)
    operator fun times(factor: Int) = Vector2(x * factor, y * factor)
    operator fun div(divisor: Int) = Vector2(x / divisor, y / divisor)
    operator fun rem(divisor: Int) = Vector2(x % divisor, y % divisor)

    operator fun unaryMinus() = Vector2(-x, -y)

    fun distanceTo(other: Vector2) = (other - this).magnitude

    fun neighbours() = Direction.values().map { this + it.delta }

    val magnitude: Int get() = abs(x) + abs(y)
    val normalized: Vector2 get() = abs(gcd(x, y)).let { Vector2(x / it, y / it)  }
}
