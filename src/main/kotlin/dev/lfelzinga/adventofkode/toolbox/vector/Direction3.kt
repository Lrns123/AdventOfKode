package dev.lfelzinga.adventofkode.toolbox.vector

enum class Direction3(val delta: Vector3) {
    LEFT(Vector3(-1, 0, 0)),
    RIGHT(Vector3(1, 0, 0)),
    UP(Vector3(0, -1, 0)),
    DOWN(Vector3(0, 1, 0)),
    BACK(Vector3(0, 0, -1)),
    FRONT(Vector3(0, 0, 1));

    inline val dx: Int
        get() = delta.x

    inline val dy: Int
        get() = delta.y

    inline val dz: Int
        get() = delta.z
}
