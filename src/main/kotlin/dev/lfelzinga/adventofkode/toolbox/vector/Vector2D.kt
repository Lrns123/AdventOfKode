package dev.lfelzinga.adventofkode.toolbox.vector

import kotlin.math.PI
import kotlin.math.atan2
import kotlin.math.sqrt

data class Vector2D(val x: Double, val y: Double) {
    companion object {
        val ORIGIN = Vector2D(0.0, 0.0)
    }

    operator fun plus(other: Vector2D) = Vector2D(x + other.x, y + other.y)
    operator fun minus(other: Vector2D) = Vector2D(x - other.x, y - other.y)
    operator fun times(factor: Double) = Vector2D(x * factor, y * factor)

    operator fun unaryMinus() = Vector2D(-x, -y)

    fun angleToRad(other: Vector2D) = atan2(other.y - y, other.x - x)
    fun angleToDeg(other: Vector2D) = (angleToRad(other) * (180 / PI) + 90.0 + 360.0) % 360.0

    fun distanceTo(other: Vector2D) = (this - other).magnitude

    val magnitude: Double get() = sqrt(x * x + y * y)
}
