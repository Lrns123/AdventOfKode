package dev.lfelzinga.adventofkode.toolbox.vector.extensions

import dev.lfelzinga.adventofkode.toolbox.grid.BooleanGrid
import dev.lfelzinga.adventofkode.toolbox.grid.CharGrid
import dev.lfelzinga.adventofkode.toolbox.grid.IntGrid
import dev.lfelzinga.adventofkode.toolbox.vector.*

fun Vector2.toDirection() = Direction.values().find { it.delta == this }

operator fun Vector2.plus(dir: Direction) = this + dir.delta
operator fun Vector2.minus(dir: Direction) = this - dir.delta

fun Vector2.rotateLeft() = Vector2(y, -x)
fun Vector2.rotateRight() = Vector2(-y, x)

operator fun Direction.times(factor: Int) = delta * factor

operator fun BooleanGrid.get(vector: Vector2) = get(vector.x, vector.y)
operator fun CharGrid.get(vector: Vector2) = get(vector.x, vector.y)
operator fun IntGrid.get(vector: Vector2) = get(vector.x, vector.y)

operator fun BooleanGrid.set(vector: Vector2, value: Boolean) = set(vector.x, vector.y, value)
operator fun CharGrid.set(vector: Vector2, value: Char) = set(vector.x, vector.y, value)
operator fun IntGrid.set(vector: Vector2, value: Int) = set(vector.x, vector.y, value)

fun BooleanGrid.bounds() = Rect(Vector2(0, 0), Vector2(width - 1, height - 1))
fun CharGrid.bounds() = Rect(Vector2(0, 0), Vector2(width - 1, height - 1))
fun IntGrid.bounds() = Rect(Vector2(0, 0), Vector2(width - 1, height - 1))

operator fun BooleanGrid.contains(vector: Vector2): Boolean = vector.x in 0 until width && vector.y in 0 until height
operator fun CharGrid.contains(vector: Vector2): Boolean = vector.x in 0 until width && vector.y in 0 until height
operator fun IntGrid.contains(vector: Vector2): Boolean = vector.x in 0 until width && vector.y in 0 until height

private fun coordinateSequence(width: Int, height: Int) = sequence {
    for (x in 0 until width) {
        for (y in 0 until height) {
            yield(Vector2(x, y))
        }
    }
}

val BooleanGrid.coordinates get() = coordinateSequence(width, height)
val CharGrid.coordinates get() = coordinateSequence(width, height)
val IntGrid.coordinates get() = coordinateSequence(width, height)

fun <T> Map<Vector2, T>.bounds(): Rect {
    require(isNotEmpty()) { "Cannot compute bounds for empty map" }

    return keys.bounds()
}

fun Set<Vector2>.bounds(): Rect {
    require(isNotEmpty()) { "Cannot compute bounds for empty set" }

    var minX = first().x
    var minY = first().y
    var maxX = minX
    var maxY = minY

    for (vector in this) {
        if (vector.x < minX) minX = vector.x
        if (vector.x > maxX) maxX = vector.x
        if (vector.y < minY) minY = vector.y
        if (vector.y > maxY) maxY = vector.y
    }

    return Rect(Vector2(minX, minY), Vector2(maxX, maxY))
}

fun Set<Vector3>.bounds(): Cuboid {
    require(isNotEmpty()) { "Cannot compute bounds for empty set" }

    var minX = first().x
    var minY = first().y
    var minZ = first().z
    var maxX = minX
    var maxY = minY
    var maxZ = minZ

    for (vector in this) {
        if (vector.x < minX) minX = vector.x
        if (vector.x > maxX) maxX = vector.x
        if (vector.y < minY) minY = vector.y
        if (vector.y > maxY) maxY = vector.y
        if (vector.z < minZ) minZ = vector.z
        if (vector.z > maxZ) maxZ = vector.z
    }

    return Cuboid(Vector3(minX, minY, minZ), Vector3(maxX, maxY, maxZ))
}

fun Rect.expand(dx: Int, dy: Int) = Rect(min + Vector2(-dx, -dy), max + Vector2(+dx, +dy))
fun Cuboid.expand(dx: Int, dy: Int, dz: Int) = Cuboid(min + Vector3(-dx, -dy, -dz), max + Vector3(+dx, +dy, +dz))
