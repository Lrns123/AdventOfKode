package dev.lfelzinga.adventofkode.toolbox.vector

import kotlin.math.abs

data class Vector3(val x: Int, val y: Int, val z: Int) {
    companion object {
        val ORIGIN = Vector3(0, 0, 0)
    }

    operator fun plus(other: Vector3) = Vector3(x + other.x, y + other.y, z + other.z)
    operator fun minus(other: Vector3) = Vector3(x - other.x, y - other.y, z - other.z)
    operator fun times(factor: Int) = Vector3(x * factor, y * factor, z * factor)
    operator fun div(factor: Int) = Vector3(x / factor, y / factor, z / factor)

    operator fun unaryMinus() = Vector3(-x, -y, -z)

    fun distanceTo(other: Vector3) = (other - this).magnitude

    fun neighbours() = Direction3.values().map { this + it.delta }

    val magnitude: Int get() = abs(x) + abs(y) + abs(z)
}
