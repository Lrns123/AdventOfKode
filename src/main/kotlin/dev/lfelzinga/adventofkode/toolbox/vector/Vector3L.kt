package dev.lfelzinga.adventofkode.toolbox.vector

import kotlin.math.abs

data class Vector3L(val x: Long, val y: Long, val z: Long) {
    companion object {
        val ORIGIN = Vector3L(0, 0, 0)
    }

    operator fun plus(other: Vector3L) = Vector3L(x + other.x, y + other.y, z + other.z)
    operator fun minus(other: Vector3L) = Vector3L(x - other.x, y - other.y, z - other.z)
    operator fun times(factor: Long) = Vector3L(x * factor, y * factor, z * factor)
    operator fun div(factor: Long) = Vector3L(x / factor, y / factor, z / factor)

    operator fun unaryMinus() = Vector3L(-x, -y, -z)

    fun squared() = Vector3L(x * x, y * y, z * z)
    fun distanceTo(other: Vector3L) = (other - this).magnitude

    val magnitude: Long get() = abs(x) + abs(y) + abs(z)
}
