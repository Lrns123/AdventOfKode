package dev.lfelzinga.adventofkode.toolbox.vector

import kotlin.math.abs

data class Vector4(val x: Int, val y: Int, val z: Int, val w: Int) {
    companion object {
        val ORIGIN = Vector4(0, 0, 0, 0)
    }

    operator fun plus(other: Vector4) = Vector4(x + other.x, y + other.y, z + other.z, w + other.w)
    operator fun minus(other: Vector4) = Vector4(x - other.x, y - other.y, z - other.z, w - other.w)
    operator fun times(factor: Int) = Vector4(x * factor, y * factor, z * factor, w * factor)

    operator fun unaryMinus() = Vector4(-x, -y, -z, - w)

    fun distanceTo(other: Vector4) = (other - this).magnitude

    val magnitude: Int get() = abs(x) + abs(y) + abs(z) + abs(w)
}
