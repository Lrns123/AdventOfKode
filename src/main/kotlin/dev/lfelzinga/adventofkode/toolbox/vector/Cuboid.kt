package dev.lfelzinga.adventofkode.toolbox.vector

import kotlin.math.max
import kotlin.math.min

data class Cuboid(val min: Vector3, val max: Vector3) {
    init {
        require(min.x <= max.x && min.y <= max.y && min.z <= max.z) { "Invalid bounds" }
    }

    operator fun contains(vector: Vector3): Boolean = vector.x in min.x..max.x && vector.y in min.y..max.y && vector.z in min.z..max.z

    val volume: Long
        get() = (max.x - min.x + 1).toLong() * (max.y - min.y + 1).toLong() * (max.z - min.z + 1).toLong()

    fun intersectsWith(other: Cuboid): Boolean = intersect(other) != null

    infix fun intersect(other: Cuboid): Cuboid? {
        val min = Vector3(max(min.x, other.min.x), max(min.y, other.min.y), max(min.z, other.min.z))
        val max = Vector3(min(max.x, other.max.x), min(max.y, other.max.y), min(max.z, other.max.z))

        return if (min.x <= max.x && min.y <= max.y && min.z <= max.z) {
            Cuboid(min, max)
        } else {
            null
        }
    }
}
