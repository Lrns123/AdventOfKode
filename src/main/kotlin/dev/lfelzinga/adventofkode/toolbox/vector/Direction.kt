package dev.lfelzinga.adventofkode.toolbox.vector

enum class Direction(val delta: Vector2) {
    NORTH(Vector2(0, -1)),
    EAST(Vector2(1, 0)),
    SOUTH(Vector2(0, 1)),
    WEST(Vector2(-1, 0));

    companion object {
        inline val UP get() = NORTH
        inline val DOWN get() = SOUTH
        inline val LEFT get() = WEST
        inline val RIGHT get() = EAST

        fun fromChar(ch: Char) = when (ch) {
            in "nNuU^" -> NORTH
            in "eErR>" -> EAST
            in "sSdDv" -> SOUTH
            in "wWlL<" -> WEST
            else -> error("Unrecognised direction '$ch'")
        }
    }

    fun right() = values()[(ordinal + 1) % 4]
    operator fun inc() = right()

    fun left() = values()[(ordinal + 3) % 4]
    operator fun dec() = left()

    fun opposite() = values()[(ordinal + 2) % 4]
    operator fun unaryMinus() = opposite()

    inline val dx: Int
        get() = delta.x

    inline val dy: Int
        get() = delta.y
}
