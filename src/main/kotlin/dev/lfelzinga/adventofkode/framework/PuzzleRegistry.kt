package dev.lfelzinga.adventofkode.framework

import org.reflections.Reflections
import java.util.*

class PuzzleRegistry {
    val puzzles = TreeSet<Puzzle<*>>()

    fun discoverPuzzles(basePackage: String) {
        puzzles.clear()

        Reflections(basePackage).getTypesAnnotatedWith(AdventOfCodePuzzle::class.java, true)
            .filter { it !== Puzzle::class.java }
            .map { it.getDeclaredConstructor().newInstance() as Puzzle<*> }
            .forEach(::registerPuzzle)
    }

    private fun registerPuzzle(puzzle: Puzzle<*>) {
        puzzles += puzzle
    }
}
