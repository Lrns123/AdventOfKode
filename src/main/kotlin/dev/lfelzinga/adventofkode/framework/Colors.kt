package dev.lfelzinga.adventofkode.framework

const val RED = "\u001b[31m"
const val BRIGHTRED = "\u001b[91m"
const val DARKGREEN = "\u001b[32m"
const val GREEN = "\u001b[92m"
const val BOLDGREEN = "\u001b[92;1m"
const val YELLOW = "\u001b[93m"
const val BROWN = "\u001b[33m"
const val BOLDBROWN = "\u001b[33;1m"
const val WHITE = "\u001b[37m"
const val GRAY = "\u001b[37m"
const val RESET = "\u001b[0m"
const val CLEARLINE = "\u001b[2K"
