package dev.lfelzinga.adventofkode.framework

import java.lang.annotation.Inherited

@Inherited
annotation class AdventOfCodePuzzle
