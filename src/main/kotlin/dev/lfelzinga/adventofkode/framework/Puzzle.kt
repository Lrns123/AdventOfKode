package dev.lfelzinga.adventofkode.framework

import dev.lfelzinga.adventofkode.framework.transformers.Transformer
import java.io.InputStream

@AdventOfCodePuzzle
abstract class Puzzle<T>(val year: Int, val day: Int, private val transformer: Transformer<T>) : Comparable<Puzzle<T>> {
    protected abstract fun solveFirstPart(input: T): String
    protected abstract fun solveSecondPart(input: T): String

    fun runFirstPart(input: InputStream) = solveFirstPart(transformer.transform(input))
    fun runSecondPart(input: InputStream) = solveSecondPart(transformer.transform(input))

    override fun compareTo(other: Puzzle<T>) = if (other.year == year) day - other.day else year - other.year
}
