package dev.lfelzinga.adventofkode.framework

/**
 * Skips the execution of the code-block, returning "< SKIPPED >" instead.
 * Used to disable days/parts with significant execution time.
 */
@Suppress("UNUSED_PARAMETER")
inline fun skip(result: String, block: () -> Unit): String {
    return result
}
