package dev.lfelzinga.adventofkode.framework

import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.InputStream
import java.util.*
import java.util.concurrent.Executors

class PuzzleRunner(private val registry: PuzzleRegistry, parallel: Boolean) : AutoCloseable {
    private val threadPool = Executors.newFixedThreadPool(if (parallel) Runtime.getRuntime().availableProcessors() else 1)
    private val context = threadPool.asCoroutineDispatcher()
    val answers = TreeMap<Puzzle<*>, PuzzleAnswer?>()

    fun queueYear(year: Int) {
        registry.puzzles
            .filter { it.year == year }
            .forEach { answers += it to null }
    }

    fun queueDay(year: Int, day: Int) {
        registry.puzzles
            .filter { it.year == year && it.day == day }
            .forEach { answers += it to null }
    }

    fun queueAll() {
        registry.puzzles
            .forEach { answers += it to null }
    }

    fun runPuzzles(progressCallback: (current: Int, total: Int) -> Unit = { _, _ -> }) {
        runBlocking {
            val channel = Channel<Pair<Puzzle<*>, PuzzleAnswer>>()

            for (puzzle in answers.keys) {
                launch(context) {
                    channel.send(
                        Pair(
                            puzzle,
                            PuzzleAnswer(
                                tryRunPuzzle(puzzle) { puzzle.runFirstPart(it) },
                                tryRunPuzzle(puzzle) { puzzle.runSecondPart(it) }
                            )
                        )
                    )
                }
            }

            progressCallback(0, answers.size)

            while(answers.any { it.value == null }) {
                channel.receive().let {
                    answers[it.first] = it.second
                }

                progressCallback(answers.count { it.value != null }, answers.size)
            }
        }
    }

    override fun close() {
        threadPool.shutdown()
    }

    private inline fun tryRunPuzzle(puzzle: Puzzle<*>, executor: (input: InputStream) -> String): Pair<String, Double>? {
        return try {
            runPuzzle(puzzle, executor)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    private inline fun runPuzzle(puzzle: Puzzle<*>, executor: (input: InputStream) -> String): Pair<String, Double> {
        puzzle.input.use {
            return measurePuzzleTime {
                executor(it)
            }
        }
    }

    private inline fun <T> measurePuzzleTime(block: () -> T): Pair<T, Double> {
        val startTime = System.nanoTime()
        val result = block()
        val endTime = System.nanoTime()

        return result to (endTime - startTime) / 1e6
    }

    private val Puzzle<*>.input: InputStream
        get() = javaClass.getResourceAsStream("/input/$year/day$day") ?: throw IllegalStateException("No puzzle input for $year-$day!")
}
