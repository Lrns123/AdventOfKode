package dev.lfelzinga.adventofkode.framework

data class PuzzleAnswer(
    val firstPart: Pair<String, Double>?,
    val secondPart: Pair<String, Double>?
)
