package dev.lfelzinga.adventofkode.framework.transformers

import java.io.InputStream

class IntCodeTransformer : Transformer<List<Long>> {
    override fun transform(input: InputStream) = String(input.readBytes()).split(',').map { it.toLong() }
}
