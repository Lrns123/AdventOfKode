package dev.lfelzinga.adventofkode.framework.transformers

import java.io.InputStream

class StringTransformer : Transformer<String> {
    override fun transform(input: InputStream): String = String(input.readBytes())
}
