package dev.lfelzinga.adventofkode.framework.transformers

import java.io.InputStream

class NumberTransformer : Transformer<Long> {
    override fun transform(input: InputStream) = String(input.readBytes()).toLong()
}
