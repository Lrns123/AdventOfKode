package dev.lfelzinga.adventofkode.framework.transformers

import java.io.InputStream

interface Transformer<out T> {
    fun transform(input: InputStream): T
}
