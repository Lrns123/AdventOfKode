package dev.lfelzinga.adventofkode.framework.transformers

import java.io.InputStream

class TokenizingTransformer(private val separator: String) : Transformer<List<String>> {
    override fun transform(input: InputStream) = String(input.readBytes()).split(separator)
}
