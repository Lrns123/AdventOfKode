package dev.lfelzinga.adventofkode.framework.transformers

import java.io.InputStream

class LinesTransformer : Transformer<Sequence<String>> {
    override fun transform(input: InputStream) = input.bufferedReader().lineSequence()
}
