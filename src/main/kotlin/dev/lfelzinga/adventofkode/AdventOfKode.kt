package dev.lfelzinga.adventofkode

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.int
import dev.lfelzinga.adventofkode.framework.BOLDBROWN
import dev.lfelzinga.adventofkode.framework.BOLDGREEN
import dev.lfelzinga.adventofkode.framework.BRIGHTRED
import dev.lfelzinga.adventofkode.framework.BROWN
import dev.lfelzinga.adventofkode.framework.CLEARLINE
import dev.lfelzinga.adventofkode.framework.DARKGREEN
import dev.lfelzinga.adventofkode.framework.GRAY
import dev.lfelzinga.adventofkode.framework.GREEN
import dev.lfelzinga.adventofkode.framework.PuzzleRegistry
import dev.lfelzinga.adventofkode.framework.PuzzleRunner
import dev.lfelzinga.adventofkode.framework.RED
import dev.lfelzinga.adventofkode.framework.RESET
import dev.lfelzinga.adventofkode.framework.YELLOW

class AdventOfKode : CliktCommand(printHelpOnEmptyArgs = true) {
    private val all by option("-a", "--all").flag()
    private val years by option("-y", "--years").int().split(",")
    private val days by option("-d", "--days").convert { parseDay(it) }.split(",")
    private val parallel by option("-p", "--parallel").flag()

    private val registry = PuzzleRegistry()

    override fun run() {
        println("${GREEN}Advent of Code$RESET")
        println("$GREEN==============$RESET")
        println()

        print("Loading puzzles... ")
        registry.discoverPuzzles("dev.lfelzinga.adventofkode")
        println("$BOLDGREEN${registry.puzzles.size}$RESET puzzle(s) loaded.")
        val runner = PuzzleRunner(registry, parallel)

        if (all) {
            runner.queueAll()
        } else {
            years?.forEach { runner.queueYear(it) }
            days?.forEach { (year, day) -> runner.queueDay(year, day) }
        }

        runner.runPuzzles { current, total ->
            print("$CLEARLINE\rRunning puzzles... $BOLDGREEN$current $GRAY/ $BOLDGREEN$total$RESET")
        }

        runner.close()

        println("$CLEARLINE\rRunning puzzles... ${BOLDGREEN}Done$RESET")

        val firstPadding = runner.answers.values.filterNotNull().maxOfOrNull { it.firstPart?.first?.length ?: 3 } ?: 0
        val secondPadding = runner.answers.values.filterNotNull().maxOfOrNull { it.secondPart?.first?.length ?: 3 } ?: 0

        var lastYear: Int? = null

        for ((puzzle, answer) in runner.answers) {
            if (lastYear != puzzle.year) {
                if (lastYear != null) {
                    println("-".repeat(firstPadding + secondPadding + 11))
                }

                println()
                println("Advent of Code $BOLDGREEN${puzzle.year}$RESET")
                println("=".repeat(firstPadding + secondPadding + 11))

                lastYear = puzzle.year
            }

            print("${BOLDBROWN}Day ${puzzle.day.toString().padStart(2, '0')}: ")

            if (answer != null) {
                val first = answer.firstPart?.let { (it) -> "$BOLDGREEN${it.padStart(firstPadding)}" } ?: "$RED${"???".padStart(firstPadding)}"
                val second = answer.secondPart?.let { (it) -> "$BOLDGREEN${it.padStart(secondPadding)}" } ?: "$RED${"???".padStart(secondPadding)}"

                print("$first $GRAY- $second$RESET  ")

                println("${formatDuration(answer.firstPart?.second)} $GRAY- ${formatDuration(answer.secondPart?.second)}$RESET")
            } else {
                println("$RED< Internal Error >$RESET")
            }
        }
    }

    private fun parseDay(input: String): Pair<Int, Int> {
        val tokens = input.split(':')
        require(tokens.size == 2) { "Invalid day: $input" }

        return tokens[0].toInt() to tokens[1].toInt()
    }

    private fun formatDuration(duration: Double?): String {
        if (duration == null) {
            return "$RED      ??? ms"
        }

        val color = when (duration.toInt()) {
            in 0..5 -> GREEN
            in 5..25 -> DARKGREEN
            in 25..75 -> YELLOW
            in 75..250 -> BROWN
            in 250..1000 -> RED
            else -> BRIGHTRED
        }

        return "$color%9.3f ms".format(duration)
    }
}

fun main(args: Array<String>) = AdventOfKode().main(args)
