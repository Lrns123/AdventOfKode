package dev.lfelzinga.adventofkode

import dev.lfelzinga.adventofkode.framework.Puzzle
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.io.InputStream
import kotlin.test.assertEquals
import kotlin.test.fail

class RegressionTest {
    @ParameterizedTest
    @MethodSource("regressionSource")
    fun runRegressionTest(data: RegressionData) {
        data.puzzle.input.use {
            assertEquals(data.solutionPart1, data.puzzle.runFirstPart(it), "Wrong answer for ${data.puzzle.year}:${data.puzzle.day} part one")
        }

        data.puzzle.input.use {
            assertEquals(data.solutionPart2, data.puzzle.runSecondPart(it), "Wrong answer for ${data.puzzle.year}:${data.puzzle.day} part two")
        }
    }

    companion object {
        @JvmStatic
        fun regressionSource() = RegressionData.data
    }

    private val Puzzle<*>.input: InputStream
        get() = javaClass.getResourceAsStream("/input/$year/day$day") ?: fail("No puzzle input for $year-$day!")
}
