package dev.lfelzinga.adventofkode

import dev.lfelzinga.adventofkode.framework.Puzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day1.NotQuiteLispPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day10.LookAndSayPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day12.JsonPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day14.ReindeerRacePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day16.AuntSuePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day17.EggnogPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day18.LightPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day19.MoleculePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day2.WrappingPaperPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day20.PresentsPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day21.BattlePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day22.WizardPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day23.ComputerPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day24.CompartmentPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day25.CodePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day3.PresentDeliveryPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day4.AdventCoinMinerPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day5.NaughtyWordsPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day6.LightGridPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day7.LogicGatePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day8.StringEncodingPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day9.PathFinderPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day1.CityGridPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day10.BotPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day11.RTGPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day12.AssembunnyPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day13.MazePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day14.OneTimePadPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day15.CapsulePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day16.DragonChecksumPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day17.VaultMazePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day18.TrapPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day19.WhiteElephantPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day2.KeypadPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day21.PasswordScramblingPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day22.StorageGridPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day23.SafeCrackingPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day24.RobotMazePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day25.ClockSignalPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day3.TrianglePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day4.EncryptedRoomPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day5.PasswordCrackingPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day6.ErrorCorrectionPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day7.IPPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day8.LCDScreenPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc16.day9.DecompressionPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day1.InverseCaptchaPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day10.KnotHashPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day11.HexGridPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day12.DigitalPlumberPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day14.DefragmentationPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day15.GeneratorPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day16.DancePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day17.SpinLockPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day18.DuetPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day19.TubesPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day2.CorruptionChecksumPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day20.ParticlePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day21.FractalPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day22.VirusPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day23.CoprocessorPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day24.BridgePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day25.TuringMachinePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day3.SpiralMemoryPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day4.PassphrasePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day5.JumpPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day6.ReallocationPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day8.RegisterPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc17.day9.StreamProcessingPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day1.FrequencyDriftPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day10.AlignmentPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day12.PlantPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day13.TrainPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day14.RecipePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day15.GoblinPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day16.AssemblyPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day17.WaterClayPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day18.LumberPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day19.AssemblyPuzzle2
import dev.lfelzinga.adventofkode.puzzles.aoc18.day2.ChecksumPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day20.MapPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day21.AssemblyPuzzle3
import dev.lfelzinga.adventofkode.puzzles.aoc18.day22.ModeMazePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day23.NanobotPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day24.ImmuneSystemPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day25.ConstellationPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day3.FabricPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day4.GuardPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day5.PolymerPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day6.ChronalPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day7.BuildStepsPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day8.LicensePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc18.day9.MarblePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day1.FuelRequirementPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day10.AsteroidPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day11.PaintRobotPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day12.MoonPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day13.ArcadePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day15.RepairRobotPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day16.FrequencyPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day17.ScaffoldPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day18.VaultPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day19.TractorBeamPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day2.IntCodePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day20.PortalMazePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day21.SpringDroidPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day22.CardShufflePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day23.NetworkingPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day24.BugsPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day25.AirlockPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day3.WirePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day5.DiagnosticPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day6.OrbitPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day7.AmplifierPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day8.PasswordImagePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc19.day9.BoostPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day1.ExpensePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day10.AdapterPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day12.FerryPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day13.BusPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day14.MemoryPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day15.MemoryGamePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day16.TicketPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day17.ConwayCubesPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day18.ParserPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day19.RulesPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day2.PasswordPolicyPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day20.CameraPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day22.CardsPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day23.CupsPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day24.HexTilePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day25.KeyCardPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day4.PassportPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day5.BoardingPassPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day6.CustomsPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day7.BaggagePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day8.ConsolePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc20.day9.EncodingPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day1.SubmarinePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day10.SyntaxCheckerPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day11.DumboOctopusPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day12.CavePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day13.FoldPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day14.PolymerizationPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day15.ChitonPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day16.PacketPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day17.LauncherPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day18.SnailfishPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day19.ScannerPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day2.SubmarinePositionPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day20.ImageEnhancementPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day21.DiracDicePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day22.ReactorPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day23.AmphipodPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day24.SubmarineALUPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day25.SeaCucumberPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day4.BingoPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day5.HydrothermalPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day6.LanternFishPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day7.CrabPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day8.SevenSegmentDisplayPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc21.day9.BasinPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day1.CaloriePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day10.CRTPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day11.MonkeyPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day12.HillClimbingPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day13.SignalPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day14.SandPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day15.BeaconPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day16.PressurePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day17.TetrisPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day18.LavaDropletPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day19.RobotPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day2.RockPaperScissorsPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day20.DecryptionPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day21.MonkeyMathPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day22.GrovePathPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day23.ElfSeedPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day24.BlizzardPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day25.SnafuConversionPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day3.RucksackPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day4.CleanupPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day5.StackPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day6.TuningPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day7.FilesystemPuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc22.day9.RopePuzzle
import dev.lfelzinga.adventofkode.puzzles.aoc15.day11.PasswordPuzzle as PasswordPuzzle15
import dev.lfelzinga.adventofkode.puzzles.aoc15.day13.SeatingPuzzle as SeatingPuzzle15
import dev.lfelzinga.adventofkode.puzzles.aoc15.day15.IngredientPuzzle as IngredientPuzzle15
import dev.lfelzinga.adventofkode.puzzles.aoc16.day20.FirewallPuzzle as FirewallPuzzle16
import dev.lfelzinga.adventofkode.puzzles.aoc17.day13.FirewallPuzzle as FirewallPuzzle17
import dev.lfelzinga.adventofkode.puzzles.aoc17.day7.TreePuzzle as TreePuzzle17
import dev.lfelzinga.adventofkode.puzzles.aoc18.day11.FuelPuzzle as FuelPuzzle18
import dev.lfelzinga.adventofkode.puzzles.aoc19.day14.FuelPuzzle as FuelPuzzle19
import dev.lfelzinga.adventofkode.puzzles.aoc19.day4.PasswordPuzzle as PasswordPuzzle19
import dev.lfelzinga.adventofkode.puzzles.aoc20.day11.SeatingPuzzle as SeatingPuzzle20
import dev.lfelzinga.adventofkode.puzzles.aoc20.day21.IngredientPuzzle as IngredientPuzzle20
import dev.lfelzinga.adventofkode.puzzles.aoc20.day3.TreePuzzle as TreePuzzle20
import dev.lfelzinga.adventofkode.puzzles.aoc21.day3.DiagnosticPuzzle as DiagnosticPuzzle21
import dev.lfelzinga.adventofkode.puzzles.aoc22.day8.TreePuzzle as TreePuzzle22

data class RegressionData(val puzzle: Puzzle<*>, val solutionPart1: String, val solutionPart2: String) {
    companion object {
        val data = listOf(
            // AoC 2015
            RegressionData(NotQuiteLispPuzzle(), "280", "1797"),
            RegressionData(WrappingPaperPuzzle(), "1606483", "3842356"),
            RegressionData(PresentDeliveryPuzzle(), "2081", "2341"),
            RegressionData(AdventCoinMinerPuzzle(), "254575", "1038736"),
            RegressionData(NaughtyWordsPuzzle(), "236", "51"),
            RegressionData(LightGridPuzzle(), "543903", "14687245"),
            RegressionData(LogicGatePuzzle(), "956", "40149"),
            RegressionData(StringEncodingPuzzle(), "1371", "2117"),
            RegressionData(PathFinderPuzzle(), "251", "898"),
            RegressionData(LookAndSayPuzzle(), "492982", "6989950"),
            RegressionData(PasswordPuzzle15(), "hepxxyzz", "heqaabcc"),
            RegressionData(JsonPuzzle(), "119433", "68466"),
            RegressionData(SeatingPuzzle15(), "618", "601"),
            RegressionData(ReindeerRacePuzzle(), "2640", "1102"),
            RegressionData(IngredientPuzzle15(), "21367368", "1766400"),
            RegressionData(AuntSuePuzzle(), "40", "241"),
            RegressionData(EggnogPuzzle(), "654", "57"),
            RegressionData(LightPuzzle(), "768", "781"),
            RegressionData(MoleculePuzzle(), "509", "195"),
            RegressionData(PresentsPuzzle(), "831600", "884520"),
            RegressionData(BattlePuzzle(), "121", "201"),
            RegressionData(WizardPuzzle(), "900", "1216"),
            RegressionData(ComputerPuzzle(), "255", "334"),
            RegressionData(CompartmentPuzzle(), "10723906903", "74850409"),
            RegressionData(CodePuzzle(), "2650453", "Merry Christmas!"),

            // AoC 2016
            RegressionData(CityGridPuzzle(), "278", "161"),
            RegressionData(KeypadPuzzle(), "48584", "563B6"),
            RegressionData(TrianglePuzzle(), "982", "1826"),
            RegressionData(EncryptedRoomPuzzle(), "158835", "993"),
            RegressionData(PasswordCrackingPuzzle(), "801b56a7", "424a0197"),
            RegressionData(ErrorCorrectionPuzzle(), "xhnqpqql", "brhailro"),
            RegressionData(IPPuzzle(), "115", "231"),
            RegressionData(LCDScreenPuzzle(), "121", "RURUCEOEIL"),
            RegressionData(DecompressionPuzzle(), "138735", "11125026826"),
            RegressionData(BotPuzzle(), "147", "55637"),
            RegressionData(RTGPuzzle(), "33", "57"),
            RegressionData(AssembunnyPuzzle(), "317993", "9227647"),
            RegressionData(MazePuzzle(), "96", "141"),
            RegressionData(OneTimePadPuzzle(), "16106", "22423"),
            RegressionData(CapsulePuzzle(), "376777", "3903937"),
            RegressionData(DragonChecksumPuzzle(), "10100011010101011", "01010001101011001"),
            RegressionData(VaultMazePuzzle(), "DDRLRRUDDR", "556"),
            RegressionData(TrapPuzzle(), "1926", "19986699"),
            RegressionData(WhiteElephantPuzzle(), "1816277", "1410967"),
            RegressionData(FirewallPuzzle16(), "32259706", "113"),
            RegressionData(PasswordScramblingPuzzle(), "bfheacgd", "gcehdbfa"),
            RegressionData(StorageGridPuzzle(), "1007", "242"),
            RegressionData(SafeCrackingPuzzle(), "11739", "479008299"),
            RegressionData(RobotMazePuzzle(), "518", "716"),
            RegressionData(ClockSignalPuzzle(), "196", "Merry Christmas!"),

            // AoC 2017
            RegressionData(InverseCaptchaPuzzle(), "1031", "1080"),
            RegressionData(CorruptionChecksumPuzzle(), "47623", "312"),
            RegressionData(SpiralMemoryPuzzle(), "438", "266330"),
            RegressionData(PassphrasePuzzle(), "325", "119"),
            RegressionData(JumpPuzzle(), "372671", "25608480"),
            RegressionData(ReallocationPuzzle(), "12841", "8038"),
            RegressionData(TreePuzzle17(), "eqgvf", "757"),
            RegressionData(RegisterPuzzle(), "4416", "5199"),
            RegressionData(StreamProcessingPuzzle(), "12803", "6425"),
            RegressionData(KnotHashPuzzle(), "29240", "4db3799145278dc9f73dcdbc680bd53d"),
            RegressionData(HexGridPuzzle(), "808", "1556"),
            RegressionData(DigitalPlumberPuzzle(), "128", "209"),
            RegressionData(FirewallPuzzle17(), "1632", "3834136"),
            RegressionData(DefragmentationPuzzle(), "8190", "1134"),
            RegressionData(GeneratorPuzzle(), "567", "323"),
            RegressionData(DancePuzzle(), "fnloekigdmpajchb", "amkjepdhifolgncb"),
            RegressionData(SpinLockPuzzle(), "1025", "37803463"),
            RegressionData(DuetPuzzle(), "4601", "6858"),
            RegressionData(TubesPuzzle(), "LIWQYKMRP", "16764"),
            RegressionData(ParticlePuzzle(), "150", "657"),
            RegressionData(FractalPuzzle(), "142", "1879071"),
            RegressionData(VirusPuzzle(), "5223", "2511456"),
            RegressionData(CoprocessorPuzzle(), "9409", "913"),
            RegressionData(BridgePuzzle(), "1511", "1471"),
            RegressionData(TuringMachinePuzzle(), "2832", "Merry Christmas!"),

            // AoC 2018
            RegressionData(FrequencyDriftPuzzle(), "427", "341"),
            RegressionData(ChecksumPuzzle(), "5681", "uqyoeizfvmbistpkgnocjtwld"),
            RegressionData(FabricPuzzle(), "115348", "188"),
            RegressionData(GuardPuzzle(), "87681", "136461"),
            RegressionData(PolymerPuzzle(), "9822", "5726"),
            RegressionData(ChronalPuzzle(), "3358", "45909"),
            RegressionData(BuildStepsPuzzle(), "DFOQPTELAYRVUMXHKWSGZBCJIN", "1036"),
            RegressionData(LicensePuzzle(), "45865", "22608"),
            RegressionData(MarblePuzzle(), "423717", "3553108197"),
            RegressionData(AlignmentPuzzle(), "XECXBPZB", "10124"),
            RegressionData(FuelPuzzle18(), "21,34", "90,244,16"),
            RegressionData(PlantPuzzle(), "1991", "1100000000511"),
            RegressionData(TrainPuzzle(), "82,104", "121,22"),
            RegressionData(RecipePuzzle(), "6289129761", "20207075"),
            RegressionData(GoblinPuzzle(), "319410", "63168"),
            RegressionData(AssemblyPuzzle(), "618", "514"),
            RegressionData(WaterClayPuzzle(), "27736", "22474"),
            RegressionData(LumberPuzzle(), "678529", "224005"),
            RegressionData(AssemblyPuzzle2(), "2016", "22674960"),
            RegressionData(MapPuzzle(), "3699", "8517"),
            RegressionData(AssemblyPuzzle3(), "103548", "14256686"),
            RegressionData(ModeMazePuzzle(), "7743", "1029"),
            RegressionData(NanobotPuzzle(), "433", "107272899"),
            RegressionData(ImmuneSystemPuzzle(), "26868", "434"),
            RegressionData(ConstellationPuzzle(), "363", "Merry Christmas!"),

            // AoC 2019
            RegressionData(FuelRequirementPuzzle(), "3339288", "5006064"),
            RegressionData(IntCodePuzzle(), "4462686", "5936"),
            RegressionData(WirePuzzle(), "273", "15622"),
            RegressionData(PasswordPuzzle19(), "1929", "1306"),
            RegressionData(DiagnosticPuzzle(), "14522484", "4655956"),
            RegressionData(OrbitPuzzle(), "314702", "439"),
            RegressionData(AmplifierPuzzle(), "914828", "17956613"),
            RegressionData(PasswordImagePuzzle(), "2159", "CJZHR"),
            RegressionData(BoostPuzzle(), "3989758265", "76791"),
            RegressionData(AsteroidPuzzle(), "292", "317"),
            RegressionData(PaintRobotPuzzle(), "2211", "EFCKUEGC"),
            RegressionData(MoonPuzzle(), "5517", "303070460651184"),
            RegressionData(ArcadePuzzle(), "372", "19297"),
            RegressionData(FuelPuzzle19(), "1590844", "1184209"),
            RegressionData(RepairRobotPuzzle(), "380", "410"),
            RegressionData(FrequencyPuzzle(), "27229269", "26857164"),
            RegressionData(ScaffoldPuzzle(), "14332", "1034009"),
            RegressionData(VaultPuzzle(), "3146", "2194"),
            RegressionData(TractorBeamPuzzle(), "234", "9290812"),
            RegressionData(PortalMazePuzzle(), "654", "7360"),
            RegressionData(SpringDroidPuzzle(), "19354392", "1139528802"),
            RegressionData(CardShufflePuzzle(), "6431", "100982886178958"),
            RegressionData(NetworkingPuzzle(), "18966", "14370"),
            RegressionData(BugsPuzzle(), "32506911", "2025"),
            RegressionData(AirlockPuzzle(), "136839232", "Merry Christmas!"),

            // AoC 2020
            RegressionData(ExpensePuzzle(), "1016131", "276432018"),
            RegressionData(PasswordPolicyPuzzle(), "477", "686"),
            RegressionData(TreePuzzle20(), "240", "2832009600"),
            RegressionData(PassportPuzzle(), "208", "167"),
            RegressionData(BoardingPassPuzzle(), "947", "636"),
            RegressionData(CustomsPuzzle(), "6549", "3466"),
            RegressionData(BaggagePuzzle(), "139", "58175"),
            RegressionData(ConsolePuzzle(), "1134", "1205"),
            RegressionData(EncodingPuzzle(), "530627549", "77730285"),
            RegressionData(AdapterPuzzle(), "2277", "37024595836928"),
            RegressionData(SeatingPuzzle20(), "2265", "2045"),
            RegressionData(FerryPuzzle(), "1496", "63843"),
            RegressionData(BusPuzzle(), "4315", "556100168221141"),
            RegressionData(MemoryPuzzle(), "9628746976360", "4574598714592"),
            RegressionData(MemoryGamePuzzle(), "1696", "37385"),
            RegressionData(TicketPuzzle(), "20048", "4810284647569"),
            RegressionData(ConwayCubesPuzzle(), "252", "2160"),
            RegressionData(ParserPuzzle(), "21993583522852", "122438593522757"),
            RegressionData(RulesPuzzle(), "111", "343"),
            RegressionData(CameraPuzzle(), "18482479935793", "2118"),
            RegressionData(IngredientPuzzle20(), "2098", "ppdplc,gkcplx,ktlh,msfmt,dqsbql,mvqkdj,ggsz,hbhsx"),
            RegressionData(CardsPuzzle(), "33473", "31793"),
            RegressionData(CupsPuzzle(), "24987653", "442938711161"),
            RegressionData(HexTilePuzzle(), "317", "3804"),
            RegressionData(KeyCardPuzzle(), "1890859", "Merry Christmas!"),

            // AoC 2021
            RegressionData(SubmarinePuzzle(), "1154", "1127"),
            RegressionData(SubmarinePositionPuzzle(), "2322630", "2105273490"),
            RegressionData(DiagnosticPuzzle21(), "3969000", "4267809"),
            RegressionData(BingoPuzzle(), "60368", "17435"),
            RegressionData(HydrothermalPuzzle(), "5145", "16518"),
            RegressionData(LanternFishPuzzle(), "388739", "1741362314973"),
            RegressionData(CrabPuzzle(), "349812", "99763899"),
            RegressionData(SevenSegmentDisplayPuzzle(), "493", "1010460"),
            RegressionData(BasinPuzzle(), "575", "1019700"),
            RegressionData(SyntaxCheckerPuzzle(), "364389", "2870201088"),
            RegressionData(DumboOctopusPuzzle(), "1681", "276"),
            RegressionData(CavePuzzle(), "3779", "96988"),
            RegressionData(FoldPuzzle(), "631", "EFLFJGRF"),
            RegressionData(PolymerizationPuzzle(), "3587", "3906445077999"),
            RegressionData(ChitonPuzzle(), "769", "2963"),
            RegressionData(PacketPuzzle(), "938", "1495959086337"),
            RegressionData(LauncherPuzzle(), "4095", "3773"),
            RegressionData(SnailfishPuzzle(), "3551", "4555"),
            RegressionData(ScannerPuzzle(), "447", "15672"),
            RegressionData(ImageEnhancementPuzzle(), "5583", "19592"),
            RegressionData(DiracDicePuzzle(), "925605", "486638407378784"),
            RegressionData(ReactorPuzzle(), "647076", "1233304599156793"),
            RegressionData(AmphipodPuzzle(), "13336", "53308"),
            RegressionData(SubmarineALUPuzzle(), "52926995971999", "11811951311485"),
            RegressionData(SeaCucumberPuzzle(), "482", "Merry Christmas!"),

            // AoC 2022
            RegressionData(CaloriePuzzle(), "71300", "209691"),
            RegressionData(RockPaperScissorsPuzzle(), "12535", "15457"),
            RegressionData(RucksackPuzzle(), "7917", "2585"),
            RegressionData(CleanupPuzzle(), "424", "804"),
            RegressionData(StackPuzzle(), "TWSGQHNHL", "JNRSCDWPP"),
            RegressionData(TuningPuzzle(), "1855", "3256"),
            RegressionData(FilesystemPuzzle(), "1915606", "5025657"),
            RegressionData(TreePuzzle22(), "1859", "332640"),
            RegressionData(RopePuzzle(), "6037", "2485"),
            RegressionData(CRTPuzzle(), "17840", "EALGULPG"),
            RegressionData(MonkeyPuzzle(), "55216", "12848882750"),
            RegressionData(HillClimbingPuzzle(), "484", "478"),
            RegressionData(SignalPuzzle(), "6415", "20056"),
            RegressionData(SandPuzzle(), "1068", "27936"),
            RegressionData(BeaconPuzzle(), "5367037", "11914583249288"),
            RegressionData(PressurePuzzle(), "2059", "2790"),
            RegressionData(TetrisPuzzle(), "3065", "1562536022966"),
            RegressionData(LavaDropletPuzzle(), "4450", "2564"),
            RegressionData(RobotPuzzle(), "1565", "10672"),
            RegressionData(DecryptionPuzzle(), "27726", "4275451658004"),
            RegressionData(MonkeyMathPuzzle(), "21120928600114", "3453748220116"),
            RegressionData(GrovePathPuzzle(), "97356", "120175"),
            RegressionData(ElfSeedPuzzle(), "4241", "1079"),
            RegressionData(BlizzardPuzzle(), "288", "861"),
            RegressionData(SnafuConversionPuzzle(), "2-0-0=1-0=2====20=-2", "Merry Christmas!"),

        )
    }
}
