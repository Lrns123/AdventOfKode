plugins {
    kotlin("jvm") version "1.7.21"
    application
}

group = "dev.lfelzinga"
version = "1.0-SNAPSHOT"

application {
    mainClass.set("dev.lfelzinga.adventofkode.AdventOfCodeKt")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.6.4")

    implementation("com.github.ajalt.clikt:clikt:3.5.0")

    implementation("org.json:json:20220924")

    implementation("org.reflections:reflections:0.10.2")

    implementation("org.slf4j:slf4j-nop:2.0.4")

    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.9.0")

}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf(
                "-opt-in=kotlin.RequiresOptIn",
                "-opt-in=kotlin.ExperimentalStdlibApi",
                "-opt-in=kotlinx.coroutines.ExperimentalCoroutinesApi"
            )
        }
    }

    test {
        useJUnitPlatform()
    }
}
